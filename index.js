import 'react-native-gesture-handler';
import './shim';
import 'react-native-get-random-values';
import 'react-native-url-polyfill/auto';
import {AppRegistry} from 'react-native';
import notifee from '@notifee/react-native';
import messaging from '@react-native-firebase/messaging';
import App from './App';
import {name as appName} from './app.json';

async function onMessageReceived({notification, data}) {
  const {title, body} = notification;

  const channelId = await notifee.createChannel({
    id: 'default',
    name: 'Default Channel',
  });
  notifee.displayNotification({
    id: 'default',
    title,
    body,
    data,
    android: {
      smallIcon: 'ic_scoby_icon',
      channelId,
    },
    ios: {},
  });
}

messaging().onMessage(onMessageReceived);
messaging().setBackgroundMessageHandler(onMessageReceived);

notifee.onBackgroundEvent(async () => {});

AppRegistry.registerComponent(appName, () => App);
