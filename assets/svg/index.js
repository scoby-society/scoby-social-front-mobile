import AddFileIco from './scobyBox/addFile.svg';
import AddHashtagsIco from './scobyBox/addHashtags.svg';
import BroadcastBlackIco from './scobyBox/broadcastBlack.svg';
import BroadcastWhiteIco from './scobyBox/broadcastWhite.svg';
import ForwardIco from './scobyBox/forward.svg';
import GiftBagIco from './scobyBox/giftBag.svg';
import InviteIco from './scobyBox/invite.svg';
import MuteMicIco from './scobyBox/muteMic.svg';
import OffCameraIco from './scobyBox/offCamera.svg';
import OnCameraIco from './scobyBox/onCamera.svg';
import PeopleIco from './scobyBox/participants.svg';
import RecordIco from './scobyBox/record.svg';
import StopRecordIco from './scobyBox/stopRecord.svg';
import SwitchCameraIco from './scobyBox/switchCamera.svg';
import UnmuteMicIco from './scobyBox/unmuteMic.svg';
import Chat from './scobyBox/Chat.svg';
import RemoveSpeaker from './scobyBox/removeSpeaker.svg';
import GreenRoom from './scobyBox/greenRoomWithText.svg';
import InviteIcon from './scobyBox/InviteIco.svg';
import PeopleIcon from './scobyBox/viewersAllWithText.svg';
import ViewersEyeIcon from './scobyBox/viewerEye.svg';
import ThreeDotsMenu from './scobyBox/threeDotsMenu.svg';
import LeaveIcon from './leaveIcon.svg';
import SendChatIcon from './scobyBox/sendChat.svg';
import CogIcon from './cog.svg';
import CameraIcon from './camera.svg';
import BrowseIco from './tabBarNavigate/searchInactive.svg';
import BrowseActiveIco from './tabBarNavigate/searchActive.svg';
import HomeIco from './tabBarNavigate/landingInactive.svg';
import HomeActiveIco from './tabBarNavigate/landingActive.svg';
import InboxIco from './tabBarNavigate/messagesInactive.svg';
import InboxActiveIco from './tabBarNavigate/messagesActive.svg';
import ProfileIco from './tabBarNavigate/profile.svg';
import ProfileActiveIco from './tabBarNavigate/profileActive.svg';
import StreamIcoActive from './tabBarNavigate/streamBarIcoActive.svg';
import StreamIcoInactive from './tabBarNavigate/streamBarIco.svg';
import UserIco from './tabBarNavigate/userIco.svg';
import DotMenuIco from './dotMenu.svg';
import CloseIco from './close.svg';
import FacebookIco from './social/facebook.svg';
import TwitterIco from './social/twitter.svg';
import InstragramIco from './social/instagram.svg';
import CloseWhiteIco from './closeWhite.svg';
import CongratsIco from './congrats.svg';
import CongratsRegistrationIco from './congratsRegistration.svg';
import BackIco from './backArrow.svg';
import CorrectMarked from './correctMarked.svg';
import HidedPassword from './hidedPassword.svg';
import IncorrectMark from './incorrectMark.svg';
import ShowPassword from './showPassword.svg';
import VerifiedIco from './feed/verifiedIcon.svg';
import SearchIco from './search.svg';
import SocialImg from './social.svg';
import EyeIco from './password/eye.svg';
import EyeCrossedIco from './password/eyeCrossed.svg';
import NoSession from './feed/noSessionIco.svg';
import Event from './chooseExperience/Event.svg';
import Serie from './chooseExperience/Serie.svg';
import Session from './chooseExperience/Session.svg';
import Channel from './chooseExperience/channel.svg';
import WriteMessage from './tabBarNavigate/writeMessage.svg';
import BellActive from './tabBarNavigate/bellActive.svg';
import BellInactive from './tabBarNavigate/bellInactive.svg';
import BottomLine from './tabBarNavigate/bottomLine.svg';
import Crowd from './crowd.svg';
import CheckBoxIcon from './check-square-solid.svg';
import UnCheckBoxIcon from './check-square.svg';
import PcSmartphone from './pcSmartphone.svg';
import Check from './Check.svg';
import Wrong from './Wrong.svg';
import ShareSvg from './Share.svg';
import Invite from './Invite.svg';
import DotsHorizontal from './dotsHorizontal.svg';
import MenuDotsGray from './menuDotsGray.svg';
import UncheckBoxIconBlack from './check-square-black.svg';
import Add from './add.svg';
import AddUncheck from './add-uncheck.svg';
import Remove from './remove.svg';
import CloseGray from './closeGray.svg';
import PhantomLogo from './phantom-icon-purple.svg';
import Dots from './dots.svg';
import Creator from './flags/creator.svg';
import Guest from './flags/guest.svg';
import Guide from './flags/guide.svg';
import Member from './flags/member.svg';
import Royalty from './flags/royalty.svg';
import Corona from './corona.svg';
import Item1 from './item1.svg';
import UnCheckCirculeIcon from './check-circule.svg';
import UnCheckRoundedIcon from './Ellipse399.svg';
import CheckRoundedIcon from './RadioButton.svg';
import HeartRoundedIcon from './heart-02.svg';
import AddRoundedIcon from './add-circle-01.svg';
import VectorIcon from './Vector.svg';
import VectorReverseIcon from './VectorReverse.svg';
import MinCirculeIcon from './MinCircule.svg';
import VectorPlusIcon from './VectorPlus.svg';
import SeeIcon from './see.svg';
import ClosePurpleIcon from './ClosePurple.svg';
import Tech from './Tech.svg';
import Business from './Business&Startups.svg';
import Finance from './Finance.svg';
import IonWallet from './ion_wallet.svg';
import Royalties from './royalty.svg';
import HostSearchIco from './hostSearch.svg';
import OpenArrowIco from './openArrow.svg';
import CloseArrowIco from './closeArrow.svg';
import RadiobuttonChecked from './RadiobuttonChecked.svg';
import RadiobuttonunChecked from './RadiobuttonunChecked.svg';
import Img from './img.svg';

export {
  Item1,
  Img,
  RadiobuttonChecked,
  RadiobuttonunChecked,
  AddFileIco,
  IonWallet,
  Business,
  Finance,
  Royalties,
  Corona,
  AddHashtagsIco,
  BroadcastBlackIco,
  BroadcastWhiteIco,
  Dots,
  ForwardIco,
  GiftBagIco,
  InviteIco,
  MuteMicIco,
  OffCameraIco,
  OnCameraIco,
  PeopleIco,
  RecordIco,
  StopRecordIco,
  SwitchCameraIco,
  UnmuteMicIco,
  Chat,
  RemoveSpeaker,
  GreenRoom,
  InviteIcon,
  PeopleIcon,
  ThreeDotsMenu,
  BrowseIco,
  BrowseActiveIco,
  HomeIco,
  HomeActiveIco,
  InboxIco,
  InboxActiveIco,
  ProfileIco,
  ProfileActiveIco,
  StreamIcoActive,
  StreamIcoInactive,
  ViewersEyeIcon,
  SendChatIcon,
  UserIco,
  CorrectMarked,
  HidedPassword,
  IncorrectMark,
  ShowPassword,
  DotMenuIco,
  CloseIco,
  CloseWhiteIco,
  VerifiedIco,
  LeaveIcon,
  SocialImg,
  EyeCrossedIco,
  EyeIco,
  FacebookIco,
  TwitterIco,
  InstragramIco,
  CongratsIco,
  CongratsRegistrationIco,
  SearchIco,
  NoSession,
  BackIco,
  CogIcon,
  CameraIcon,
  Event,
  Serie,
  Session,
  Channel,
  WriteMessage,
  BellActive,
  BellInactive,
  BottomLine,
  Crowd,
  CheckBoxIcon,
  UnCheckBoxIcon,
  PcSmartphone,
  Wrong,
  Check,
  ShareSvg,
  Invite,
  DotsHorizontal,
  MenuDotsGray,
  UncheckBoxIconBlack,
  Add,
  AddUncheck,
  Tech,
  Remove,
  CloseGray,
  PhantomLogo,
  Creator,
  Guest,
  Guide,
  Member,
  Royalty,
  UnCheckCirculeIcon,
  UnCheckRoundedIcon,
  CheckRoundedIcon,
  HeartRoundedIcon,
  AddRoundedIcon,
  VectorIcon,
  VectorReverseIcon,
  MinCirculeIcon,
  VectorPlusIcon,
  SeeIcon,
  ClosePurpleIcon,
  HostSearchIco,
  OpenArrowIco,
  CloseArrowIco,
};
