/* eslint-disable no-use-before-define */
/* eslint-disable no-empty */
import {
  SystemProgram,
  Keypair,
  TransactionInstruction,
  SYSVAR_RENT_PUBKEY,
  PublicKey,
  Transaction,
} from '@solana/web3.js';
import axios from 'axios';
import {Alert, Linking} from 'react-native';
import {MintLayout, Token, TOKEN_PROGRAM_ID, ASSOCIATED_TOKEN_PROGRAM_ID, AccountLayout} from '@solana/spl-token';
import * as anchor from 'src/dist/browser/index';
import AsyncStorage from '@react-native-community/async-storage';
import bs58 from 'bs58';
import nacl from 'tweetnacl';

export function createUninitializedMint(instructions, payer, amount, signers) {
  const account = Keypair.generate();
  instructions.push(
    SystemProgram.createAccount({
      fromPubkey: payer,
      newAccountPubkey: account.publicKey,
      lamports: amount,
      space: MintLayout.span,
      programId: TOKEN_PROGRAM_ID,
    }),
  );

  signers.push(account);

  return account.publicKey;
}

export function createMint(instructions, payer, mintRentExempt, decimals, owner, freezeAuthority, signers) {
  const account = createUninitializedMint(instructions, payer, mintRentExempt, signers);

  instructions.push(Token.createInitMintInstruction(TOKEN_PROGRAM_ID, account, decimals, owner, freezeAuthority));

  return account;
}

export function createAssociatedTokenAccountInstruction(
  instructions,
  associatedTokenAddress,
  payer,
  walletAddress,
  splTokenMintAddress,
) {
  const keys = [
    {
      pubkey: payer,
      isSigner: true,
      isWritable: true,
    },
    {
      pubkey: associatedTokenAddress,
      isSigner: false,
      isWritable: true,
    },
    {
      pubkey: walletAddress,
      isSigner: false,
      isWritable: false,
    },
    {
      pubkey: splTokenMintAddress,
      isSigner: false,
      isWritable: false,
    },
    {
      pubkey: SystemProgram.programId,
      isSigner: false,
      isWritable: false,
    },
    {
      pubkey: TOKEN_PROGRAM_ID,
      isSigner: false,
      isWritable: false,
    },
    {
      pubkey: SYSVAR_RENT_PUBKEY,
      isSigner: false,
      isWritable: false,
    },
  ];
  instructions.push(
    new TransactionInstruction({
      keys,
      programId: ASSOCIATED_TOKEN_PROGRAM_ID,
      data: Buffer.from([]),
    }),
  );
}

const confirmOption = {commitment: 'finalized', preflightCommitment: 'finalized', skipPreflight: false};

const encryptPayload = (payload, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');

  const nonce = nacl.randomBytes(24);

  const encryptedPayload = nacl.box.after(Buffer.from(JSON.stringify(payload)), nonce, sharedSecret);

  return [nonce, encryptedPayload];
};

const buildUrl = (path, params) => `phantom://v1/${path}?${params.toString()}`;

export const ExecuteMint = (pool, conn, userNft, membershipabi, profileOfUser, setHasStarted, setLoading) => {
  const {POOL, PROGRAMID, SYMBOL, TOKEN_METADATA_PROGRAM_ID, onTransactionRedirectLink} = pool;
  const getMetaDataExtend = async (nft, program) => {
    try {
      const nftMint = new anchor.web3.PublicKey(nft?.contractAdress);
      const [metadataExtended] = await PublicKey.findProgramAddress([nftMint.toBuffer(), POOL.toBuffer()], PROGRAMID);

      if ((await conn.getAccountInfo(metadataExtended)) == null) return '';
      const extendedData = await program.account.metadataExtended.fetch(metadataExtended);

      return {...nft, metadataExtended, extendedData};
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
      return '';
    }
  };

  const getNftsForOwner = async (symbol) => {
    const randWallet = anchor.web3.Keypair.generate();
    const provider = new anchor.AnchorProvider(conn, randWallet, confirmOption);
    const program = new anchor.Program(membershipabi, PROGRAMID, provider);
    const filterBySymbol = userNft.filter((item) => item?.symbol === symbol);
    const nftMetaData = Promise.all(filterBySymbol.map((nftItem) => getMetaDataExtend(nftItem, program)));
    return (await nftMetaData).sort((a, b) => {
      if (a.extendedData.number < b.extendedData.number) {
        return -1;
      }
      if (a.extendedData.number > b.extendedData.number) {
        return 1;
      }
      return 0;
    });
  };

  const getEdition = async (mint) =>
    (
      await anchor.web3.PublicKey.findProgramAddress(
        [Buffer.from('metadata'), TOKEN_METADATA_PROGRAM_ID.toBuffer(), mint.toBuffer(), Buffer.from('edition')],
        TOKEN_METADATA_PROGRAM_ID,
      )
    )[0];

  const getTokenWallet = async (owner, mint) =>
    (
      await anchor.web3.PublicKey.findProgramAddress(
        [owner.toBuffer(), TOKEN_PROGRAM_ID.toBuffer(), mint.toBuffer()],
        ASSOCIATED_TOKEN_PROGRAM_ID,
      )
    )[0];

  const getMetadata = async (mint) =>
    (
      await anchor.web3.PublicKey.findProgramAddress(
        [Buffer.from('metadata'), TOKEN_METADATA_PROGRAM_ID.toBuffer(), mint.toBuffer()],
        TOKEN_METADATA_PROGRAM_ID,
      )
    )[0];

  const mint = async () => {
    setHasStarted(true);
    setLoading(true);

    let publicSession;
    let userublicKey;

    try {
      publicSession = await AsyncStorage.getItem('public_session');
      userublicKey = await AsyncStorage.getItem('public_key');
    } catch (e) {
      return;
    }
    if (!publicSession || !userublicKey) {
      Alert.alert('Error', 'Your wallet is not connected');
      setLoading(false);
      setHasStarted(false);
    }
    const phantomWalletPublicKey = new anchor.web3.PublicKey(userublicKey);
    const randWallet = anchor.web3.Keypair.generate();
    const provider = new anchor.AnchorProvider(conn, randWallet, confirmOption);
    const program = new anchor.Program(membershipabi, PROGRAMID, provider);
    const poolData = await program.account.pool.fetch(POOL);

    // eslint-disable-next-line prefer-const
    let transaction = new Transaction();
    // eslint-disable-next-line prefer-const
    let instructions = [];
    // eslint-disable-next-line prefer-const
    let signers = [];

    const mintRent = await conn.getMinimumBalanceForRentExemption(MintLayout.span);
    const mintKey = createMint(
      instructions,
      phantomWalletPublicKey,
      mintRent,
      0,
      phantomWalletPublicKey,
      phantomWalletPublicKey,
      signers,
    );

    const recipientKey = await getTokenWallet(phantomWalletPublicKey, mintKey);
    createAssociatedTokenAccountInstruction(
      instructions,
      recipientKey,
      phantomWalletPublicKey,
      phantomWalletPublicKey,
      mintKey,
    );
    const mintinstructionforKey = await Token.createMintToInstruction(
      TOKEN_PROGRAM_ID,
      mintKey,
      recipientKey,
      phantomWalletPublicKey,
      [],
      1,
    );
    instructions.push(mintinstructionforKey);
    instructions.forEach((item) => transaction.add(item));

    const metadata = await getMetadata(mintKey);
    const masterEdition = await getEdition(mintKey);
    const [metadataExtended, bump] = await anchor.web3.PublicKey.findProgramAddress(
      [mintKey.toBuffer(), POOL.toBuffer()],
      PROGRAMID,
    );

    if (poolData.countMinting === 0) {
      const mintRootInstruction = program.instruction.mintRoot(new anchor.BN(bump)).accounts({
        owner: phantomPublicKey,
        pool: POOL,
        config: poolData.config,
        nftMint: mintKey,
        nftAccount: recipientKey,
        metadata,
        masterEdition,
        metadataExtended,
        // relatedNfpMint : relatedNfp,
        // relatedNfpAccount : relatedNfpAccount,
        scobyWallet: poolData.scobyWallet,
        tokenProgram: TOKEN_PROGRAM_ID,
        tokenMetadataProgram: TOKEN_METADATA_PROGRAM_ID,
        systemProgram: SystemProgram.programId,
        rent: SYSVAR_RENT_PUBKEY,
      });
      transaction.add(mintRootInstruction);
    } else {
      const memberships = await getNftsForOwner(SYMBOL);
      if (memberships.length === 0) throw new Error("That member don't have any member pass in his wallet");
      const parentMembership = memberships[0];

      const parentMembershipResp = await conn.getTokenLargestAccounts(parentMembership.extendedData.mint, 'finalized');
      if (parentMembershipResp == null || parentMembershipResp.value == null || parentMembershipResp.value.length === 0)
        throw new Error('Invalid NFP');
      const parentMembershipAccount = parentMembershipResp.value[0].address;

      const grandParentMembershipResp = await conn.getTokenLargestAccounts(
        parentMembership.extendedData.parentNfp,
        'finalized',
      );
      if (
        grandParentMembershipResp == null ||
        grandParentMembershipResp.value == null ||
        grandParentMembershipResp.value.length === 0
      )
        throw new Error('Invalid NFP');
      const grandParentMembershipAccount = grandParentMembershipResp.value[0].address;
      let infoMint = await conn.getAccountInfo(grandParentMembershipAccount, 'finalized');
      if (infoMint == null) throw new Error('grand parent membership info failed');
      let accountInfo = AccountLayout.decode(infoMint.data);
      if (Number(accountInfo.amount) === 0) throw new Error('Invalid Grand Parent Membership Nft info');
      // eslint-disable-next-line no-console
      console.log(accountInfo.owner);
      const grandParentMembershipOwner = new anchor.web3.PublicKey(accountInfo.owner);

      const grandGrandParentMembershipResp = await conn.getTokenLargestAccounts(
        parentMembership.extendedData.grandParentNfp,
        'finalized',
      );
      if (
        grandGrandParentMembershipResp == null ||
        grandGrandParentMembershipResp.value == null ||
        grandGrandParentMembershipResp.value.length === 0
      )
        throw new Error('Invalid NFP');
      const grandGrandParentMembershipAccount = grandGrandParentMembershipResp.value[0].address;
      infoMint = await conn.getAccountInfo(grandGrandParentMembershipAccount, 'finalized');
      if (infoMint == null) throw new Error('grand parent membership info failed');
      accountInfo = AccountLayout.decode(infoMint.data);
      if (Number(accountInfo.amount) === 0) throw new Error('Invalid Grand Parent Membership Nft info');
      const grandGrandParentMembershipOwner = new anchor.web3.PublicKey(accountInfo.owner);

      const grandGrandGrandParentMembershipResp = await conn.getTokenLargestAccounts(
        parentMembership.extendedData.grandGrandParentNfp,
        'finalized',
      );
      if (
        grandGrandGrandParentMembershipResp == null ||
        grandGrandGrandParentMembershipResp.value == null ||
        grandGrandGrandParentMembershipResp.value.length === 0
      )
        throw new Error('Invalid NFP');
      const grandGrandGrandParentMembershipAccount = grandGrandGrandParentMembershipResp.value[0].address;
      infoMint = await conn.getAccountInfo(grandGrandGrandParentMembershipAccount, 'finalized');
      if (infoMint == null) throw new Error('grand parent membership info failed');
      accountInfo = AccountLayout.decode(infoMint.data);
      if (Number(accountInfo.amount) === 0) throw new Error('Invalid Grand Parent Membership Nft info');
      const grandGrandGrandParentMembershipOwner = new anchor.web3.PublicKey(accountInfo.owner);
      const mintinstruction = await program.methods
        .mint(new anchor.BN(bump))
        .accounts({
          owner: phantomWalletPublicKey,
          pool: POOL,
          config: poolData.config,
          nftMint: mintKey,
          nftAccount: recipientKey,
          metadata,
          masterEdition,
          metadataExtended,
          parentNftMint: parentMembership?.contractAdress,
          parentNftAccount: parentMembershipAccount,
          parentNftOwner: profileOfUser.publicKey,
          parentMetadataExtended: parentMembership.metadataExtended,
          grandParentNftMint: parentMembership.extendedData.parentNfp,
          grandParentNftAccount: grandParentMembershipAccount,
          grandParentNftOwner: grandParentMembershipOwner,
          grandGrandParentNftMint: parentMembership.extendedData.grandParentNfp,
          grandGrandParentNftAccount: grandGrandParentMembershipAccount,
          grandGrandParentNftOwner: grandGrandParentMembershipOwner,
          // grandGrandGrandParentNftMint : parentMembership.extendedData.grandGrandParentNfp,
          grandGrandGrandParentNftAccount: grandGrandGrandParentMembershipAccount,
          grandGrandGrandParentNftOwner: grandGrandGrandParentMembershipOwner,

          scobyWallet: poolData.scobyWallet,
          tokenProgram: TOKEN_PROGRAM_ID,
          tokenMetadataProgram: TOKEN_METADATA_PROGRAM_ID,
          systemProgram: SystemProgram.programId,
          rent: SYSVAR_RENT_PUBKEY,
        })
        .instruction();
      transaction.add(mintinstruction);
      // eslint-disable-next-line no-console
      console.log('testing testing testing');
    }
    const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
    const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
    const dappPublicKey = await AsyncStorage.getItem('dapp_public_key');
    const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));

    transaction.feePayer = phantomWalletPublicKey;
    const anyTransaction = transaction;
    anyTransaction.recentBlockhash = (await conn.getLatestBlockhash()).blockhash;
    anyTransaction.sign(signers[0]);
    const serializedTransaction = anyTransaction.serialize({
      requireAllSignatures: false,
    });

    const payload = {
      session: publicSession,
      transaction: bs58.encode(serializedTransaction),
    };

    const [nonce, encryptedPayload] = encryptPayload(payload, sharedSecretDapp);

    const params = new URLSearchParams({
      dapp_encryption_public_key: dappPublicKey,
      nonce: bs58.encode(nonce),
      redirect_link: onTransactionRedirectLink,
      payload: bs58.encode(encryptedPayload),
    });
    const url = buildUrl('signAndSendTransaction', params);

    Linking.openURL(url);

    setLoading(false);
  };

  /* const print = async () => {
    // mint();
    const getMetadataDb = await getNftsForOwner(SYMBOL);
    console.log(JSON.stringify(getMetadataDb, null, 4));
  }; */

  mint();
};

export async function getSolanaPrice() {
  const URL =
    // eslint-disable-next-line max-len
    'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=solana&order=market_cap_desc&per_page=100&page=1&sparkline=false';
  const response = await axios.get(URL);
  const currPrice = response.data[0].current_price;
  return currPrice;
}
