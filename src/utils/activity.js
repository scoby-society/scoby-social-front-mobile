export const checkIsUserJoined = (members, userId) => {
  const findResult = members.find((member) => member.user.id === userId && member.isAccepted);
  if (findResult) {
    return true;
  }
  return false;
};
