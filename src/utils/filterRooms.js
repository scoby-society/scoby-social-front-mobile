export const filterRooms = (rooms, filter, currUser) =>
  rooms?.filter((room) => {
    const userIdx = room?.participantUsers?.findIndex(({id}) => id !== currUser.id);

    return (
      room?.participantUsers[userIdx]?.fullName.toLowerCase().indexOf(filter.toLowerCase()) > -1 ||
      room?.participantUsers[userIdx]?.username.toLowerCase().indexOf(filter.toLowerCase()) > -1
    );
  });
