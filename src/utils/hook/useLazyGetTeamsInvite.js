import {useLazyQuery} from '@apollo/client';
import { GET_TEAM_USERS_TO_INVITE_BY_NFT} from 'src/graphql/queries/team';

export const useLazyGetTeamsInvite = (limit, page, query, setTotal, setIsLoading, nftsRequired,setUsersTeamsToInviteNft, objectName) =>{
  const [refetch, {data = {[objectName]: {data: []}}, fetchMore, loading, error, networkStatus}]=useLazyQuery(GET_TEAM_USERS_TO_INVITE_BY_NFT, {
    variables: {
      nftsRequired,
      paging: {
        limit,
        page,
      },
      query,
    },
    onCompleted({getUsersTeamsToInviteNft}) {
      if(nftsRequired>0){
        setUsersTeamsToInviteNft({data:getUsersTeamsToInviteNft.data.filter((i)=>i.isNft===true)})
      }else{
        setUsersTeamsToInviteNft(getUsersTeamsToInviteNft)
      }
      setTotal(getUsersTeamsToInviteNft.paging.total);
      setIsLoading(false);
    },
    notifyOnNetworkStatusChange: true,
  })
  
  return [refetch, data, fetchMore, loading, error, networkStatus]
};
