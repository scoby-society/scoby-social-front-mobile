import {useLazyQuery} from '@apollo/client';
import {GET_TEAMS} from 'src/graphql/queries/profile';

export const useLazyGetTeams = (limit, page, query, setTotal, setIsLoading) =>
  useLazyQuery(GET_TEAMS, {
    variables: {
      paging: {
        limit,
        page,
      },
      query,
    },
    onCompleted({getAllTeams}) {
      setTotal(getAllTeams.paging.total);
      setIsLoading(false);
    },
    notifyOnNetworkStatusChange: true,
  });
