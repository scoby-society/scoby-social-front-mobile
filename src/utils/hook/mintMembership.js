/* eslint-disable indent */
import {clusterApiUrl} from '@solana/web3.js';
import {CLUSTER} from 'src/config/env';
import * as anchor from 'src/dist/browser/index';
import {useQuery} from '@apollo/client';
import {GET_NFTS_BY_WALLET} from 'src/graphql/queries/profile';
import AsyncStorage from '@react-native-community/async-storage';
import {Linking} from 'react-native';
import bs58 from 'bs58';
import nacl from 'tweetnacl';
import {membershipPass} from 'src/config/poolMint';
import {membershipabi} from 'src/abi/membership';
import {ExecuteMint} from '../solanaUtils';

const {useEffect, useState, useCallback} = require('react');

let deepLinkingListener = null;

const decryptPayload = (data, nonce, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');

  const decryptedData = nacl.box.open.after(bs58.decode(data), bs58.decode(nonce), sharedSecret);
  if (!decryptedData) {
    throw new Error('Unable to decrypt data');
  }
  return JSON.parse(Buffer.from(decryptedData).toString('utf8'));
};

export const useMintMembership = (USER) => {
  const connection = new anchor.web3.Connection(clusterApiUrl(CLUSTER));
  const [loadingTransation, setLoadingTransation] = useState(false);
  const [hasStarted, setHasStarted] = useState(false);
  const [mintResult, setMintResult] = useState(null);

  const {data: {getNftByWallet = []} = [], loading} = useQuery(GET_NFTS_BY_WALLET, {
    variables: {walletAdress: USER?.publicKey},
  });

  const processTransaction = useCallback(
    async (params) => {
      // eslint-disable-next-line no-console
      console.log('hasStarted123 ', hasStarted);
      if (!hasStarted) {
        return;
      }
      setHasStarted(false);

      const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
      const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
      const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));

      const signTransactionData = decryptPayload(params.get('data'), params.get('nonce'), sharedSecretDapp);

      // eslint-disable-next-line no-console
      console.log('hasStarted1234', hasStarted);

      // eslint-disable-next-line no-console
      console.log('signTransactionData ', signTransactionData);
    },
    [hasStarted],
  );

  const handleDeepLink = useCallback(
    ({url}) => {
      Linking.canOpenURL(url).then((supported) => {
        if (/signAndSendTransaction/.test(url)) {
          if (supported) {
            setHasStarted(false);
            const urlData = new URL(url);
            // eslint-disable-next-line no-console
            console.log('url is ', url);
            const params = urlData.searchParams;
            if (params.get('errorCode')) {
              // eslint-disable-next-line no-console
              console.log("params.get('errorCode') ", params.get('errorCode'), params.get('errorMessage'));
              setMintResult('error');
              return;
            }
            setMintResult('success');
            processTransaction(params);
          }
        }
      });
    },
    [processTransaction],
  );

  useEffect(() => {
    if (deepLinkingListener) {
      deepLinkingListener.remove();
    }
    deepLinkingListener = Linking.addEventListener('url', handleDeepLink);
  }, [handleDeepLink]);

  const mintMembership = async () => {
    await ExecuteMint(
      membershipPass,
      connection,
      getNftByWallet,
      membershipabi,
      USER,
      setHasStarted,
      setLoadingTransation,
    );
  };

  const resetMintResult = () => setMintResult(null);

  return {
    loading,
    nftDb: getNftByWallet,
    mintMembership,
    loadingTransation,
    mintingInProgress: hasStarted,
    mintResult,
    resetMintResult,
  };
};
