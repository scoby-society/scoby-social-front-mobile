const fonts = {
  roboto: 'Roboto-Regular',
  robotoMedium: 'Roboto-Medium',
  robotoBold: 'Roboto-Bold',
  goudy: 'Roboto-Bold',
};

export default fonts;
