import Config from 'react-native-config';

export const SCOBY_TOKEN = Config.TOKEN;

export const SCOBY_ENV = Config.ENVIRONMENT;

export const {
  CLUSTER,
  SCOBY_BE,
  MEDIASOUP_BE,
  CONNECT_WALLET_LOGO,
  SYMBOL,
  TOKEN_METADATA,
  PROGRAM_ID,
  POOL_KEY,
  REDIRECT_TRANSACTION_LINK,
  GIVEAWAY_PROGRAM_ID,
} = Config;

export const isProd = () => SCOBY_ENV === 'prod';

export const isDev = () => SCOBY_ENV === 'dev';

export const isStage = () => SCOBY_ENV === 'stage';

export const isTest = () => SCOBY_ENV === 'test';
