import {PublicKey} from '@solana/web3.js';
import * as anchor from 'src/dist/browser/index';
import {POOL_KEY, PROGRAM_ID, REDIRECT_TRANSACTION_LINK, SYMBOL, TOKEN_METADATA} from './env';

export const membershipPass = {
  POOL: new PublicKey(POOL_KEY),
  PROGRAMID: new PublicKey(PROGRAM_ID),
  SYMBOL,
  TOKEN_METADATA_PROGRAM_ID: new anchor.web3.PublicKey(TOKEN_METADATA),
  onTransactionRedirectLink: REDIRECT_TRANSACTION_LINK,
};
