import Config from 'react-native-config';
import {SERVER_ROOT} from './api';
import {isProd} from './env';

export const GOOGLE_API_KEY = '';

export const SCOBY_WEBSITE_LINK = '';

export const SCOBY_PRIVACY_POLICY_LINK = '';
export const SCOBY_POLICY_LINK = '';
export const SCOBY_COOKIE_POLICY_LINK = '';
export const SCOBY_TERMS_OF_USE_LINK = '';

export const ANDROID_APP_ID = isProd() ? ' ' : ' ';
export const IOS_APP_ID = isProd() ? '' : ' ';
export const IOS_BUNDLE_ID = isProd() ? ' ' : ' ';

export const {ANDROID_APP_LINK, APP_IOS_LINK} = Config;

export const SCOBY_IMAGE = `${SERVER_ROOT}/scoby.png`;
