import ChatKitty from 'react-native-chatkitty';
import {SCOBY_BE, SCOBY_ENV} from './env';

export const SERVER_ROOT = `https://${SCOBY_BE}`;

export const API_ROOT = `${SERVER_ROOT}`;

export const WEBSOCKET_URL = `wss://${SCOBY_BE}`;

export const apiKey = SCOBY_ENV === 'stage' ? '' : '';

export const kitty = ChatKitty.getInstance(apiKey);
// export const WEBSOCKET_URL = `wss://${SCOBY_BE}/ws`;
