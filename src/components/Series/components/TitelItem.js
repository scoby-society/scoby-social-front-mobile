import React from 'react';
import styled from 'styled-components';
import Fonts from 'src/constants/Fonts';
import Colors from 'src/constants/Colors';

const TitleWrapper = styled.Pressable({
  zIndex: 1,
  marginTop: 35,
});
const TextBoldDark = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.titleText,
  fontSize: 14,
});
const TextDark = styled.Text({
  ...Fonts.avenir,
  color: Colors.titleText,
  fontSize: 13,
});

const TitleSerie = ({enable, ownerUser, coOwner, seriesName, suscribeUsers}) => {
  if (enable) {
    return (
      <TitleWrapper>
        <TextBoldDark>{seriesName}</TextBoldDark>
        <TextDark>with {ownerUser.fullName}</TextDark>
        {coOwner && <TextDark>{`with ${coOwner.fullName}`}</TextDark>}
        {ownerUser.username && <TextBoldDark>{`@${ownerUser.username}`}</TextBoldDark>}
      </TitleWrapper>
    );
  }

  if (!suscribeUsers) {
    return (
      <TitleWrapper>
        <TextBoldDark>{seriesName}</TextBoldDark>
        <TextDark>{ownerUser.fullName}</TextDark>
        {coOwner && <TextDark>{`${coOwner.fullName}`}</TextDark>}
        {ownerUser.username && <TextBoldDark>{`@${ownerUser.username}`}</TextBoldDark>}
      </TitleWrapper>
    );
  }

  return (
    <TitleWrapper>
      <TextDark>{ownerUser.fullName}</TextDark>
      {coOwner && <TextDark>{`${coOwner.fullName}`}</TextDark>}
      {ownerUser.username && <TextBoldDark>{`@${ownerUser.username}`}</TextBoldDark>}
    </TitleWrapper>
  );
};

export default TitleSerie;
