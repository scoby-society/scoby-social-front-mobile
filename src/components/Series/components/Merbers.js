import React, {useMemo} from 'react';
import {StyleSheet} from 'react-native';
import styled from 'styled-components';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import Crowd from 'assets/svg/crowd.svg';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';

const Container = styled.TouchableOpacity({
  marginVertical: 5,
});

const JoinedAvatar = styled.Image({width: 26, height: 26, top: -14, opacity: 1, resizeMode: 'cover', borderRadius: 40});
const JoinedAvatar2 = styled.Image({
  width: 26,
  height: 26,
  opacity: 1,
  resizeMode: 'cover',
  borderRadius: 40,
  top: -12,
});

const StatusTextWrapper = styled.View({
  alignSelf: 'center',
  marginTop: 0,
  top: -10,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
});

const PurpleCircle = styled.View({
  width: 26,
  height: 26,
  borderRadius: 50,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: Colors.purpleBackgroundHalfOpacity,
});

const ThirdMemberAvatar = styled.View({
  width: 18,
  height: 18,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: Colors.pinkMagenta,
  borderRadius: 50,
});

const JoinedWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 3,
});

const Text = styled.Text({...Fonts.avenirSemiBold, color: Colors.white, fontSize: 12, alignSelf: 'center'});
const MoreMembersText = styled.Text({...Fonts.avenir, color: Colors.white, fontSize: 12});

const styles = StyleSheet.create({
  crowdIcon: {marginLeft: 4},
});

const Members = ({suscribeUsers, owner, subscribed, navigation, setVisible}) => {
  const handleWatchMembers = (subscribedUsers) => {
    if (typeof setVisible === 'function') {
      setVisible();
    }
    navigation.navigate('SeriesMembersJoined', {subscribedUsers});
  };

  const title = useMemo(() => {
    if (suscribeUsers) {
      if (suscribeUsers.length > 0) {
        return <Text style={{top: -13}}>Members joined</Text>;
      }
      if (!subscribed && !owner) {
        return (
          <StatusTextWrapper>
            <Text>Let`s fill the seats</Text>
            <Crowd style={styles.crowdIcon} />
          </StatusTextWrapper>
        );
      }
    }
    return <></>;
  }, [suscribeUsers, subscribed, owner]);

  if (suscribeUsers) {
    return (
      <Container onPress={() => handleWatchMembers(suscribeUsers)}>
        {title}
        <JoinedWrapper style={{transform: [{translateX: suscribeUsers.length <= 3 ? suscribeUsers.length * 3 : 9}]}}>
          {suscribeUsers.length > 0 && (
            <JoinedAvatar source={suscribeUsers[0].avatar ? {uri: suscribeUsers[0].avatar} : avatarSrc} />
          )}
          {suscribeUsers.length > 1 && (
            <JoinedAvatar2
              source={suscribeUsers[1].avatar ? {uri: suscribeUsers[1].avatar} : avatarSrc}
              style={{transform: [{translateX: -9}]}}
            />
          )}
          {suscribeUsers.length > 2 && (
            <PurpleCircle style={{transform: [{translateX: -18}, {translateY: -12}]}}>
              <ThirdMemberAvatar>
                <MoreMembersText>+{suscribeUsers.length > 11 ? 9 : suscribeUsers.length - 2}</MoreMembersText>
              </ThirdMemberAvatar>
            </PurpleCircle>
          )}
        </JoinedWrapper>
      </Container>
    );
  }
  return <></>;
};

export default Members;
