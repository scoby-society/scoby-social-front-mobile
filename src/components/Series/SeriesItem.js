/* eslint-disable no-use-before-define */
import React, {useContext, useCallback, useState} from 'react';
import {StyleSheet, View, FlatList, ActivityIndicator, Dimensions, Alert} from 'react-native';
import Eye from 'assets/svg/eye.svg';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {FollowersContext} from 'src/containers/followers';
import {SERIES_GO_LIVE, SERIES_JOIN, SERIES_JOIN_SESSION} from 'src/constants/Texts';
import {GET_SERIES_SESSION} from 'src/graphql/queries/session';
import {JOIN_SERIES, LEAVE_SERIES} from 'src/graphql/mutations/series';
import {useMutation, useQuery} from '@apollo/client';
import {GET_SERIE_BY_ID} from 'src/graphql/queries/series';
import moment from 'moment';
import {getCurrentWeekDay, getCurrentWeekNumber} from 'src/utils/series';
import {useNavigation} from '@react-navigation/native';
import {GET_FOLLOWERS} from 'src/graphql/queries/followers';
import {HOME_PROJECT_FEED} from 'src/graphql/queries/universalSearch';
import {LIMIT, PAGE} from 'src/utils/pagination';
import RegularButton from '../RegularButton';
import TitleSerie from './components/TitelItem';
import Menu from './components/Menu';
import Members from './components/Merbers';
import ManageBottoms from './components/ManageBottoms';
import PromptModal from '../Modal/PromptModal';

const {width} = Dimensions.get('window');

const Wrapper = styled.TouchableOpacity({
  width: '95%',
  height: '320px',
  alignSelf: 'center',
  flexDirection: 'row',
  backgroundColor: Colors.white,
  borderRadius: 10,
  marginVertical: 10,
  marginHorizontal: 10,
  zIndex: -1,
});

const LeftSide = styled.View({width: '50%'});

const RightSide = styled.View({
  width: '50%',
  flexDirection: 'column',
  justifyContent: 'space-between',
  paddingVertical: 20,
  paddingHorizontal: 6,
});

const Schedule = styled.View({
  width: 178,
  maxHeight: 300,
  position: 'absolute',
  backgroundColor: Colors.white,
  zIndex: 2000,
  marginVertical: 20,
  marginHorizontal: 7,
  borderRadius: 6,
  borderColor: 'rgba(105, 105, 105, 0.3)',
  borderWidth: 0.9,
});

const LeftSideTop = styled.View({
  paddingBottom: -25,
});

const Text = styled.Text({...Fonts.avenirSemiBold, color: Colors.white, fontSize: 12});

const ScheduleText = styled.Text({
  ...Fonts.avenir,
  color: Colors.purple,
  fontSize: 10,
});

const ScheduleWrraper = styled.View({
  flexDirection: 'row',
  justifyContent: 'center',
  marginVertical: 6,
});

const ScheduleTitle = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.purple,
  fontSize: 13,
});

const ScheduleClose = styled.TouchableOpacity({
  width: '25%',
});

const ScheduleTitleWrraper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
});

const Arrow = styled.Text({...Fonts.avenirBold, color: Colors.white});

const TextDark = styled.Text({...Fonts.avenir, color: Colors.titleText, fontSize: 13});

const TextPurple = styled.Text({...Fonts.avenir, color: Colors.purpleBorder, fontSize: 12, marginHorizontal: 2});

const CenteredWrapper = styled.View({flexDirection: 'row', alignItems: 'center'});

const SpaceBetweenWrapper = styled.TouchableOpacity({flexDirection: 'row', justifyContent: 'space-between'});

const Avatar = styled.View({
  width: 85,
  height: 85,
  borderRadius: 50,
  padding: 2,
  overflow: 'hidden',
  flexDirection: 'row',
  alignSelf: 'center',
  marginTop: 15,
  top: -13,
  borderColor: Colors.purpleBorder,
  borderWidth: 3,
});

const AvatarImage = styled.Image({
  width: '100%',
  height: '100%',
  opacity: 1,
  resizeMode: 'cover',
  borderRadius: 40,
});

const Topic = styled.View({
  paddingHorizontal: 10,
  paddingVertical: 4,
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'flex-start',
  backgroundColor: Colors.transluentPurple,
  marginVertical: 2,
  borderRadius: 6,
  width: '94%',
});

const TopicWrapper = styled.View({});

const extractKey = (index) => `showSchedule-${Math.random() + index}`;

const WrapperLoading = styled.View({
  height: 300,
  width,
  alignSelf: 'center',
  justifyContent: 'center',
  alignItems: 'center',
});

const styles = StyleSheet.create({
  leftGradient: {
    height: '100%',
    paddingVertical: 25,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    marginLeft: -4,
  },
  mt6: {marginTop: 6, marginRight: 10},
  eye: {width: 18, height: 18, color: Colors.white},

  live: {paddingHorizontal: 24, paddingVertical: 2},
  viewers: {paddingHorizontal: 2, paddingVertical: 2},
  linearGradient: {
    paddingHorizontal: 6,
    paddingVertical: 4,
    marginHorizontal: 2,
    borderRadius: 4,
  },
  btnText: {fontSize: 14},
  btnIcon: {color: Colors.white, width: 22, height: 22, marginRight: 6},
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    height: '12%',
    alignSelf: 'center',
    borderRadius: 8,
  },
  shadow: {
    borderColor: Colors.translucentGrey,
    borderWidth: 0.24,
    shadowColor: Colors.translucentGrey,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 1,
  },
  joinButton: {
    backgroundColor: '#7A015450',
    borderColor: '#7A015450',
  },
  serieIcon: {height: 32, width: 32, alignSelf: 'flex-start', marginTop: 20},
});

const SeriesItem = ({series, onJoinSession, openSerie, enable, setVisible}) => {
  const {currentUserProfile, currentCategory} = useContext(FollowersContext);
  const {
    ownerUser,
    coOwner,
    description,
    topics,
    session,
    suscribeUsers,
    viewers,
    subscribed,
    id,
    isAvailable,
    schedule = [],
    seriesName,
  } = series;

  const [showSchedule, setShowSchedule] = useState(false);
  const [isVisible, setIsVisible] = useState(false);
  const navigation = useNavigation();
  const isOwner = useCallback(() => currentUserProfile.id === ownerUser.id, [series]);
  const {data: {getFollowerUsers} = {getFollowerUsers: {data: []}}} = useQuery(GET_FOLLOWERS, {
    variables: {
      paging: {
        limit: 100,
        page: 1,
      },
      userId: currentUserProfile.id,
    },
  });

  const openLandingPage = (event) => {
    if (typeof openSerie === 'function') {
      openSerie(event);
    }
    if (typeof openSerie === 'string') {
      navigation.navigate(openSerie, {id: event.id});
    }
    if (typeof openSerie === 'undefined') {
      navigation.navigate('SeriesLandingView', {id: event.id});
    }
  };

  const openJoinSession = (sessionItem) => {
    const useJoinSession = {
      hostName: sessionItem.ownerUser.username,
      sessionName: sessionItem.title,
      sessionDescription: session.description,
      id: sessionItem.id,
      vonageSessionId: sessionItem.vonageSessionToken,
      userId: currentUserProfile.id,
      followers: getFollowerUsers && getFollowerUsers.data,
    };
    if (typeof onJoinSession === 'function') {
      onJoinSession();
    }
    if (typeof onJoinSession === 'string') {
      navigation.navigate(onJoinSession, {params: useJoinSession});
    }
    if (typeof onJoinSession === 'undefined') {
      navigation.navigate('JoinSession', {params: useJoinSession});
    }
  };

  const [joinSeries, {loading: JoinLoading}] = useMutation(JOIN_SERIES, {
    refetchQueries: [
      {query: GET_SERIES_SESSION},
      {query: GET_SERIE_BY_ID, variables: {id}},
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: LIMIT,
            page: PAGE,
          },
          category: currentCategory,
        },
      },
    ],
  });

  const [leaveSeries, {loading: LeaveLoading}] = useMutation(LEAVE_SERIES, {
    refetchQueries: [
      {query: GET_SERIES_SESSION},
      {query: GET_SERIE_BY_ID, variables: {id}},
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: LIMIT,
            page: PAGE,
          },
          category: currentCategory,
        },
      },
    ],
  });

  const handleSubscribeSerie = () => {
    if (!subscribed) {
      joinSeries({
        variables: {
          id,
        },
        onError(e) {
          Alert.alert('Error', e.message);
        },
      });
    } else {
      setIsVisible(true);
    }
  };

  const handleLeaveSerie = () => {
    leaveSeries({
      variables: {
        id,
      },
    });

    setIsVisible(false);
  };

  const handleJoinBottom = () => {
    if (session) {
      openJoinSession({...session, ownerUser});
    } else if (currentUserProfile.id === ownerUser.id) {
      navigation.reset({
        index: 0,
        routes: [{name: 'CreateSeriesSession', params: {sessionParams: {id}, experience: 'series'}}],
      });
    } else {
      handleSubscribeSerie();
    }
  };

  const setJoinBottomText = useCallback(() => {
    if (currentUserProfile.id === ownerUser.id) {
      return SERIES_GO_LIVE;
    }
    if (session || subscribed) {
      return SERIES_JOIN_SESSION;
    }
    return SERIES_JOIN;
  }, [session, currentUserProfile]);

  const sortDates = useCallback(() => {
    if (schedule !== null) {
      const sortSchedule = schedule.slice().sort((a, b) => {
        const firts = getCurrentWeekNumber(a.day);
        const second = getCurrentWeekNumber(b.day);

        if (firts > second) {
          return 1;
        }
        if (firts < second) {
          return -1;
        }
        return 0;
      });
      return sortSchedule;
    }
    return [];
  }, [schedule]);

  const setDate = useCallback(() => {
    const Today = sortDates().filter((item) => getCurrentWeekDay(Date.now()) === item.day)[0];
    if (Today) {
      return `${Today.day} ${moment(Today.start, 'HH:mm:ss').format('hh:mm a')} to ${moment(
        Today.end,
        'HH:mm:ss',
      ).format('hh:mm a')}`;
    }
    return sortDates()[0]
      ? `${sortDates()[0].day} ${moment(sortDates()[0].start, 'HH:mm:ss').format('hh:mm a')} to ${moment(
          sortDates()[0].end,
          'HH:mm:ss',
        ).format('hh:mm a')}`
      : 'No dates';
  }, []);

  const getScheduleText = useCallback(
    (item) =>
      `${item.item.day} ${moment(item.item.start, 'HH:mm:ss').format('hh:mm a')} to ${moment(
        item.item.end,
        'HH:mm:ss',
      ).format('hh:mm a')}`,
    [],
  );

  const RenderSchedule = useCallback(
    (item) => (
      <ScheduleWrraper>
        <ScheduleText>{getScheduleText(item)}</ScheduleText>
      </ScheduleWrraper>
    ),
    [schedule],
  );

  const RenderHeaderSchedule = useCallback(() => (
    <ScheduleTitleWrraper>
      <ScheduleClose onPress={() => setShowSchedule(false)}>
        <ScheduleTitle>{'<'}</ScheduleTitle>
      </ScheduleClose>
      <ScheduleTitle>Schedule</ScheduleTitle>
    </ScheduleTitleWrraper>
  ));

  if (JoinLoading || LeaveLoading) {
    return (
      <WrapperLoading>
        <ActivityIndicator size="large" color={Colors.white} />
      </WrapperLoading>
    );
  }

  return (
    <Wrapper
      activeOpacity={0.8}
      onPress={() => {
        openLandingPage(series);
      }}
      disabled={showSchedule || !enable}>
      <PromptModal
        visible={isVisible}
        setVisible={setIsVisible}
        text="You are about to unsubscribe from this Series. Are you sure?"
        leftButtonText="Yes"
        rightButtonText="No"
        onLeftButtonPress={handleLeaveSerie}
        onRightButtonPress={() => setIsVisible(false)}
      />
      {showSchedule && (
        <Schedule>
          <FlatList
            ListHeaderComponent={RenderHeaderSchedule}
            data={sortDates()}
            renderItem={RenderSchedule}
            keyExtractor={extractKey}
            style={{paddingHorizontal: 10, paddingVertical: 20, height: '90%'}}
            ListFooterComponent={<ScheduleWrraper />}
          />
        </Schedule>
      )}
      <LeftSide>
        <LinearGradient colors={[Colors.confirmationGreen, Colors.gradientYellow]} style={styles.leftGradient}>
          <LeftSideTop>
            {session && (
              <CenteredWrapper>
                <LinearGradient
                  colors={[Colors.translucentGrey, Colors.translucentWhite]}
                  style={[styles.linearGradient, styles.live]}>
                  <Text>LIVE</Text>
                </LinearGradient>
                <LinearGradient
                  colors={[Colors.translucentWhite, Colors.translucentWhite]}
                  style={[styles.linearGradient, styles.viewers]}>
                  <CenteredWrapper>
                    <Eye style={styles.eye} />
                    <Text>{viewers}</Text>
                  </CenteredWrapper>
                </LinearGradient>
              </CenteredWrapper>
            )}
            <View style={styles.mt6}>
              <LinearGradient
                start={{x: 0.1, y: 0.2}}
                end={{x: 0.83, y: 0.8}}
                locations={[0.2, 1]}
                colors={['rgba(3, 204, 217, 0.2)', 'rgba(16,14,89,0.3)']}
                style={styles.linearGradient}>
                <SpaceBetweenWrapper onPress={() => setShowSchedule(true)}>
                  <Text>{setDate()}</Text>
                  <Arrow>{`>`}</Arrow>
                </SpaceBetweenWrapper>
              </LinearGradient>
            </View>
          </LeftSideTop>
          <Avatar>
            <AvatarImage source={ownerUser.avatar ? {uri: ownerUser.avatar} : avatarSrc} />
          </Avatar>
          <Members
            owner={isOwner()}
            subscribed={subscribed}
            suscribeUsers={suscribeUsers}
            session={session}
            navigation={navigation}
            setVisible={setVisible}
          />
          <ManageBottoms
            isOwner={isOwner()}
            navigation={navigation}
            suscribeUsers={suscribeUsers}
            series={series}
            setVisible={setVisible}
          />
          {suscribeUsers && (
            <RegularButton
              disabled={(JoinLoading || LeaveLoading || session ? false : subscribed) || (!isAvailable && !isOwner())}
              onPress={handleJoinBottom}
              title={setJoinBottomText()}
              style={[
                styles.regularBtn,
                session ? {} : subscribed && styles.joinButton,
                !isAvailable && !isOwner() && {backgroundColor: Colors.disabledPink},
              ]}
              loading={LeaveLoading || JoinLoading}
            />
          )}
        </LinearGradient>
      </LeftSide>
      <RightSide>
        <Menu
          isOwner={isOwner()}
          enable={enable}
          series={series}
          navigation={navigation}
          handleLeave={handleSubscribeSerie}
        />
        <TitleSerie
          enable={enable}
          ownerUser={ownerUser}
          coOwner={coOwner}
          seriesName={seriesName}
          suscribeUsers={suscribeUsers}
        />
        <TextDark>{description}</TextDark>
        {topics && (
          <TopicWrapper>
            {topics.slice(0, 4).map((topic) => (
              <>
                <Topic key={topic.icon}>
                  <TextPurple>{topic.icon}</TextPurple>
                  <TextPurple>{topic.name}</TextPurple>
                </Topic>
              </>
            ))}
          </TopicWrapper>
        )}
      </RightSide>
    </Wrapper>
  );
};

export default SeriesItem;
