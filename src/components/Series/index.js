import React from 'react';
import SeriesHomeItem from './SeriesItem';

const SerieHome = ({series, navigation, onJoinSession, openSerie, enable, setVisible, goBackModal}) => (
  <SeriesHomeItem
    series={series}
    navigation={navigation}
    onJoinSession={onJoinSession}
    openSerie={openSerie}
    enable={enable}
    setVisible={setVisible}
    goBackModal={goBackModal}
  />
);

export default SerieHome;
