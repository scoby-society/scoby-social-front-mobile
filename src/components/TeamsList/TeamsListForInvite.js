import React, { useRef, useEffect} from 'react';
import {StyleSheet, KeyboardAvoidingView, Platform} from 'react-native';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import {tabsHeight} from 'src/navigation/tabs';
import SearchInput from 'src/components/SearchInput';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  overflow: 'hidden',
});

const Teams = styled.FlatList({
  flex: 1,
});

function keyExtractor({id}) {
  return `team-row-${id}-${Math.random()}`;
}

const TeamsList = ({
  renderItem,
  ListHeaderComponent,
  emptyDataComponent,
  emptySearchResultsComponent,
  UsersTeamsToInviteNft,
  setQuery,
  query
}) => {
  const isInitialEmpty = useRef(false);
  const scrollView = useRef();

  const EmptyDataComponent = emptyDataComponent;
  const EmptyResultsSearchComponent = emptySearchResultsComponent;

  useEffect(() => {
    isInitialEmpty.current = query === '' && UsersTeamsToInviteNft.data.length === 0;
  }, [query, UsersTeamsToInviteNft.data.length]);

  const styles = StyleSheet.create({
    scroll: {
      paddingBottom: tabsHeight + 24,
    },
  });

  return (
    <Wrapper>
      <SearchInput value={query} onChangeText={setQuery} autoCorrect={false} />
      {isInitialEmpty.current && EmptyDataComponent ? (
        <EmptyDataComponent />
      ) : UsersTeamsToInviteNft.data.length === 0 && EmptyResultsSearchComponent ? (
        <EmptyResultsSearchComponent />
      ) : (
        <Teams
          ref={scrollView}
          data={UsersTeamsToInviteNft.data}
          keyboardShouldPersistTaps="handled"
          keyboardDismissMode="interactive"
          contentContainerStyle={styles.scroll}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          onEndReachedThreshold={0.8}
          refreshing={false}
          showsVerticalScrollIndicator={false}
          endReached
          ListHeaderComponent={ListHeaderComponent || undefined}
        />
      )}

      <KeyboardAvoidingView
        enabled={Platform.OS === 'android'}
        behavior="padding"
        keyboardVerticalOffset={-tabsHeight}
      />
    </Wrapper>
  );
};

export default TeamsList;
