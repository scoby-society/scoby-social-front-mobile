import React, {useState, useCallback, useRef, useEffect} from 'react';
import {StyleSheet, KeyboardAvoidingView, Platform, ActivityIndicator} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import {tabsHeight} from 'src/navigation/tabs';
import SearchInput from 'src/components/SearchInput';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  overflow: 'hidden',
});

const Teams = styled.FlatList({
  flex: 1,
});

function keyExtractor({id}) {
  return `team-row-${id}-${Math.random()}`;
}

const TeamsList = ({
  limit,
  renderItem,
  useLazyQueryHook,
  objectName,
  ListHeaderComponent,
  emptyDataComponent,
  emptySearchResultsComponent,
}) => {
  const navigation = useNavigation();
  const isInitialEmpty = useRef(false);
  const [isLoading, setIsLoading] = useState(true);
  const [query, setQuery] = useState('');
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const scrollView = useRef();

  const EmptyDataComponent = emptyDataComponent;
  const EmptyResultsSearchComponent = emptySearchResultsComponent;

  const [refetch, {data = {[objectName]: {data: []}}, fetchMore, loading, error, networkStatus}] = useLazyQueryHook(
    limit,
    page,
    query,
    setTotal,
    setIsLoading,
  );

  const fetchedData = data[objectName];

  useEffect(() => {
    isInitialEmpty.current = query === '' && fetchedData.data.length === 0;
  }, [query, fetchedData.data.length]);

  const handleEndReached = useCallback(() => {
    if (fetchedData.data.length < total) {
      setPage(page + 1);
      fetchMore({
        variables: {
          paging: {
            limit,
            page: page + 1,
          },
          query,
        },
        fetchPolicy: 'network-only',
      });
    }
  }, [fetchMore, page, query, total, fetchedData.data.length, limit]);

  const handleRefresh = useCallback(() => {
    setPage(1);
    refetch({
      fetchPolicy: 'no-cache',
      variables: {
        paging: {
          limit,
          page: 1,
        },
        query,
      },
    });
    scrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
  }, [query, refetch, limit]);

  const styles = StyleSheet.create({
    scroll: {
      paddingBottom: tabsHeight + 24,
    },
  });

  useEffect(() => {
    scrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
    const timeout = setTimeout(
      () => {
        setPage(1);
        refetch({
          variables: {
            paging: {
              limit,
              page: 1,
            },
            query,
          },
        });
      },
      query ? 400 : 0,
    );

    return () => {
      clearTimeout(timeout);
    };
  }, [query, refetch, limit]);

  useEffect(() => {
    setTotal(data[objectName]?.paging?.total);
  }, [data[objectName]?.paging?.total]);

  useEffect(
    () =>
      navigation.addListener('focus', () => {
        refetch();
        setIsLoading(false);
        setPage(1);
      }),
    [navigation, refetch],
  );

  useEffect(
    () =>
      navigation.addListener('blur', () => {
        setIsLoading(true);
      }),
    [navigation],
  );

  const isIndicatorShown = isLoading || networkStatus === 4;
  const isAnimating = (loading || fetchedData.data.length < total) && !error;

  return (
    <Wrapper>
      <SearchInput value={query} onChangeText={setQuery} textColor={colors.black} autoCorrect={false} />
      {isIndicatorShown ? (
        <ActivityIndicator size="large" color={colors.white} />
      ) : isInitialEmpty.current && EmptyDataComponent ? (
        <EmptyDataComponent />
      ) : fetchedData.data.length === 0 && EmptyResultsSearchComponent ? (
        <EmptyResultsSearchComponent />
      ) : (
        <Teams
          ref={scrollView}
          data={fetchedData.data}
          keyboardShouldPersistTaps="handled"
          keyboardDismissMode="interactive"
          contentContainerStyle={styles.scroll}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          onEndReached={handleEndReached}
          onEndReachedThreshold={0.8}
          refreshing={false}
          onRefresh={handleRefresh}
          showsVerticalScrollIndicator={false}
          endReached
          ListHeaderComponent={ListHeaderComponent || undefined}
          ListFooterComponent={<ActivityIndicator size="large" color={colors.white} animating={isAnimating} />}
        />
      )}

      <KeyboardAvoidingView
        enabled={Platform.OS === 'android'}
        behavior="padding"
        keyboardVerticalOffset={-tabsHeight}
      />
    </Wrapper>
  );
};

export default TeamsList;
