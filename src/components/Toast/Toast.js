import React, {useEffect, useRef} from 'react';

import {Animated, Text, View} from 'react-native';
import {goudy} from 'src/constants/Fonts';

const Message = (props) => {
  const opacity = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true,
      }),

      Animated.delay(800),
      Animated.timing(opacity, {
        toValue: 0,
        duration: 500,
        useNativeDriver: true,
      }),
    ]).start(() => {
      props.onHide();
    });
  }, []);

  return (
    <Animated.View
      style={{
        opacity,
        display: 'flex',

        alignItems: 'center',
        justifyContent: 'center',
        width: 120,
        transform: [
          {
            translateY: opacity.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0],
            }),
          },
        ],
        margin: 10,
        marginBottom: 5,
        backgroundColor: 'background: rgba(101, 54, 187, .8)',
        padding: 10,
        borderRadius: 9,
        shadowColor: 'black',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.15,
        shadowRadius: 5,
        elevation: 6,
      }}>
      <Text style={{fontSize: 16, fontFamily: goudy.fontFamily}}>{props.message}</Text>
    </Animated.View>
  );
};
const Toast = ({messages, setMessages}) => (
  <>
    <View
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          top: 300,
          left: 0,
          right: 0,
        }}>
        {messages.map((message) => (
          <Message
            key={message}
            message={message}
            onHide={() => {
              setMessages((messagess) => messagess.filter((currentMessage) => currentMessage !== message));
            }}
          />
        ))}
      </View>
    </View>
  </>
);

export default Toast;
