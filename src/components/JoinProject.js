import React from 'react';
import {StyleSheet} from 'react-native';
import RegularButton from 'src/components/RegularButton';

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const JoinProject = ({large, style, widthContainer, navigation, id}) => {
  const openProject = () => {
    navigation.navigate('OneViewProjectScreen', {projectId: id});
  };

  return (
    <RegularButton
      style={[styles.button, style]}
      large={large}
      title={`Let's go`}
      active={false}
      loading={false}
      numberOfLines={1}
      widthContainer={widthContainer}
      onPress={openProject}
    />
  );
};

export default JoinProject;
