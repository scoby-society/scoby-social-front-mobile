import React from 'react';
import styled from 'styled-components';
import {StyleSheet, Text} from 'react-native';
import {CloseWhiteIco} from 'assets/svg';
import LinearGradient from 'react-native-linear-gradient';
import Colors from 'src/constants/Colors';

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: -10,
  top: -10,
});

const styles = StyleSheet.create({
  linearGradient: {
    width: 272,
    height: 138,
    position: 'relative',
    zIndex: 1,
    top: '100%',
    right: 60,
    backgroundColor: Colors.primaryPurpleColor,
    borderColor: '(rgba(255, 255, 255, 0.3), rgba(255, 255, 255,8) , rgba(255, 255, 255, 0.2) )',
    borderWidth: 1,
    borderRadius: 16,
    paddingTop: 40,
    paddingBottom: 32,
    paddingLeft: 10,
    paddingRight: 10,
    alignContent: 'center',

    color: 'white',
  },
});

function ReportAlert({show, setShow, user, reported}) {
  return (
    <>
      {show && (
        <LinearGradient
          colors={['rgba(255, 255, 255, 0.08)', ' rgba(255, 255, 255, 0.22)']}
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 1.0}}
          style={styles.linearGradient}>
          <CloseButton onPress={() => setShow(false)}>
            <CloseWhiteIco />
          </CloseButton>
          {reported ? (
            <Text style={{color: 'white'}}>
              Thank you for reporting {user.username} Your report will be reviewed by our Guides and they'll respond
              promptly.
            </Text>
          ) : (
            <Text style={{color: 'white'}}>This user already has a report. wait 24 hours to report again</Text>
          )}
        </LinearGradient>
      )}
    </>
  );
}

export default ReportAlert;
