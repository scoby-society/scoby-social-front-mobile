import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import styled from 'styled-components/native';
import {avenir} from '../constants/Fonts';
import colors from '../constants/Colors';

const DescriptionText = styled.Text({
  avenir,
  fontSize: 14,
  color: colors.white,
});
const styles = StyleSheet.create({
  btn1: {
    alignSelf: 'flex-end',
    paddingTop: 15,
    paddingBottom: 10,
  },
});

function SkipButton({skip, variation}) {
  return (
    <TouchableOpacity style={variation === 1 && styles.btn1} onPress={skip}>
      <DescriptionText>Skip </DescriptionText>
    </TouchableOpacity>
  );
}

export default SkipButton;
