import React from 'react';
import styled from 'styled-components/native';
import {Text} from 'react-native';
import fonts from 'src/constants/Fonts';

const FirstText = styled.Text({
  ...fonts.avenir,
  color: 'rgb(255,255,255)',
});
const SecondText = styled.Text({
  ...fonts.avenir,
  color: 'rgba(255,255,255, .6)',
});
const Container = styled.View({
  flexDirection: 'row',
});
const Steps = ({FirstValue, SecondValue}) => (
  <>
    <Container>
      <FirstText>{FirstValue}</FirstText>
      <Text
        style={{
          ...fonts.avenir,
          color: '#fff',
        }}>
        /
      </Text>
      <SecondText>{SecondValue}</SecondText>
    </Container>
  </>
);
export default Steps;
