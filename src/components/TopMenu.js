// import {useNavigation} from '@react-navigation/native';
import {Item1, IonWallet,HandShakeIco} from 'assets/svg';
import React, {useContext} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import {shortendPublicKey} from 'src/utils/helpers';
import LinearGradient from 'react-native-linear-gradient';
import {FollowersContext} from 'src/containers/followers';
import styled from 'styled-components/native';
import Colors from 'src/constants/Colors';
import {GET_NFTS_BY_WALLET} from 'src/graphql/queries/profile';
import {useLazyQuery} from '@apollo/client';

const styles = StyleSheet.create({
  menu: {
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 40,
    paddingRight: 30,
    paddingTop: Platform.OS === 'android' ? 30 : 0,
    paddingBottom: 10,
    borderBottomColor: '#2A136C',
    borderBottomWidth: 1,
  },
  containIcon: {
    flexDirection:'row',
    justifyContent:'space-between',
  }
});

const ActivityIndicator = styled.ActivityIndicator({});

function TopMenu(props) {
  const {currentUserProfile} = useContext(FollowersContext);
  const [getNfts, {loading}] = useLazyQuery(GET_NFTS_BY_WALLET, {
    variables: {walletAdress: currentUserProfile.publicKey},
    fetchPolicy: 'network-only',
    onCompleted({getNftByWallet}) {
      if (getNftByWallet?.length > 0) {
        props.navigation.navigate('UserNftList');
      } else {
        props.navigation.navigate('WalletConnectUser', {from: 'user'});
      }
    },
    onError() {
      // eslint-disable-next-line no-console
      props.navigation.navigate('WalletConnectUser', {from: 'user'});
    },
  });

  const gotoWallet = async () => {
    await getNfts();
  };
  return (
    <View style={styles.menu}>
     {/* <View style={styles.containIcon}>

     </View> */}
      <TouchableOpacity onPress={() => props.navigation.navigate('Royalties')}>
        <Item1 style={{opacity: 0.6}} />
      </TouchableOpacity>
      {props.walletKey === null && (
        <TouchableOpacity disabled={loading} onPress={gotoWallet}>
          {loading && <ActivityIndicator size="small" color={Colors.white} />}
          {!loading && (
            <Text style={{color: '#fff'}}>
              <IonWallet />
            </Text>
          )}
        </TouchableOpacity>
      )}
      {props.walletKey !== null && (
        <LinearGradient
          start={{x: 0, y: 0.3}}
          end={{x: 1, y: 0}}
          colors={['#cd068e', '#9e0f92', '#7a1794', '#5f1c96', '#501f98', '#4a2098']}
          style={{
            padding: 5,
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 20,
          }}>
          <TouchableOpacity disabled={loading} onPress={gotoWallet}>
            {loading && <ActivityIndicator size="small" color={Colors.white} />}
            {!loading && <Text style={{color: '#fff'}}>{shortendPublicKey(props.walletKey)}</Text>}
          </TouchableOpacity>
        </LinearGradient>
      )}
    </View>
  );
}

export default TopMenu;
