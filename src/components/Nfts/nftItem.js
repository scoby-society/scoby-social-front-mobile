import React, {useMemo} from 'react';
import {ActivityIndicator, TouchableOpacity, View} from 'react-native';
import styled from 'styled-components';
import magicSpore from 'assets/images/Magic_spore.png';
import Fonts from 'src/constants/Fonts';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import openLink from 'src/utils/hook/openLink';
import {CLUSTER} from 'src/config/env';
import Colors from 'src/constants/Colors';
import ImageGradient from './components/ImageGradient';
import Gradient from './components/Gradient';

const Container = styled.View({
  width: '90%',
  alignSelf: 'center',
  minHeight: 200,
  margin: 20,
});

const NftImage = styled.Image({
  width: '100%',
  height: '100%',
  opacity: 1,
  resizeMode: 'cover',
  borderRadius: 15,
});

const Avatar = styled.View({
  width: 138,
  height: 138,
  padding: 5,
  margin: '0 10px 0 10px',
  borderRadius: 16,
});

const Title = styled.Text({
  color: '#fff',
  fontFamily: Fonts.avenirBold.fontFamily,
  fontWeight: 700,
  fontSize: 15,
  marginVertical: 10,
});

const Description = styled.Text({
  color: '#fff',
  fontFamily: Fonts.avenir.fontFamily,
  fontSize: 12,
  maxHeight: 120,
  marginBottom: 10,
});

const ViewOnWeb = styled.TouchableOpacity({
  borderRadius: 5,
  margin: 10,
});

const ViewOnWebCallToAction = styled.TouchableOpacity({
  backgroundColor: '#CD068E',
  borderRadius: 5,
  margin: 10,
});

const BottonWhiteTextCTA = styled.Text({
  ...Fonts.avenir,
  fontSize: 12,
  color: '#fff',
  textAlign: 'center',
  paddingVertical: 5,
});

const LeftContainer = styled(ImageGradient)(({CurrentUser, experience}) => ({
  margin: 10,
  height: CurrentUser ? 'auto' : 180,
  minHeight: experience ? 170 : 190,
  borderRadius: 16,
  justifyContent: 'space-between',
}));

const RightContainer = styled.View({
  width: '42%',
  paddingVertical: 10,
  justifyContent: 'space-between',
});

const LinkText = styled.Text({
  color: '#fff',
  textDecoration: 'underline #fff',
  fontFamily: Fonts.avenir.fontFamily,
  fontSize: 12,
  textAlign: 'center',
});

const BottonWhiteText = styled.Text({
  ...Fonts.avenir,
  fontSize: 10,
  color: '#fff',
  textAlign: 'center',
  paddingVertical: 5,
  textDecorationLine: 'underline',
});

const UserAvatar = styled.Image({
  height: 35,
  width: 35,
  borderRadius: 40,
});

const BottomContainer = styled.View({
  height: 70,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
});

const BottomButtom = styled.TouchableOpacity(({selected}) => ({
  borderRadius: 5,
  borderWidth: selected ? 0 : 1,
  borderColor: '#fff',
  marginHorizontal: 5,
  paddingHorizontal: 4,
  background: selected ? Colors.purple : null,
}));

const TopView = styled.View({
  flexDirection: 'row',
});

const BottonText = styled.Text({
  ...Fonts.avenir,
  fontSize: 12,
  color: '#fff',
  textAlign: 'center',
  paddingVertical: 5,
});

const NftItem = ({
  item,
  CurrentUser = true,
  experience = null,
  isFeed = false,
  holder,
  require,
  primary,
  setHolder,
  setPrimary,
  setRequire,
  mintMembership,
  loadingTransation,
}) => {
  const {urlImage, name, isVisible, description, user, id, contractAdress, webSite} = item;
  const isSelectedHolders = useMemo(() => holder?.find((i) => i === id) || false, [id, holder]);
  const isSelectedAsRequire = useMemo(() => require?.find((i) => i === id) || false, [id, require]);
  const isSelectedAsPrimary = useMemo(() => id === primary, [id, primary]);

  const InviteHolder = () => {
    if (typeof setHolder === 'function') {
      if (isSelectedHolders) {
        setHolder((nft) => nft.filter((i) => i !== id));
      } else {
        setHolder((nft) => [...nft, id]);
      }
    }
  };

  const asReqire = () => {
    if (typeof setRequire === 'function') {
      if (isSelectedAsRequire) {
        setRequire((nft) => nft.filter((i) => i !== id));
      } else {
        setRequire((nft) => [...nft, id]);
      }
    }
  };

  const asPrimary = () => {
    if (typeof setPrimary === 'function') {
      setPrimary((i) => (i === id ? null : id));
    }
  };

  const parseUrl = useMemo(() => {
    let urlText = `${webSite}`.replace('https://', '');
    urlText = `${urlText}`.replace('http://', '');
    urlText = `${urlText}`.replace('www.', '');
    urlText = `${urlText}`.split('/')[0] ?? '';
    return urlText === 'null' ? '' : urlText;
  }, [webSite]);

  const Mint = () => {
    if (typeof mintMembership === 'function') {
      mintMembership();
    }
  };

  return (
    <Container style={{display: isVisible === false ? 'none' : 'flex'}}>
      <Gradient>
        <TopView>
          <LeftContainer CurrentUser experience={experience}>
            <Avatar>
              <NftImage source={urlImage ? {uri: urlImage} : magicSpore} />
              {!isFeed && (
                <View style={{transform: [{translateY: -20}, {translateX: -7}]}}>
                  <UserAvatar source={user?.avatar ? {uri: user?.avatar} : avatarSrc} />
                </View>
              )}
              {!experience && (
                <TouchableOpacity
                  onPress={() => openLink(`${webSite}`)}
                  style={{maxWidth: 120, justifyContent: 'center', alignItems: 'center'}}>
                  <LinkText numberOfLines={1}>{parseUrl}</LinkText>
                </TouchableOpacity>
              )}
            </Avatar>
          </LeftContainer>
          <RightContainer>
            <Title>{name}</Title>
            <Description>{description?.length > 120 ? `${description?.slice(0, 120)}...` : description}</Description>
            {loadingTransation && item.symbol === 'SCOBYNYC' && <ActivityIndicator size="small" color="#fff" />}
            {!loadingTransation && item.symbol === 'SCOBYNYC' && !isFeed && (
              <ViewOnWebCallToAction
                disabled={item.symbol !== 'SCOBYNYC' || CurrentUser || loadingTransation}
                onPress={Mint}>
                <BottonWhiteTextCTA>
                  {item.symbol === 'SCOBYNYC' && !CurrentUser ? 'Mint Pass!' : 'Coming Soon'}
                </BottonWhiteTextCTA>
              </ViewOnWebCallToAction>
            )}
            <ViewOnWeb onPress={() => openLink(`https://solscan.io/token/${contractAdress}?cluster=${CLUSTER}`)}>
              <BottonWhiteText>View on solscan</BottonWhiteText>
            </ViewOnWeb>
          </RightContainer>
        </TopView>
        {experience && (
          <BottomContainer>
            <BottomButtom onPress={InviteHolder} selected={isSelectedHolders}>
              <BottonText>Invite Holders</BottonText>
            </BottomButtom>
            <BottomButtom onPress={asReqire} selected={isSelectedAsRequire}>
              <BottonText>Require for Entry</BottonText>
            </BottomButtom>
            <BottomButtom onPress={asPrimary} selected={isSelectedAsPrimary}>
              <BottonText>Set as Primary</BottonText>
            </BottomButtom>
          </BottomContainer>
        )}
      </Gradient>
    </Container>
  );
};

export default NftItem;
