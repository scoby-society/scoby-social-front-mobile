import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
  Gradient: {
    borderRadius: 10,
    flexDirection: 'column',
  },
});

const Gradient = ({children}) => (
  <LinearGradient
    start={{
      x: 0,
      y: 0.1,
    }}
    end={{
      x: 0,
      y: 0.8,
    }}
    colors={['rgba(216, 88, 188, 0.5)', 'rgba(60, 0, 255, 0.4)']}
    style={styles.Gradient}>
    {children}
  </LinearGradient>
);

export default Gradient;