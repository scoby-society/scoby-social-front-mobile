import React from 'react'
import LinearGradient from 'react-native-linear-gradient'

const ImageGradient = (props) => {
    return (
        <LinearGradient
            {...props}
            colors={['rgba(255, 255, 255, 0.2)', 'rgba(255, 255, 255, 0.1)']}
        >
            {props.children}
        </LinearGradient>
    )
}

export default ImageGradient