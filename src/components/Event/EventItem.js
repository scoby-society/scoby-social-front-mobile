import {useMutation, useQuery} from '@apollo/client';
import {useNavigation} from '@react-navigation/native';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import Eye from 'assets/svg/eye.svg';
import moment from 'moment';
import React, {useCallback, useContext, useState} from 'react';
import {ActivityIndicator, Alert, Dimensions, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {EVENT_JOIN, SERIES_GO_LIVE} from 'src/constants/Texts';
import {FollowersContext} from 'src/containers/followers';
import {JOIN_EVENT, LEAVE_EVENT} from 'src/graphql/mutations/event';
import {GET_EVENT_BY_ID} from 'src/graphql/queries/events';
import {GET_FOLLOWERS} from 'src/graphql/queries/followers';
import {GET_SERIES_SESSION} from 'src/graphql/queries/session';
import {HOME_PROJECT_FEED} from 'src/graphql/queries/universalSearch';
import {LIMIT, PAGE} from 'src/utils/pagination';
import styled from 'styled-components';

import PromptModal from '../Modal/PromptModal';
import RegularButton from '../RegularButton';
import Members from './components/Members';
import Menu from './components/Menu';
import LeftGradient from './LeftGradient';
import ManageBottoms from './ManageBottom';
import ScheduleGradiant from './ScheduleGradiant';
import TitleEvent from './TitleEvent';

const {width} = Dimensions.get('window');

const Wrapper = styled.TouchableOpacity({
  width: '95%',
  height: '320px',
  alignSelf: 'center',
  flexDirection: 'row',
  backgroundColor: Colors.white,
  borderRadius: 10,
  marginVertical: 10,
  marginHorizontal: 10,
  zIndex: -1,
});

const Avatar = styled.View({
  width: 85,
  height: 85,
  borderRadius: 50,
  padding: 2,
  overflow: 'hidden',
  flexDirection: 'row',
  alignSelf: 'center',
  marginTop: 20,
  borderColor: Colors.purpleBorder,
  borderWidth: 3,
});

const AvatarImage = styled.Image({
  width: '100%',
  height: '100%',
  opacity: 1,
  resizeMode: 'cover',
  borderRadius: 40,
});

const Text = styled.Text({...Fonts.avenirSemiBold, color: Colors.white, fontSize: 12});

const TextDark = styled.Text({
  ...Fonts.avenir,
  color: Colors.titleText,
  fontSize: 13,
  marginLeft: 10,
  marginVertical: 10,
});

const LeftSide = styled.View({width: '50%', paddingVertical: 25});

const RightSide = styled.View({
  width: '50%',
  flexDirection: 'column',
  paddingVertical: 20,
  paddingHorizontal: 6,
  backgroundColor: Colors.white,
  borderTopRightRadius: 10,
  borderBottomRightRadius: 10,
  justifyContent: 'space-between',
});

const TopicWrapper = styled.View({marginLeft: 0});
const CenteredWrapper = styled.View({flexDirection: 'row', alignItems: 'center'});

const TextPurple = styled.Text({...Fonts.avenir, color: Colors.purpleBorder, fontSize: 12, marginHorizontal: 2});

const Topic = styled.View({
  paddingHorizontal: 10,
  paddingVertical: 4,
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'flex-start',
  backgroundColor: Colors.transluentPurple,
  marginVertical: 2,
  borderRadius: 6,
  maxWidth: 150,
});

const styles = StyleSheet.create({
  serieIcon: {
    height: 32,
    width: 32,
    alignSelf: 'flex-end',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    height: 40,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 6,
  },
  joinButton: {
    backgroundColor: '#7A015450',
    borderColor: '#7A015450',
  },
  linearGradient: {
    paddingHorizontal: 6,
    paddingVertical: 4,
    marginHorizontal: 2,
    borderRadius: 4,
  },
  live: {paddingHorizontal: 24, paddingVertical: 2},
  viewers: {paddingHorizontal: 2, paddingVertical: 2},
  eye: {width: 18, height: 18, color: Colors.white},
});

const WrapperLoading = styled.View({
  height: 300,
  width,
  alignSelf: 'center',
  justifyContent: 'center',
  alignItems: 'center',
});

const EventItem = ({event, enable, openEvent, onJoinSession, setVisible}) => {
  const {
    day,
    start,
    end,
    ownerUser,
    coOwner,
    description,
    topics,
    title,
    suscribeUsers,
    session,
    subscribed,
    id,
    isAvailable,
    viewers,
  } = event;
  const {currentUserProfile, currentCategory} = useContext(FollowersContext);
  const [isVisible, setIsVisible] = useState(false);
  const navigation = useNavigation();

  const isOwner = useCallback(() => currentUserProfile.id === ownerUser.id, []);

  const {data: {getFollowerUsers} = {getFollowerUsers: {data: []}}} = useQuery(GET_FOLLOWERS, {
    variables: {
      paging: {
        limit: 100,
        page: 1,
      },
      userId: currentUserProfile.id,
    },
  });

  const openLandingPage = (eventItem) => {
    if (typeof openEvent === 'function') {
      openEvent(eventItem);
    }
    if (typeof openEvent === 'string') {
      navigation.navigate(openEvent, {id: eventItem.id});
    }
    if (typeof openEvent === 'undefined') {
      navigation.navigate('EventLandingView', {id: eventItem.id});
    }
  };

  const openJoinSession = (sessionItem) => {
    const useJoinSession = {
      hostName: sessionItem.ownerUser.username,
      sessionName: sessionItem.title,
      sessionDescription: sessionItem.description,
      id: sessionItem.id,
      vonageSessionId: sessionItem.vonageSessionToken,
      userId: currentUserProfile.id,
      followers: getFollowerUsers && getFollowerUsers.data,
    };
    if (typeof onJoinSession === 'function') {
      onJoinSession(sessionItem);
    }
    if (typeof onJoinSession === 'string') {
      navigation.navigate(onJoinSession, {params: useJoinSession});
    }
    if (typeof onJoinSession === 'undefined') {
      navigation.navigate('JoinSession', {params: useJoinSession});
    }
  };

  const setJoinBottomText = useCallback(() => {
    if (isOwner()) {
      return SERIES_GO_LIVE;
    }
    if (subscribed) {
      return EVENT_JOIN;
    }
    return EVENT_JOIN;
  }, [isOwner, subscribed]);

  const [joinEvent, {loading: JoinLoading}] = useMutation(JOIN_EVENT, {
    refetchQueries: [
      {query: GET_EVENT_BY_ID, variables: {id}},
      {query: GET_SERIES_SESSION},
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: LIMIT,
            page: PAGE,
          },
          category: currentCategory,
        },
      },
    ],
  });

  const [leaveEvent, {loading: LeaveLoading}] = useMutation(LEAVE_EVENT, {
    refetchQueries: [
      {query: GET_EVENT_BY_ID, variables: {id}},
      {query: GET_SERIES_SESSION},
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: LIMIT,
            page: PAGE,
          },
          category: currentCategory,
        },
      },
    ],
  });

  const handleSubscribeEvent = () => {
    if (!subscribed) {
      joinEvent({
        variables: {
          id,
        },
        onError(e) {
          Alert.alert('Error', e.message);
        },
      });
    } else {
      setIsVisible(true);
    }
  };

  const handleLeaveEvent = () => {
    leaveEvent({
      variables: {
        id,
      },
    });
    setIsVisible(false);
  };

  const handleJoinBottom = () => {
    if (session) {
      openJoinSession({...session, ownerUser});
    } else if (currentUserProfile.id === ownerUser.id) {
      navigation.reset({
        index: 0,
        routes: [{name: 'CreateSeriesSession', params: {sessionParams: {id}, experience: 'events'}}],
      });
    } else {
      handleSubscribeEvent();
    }
  };

  if (JoinLoading || LeaveLoading) {
    return (
      <WrapperLoading>
        <ActivityIndicator size="large" color={Colors.white} />
      </WrapperLoading>
    );
  }

  return (
    <Wrapper disabled={!enable} onPress={() => openLandingPage(event)} activeOpacity={0.8}>
      <PromptModal
        visible={isVisible}
        setVisible={setIsVisible}
        text="You are about to unsubscribe from this Event. Are you sure?"
        leftButtonText="Yes"
        rightButtonText="No"
        onLeftButtonPress={handleLeaveEvent}
        onRightButtonPress={() => setIsVisible(false)}
      />
      <LeftGradient>
        <LeftSide>
          {session && (
            <CenteredWrapper>
              <LinearGradient
                colors={[Colors.translucentGrey, Colors.translucentWhite]}
                style={[styles.linearGradient, styles.live]}>
                <Text>LIVE</Text>
              </LinearGradient>
              <LinearGradient
                colors={[Colors.translucentWhite, Colors.translucentWhite]}
                style={[styles.linearGradient, styles.viewers]}>
                <CenteredWrapper>
                  <Eye style={styles.eye} />
                  <Text>{viewers}</Text>
                </CenteredWrapper>
              </LinearGradient>
            </CenteredWrapper>
          )}
          <ScheduleGradiant>
            <Text>
              {`${moment(day, 'yyyy-MM-DD').format('MM/DD/yyyy')} \n${moment(start, 'HH:mm:ss').format(
                'hh:mm a',
              )} ${moment(end, 'HH:mm:ss').format('hh:mm a')}`}
            </Text>
          </ScheduleGradiant>
          <Avatar>
            <AvatarImage source={ownerUser.avatar ? {uri: ownerUser.avatar} : avatarSrc} />
          </Avatar>
          <Members
            navigation={navigation}
            owner={isOwner()}
            session={session}
            subscribed={subscribed}
            suscribeUsers={suscribeUsers}
            setVisible={setVisible}
          />
          <ManageBottoms navigation={navigation} events={event} suscribeUsers={suscribeUsers} setVisible={setVisible} />
          {suscribeUsers && (
            <RegularButton
              disabled={(JoinLoading || LeaveLoading || session ? false : subscribed) || (!isAvailable && !isOwner())}
              onPress={handleJoinBottom}
              title={`${setJoinBottomText()}`}
              style={[
                styles.regularBtn,
                session ? {} : subscribed && styles.joinButton,
                !isAvailable && !isOwner() && {backgroundColor: Colors.disabledPink},
              ]}
              loading={LeaveLoading || JoinLoading}
            />
          )}
        </LeftSide>
        <RightSide>
          <TitleEvent
            enable={enable}
            coOwner={coOwner}
            eventsName={title}
            ownerUser={ownerUser}
            suscribeUsers={suscribeUsers}
          />
          <TextDark>{description}</TextDark>
          {topics && (
            <TopicWrapper>
              {topics.slice(0, 4).map((topic) => (
                <Topic key={topic.icon}>
                  <TextPurple>{topic.icon}</TextPurple>
                  <TextPurple>{topic.name}</TextPurple>
                </Topic>
              ))}
            </TopicWrapper>
          )}
          <Menu
            enable={enable}
            isOwner={isOwner()}
            navigation={navigation}
            event={event}
            handleLeave={handleSubscribeEvent}
          />
        </RightSide>
      </LeftGradient>
    </Wrapper>
  );
};

export default EventItem;
