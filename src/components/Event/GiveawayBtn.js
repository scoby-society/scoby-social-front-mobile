import React from 'react';
import {StyleSheet, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Entypo from 'react-native-vector-icons/Entypo';

const styles = StyleSheet.create({
  linearGradient: {
    paddingHorizontal: 8,
    paddingVertical: 8,
    borderRadius: 8,
    marginTop: 6,
    marginRight: '3%',
    borderColor: '#ffffff',
    borderWidth: 1,
  },
  btn: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});

const GiveawayBtn = ({children, icon}) => (
  <LinearGradient
    start={{
      x: 0.01,
      y: 0.1,
    }}
    end={{
      x: 0.53,
      y: 0.8,
    }}
    locations={[0.1, 1, 0.6, 0.8]}
    colors={[
      'rgba(110, 103, 246, 0.76) -7.77%',
      'rgba(255, 255, 255, 0.09) 126.5%',
      'rgba(255, 255, 255, 0.09) 126.5%',
    ]}
    blurRadius="72px"
    style={styles.linearGradient}>
    <View style={styles.btn}>
      <Entypo name={icon} size={15} color="#FFFFFF" />
      {children}
    </View>
  </LinearGradient>
);

export default GiveawayBtn;
