import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
  leftGradient: {
    borderRadius: 10,
    marginLeft: -4,
    flexDirection: 'row',
  },
});

const LeftGradient = ({children}) => (
  <LinearGradient
    start={{
      x: 0.01,
      y: 0.1,
    }}
    end={{
      x: 1.1,
      y: 0.7,
    }}
    locations={[0.19, 0.7]}
    colors={['#f7941d', '#beef00']}
    style={styles.leftGradient}>
    {children}
  </LinearGradient>
);

export default LeftGradient;
