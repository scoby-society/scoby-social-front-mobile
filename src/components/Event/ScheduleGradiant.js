import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

const styles = StyleSheet.create({
  linearGradient: {
    paddingHorizontal: 6,
    paddingVertical: 4,
    borderRadius: 4,
    marginTop: 6,
    marginRight: '25%',
    width:'95%'
  },
});

const ScheduleGradiant = ({children}) => (
  <LinearGradient
    start={{
      x: 0.01,
      y: 0.1,
    }}
    end={{
      x: 0.53,
      y: 0.8,
    }}
    locations={[0.1, 1]}
    colors={['rgba(3, 204, 217, 0.2)', 'rgba(16,14,89,0.2)']}
    style={styles.linearGradient}>
    {children}
  </LinearGradient>
);

export default ScheduleGradiant;
