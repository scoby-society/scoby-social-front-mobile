import React from 'react';
import styled from 'styled-components/native';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';

const Wrapper = styled.TouchableOpacity(({size}) => ({
  borderColor: `${Colors.white}fffaa`,
  flexDirection: 'row',
  borderWidth: 1,
  width: size,
  borderRadius: 10,
  height: 35,
  paddingHorizontal: 10,
  backgroundColor: `${Colors.white}fff35`,
  justifyContent: 'center',
  alignItems: 'center',
}));

const Text = styled.Text(({color}) => ({
  ...Fonts.avenir,
  color,
}));

const DateTimeTouchable = ({DateTime, type, size = '40%', onPress = () => {}}) => (
  <Wrapper onPress={onPress} size={size}>
    {DateTime && <Text color={Colors.white}>{DateTime}</Text>}
    {!DateTime && <Text color={`${Colors.white}fffaa`}>{type === 'date' ? 'mm/dd/yy' : '00:00'}</Text>}
  </Wrapper>
);

export default DateTimeTouchable;
