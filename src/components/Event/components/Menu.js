import React, {useState} from 'react';
import {StyleSheet, ActivityIndicator} from 'react-native';
import {MenuDotsGray, CloseGray, Event} from 'assets/svg';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {useMutation} from '@apollo/client';
import {DELETE_EVENT} from 'src/graphql/mutations/event';
import {GET_USER_EVENTS} from 'src/graphql/queries/profile';
import PromptModal from 'src/components/Modal/PromptModal';
import {GET_PROJECT_BY_ID} from 'src/graphql/queries/projects';
import {HOME_PROJECT_FEED,GET_PROJECTS_BY_USER} from 'src/graphql/queries/universalSearch';
const Container = styled.View(({open}) => ({
  position: open ? 'absolute' : 'relative',
  backgroundColor: Colors.white,
  borderRadius: 6,
  borderColor: 'rgba(105, 105, 105, 0.3)',
  borderWidth: 0.9,
  paddingHorizontal: 5,
  width: '60%',
  height: 100,
  marginLeft: '40%',
  zIndex: 4000,
  paddingVertical: 10,
  marginTop: 10,
  justifyContent: 'center',
}));

const Option = styled.TouchableOpacity({
  backgroundColor: Colors.white,
  borderBottomWidth: 0.8,
  borderColor: Colors.grey,
  paddingVertical: 5,
});

const Text = styled.Text({
  ...Fonts.avenirSemiBold,
  color: '#4A2098',
  fontSize: 15,
});

const ScheduleTitle = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.purple,
  fontSize: 13,
});

const ScheduleClose = styled.TouchableOpacity({
  width: '25%',
});

const ScheduleTitleWrraper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: 5,
});

const CloseButton = styled.TouchableOpacity({
  padding: 5,
  marginTop: 10,
});

const TopContainer = styled.View(({owner}) => ({
  width: '100%',
  height: '100%',
  zIndex: 2000,
  minHeight: 40,
  justifyContent: owner ? 'space-between' : 'flex-end',
  paddingTop: 10,
  position: 'absolute',
  flexDirection: 'row',
}));

const DotsButton = styled.TouchableOpacity({padding: 5, marginTop: 10, height: 30, width: 30});

const styles = StyleSheet.create({
  serieIcon: {
    height: 32,
    width: 32,
    alignSelf: 'flex-start',
  },
});

const Menu = ({open, setOpen, event, navigation}) => {
  console.log(event?.ownerUser?.id,'IDIDIDID');
  const [visible, setVisible] = useState(false);
  const [endEvent, {loading}] = useMutation(DELETE_EVENT, {
    refetchQueries: [
      {query: GET_USER_EVENTS},
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: 100,
            page: 1,
          },
          category: "all",
        },
      },
      {
        query: GET_PROJECTS_BY_USER,
        variables: {
          paging: {
            limit: 20,
            page: 1,
          },
          category: "all",
          type: "all",
          profileId: event?.ownerUser?.id,
        },
      },
      {
        query: GET_PROJECT_BY_ID,
        variables: {
          projectId: event?.projects?.id,
        },
      },
    ],
    onCompleted() {
      navigation.goBack();
    },
  });

  const handleEdit = (eventObj) => {
    navigation.reset({
      index: 1,
      routes: [{name: 'MainTabs'}, {name: 'EditEventStack', params: {experience: 'event', event: eventObj}}],
    });
  };

  const handleDelete = async (idEvent) => {
    setVisible(false);
    endEvent({
      variables: {
        idEvent,
      },
    });
  };

  if (!open) {
    return (
      <DotsButton onPress={() => setOpen(true)}>
        <MenuDotsGray />
      </DotsButton>
    );
  }

  return loading ? (
    <Container open>
      <ActivityIndicator size="small" color={Colors.black} />
    </Container>
  ) : (
    <Container open>
      <PromptModal
        visible={visible}
        onLeftButtonPress={() => handleDelete(event.id)}
        onRightButtonPress={() => setVisible(false)}
        setVisible={setVisible}
        leftButtonText="Yes"
        rightButtonText="No"
        text="Are you sure?"
      />
      <ScheduleTitleWrraper>
        <ScheduleClose onPress={() => setOpen(false)}>
          <ScheduleTitle>{'<'}</ScheduleTitle>
        </ScheduleClose>
        <ScheduleTitle>Menu</ScheduleTitle>
      </ScheduleTitleWrraper>
      <Option onPress={() => handleEdit(event)}>
        <Text>Edit</Text>
      </Option>
      <Option onPress={() => setVisible(true)}>
        <Text>Delete</Text>
      </Option>
    </Container>
  );
};

const Header = ({isOwner, enable, event, navigation, handleLeave}) => {
  const [open, setOpen] = useState(false);

  return (
    <TopContainer owner={(!enable && isOwner) || event.subscribed}>
      <Event style={styles.serieIcon} />
      {isOwner && !enable && <Menu open={open} setOpen={setOpen} event={event} navigation={navigation} />}
      {!isOwner && !enable && event.subscribed && (
        <CloseButton onPress={handleLeave}>
          <CloseGray style={{width: 100, height: 100}} />
        </CloseButton>
      )}
    </TopContainer>
  );
};

export default Header;
