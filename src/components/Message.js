import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {StyleSheet} from 'react-native';
import RegularButton from 'src/components/RegularButton';
import {kitty} from 'src/config/api';
import {ACTIVITY_KEYS} from 'src/screens/Activity/ActivityKeys';

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Message = ({large, style, widthContainer, id}) => {
  const navigator = useNavigation();

  const goToPrivateChat = async (profile) => {
    const channel = await kitty.createChannel({type: 'DIRECT', members: [{username: id}]});
    if (profile) navigator.navigate(ACTIVITY_KEYS.PRIVATE_CHAT_KITTY, {channel: channel.channel});
  };

  return (
    <RegularButton
      style={[styles.button, style]}
      large={large}
      title="Reply"
      active={false}
      numberOfLines={1}
      widthContainer={widthContainer}
      onPress={goToPrivateChat}
    />
  );
};

export default Message;
