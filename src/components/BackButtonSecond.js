import React from "react"
import styled from 'styled-components/native';
import fonts from 'src/constants/Fonts';
const BackButtonsecond=styled.TouchableOpacity({
    width: 107,
    height: 42,

  
    alignItems:"center",
    justifyContent:"center",
    borderRadius:8
  })
  const Text = styled.Text({
    ...fonts.avenir,
    color:"rgb(255,255,255)"
      })
const BackButton=({navigation})=>{
    return(
        <>
        <BackButtonsecond onPress={()=>navigation.goBack()}>
            <Text>
                Back
            </Text>
        </BackButtonsecond>
        </>
    )
}
export default BackButton