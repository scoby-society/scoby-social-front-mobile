import {Creator, Member, Guide, Guest, Royalty} from 'assets/svg';
import React from 'react';
import {Text, StyleSheet} from 'react-native';
import styled from 'styled-components/native';
import Fonts from 'src/constants/Fonts';

const FlagStyled = styled.View`
  width: 88;
  height: 25;
  flex: 1;
  position: absolute;
  right: 78.5%;
`;

const styles = StyleSheet.create({
  text: {
    position: 'absolute',
    zIndex: 1,
    ...Fonts.avenir,
    color: 'white',
    marginTop: 2,
    marginLeft: 15,
  },
});

const Flag = ({role}) => {
  if (role === 'creator') {
    return (
      <FlagStyled>
        <Text style={styles.text}>Creator</Text>
        <Creator />
      </FlagStyled>
    );
  }

  if (role === 'member') {
    return (
      <FlagStyled>
        <Text style={styles.text}>Member</Text>
        <Member />
      </FlagStyled>
    );
  }

  if (role === 'guide') {
    return (
      <FlagStyled>
        <Text style={styles.text}>Guide</Text>
        <Guide />
      </FlagStyled>
    );
  }

  if (role === 'royalty') {
    return (
      <FlagStyled>
        <Text style={styles.text}>Royalty</Text>
        <Royalty />
      </FlagStyled>
    );
  }

  if (role === 'guest') {
    return (
      <FlagStyled>
        <Text style={styles.text}>Guest</Text>
        <Guest />
      </FlagStyled>
    );
  }
};

export default Flag;
