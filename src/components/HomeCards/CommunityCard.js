import { useNavigation } from '@react-navigation/native';
import { Tech, Finance, Business } from 'assets/svg';
import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Fonts from 'src/constants/Fonts';
import { width } from 'src/constants/Layout';
import styled from 'styled-components/native';
import Topics from '../Topics';

const Tag = styled.View((props) => ({
  width: props.w,
  height: 30,
  borderRadius: 6,
  backgroundColor: '#6536BB80',
  justifyContent: 'center',
  alignItems: 'center',
  marginBottom: 2,
}));
function CommunityCard({ item }) {

  const navigation = useNavigation()


  const goToTeamDetails = () => navigation.navigate('TeamScreen', { id: item.id });
  return (
    <View style={styles.card}>
      <View style={styles.wrapper}>
        <View style={styles.sectionOne}>
          <Image
            style={styles.avatar}
            source={{
              uri: item?.ownerUser?.avatar,
            }}
          />
          <Text style={styles.username}>{item?.ownerUser?.fullName}</Text>
          <Text style={styles.nickname}>@{item?.ownerUser?.username}</Text>
        </View>
        <View style={styles.sectionTwo}>
          <Text style={styles.title}>{item?.name}</Text>

          <Text style={styles.description}>{item?.description}</Text>
        </View>
      </View>
      <View style={styles.wrapper2}>
        <View style={styles.tagSection}>
          <Tag w={130}>
            <Text style={{ fontSize: 9,color:'#6536BB' }}>
              {item.topics[0].icon} {item.topics[0].name}
            </Text>
          </Tag>

          <View style={{ flexDirection: 'row', width: 130, flexWrap: 'wrap' }}>
            {item.topics.slice(1, 5).map(topic => {
              return (
                <Tag w={65}>
                  <Text style={{ fontSize: 8,textAlign:'center',color:'#6536BB' }}>{topic.icon}  {topic.name}</Text>
                </Tag>
              )
            })

            }

          </View>
        </View>
       <View style={styles.wrapBTN}>
       <TouchableOpacity style={styles.btn} onPress={()=>goToTeamDetails()}>
          <Text style={styles.textBTN}>Go to Community</Text>
        </TouchableOpacity>
       </View>
      </View>
     
    </View>
  );
}

const styles = StyleSheet.create({
  card: {
    width: 350,
    height: 250.31,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: 10,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: 'white',
  },
  avatar: {
    width: 70,
    height: 70,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#CD068E',
  },
  sectionOne: {
    height: 110,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginTop: 5,
  },
  username: {
    color: '#4A2098',
    ...Fonts.ttNormsBold,
  },
  nickname: {
    color: 'gray',
    ...Fonts.ttNormsBold,
  },
  wrapper: {
    flexDirection: 'row',
  },
  sectionTwo: {
    width: 248,
    marginTop: 10,
    marginLeft: 10
  },
  description: {
    color: 'gray',
    ...Fonts.avenir,
  },
  wrapBTN:{
   
    width:125,
    height:30,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    borderRadius: 6,
    marginRight: 10,
    marginTop: 50,
  },
  title: {
    color: '#484848',
    fontWeight: '700',
    ...Fonts.avenirBold,
  },
  wrapper2: {
    justifyContent: 'space-between',
   
  },
  tagSection: {
    width: 200,
    position: 'absolute',
    top: 25,
    marginLeft: 5,
  },
  btn: {
    width: 120,
    height: 26,
    backgroundColor: '#CD068E',
    borderRadius:6,
    justifyContent: 'center',
    
  },
  textBTN: {
    alignSelf: 'center',
    fontWeight: '700',
    fontSize: 12,
    color: '#ffffff',
    ...Fonts.ttNormsBold,
  },
});

export default CommunityCard;
