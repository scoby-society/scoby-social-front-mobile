import React from 'react';
import {View, Text, StyleSheet, Linking, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';
import Fonts from 'src/constants/Fonts';

const Picture = styled.Image({
  width: 130,
  height: 130,
  alignSelf: 'center',
  marginTop: 20,
  borderRadius: 15,
});

const styles = StyleSheet.create({
  card: {
    width: 350,
    height: 250.31,
    alignSelf: 'center',
    borderRadius: 14,
    marginTop: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  blurView: {
    width: '50%',
    height: '95%',
    backgroundColor: '#ffffff30',

    borderRadius: 14,
    marginTop: 5,
    marginLeft: 5,
  },
  web: {
    ...Fonts.avenir,
    alignSelf: 'center',
    textDecorationLine: 'underline',
    color: 'white',
    marginTop: 10,
    fontSize: 15,
  },
  royalty: {
    ...Fonts.avenir,
    fontWeight: '700',
    color: 'white',
    alignSelf: 'center',
    fontSize: 20,
  },
  sectwo: {
    width: '50%',
  },
  collectionTitle: {
    fontSize: 15,
    marginLeft: 10,
    marginTop: 15,
    ...Fonts.ttNormsBold,
    fontWeight: '700',
    color: 'white',
  },
  description: {
    ...Fonts.avenir,
    fontSize: 15,
    marginTop: 10,
    marginLeft: 10,
  },
  btn: {
    width: 77.8,
    height: 20.74,
    backgroundColor: 'rgba(236, 0, 140, 1)',
    alignSelf: 'center',
    borderRadius: 6,
    marginTop: 10,
  },
  textBTN: {
    color: 'white',
    alignSelf: 'center',
    fontWeight: '700',
  },
  foot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    marginTop: 10,
    marginLeft: 5,
  },
  footText: {
    ...Fonts.ttNormsBold,
    fontWeight: '700',
    color: 'white',
    alignSelf: 'center',
  },
});

function CollectionCard({item}) {
  return (
    <>
      {item !== undefined && (
        <LinearGradient
          onPress={() => {
            /* setShow(false) */
          }}
          style={styles.card}
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
          colors={['#D858BC', '#3C00FF']}>
          <View style={styles.blurView}>
            <Picture source={{uri: item.urlImage}} />

            {item.webSite !== null && (
              <TouchableOpacity onPress={() => Linking.openURL(item.webSite)}>
                <Text style={styles.web}>{item.webSite.slice(0, 39)}...</Text>
              </TouchableOpacity>
            )}
            <Text style={styles.royalty}>Royalties</Text>
          </View>

          <View style={styles.sectwo}>
            <Text style={styles.collectionTitle}>{item.name}</Text>

            <Text style={styles.description}>{item.description.slice(0, 120)}...</Text>

            <TouchableOpacity style={styles.btn}>
              <Text style={styles.textBTN}>Mint Me!</Text>
            </TouchableOpacity>

            <View style={styles.foot}>
              <View>
                <Text style={styles.footText}>Available</Text>
                <Text style={styles.footText}>7000/6500</Text>
              </View>
              <View>
                <Text style={styles.footText}>Mint Price</Text>
                <Text style={styles.footText}>1 SOL</Text>
              </View>
            </View>
          </View>
        </LinearGradient>
      )}
    </>
  );
}

export default CollectionCard;
