import {RadiobuttonChecked, RadiobuttonunChecked} from 'assets/svg';
import React from 'react';

const RadioButton = ({checked}) => {
  if (checked === true) {
    return <RadiobuttonChecked />;
  }
  return <RadiobuttonunChecked />;
};

export default RadioButton;
