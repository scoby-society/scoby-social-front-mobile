import React, {useState} from 'react';
import {StyleSheet, ActivityIndicator} from 'react-native';
import {MenuDotsGray, CloseGray, Channel} from 'assets/svg';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {useMutation} from '@apollo/client';
import PromptModal from 'src/components/Modal/PromptModal';
import {END_CHANNEL} from 'src/graphql/mutations/channel';
import {GET_CHANNELS, GET_CHANNEL_BY_ID} from 'src/graphql/queries/channel';
import {kitty} from 'src/config/api';
import {GET_PROJECT_BY_ID} from 'src/graphql/queries/projects';

const Container = styled.View(({open}) => ({
  position: open ? 'absolute' : 'relative',
  backgroundColor: Colors.white,
  borderRadius: 6,
  borderColor: 'rgba(105, 105, 105, 0.3)',
  borderWidth: 0.9,
  paddingHorizontal: 5,
  width: '60%',
  height: 100,
  marginLeft: '40%',
  zIndex: 4000,
  paddingVertical: 10,
  marginTop: 10,
  justifyContent: 'center',
}));

const Option = styled.TouchableOpacity({
  backgroundColor: Colors.white,
  borderBottomWidth: 0.8,
  borderColor: Colors.grey,
  paddingVertical: 5,
});

const Text = styled.Text({
  ...Fonts.avenirSemiBold,
  color: '#4A2098',
  fontSize: 15,
});

const ScheduleTitle = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.purple,
  fontSize: 13,
});

const ScheduleClose = styled.TouchableOpacity({
  width: '25%',
});

const ScheduleTitleWrraper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: 5,
});

const CloseButton = styled.TouchableOpacity({
  padding: 5,
  marginTop: 10,
});

const TopContainer = styled.View(({owner}) => ({
  width: '100%',
  height: '100%',
  zIndex: 2000,
  minHeight: 40,
  justifyContent: owner ? 'space-between' : 'flex-end',
  paddingTop: 10,
  position: 'absolute',
  flexDirection: 'row',
}));

const DotsButton = styled.TouchableOpacity({padding: 5, marginTop: 10, height: 30, width: 30});

const styles = StyleSheet.create({
  serieIcon: {
    height: 32,
    width: 32,
    alignSelf: 'flex-start',
  },
});

const Menu = ({open, setOpen, channel, navigation}) => {
  const [visible, setVisible] = useState(false);
  const [endchannel, {loading}] = useMutation(END_CHANNEL, {
    refetchQueries: [
      {query: GET_CHANNELS},
      {query: GET_CHANNEL_BY_ID},
      {
        query: GET_PROJECT_BY_ID,
        variables: {
          projectId: channel?.projects?.id,
        },
      },
    ],
    onCompleted: async () => {
      const channels = await kitty.getChannels({filter: {joined: true}});
      const selectChannel = channels?.paginator?.items?.filter((e) => e.properties.id === channel.id);
      kitty.deleteChannel({channel: selectChannel[0]});
      navigation.goBack();
    },
  });

  const handleEdit = (channelObj) => {
    navigation.reset({
      index: 1,
      routes: [{name: 'MainTabs'}, {name: 'EditChannelStack', params: {experience: 'channel', channel: channelObj}}],
    });
  };

  const handleDelete = async (idchannel) => {
    setVisible(false);
    endchannel({
      variables: {
        idChat: idchannel,
      },
    });
  };

  if (!open) {
    return (
      <DotsButton onPress={() => setOpen(true)}>
        <MenuDotsGray />
      </DotsButton>
    );
  }

  return loading ? (
    <Container open>
      <ActivityIndicator size="small" color={Colors.black} />
    </Container>
  ) : (
    <Container open>
      <PromptModal
        visible={visible}
        onLeftButtonPress={() => handleDelete(channel.id)}
        onRightButtonPress={() => setVisible(false)}
        setVisible={setVisible}
        leftButtonText="Yes"
        rightButtonText="No"
        text="Are you sure?"
      />
      <ScheduleTitleWrraper>
        <ScheduleClose onPress={() => setOpen(false)}>
          <ScheduleTitle>{'<'}</ScheduleTitle>
        </ScheduleClose>
        <ScheduleTitle>Menu</ScheduleTitle>
      </ScheduleTitleWrraper>
      <Option onPress={() => handleEdit(channel)}>
        <Text>Edit</Text>
      </Option>
      <Option onPress={() => setVisible(true)}>
        <Text>Delete</Text>
      </Option>
    </Container>
  );
};

const Header = ({isOwner, enable, channel, navigation, handleLeave}) => {
  const [open, setOpen] = useState(false);

  return (
    <TopContainer owner={(!enable && isOwner) || channel.subscribed}>
      <Channel style={styles.serieIcon} />
      {isOwner && !enable && <Menu open={open} setOpen={setOpen} channel={channel} navigation={navigation} />}
      {!isOwner && !enable && channel.subscribed && (
        <CloseButton onPress={handleLeave}>
          <CloseGray style={{width: 100, height: 100}} />
        </CloseButton>
      )}
    </TopContainer>
  );
};

export default Header;
