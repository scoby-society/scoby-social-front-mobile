import React from 'react';
import styled from 'styled-components';
import Fonts from 'src/constants/Fonts';
import Colors from 'src/constants/Colors';

const TitleWrapper = styled.Pressable({
  zIndex: 1,
  marginLeft: 10,
  marginTop: 40,
});
const TextBoldDark = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.titleText,
  fontSize: 17,
});
const TextDark = styled.Text({
  ...Fonts.avenir,
  color: Colors.titleText,
  fontSize: 16,
});
const TextBoldDarkSmall = styled.Text({
  ...Fonts.avenir,
  color: Colors.titleText,
  fontSize: 12,
});

const TitleChannel = ({enable, ownerUser, coOwner, channelName, suscribeUsers}) => {
  if (enable) {
    return (
      <TitleWrapper>
        <TextBoldDark>{channelName}</TextBoldDark>
        <TextDark>with {ownerUser.fullName}</TextDark>
        {coOwner && <TextDark>{`with ${coOwner.fullName}`}</TextDark>}
        {ownerUser.username && <TextBoldDark>{`@${ownerUser.username}`}</TextBoldDark>}
      </TitleWrapper>
    );
  }

  if (!suscribeUsers) {
    return (
      <TitleWrapper>
        <TextBoldDark>{channelName}</TextBoldDark>
        <TextDark>{ownerUser.fullName}</TextDark>
        {coOwner && <TextDark>{`${coOwner.fullName}`}</TextDark>}
        {ownerUser.username && <TextBoldDarkSmall>{`@${ownerUser.username}`}</TextBoldDarkSmall>}
      </TitleWrapper>
    );
  }

  return (
    <TitleWrapper>
      <TextDark>{ownerUser.fullName}</TextDark>
      {coOwner && <TextDark>{`${coOwner.fullName}`}</TextDark>}
      {ownerUser.username && <TextBoldDark>{`@${ownerUser.username}`}</TextBoldDark>}
    </TitleWrapper>
  );
};

export default TitleChannel;
