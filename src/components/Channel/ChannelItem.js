import React, {useCallback, useContext, useState} from 'react';
import {ActivityIndicator, StyleSheet, Dimensions, Alert} from 'react-native';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {CHANNEL_GO} from 'src/constants/Texts';
import {GET_SERIES_SESSION} from 'src/graphql/queries/session';
import {FollowersContext} from 'src/containers/followers';
import {useMutation} from '@apollo/client';
import {GET_EVENTS_BY_ID} from 'src/graphql/queries/events';
import {useNavigation} from '@react-navigation/native';
import {JOIN_CHANNEL, LEAVE_CHANNEL} from 'src/graphql/mutations/channel';
import {ACTIVITY_KEYS} from 'src/screens/Activity/ActivityKeys';
import {kitty} from 'src/config/api';
import Menu from './components/Menu';
import LeftGradient from './LeftGradient';
import ManageBottoms from './ManageBottom';
import RegularButton from '../RegularButton';
import Titlechannel from './TitleChannel';
import PromptModal from '../Modal/PromptModal';
import Members from './components/Members';

const {width} = Dimensions.get('window');

const Wrapper = styled.TouchableOpacity({
  width: '96%',
  minHeight: 300,
  alignSelf: 'center',
  flexDirection: 'row',
  backgroundColor: Colors.white,
  borderRadius: 10,
  marginVertical: 10,
  marginHorizontal: 10,
  zIndex: -1,
});

const Avatar = styled.View({
  width: 85,
  height: 85,
  borderRadius: 50,
  padding: 2,
  overflow: 'hidden',
  flexDirection: 'row',
  alignSelf: 'center',
  marginTop: 20,
  borderColor: Colors.purpleBorder,
  borderWidth: 3,
});

const AvatarImage = styled.Image({
  width: '100%',
  height: '100%',
  opacity: 1,
  resizeMode: 'cover',
  borderRadius: 40,
});

const TextDark = styled.Text({
  ...Fonts.avenir,
  color: Colors.titleText,
  fontSize: 13,
  marginLeft: 10,
  marginVertical: 10,
});

const LeftSide = styled.View({width: '50%', paddingVertical: 25});

const RightSide = styled.View({
  width: '50%',
  flexDirection: 'column',
  paddingVertical: 20,
  paddingHorizontal: 6,
  backgroundColor: Colors.white,
  borderTopRightRadius: 10,
  borderBottomRightRadius: 10,
  justifyContent: 'space-between',
});

const TopicWrapper = styled.View({marginLeft: 0});

const TextPurple = styled.Text({...Fonts.avenir, color: Colors.purpleBorder, fontSize: 12, marginHorizontal: 2});

const Topic = styled.View({
  paddingHorizontal: 10,
  paddingVertical: 4,
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'flex-start',
  backgroundColor: Colors.transluentPurple,
  marginVertical: 2,
  borderRadius: 6,
});

const styles = StyleSheet.create({
  serieIcon: {
    height: 32,
    width: 32,
    alignSelf: 'flex-end',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '70%',
    height: 40,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
  },
  joinButton: {
    backgroundColor: '#7A015450',
    borderColor: '#7A015450',
  },
  linearGradient: {
    paddingHorizontal: 6,
    paddingVertical: 4,
    marginHorizontal: 2,
    borderRadius: 4,
  },
  live: {paddingHorizontal: 24, paddingVertical: 2},
  viewers: {paddingHorizontal: 2, paddingVertical: 2},
  eye: {width: 18, height: 18, color: Colors.white},
});

const WrapperLoading = styled.View({
  height: 300,
  width,
  alignSelf: 'center',
  justifyContent: 'center',
  alignItems: 'center',
});

const ChannelItem = ({channel, enable, openChannel, setVisible}) => {
  const {ownerUser, coOwner, description, topics, title, suscribeUsers, subscribed, id, isAvailable, session} = channel;

  const {currentUserProfile} = useContext(FollowersContext);
  const [isVisible, setIsVisible] = useState(false);
  const navigation = useNavigation();

  const isOwner = useCallback(() => currentUserProfile.id === ownerUser.id, [currentUserProfile.id, ownerUser.id]);

  const openLandingPage = (channelItem) => {
    if (typeof openChannel === 'function') {
      openChannel(channelItem);
    }
    if (typeof openChannel === 'string') {
      navigation.navigate(openChannel, {id: channelItem.id});
    }
    if (typeof openChannel === 'undefined') {
      navigation.navigate('ChannelLandingView', {id: channelItem.id});
    }
  };

  const setJoinBottomText = useCallback(() => CHANNEL_GO, []);

  const [joinchannel, {loading: JoinLoading}] = useMutation(JOIN_CHANNEL, {
    refetchQueries: [{query: GET_EVENTS_BY_ID, variables: {id}}, {query: GET_SERIES_SESSION}],
  });

  const [leavechannel, {loading: LeaveLoading}] = useMutation(LEAVE_CHANNEL, {
    refetchQueries: [{query: GET_EVENTS_BY_ID, variables: {id}}, {query: GET_SERIES_SESSION}],
  });

  const handleSubscribechannel = () => {
    if (!subscribed) {
      joinchannel({
        variables: {
          id,
        },
        onError(e) {
          Alert.alert('Error', e.message);
        },
      });
    } else {
      setIsVisible(true);
    }
  };

  const handleLeavechannel = () => {
    leavechannel({
      variables: {
        id,
      },
    });
    setIsVisible(false);
  };

  const handleJoinBottom = async () => {
    if (isAvailable) {
      handleSubscribechannel();
    } else if (subscribed || isOwner()) {
      const channels = await kitty.getChannels({filter: {joined: true}});
      const selectChannel = channels?.paginator?.items?.filter((e) => e.properties.id === id);
      navigation.navigate(ACTIVITY_KEYS.PRIVATE_CHAT_KITTY, {channel: selectChannel[0]});
      /* navigation.reset({
        index: 0,
        routes: [{name: ACTIVITY_KEYS.PRIVATE_CHAT_KITTY, params:selectChannel[0]}],
      });  */
    }
  };

  if (JoinLoading || LeaveLoading) {
    return (
      <WrapperLoading>
        <ActivityIndicator size="large" color={Colors.white} />
      </WrapperLoading>
    );
  }

  return (
    <Wrapper disabled={!enable} onPress={() => openLandingPage(channel)} activeOpacity={0.8}>
      <PromptModal
        visible={isVisible}
        setVisible={setIsVisible}
        text="You are about to unsubscribe from this channel. Are you sure?"
        leftButtonText="Yes"
        rightButtonText="No"
        onLeftButtonPress={handleLeavechannel}
        onRightButtonPress={() => setIsVisible(false)}
      />
      <LeftGradient>
        <LeftSide>
          <Avatar>
            <AvatarImage source={ownerUser.avatar ? {uri: ownerUser.avatar} : avatarSrc} />
          </Avatar>
          <Members
            navigation={navigation}
            owner={isOwner()}
            session={session}
            subscribed={subscribed}
            suscribeUsers={suscribeUsers}
            setVisible={setVisible}
          />
          <ManageBottoms
            navigation={navigation}
            channel={channel}
            suscribeUsers={suscribeUsers}
            setVisible={setVisible}
          />
          {suscribeUsers && (
            <RegularButton
              disabled={(JoinLoading || LeaveLoading || session ? false : subscribed) || (!isAvailable && !isOwner())}
              onPress={handleJoinBottom}
              title={`${setJoinBottomText()}`}
              style={[
                styles.regularBtn,
                session ? {} : subscribed && styles.joinButton,
                !isAvailable && !isOwner() && {backgroundColor: Colors.disabledPink},
              ]}
              loading={LeaveLoading || JoinLoading}
            />
          )}
        </LeftSide>
        <RightSide>
          <Titlechannel
            enable={enable}
            coOwner={coOwner}
            channelName={title}
            ownerUser={ownerUser}
            suscribeUsers={suscribeUsers}
          />
          <TextDark>{description}</TextDark>
          {topics && (
            <TopicWrapper>
              {topics.map((topic) => (
                <Topic key={topic.icon}>
                  <TextPurple>{topic.icon}</TextPurple>
                  <TextPurple>{topic.name}</TextPurple>
                </Topic>
              ))}
            </TopicWrapper>
          )}
          <Menu
            enable={enable}
            isOwner={isOwner()}
            navigation={navigation}
            channel={channel}
            handleLeave={handleSubscribechannel}
          />
        </RightSide>
      </LeftGradient>
    </Wrapper>
  );
};

export default ChannelItem;
