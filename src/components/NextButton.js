import React from "react"
import styled from 'styled-components/native';
import fonts from 'src/constants/Fonts';
const Button = styled.TouchableOpacity({
    width: 107,
    height: 42,
    background:" #CD068E",
    alignItems:"center",
    justifyContent:"center",
    borderRadius:8,

  })
  const Text = styled.Text({
...fonts.avenir,
color:"rgb(255,255,255)"
  })
const NextButton=({disabled,onPress})=>{
    return(
        <>
        < Button 
        onPress={onPress}
        disabled={disabled}
        style={{opacity:disabled?0.4:1}}
        >
        <Text>
            Next
        </Text>
        </Button>
        </>
    )
}
export default NextButton