import React, {useState} from 'react';
import styled from 'styled-components';
import {View, StyleSheet} from 'react-native';
import {CloseWhiteIco} from 'assets/svg';
import LinearGradient from 'react-native-linear-gradient';
import {BLOCK_USER, REPORT_USER} from 'src/graphql/mutations/blockAndReport';
import {useMutation} from '@apollo/client';
import {useBlock} from 'src/hooks/blockAndReportHooks/useBlock';
import PromptModal from './Modal/PromptModal';

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: -15,
  top: -15,
});
const BlockBtn = styled.TouchableOpacity({
  position: 'absolute',
  left: 10,
  bottom: 10,
});

const ReportBtn = styled.TouchableOpacity({
  position: 'absolute',
  left: 10,
  bottom: 50,
});

const Text = styled.Text({
  color: '#fff',
});

const styles = StyleSheet.create({
  linearGradient: {
    height: '60%',
    width: '32%',
    position: 'absolute',
    right: 5,
    top: 5,
    zIndex: 2,
    borderRadius: 16,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: 10,
    borderColor: '(rgba(255, 255, 255, 0.3), rgba(255, 255, 255,8) , rgba(255, 255, 255, 0.2) )',
  },
});

const BlockReportContainer = ({show, setShow, user, isBlockedUser}) => {
  const [visible, setVisible] = useState(false);
  const [block, setBlock] = useState(false);
  const {isBlocked, blockUsers} = useBlock();

  const [reportUser] = useMutation(REPORT_USER, {
    variables: {id: user.id},
    notifyOnNetworkStatusChange: true,

    onCompleted() {
      setBlock(false);
      setShow(false);
    },
    onError() {},
  });

  const [blockUser] = useMutation(BLOCK_USER, {
    notifyOnNetworkStatusChange: true,
    variables: {id: user.id},
    onCompleted() {
      setShow(false);
      setVisible(false);
      blockUsers.push(user);
      isBlocked(user.id);
      setBlock(false);
    },
  });

  const onShowModal = (type) => {
    if (type === 'report') {
      setBlock(false);
      reportUser();
    }
    if (type === 'block') {
      setBlock(true);
    }
    setVisible(true);
  };

  return (
    <>
      {show && (
        <LinearGradient
          colors={['rgba(255, 255, 255, 0.08)', ' rgba(255, 255, 255, 0.22)']}
          style={styles.linearGradient}>
          <CloseButton onPress={() => setShow(false)}>
            <CloseWhiteIco />
          </CloseButton>
          <ReportBtn onPress={() => onShowModal('report')}>
            <Text>Report</Text>
          </ReportBtn>
          {!isBlockedUser && (
            <View
              style={{
                borderBottomColor: 'white',
                borderBottomWidth: 1,
                opacity: 0.2,
                width: 86,
                marginTop: 5,
              }}
            />
          )}
          <BlockBtn onPress={() => onShowModal('block')}>
            {!isBlockedUser && (
              <>
                <Text>Block</Text>
              </>
            )}
          </BlockBtn>
        </LinearGradient>
      )}
      <PromptModal
        visible={visible}
        setVisible={setVisible}
        block={block}
        onLeftButtonPress={() => setVisible(false)}
        rightButtonText="Block"
        text={
          block
            ? `You are about to block ${user.username}
        \n
        ${user.username} will no longer be able to follow or message you, and you will not see notifications from
        ${user.username}`
            : `Thank you for reporting ${user.username} Your report will be reviewed 
              by our Guides and they'll respond promptly.`
        }
        onRightButtonPress={() => blockUser()}
        leftButtonText="Cancel"
      />
    </>
  );
};

export default BlockReportContainer;
