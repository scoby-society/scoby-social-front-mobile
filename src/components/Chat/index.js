import React, {useEffect, useState, useContext, useMemo} from 'react';
import {View, StyleSheet, KeyboardAvoidingView, Image} from 'react-native';
import {kitty} from 'src/config/api';
import {Bubble, GiftedChat, InputToolbar} from 'react-native-gifted-chat';
import {FollowersContext} from 'src/containers/followers';
import Colors from 'src/constants/Colors';
import styled from 'styled-components/native';
import Fonts from 'src/constants/Fonts';
import moment from 'moment';
import {useMutation} from '@apollo/client';
import {LEAVE_GROUP_CHANNEL} from 'src/graphql/mutations/chat';
import {Dots} from 'assets/svg';
import {END_CHANNEL} from 'src/graphql/mutations/channel';
import {useBlock} from 'src/hooks/blockAndReportHooks/useBlock';
import BackButton from '../BackButton';
import ModalDotContainer from '../ModalDotContainer';
import withSafeAreaWithoutMenu from '../withSafeAreaWithoutMenu';

function mapUser(user) {
  return {
    _id: user.id,
    name: user.displayName,
    avatar: user.displayPictureUrl,
    idChat: user.name,
  };
}

function mapMessage(message) {
  return {
    _id: message.id,
    text: message.body,
    createdAt: new Date(message.createdTime),
    user: mapUser(message.user),
  };
}

const Text = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.white,
  fontSize: 12,
  padding: 10,
});

const TopContent = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
`;

const InfoContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
`;

const styles = StyleSheet.create({
  inputStyles: {
    backgroundColor: Colors.backgroundSearchBar,
    borderRadius: 10,
    marginLeft: 30,
    marginRight: 30,
    paddingLeft: 10,
  },
  footer: {
    paddingRight: 10,
    paddingLeft: 10,
    paddingBottom: 5,
  },
  avoidKeyboard: {flex: 1, marginBottom: 5},
  Avatar: {
    borderRadius: 40,
    height: 40,
    width: 40,
    borderWidth: 2,
  },
});

const PrivateChatKitty = ({navigation, route}) => {
  const {channel} = route.params;
  const [messages, setMessages] = useState([]);
  const [loadEarlier, setLoadEarlier] = useState(false);
  const [isLoadingEarlier, setIsLoadingEarlier] = useState(false);
  const [messagePaginator, setMessagePaginator] = useState(null);
  const [loading, setLoading] = useState(true);
  const {currentUserProfile} = useContext(FollowersContext);
  const [typing, setTyping] = useState(null);
  const [isVisible, setIsVisible] = useState(false);

  const setVisible = () => {
    setIsVisible((e) => !e);
  };

  const isDirect = useMemo(() => channel.type === 'DIRECT', [channel]);

  const setImage = useMemo(() => {
    if (isDirect)
      return channel.members.filter((e) => currentUserProfile.id !== parseInt(e.name, 10))[0].displayPictureUrl;
    if (!isDirect) return channel.properties.avatar;
  }, [channel.members, channel.properties.avatar, currentUserProfile.id, isDirect]);
  const [block, setBlock] = useState();
  const {imBlocked} = useBlock();

  const target =
    channel.members !== undefined
      ? channel.members.filter((m) => parseInt(m.name, 10) !== currentUserProfile.id)
      : [{name: ''}];

  useEffect(() => {
    if (imBlocked(target[0].name)) {
      setBlock(true);
    }
  }, [imBlocked, target]);

  const setName = useMemo(() => {
    if (isDirect) return channel.members.filter((e) => currentUserProfile.id !== parseInt(e.name, 10))[0].displayName;
    if (!isDirect) return channel.displayName;
  }, [channel.displayName, channel.members, currentUserProfile.id, isDirect]);

  const isOwner = useMemo(
    () => currentUserProfile.id === parseInt(channel.creator.name, 10),
    [currentUserProfile, channel],
  );

  const [leaveChannel] = useMutation(LEAVE_GROUP_CHANNEL, {
    variables: {
      id: channel.properties.id,
    },
  });

  const [eraseChannel] = useMutation(END_CHANNEL, {
    variables: {
      idChat: channel.properties.id,
    },
  });

  useEffect(() => {
    const startChatSessionResult = kitty.startChatSession({
      channel,

      onReceivedMessage: (message) => {
        setMessages((currentMessages) => GiftedChat.append(currentMessages, [mapMessage(message)]));
      },
      onTypingStarted: (typingUser) => {
        if (typingUser.id !== kitty.currentUser.id) {
          setTyping(typingUser);
        }
      },
      onTypingStopped: (typingUser) => {
        if (typingUser.id !== kitty.currentUser.id) {
          setTyping(null);
        }
      },
    });

    kitty
      .getMessages({
        channel,
      })
      .then((result) => {
        setMessages(result.paginator.items.map(mapMessage));
        setMessagePaginator(result.paginator);
        setLoadEarlier(result.paginator.hasNextPage);

        setLoading(false);
      });

    return startChatSessionResult.session.end;
  }, [channel]);

  async function handleLoadEarlier() {
    if (!messagePaginator.hasNextPage) {
      setLoadEarlier(false);

      return;
    }

    setIsLoadingEarlier(true);

    const nextPaginator = await messagePaginator.nextPage();

    setMessagePaginator(nextPaginator);

    setMessages((currentMessages) => GiftedChat.prepend(currentMessages, nextPaginator.items.map(mapMessage)));

    setIsLoadingEarlier(false);
  }

  async function handleSend(pendingMessages) {
    if (!block) {
      await kitty.sendMessage({
        channel,
        body: pendingMessages[0].text,
      });
    }
  }

  function renderBubble(props) {
    const id = props?.currentMessage?.user?.idChat;
    return (
      <Bubble
        {...props}
        renderCustomView={() =>
          parseInt(id, 10) !== currentUserProfile.id ? <Text>{props.currentMessage.user.name}</Text> : null
        }
        renderTime={() => <Text>{moment(props.currentMessage.createdAt).format('hh:mm a')}</Text>}
        position={parseInt(id, 10) !== currentUserProfile.id ? 'left' : 'right'}
        textStyle={{
          right: {
            color: '#fff',
          },
          left: {
            color: '#fff',
          },
        }}
        wrapperStyle={{
          left: {
            backgroundColor: 'rgba(236,0,140,1)',
          },
          right: {
            backgroundColor: 'rgba(101, 54, 187,1)',
          },
        }}
      />
    );
  }

  function handleInputTextChanged(text) {
    kitty.sendKeystrokes({
      channel,
      keys: text,
    });
  }

  function renderFooter() {
    if (typing) {
      return (
        <View style={styles.footer}>
          <Text>{typing.displayName} is typing</Text>
        </View>
      );
    }

    return null;
  }

  if (loading) {
    return (
      <View>
        <BackButton navigation={navigation} />
        <Text>Cargando...</Text>
      </View>
    );
  }

  const leaveChat = () => {
    if (!isDirect) {
      leaveChannel({
        onCompleted: () => {
          kitty.leaveChannel({channel});
          navigation.goBack();
        },
      });
    } else {
      kitty.deleteChannel({channel});
      navigation.goBack();
    }
  };

  const muteChat = () => {
    kitty.muteChannel({channel});
  };

  const eraseChat = () => {
    eraseChannel({
      onCompleted: () => {
        kitty.deleteChannel({channel});
        navigation.goBack();
      },
    });
  };

  return (
    <>
      <TopContent>
        <InfoContainer>
          <BackButton navigation={navigation} />
          <Image style={styles.Avatar} source={{uri: setImage}} />
          <Text>{setName}</Text>
        </InfoContainer>
        {!isVisible && <Dots onPress={setIsVisible} style={{marginRight: 10}} />}
      </TopContent>
      <ModalDotContainer
        show={isVisible}
        setShow={setVisible}
        isMute={channel}
        mute={muteChat}
        abandon={leaveChat}
        add={() => {}}
        erase={eraseChat}
        remove={() => {}}
        isOwner={isOwner}
        isDirect={isDirect}
      />
      <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={-800} style={styles.avoidKeyboard}>
        <GiftedChat
          messages={messages}
          // eslint-disable-next-line react/jsx-no-bind
          onSend={handleSend}
          user={mapUser(currentUserProfile)}
          // eslint-disable-next-line react/jsx-no-bind
          renderBubble={renderBubble}
          // eslint-disable-next-line react/jsx-no-bind
          renderFooter={renderFooter}
          loadEarlier={loadEarlier}
          isLoadingEarlier={isLoadingEarlier}
          // eslint-disable-next-line react/jsx-no-bind
          onLoadEarlier={handleLoadEarlier}
          // eslint-disable-next-line react/jsx-no-bind
          onInputTextChanged={handleInputTextChanged}
          renderInputToolbar={(props) => (
            <InputToolbar
              {...props}
              isKeyboardInternallyHandlsed
              multilines={false}
              containerStyle={[styles.inputStyles]}
              textInputStyle={{color: '#fff'}}
            />
          )}
        />
      </KeyboardAvoidingView>
    </>
  );
};

export default withSafeAreaWithoutMenu(PrivateChatKitty);
