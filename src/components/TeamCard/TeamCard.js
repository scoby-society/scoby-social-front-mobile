/* eslint-disable no-use-before-define */
import React, {useCallback, useContext, useMemo} from 'react';
import Colors from 'src/constants/Colors';
import Fonts, {avenirBold} from 'src/constants/Fonts';
import styled from 'styled-components/native';
import {Alert, Image, StyleSheet, Text, View} from 'react-native';
import {FollowersContext} from 'src/containers/followers';
import {useNavigation} from '@react-navigation/core';
import {buildLink} from 'src/utils/helpers';
import Share from 'react-native-share';
import {TEAM_SHARE_TEXT, UNKNOWN_ERROR_TEXT} from 'src/constants/Texts';
import Invite from '../../../assets/svg/InviteCurrColor.svg';
import ShareIcon from '../../../assets/svg/ShareCurrColor.svg';
import avatarSrc from '../../../assets/images/profile/avatarPlaceholder.png';
import TeamFormedFlag from '../../../assets/svg/TeamFormedFlag2.png';

const TeamCardWrapper = styled.View({
  backgroundColor: Colors.white,
  borderRadius: 8,
  padding: 12,
  marginTop: 20,
  position: 'relative',
});

const TeamCardBodyWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  marginBottom: 22,
});

const TeamOwnerWrapper = styled.View({
  alignItems: 'center',
  marginLeft: 6,
});

const TeamAvatarOwnerWrapper = styled.View({
  borderRadius: 50,
  borderWidth: 3,
  borderColor: Colors.newPink,
});

const TeamOwnerFullName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 17,
  fontWeight: 'bold',
  color: Colors.violetColor,
  marginTop: 6,
});

const TeamOwnerName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  color: Colors.violetColor,
  marginTop: 6,
});

const TeamAvatarOwner = styled.Image({
  width: 80,
  height: 80,
  borderRadius: 50,
  resizeMode: 'cover',
});

const TeamCardDesc = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  maxWidth: '50%',
  flexGrow: 1,
  color: Colors.black,
});

const BottomWrapper = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
});

const TopicsWrapper = styled.View({
  flexDirection: 'row',
  flexWrap: 'wrap',
  maxWidth: '50%',
});

const Topic = styled.View({
  flexDirection: 'row',
  backgroundColor: Colors.violetColorSelfOpacity,
  borderRadius: 5,
  marginHorizontal: 4,
  marginVertical: 2,
  paddingHorizontal: 6,
  paddingVertical: 6,
  minWidth: 62,
});

const TopicText = styled.Text({
  marginLeft: 2,
});

const ShareButtonsWrapper = styled.View({
  flexDirection: 'row-reverse',
  alignItems: 'flex-end',
  alignSelf: 'flex-end',
  width: '20%',
});

const ShareButton = styled.TouchableOpacity({
  width: 70,
  paddingHorizontal: 2,
  height: 32,
  borderWidth: 1,
  borderColor: Colors.shareBtnBorder,
  marginLeft: 8,
  borderRadius: 8,
  backgroundColor: Colors.shareBtnBg,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const TeamNameWrapper = styled.View({
  width: '100%',
});

const TeamNameText = styled.Text({
  ...avenirBold,
  fontSize: 14,
  color: Colors.black,
});

const ShareText = styled.Text({
  ...Fonts.avenir,
  color: Colors.shareBtnText,
  fontSize: 14,
});

export const TeamCard = ({
  name,
  ownerUser,
  members,
  teamId,
  teamType,
  membersAllowedToInvite,
  description,
  topics,
  additionalStyles = {},
  additionalData = {},
}) => {
  const {currentUserProfile} = useContext(FollowersContext);
  const navigator = useNavigation();

  const isMemberAccepted = useMemo(
    () => members?.find(({user}) => user.id === currentUserProfile?.id)?.isAccepted,
    [currentUserProfile, members],
  );

  const isOwner = currentUserProfile?.id === ownerUser?.id;
  const isMemberAllowToInvite = isOwner || (membersAllowedToInvite && isMemberAccepted);
  const shareIconSize = additionalData.shareIconSize ? additionalData.shareIconSize : 25;
  const isShowFormedFlag = additionalData.isShowFlag && isOwner;

  const goToInvite = () => navigator.navigate('InviteFollowers', {members, teamId});

  const goToTeam = () => navigator.navigate('TeamScreen', {id: teamId, teamType});
  const WideDescription = () => (
    <View style={additionalStyles.teamCardDescContainer}>
      <TeamCardDesc style={additionalStyles.teamCardDesc}>{description}</TeamCardDesc>
    </View>
  );

  const shareUrlTeam = useCallback(async () => {
    const url = await buildLink(teamId, 'team');
    if (!url) return Alert.alert(UNKNOWN_ERROR_TEXT);
    await Share.open({
      message: TEAM_SHARE_TEXT,
      url,
    });
  }, [teamId]);

  return (
    <TeamCardWrapper style={additionalStyles.teamCardWrapper} onPress={goToTeam}>
      {isShowFormedFlag && <Image source={TeamFormedFlag} style={additionalStyles.teamFlag} />}
      {additionalData.isShowTeamName && (
        <TeamNameWrapper style={additionalStyles.teamNameWrapper}>
          <TeamNameText style={additionalStyles.teamNameText}>{name}</TeamNameText>
        </TeamNameWrapper>
      )}
      <TeamCardBodyWrapper style={additionalStyles.teamCardBodyWrapper}>
        <TeamOwnerWrapper style={additionalStyles.teamOwnerWrapper}>
          <TeamAvatarOwnerWrapper style={additionalStyles.teamAvatarOwnerWrapper}>
            <TeamAvatarOwner
              source={ownerUser?.avatar ? {uri: ownerUser?.avatar} : avatarSrc}
              style={additionalStyles.teamAvatarOwner}
            />
          </TeamAvatarOwnerWrapper>
          <TeamOwnerFullName style={additionalStyles.teamOwnerFullName}>{ownerUser?.fullName}</TeamOwnerFullName>
          <TeamOwnerName style={additionalStyles.teamOwnerName}>@{ownerUser?.username}</TeamOwnerName>
        </TeamOwnerWrapper>
        {additionalData.isWideDescription ? (
          <WideDescription />
        ) : (
          <TeamCardDesc style={additionalStyles.teamCardDesc}>{description}</TeamCardDesc>
        )}
      </TeamCardBodyWrapper>
      <BottomWrapper>
        <TopicsWrapper style={additionalStyles.topicsWrapper}>
          {topics?.map(({icon, name: topicName}) => (
            <Topic style={additionalStyles.topic} key={icon}>
              <Text style={additionalData.topicIcon}>{icon}</Text>
              <TopicText style={additionalStyles.topicText}>{topicName}</TopicText>
            </Topic>
          ))}
        </TopicsWrapper>
        <ShareButtonsWrapper>
          <ShareButton onPress={shareUrlTeam} style={additionalStyles.shareButton}>
            <ShareIcon
              width={shareIconSize}
              height={shareIconSize}
              style={[styles.iconShare, additionalStyles.shareIcon]}
            />
            <ShareText style={additionalStyles.shareText}>Share</ShareText>
          </ShareButton>
          {isMemberAllowToInvite && (
            <ShareButton onPress={goToInvite} style={additionalStyles.shareButton}>
              <Invite
                width={shareIconSize}
                height={shareIconSize}
                style={[styles.iconShare, additionalStyles.shareIcon]}
              />
              <ShareText style={additionalStyles.shareText}>Invite</ShareText>
            </ShareButton>
          )}
        </ShareButtonsWrapper>
      </BottomWrapper>
    </TeamCardWrapper>
  );
};

const styles = StyleSheet.create({
  iconShare: {color: Colors.shareBtnText},
});
