/* eslint-disable no-use-before-define */
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Colors from 'src/constants/Colors';
import {avenir, avenirBold, avenirSemiBold, ttNormsRegular} from 'src/constants/Fonts';
import {useNavigation} from '@react-navigation/core';
import {TeamCard} from './TeamCard';

export const ProfileTeamCard = (props) => {
  const navigator = useNavigation();
  const goToTeam = () => navigator.navigate('TeamScreen', {id: props.teamId});
  return (
    <TouchableOpacity onPress={goToTeam}>
      <TeamCard {...props} additionalStyles={profileTeamCardStyles} additionalData={additionalData.profileTeamCard} />
    </TouchableOpacity>
  );
};

export const DefaultTeamCard = (props) => <TeamCard {...props} />;

const profileTeamCardStyles = StyleSheet.create({
  teamCardWrapper: {
    paddingLeft: 0,
    marginTop: 10,
  },
  topicsWrapper: {
    marginLeft: 5,
    marginTop: 3,
  },
  teamFlag: {
    position: 'absolute',
    right: 0,
    top: 10,
    width: 88,
    height: 25,
  },
  teamOwnerWrapper: {
    alignItems: 'center',
    marginRight: 25,
    width: 90,
    maxWidth: 90,
    marginLeft: 0,
  },
  teamNameWrapper: {
    marginBottom: 5,
    marginLeft: 10,
  },
  teamNameText: {
    ...avenirBold,
    fontSize: 16,
  },
  teamCardBodyWrapper: {
    marginBottom: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  teamAvatarOwnerWrapper: {
    borderWidth: 2,
  },
  teamAvatarOwner: {
    width: 50,
    height: 50,
  },
  teamOwnerFullName: {
    ...avenirBold,
    fontSize: 12,
    marginTop: 0,
    maxWidth: '100%',
  },
  teamOwnerName: {
    ...avenir,
    fontSize: 10,
    marginTop: 3,
    maxWidth: '100%',
  },
  teamCardDesc: {
    ...ttNormsRegular,
    fontSize: 12,
    lineHeight: 14,
    marginTop: 10,
    maxWidth: '100%',
    color: Colors.darkGrey,
  },
  teamCardDescContainer: {
    ...avenir,
    width: '67%',
  },
  topic: {
    paddingVertical: 3,
    marginBottom: 0,
  },
  topicIcon: {
    height: 15,
    width: 15,
  },
  topicText: {
    ...avenirSemiBold,
    fontSize: 12,
    color: Colors.primaryPurpleColor,
    maxWidth: '80%',
  },
  shareButton: {
    height: 26,
    width: 80,
    justifyContent: 'center',
    borderRadius: 5,
  },
  shareIcon: {
    width: 15,
    height: 15,
    marginRight: 3,
  },
});

const additionalData = {
  profileTeamCard: {
    isShowTeamName: true,
    isShowFlag: true,
    isWideDescription: true,
    shareIconSize: 20,
  },
};
