import * as React from 'react';
import {View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

import {CloseIco} from 'assets/svg';
import Colors from 'src/constants/Colors';
import {SUCCESS_MINT, ERROR_MINT} from 'src/constants/Texts';

const CloseMintMessageButton = styled.TouchableOpacity({
  justifyContent: 'center',
  alignSelf: 'flex-end',
  color: '#FFF',
});

const StyledText = styled.Text({
  maxWidth: '70%',
  fontWeight: 800,
  color: Colors.white,
  fontSize: 10,
  alignSelf: 'center',
});

const MintResultMessage = ({result, close}) => {
  const message = result === 'success' ? SUCCESS_MINT : ERROR_MINT;
  if (!result) return <></>;

  return (
    <LinearGradient
      style={{height: 50, marginTop: 20}}
      colors={[Colors.blueBackgroundSession, '#0C0C5A', Colors.blueBackgroundSession]}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
        <View style={{width: 50}} />
        <StyledText>{message}</StyledText>
        <View style={{alignSelf: 'flex-end', justifyContent: 'flex-end', alignItems: 'center'}}>
          <CloseMintMessageButton onPress={close}>
            <CloseIco />
          </CloseMintMessageButton>
        </View>
      </View>
    </LinearGradient>
  );
};

export default MintResultMessage;
