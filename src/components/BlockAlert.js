import React from 'react';
import styled from 'styled-components';
import {Text, StyleSheet} from 'react-native';
import {CloseWhiteIco} from 'assets/svg';
import {BLOCK_USER} from 'src/graphql/mutations/blockAndReport';
import {useMutation} from '@apollo/client';
import LinearGradient from 'react-native-linear-gradient';
import Colors from 'src/constants/Colors';
import {ScrollView} from 'react-native-gesture-handler';
import {useBlock} from 'src/hooks/blockAndReportHooks/useBlock';

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: -10,
  top: -10,
});

const Button = styled.TouchableOpacity({
  width: 88,
  height: 40,
  backgroundColor: Colors.pink,
  borderRadius: 20,
  justifyContent: 'center',
  alignItems: 'center',
});

const BtnWrap = styled.View({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginTop: 20,
});

const styles = StyleSheet.create({
  linearGradient: {
    width: 292,
    height: 260,
    position: 'relative',
    zIndex: 1000,
    top: '80%',
    right: 50,
    borderRadius: 16,
    padding: 50,
    color: 'white',
    borderWidth: 1,
    backgroundColor: Colors.primaryPurpleColor,
    borderColor: '#845EC9',
  },
  blurStyles: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },
  sv: {
    height: 1,
  },
});

function BlockAlert({show, setShow, user}) {
  const {isBlocked, blockUsers} = useBlock();

  const [blockUser] = useMutation(BLOCK_USER, {
    notifyOnNetworkStatusChange: true,
    variables: {id: user.id},
    onCompleted() {
      setShow(false);
      blockUsers.push(user);
      isBlocked(user.id);
    },
  });

  return (
    <>
      {show && (
        <>
          <LinearGradient
            colors={['rgba(255, 255, 255, 0.08)', ' rgba(255, 255, 255, 0.22)']}
            style={styles.linearGradient}>
            <CloseButton onPress={() => setShow(false)}>
              <CloseWhiteIco />
            </CloseButton>

            <ScrollView style={styles.sv}>
              <Text style={{color: 'white'}}>
                You are about to block {user.username}
                {'\n\n '}
                {user.username} will no longer be able to follow or message you, and you will not see notifications from
                {` ${user.username}`}
              </Text>
            </ScrollView>

            <BtnWrap>
              <Button onPress={() => blockUser()}>
                <Text>Block</Text>
              </Button>

              <Button onPress={() => setShow(false)}>
                <Text>Cancel</Text>
              </Button>
            </BtnWrap>
          </LinearGradient>
        </>
      )}
    </>
  );
}

export default BlockAlert;
