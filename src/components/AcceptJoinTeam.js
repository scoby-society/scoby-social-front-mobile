/* eslint-disable no-use-before-define */
import {useQuery, useMutation} from '@apollo/client';
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {GET_TEAM} from 'src/graphql/queries/team';
import {APPROVE_JOIN_REQUEST} from 'src/graphql/mutations/team';
import {checkIsUserJoined} from 'src/utils/activity';
import {ACCEPT, ACCEPTED} from 'src/constants/Texts';
import RegularButton from './RegularButton';

export const AllowJoinTeam = ({large, style, widthContainer, userId, teamId}) => {
  const [isJoined, setIsJoined] = useState(false);

  const [approveJoinRequest, {loading: loadingApprove}] = useMutation(APPROVE_JOIN_REQUEST, {
    variables: {
      applicantId: userId,
      teamId: Number(teamId),
    },
    refetchQueries: [
      {
        query: GET_TEAM,
        variables: {
          teamId,
        },
      },
    ],
  });

  const {data: {getTeam} = {getTeam: {data: []}}, loading} = useQuery(GET_TEAM, {
    fetchPolicy: 'network-only',
    variables: {
      teamId,
    },
  });

  const checkIsJoined = () => {
    if (getTeam.members && userId) {
      return checkIsUserJoined(getTeam.members, userId);
    }
  };

  useEffect(() => {
    setIsJoined(checkIsJoined);
  }, [getTeam, userId]);

  return (
    <RegularButton
      large={large}
      title={isJoined ? ACCEPTED : ACCEPT}
      loading={loading || loadingApprove}
      numberOfLines={1}
      widthContainer={widthContainer}
      onPress={() => approveJoinRequest()}
      style={[styles.button, style]}
      active={isJoined}
      disabled={isJoined}
    />
  );
};

export default AllowJoinTeam;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
