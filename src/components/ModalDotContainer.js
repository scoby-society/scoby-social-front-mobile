import React from 'react';
import styled from 'styled-components';
import {StyleSheet} from 'react-native';
import {CloseWhiteIco} from 'assets/svg';
import LinearGradient from 'react-native-linear-gradient';

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: -15,
  top: -15,
});
const BlockBtn = styled.TouchableOpacity({
  position: 'absolute',
  left: 10,
  bottom: 10,
});

const ReportBtn = styled.TouchableOpacity({
  position: 'absolute',
  left: 10,
  bottom: 50,
});

const Btn = styled.TouchableOpacity({
  position: 'absolute',
  left: 10,
  bottom: 90,
});

const Text = styled.Text({
  color: '#fff',
});

const styles = StyleSheet.create({
  linearGradient: {
    height: '15%',
    width: '32%',
    position: 'absolute',
    right: 5,
    top: 5,
    zIndex: 2,
    borderRadius: 16,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingLeft: 10,
    borderColor: '(rgba(255, 255, 255, 0.3), rgba(255, 255, 255,8) , rgba(255, 255, 255, 0.2) )',
  },
});

const ModalDotContainer = ({show, setShow, mute, abandon, isOwner, add, remove, erase, isDirect}) => (
  <>
    {show && (
      <LinearGradient
        colors={['rgba(255, 255, 255, 0.08)', ' rgba(255, 255, 255, 0.22)']}
        style={styles.linearGradient}>
        <CloseButton onPress={setShow}>
          <CloseWhiteIco />
        </CloseButton>
        {!isOwner && (
          <>
            <ReportBtn onPress={mute}>
              <Text>Mute</Text>
            </ReportBtn>
            <Btn onPress={abandon}>
              <Text>Abandon</Text>
            </Btn>
          </>
        )}
        {isOwner && !isDirect && (
          <>
            <ReportBtn onPress={add}>
              <Text>add</Text>
            </ReportBtn>
            <BlockBtn onPress={remove}>
              <Text>Remove</Text>
            </BlockBtn>
            <Btn onPress={erase}>
              <Text>Delete</Text>
            </Btn>
          </>
        )}
        {isOwner && isDirect && (
          <>
            <ReportBtn onPress={mute}>
              <Text>Mute</Text>
            </ReportBtn>
            <Btn onPress={abandon}>
              <Text>Abandon</Text>
            </Btn>
          </>
        )}
      </LinearGradient>
    )}
  </>
);

export default ModalDotContainer;
