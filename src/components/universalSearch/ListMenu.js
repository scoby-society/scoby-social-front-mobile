import {CloseGray} from 'assets/svg';
import React, {useState} from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components';

const options = ['Recommended', 'Trending', 'Favorites', 'Upcoming'];

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: 5,
  top: 10,
  zIndex: 1,
});

const styles = StyleSheet.create({
  btn: {
    width: 143,
    height: 33,
    borderRadius: 17,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
  },
  list: {
    backgroundColor: 'rgba(255, 255, 255, 0.86)',
    width: 145,
    height: 122,
    top: -35,
    borderRadius: 10,
    paddingLeft: 15,
    position: 'relative',
  },
  color: {
    color: 'background: rgba(101, 54, 187, 1)',
    marginLeft: 10,
    position: 'relative',
  },
  textSelected: {
    color: 'background: rgba(101, 54, 187, 1)',
    top: 10,
    right: -10,
    fontWeight: '700',
  },
  wrap: {
    borderBottomWidth: 2,
    borderBottomColor: 'background: rgba(101, 54, 187, 0.3)',
    top: 20,
    height: 25,
    width: '90%',
  },
  wrap2: {
    top: 20,
    height: 25,
    width: '90%',
  },
});

function ListMenu({showList, setShowList}) {
  const [optionSelected, setOptionSelected] = useState('Recommended');

  return (
    <View>
      <TouchableOpacity onPress={() => setShowList(true)}>
        <LinearGradient
          style={styles.btn}
          colors={['rgba(110, 103, 246, 0.76)', 'rgba(255, 255, 255, 0.09)', 'rgba(135, 130, 235, 0.41)']}
          start={{x: 0, y: 0.5}}
          end={{x: 1, y: 0}}>
          <Text style={{color: 'white'}}>{optionSelected}</Text>
        </LinearGradient>

        {showList && (
          <View style={styles.list}>
            <CloseButton onPress={() => setShowList(false)}>
              <CloseGray style={{color: 'black'}} />
            </CloseButton>
            <Text style={styles.textSelected}>{optionSelected}</Text>

            {options
              .filter((op) => op !== optionSelected)
              .map((op, i) => (
                <TouchableOpacity
                  style={i === 2 ? styles.wrap2 : styles.wrap}
                  key={`${Math.random() + i}`}
                  onPress={() => setOptionSelected(op)}>
                  <Text style={styles.color}>{op}</Text>
                </TouchableOpacity>
              ))}
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
}

export default ListMenu;
