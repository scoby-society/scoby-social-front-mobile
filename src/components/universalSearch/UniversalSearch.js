import React, {useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Fonts from 'src/constants/Fonts';
import SearchInput from '../SearchInput';

const styles = StyleSheet.create({
  mainWrap: {
    height: 130,

    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
  },
  title: {
    ...Fonts.goudy,
    fontSize: 30,
    color: 'white',
    marginLeft: 10,
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    zIndex: 10,
  },
  input: {
    zIndex: -100,
    width: '100%',
    position: 'relative',
    marginLeft: 0,
  },
  input2: {
    zIndex: -100,
    width: '100%',
    position: 'absolute',
    marginLeft: 5,
    marginTop: 65,
  },
});

const UniversalSearch = ({setSearchWord, searchWord}) => {
  const [showList] = useState(false);

  return (
    <View style={styles.mainWrap}>
      <View style={styles.top}>
        <Text style={styles.title}>Projects</Text>
      </View>
      <SearchInput
        value={searchWord}
        onChangeText={(word) => {
          setSearchWord(word);
        }}
        style={!showList ? styles.input : styles.input2}
        type="universal"
        color="white"
        autoCorrect={false}
      />
    </View>
  );
};

export default UniversalSearch;
