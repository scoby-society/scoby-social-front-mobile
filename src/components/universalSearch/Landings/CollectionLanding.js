import {useLazyQuery} from '@apollo/client';
import React, {useCallback, useEffect, useState} from 'react';
import Sessions from 'src/components/HomeExperiences';
import {SEARCH_COLLECTIONS} from 'src/graphql/queries/universalSearch';
import {LIMIT, PAGE} from 'src/utils/pagination';

function CollectionLanding({navigation, searchWord}) {
  const [items, setItems] = useState([]);
  const [refetchCollection] = useLazyQuery(SEARCH_COLLECTIONS);

  const COLLECTION = useCallback(async () => {
    await refetchCollection({
      fetchPolicy: 'network-only',
      variables: {
        query: searchWord,
        paging: {
          limit: LIMIT,
          page: PAGE,
        },
      },
      onCompleted(data) {
        if (data.getSearchCollection.length > 0 && searchWord.length > 0) {
          setItems(data.getSearchCollection);
        }
      },
    });
  }, [refetchCollection, searchWord, setItems]);

  useEffect(() => {
    if (searchWord.length === 0) {
      setItems([]);
    } else {
      COLLECTION();
    }
  }, [COLLECTION, searchWord]);

  return (
    <Sessions
      navigation={navigation}
      category="collection"
      searchWord={searchWord}
      setItems={setItems}
      itemsSearch={items}
    />
  );
}

export default CollectionLanding;
