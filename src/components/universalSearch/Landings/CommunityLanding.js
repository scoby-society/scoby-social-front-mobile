import { useLazyQuery } from '@apollo/client';
import React,{useCallback, useEffect,useState} from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import Sessions from 'src/components/HomeExperiences';
import { SEARCH_EVENT, SEARCH_COLLECTIONS, SEARCH_SERIE, SEARCH_COMMUNITIES } from 'src/graphql/queries/universalSearch';
import { LIMIT, PAGE } from 'src/utils/pagination';

function CommunityLanding({navigation,currentCategory,searchWord,setItem,itemsSearch,index}) {
    const [items,setItems]=useState([])
    const [refetchCommunities] = useLazyQuery(SEARCH_COMMUNITIES);

    const COMMUNITY = useCallback(async () => {
        await refetchCommunities({
          fetchPolicy: 'network-only',
          variables: {
            query: searchWord,
            paging: {
              limit: LIMIT,
              page: PAGE,
            },
          },
          onCompleted(data) {
            if (data.getSearchTeam.length > 0 && searchWord.length > 0) {
              setItems(data.getSearchTeam);
            }
          },
        });
    
        return true;
      }, [refetchCommunities, searchWord, setItem]);

      useEffect(() => {
        if (searchWord.length === 0) {
          setItem([]);
        }else{
          
          COMMUNITY();
        }
      }, [ COMMUNITY,searchWord]);


    return (
        <Sessions
                navigation={navigation}
                category={'comunity'}
                searchWord={searchWord}
                setItems={setItems}
                itemsSearch={items}
            />
    );
}

export default CommunityLanding;