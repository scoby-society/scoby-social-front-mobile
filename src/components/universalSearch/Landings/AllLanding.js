import React, {useCallback, useEffect, useState} from 'react';
import Sessions from 'src/components/HomeExperiences';
import {useLazyQuery} from '@apollo/client';
import {
  SEARCH_EVENT,
  SEARCH_COLLECTIONS,
  SEARCH_SERIE,
  SEARCH_COMMUNITIES,
  SEARCH_SESSIONS,
} from 'src/graphql/queries/universalSearch';
import {LIMIT, PAGE} from 'src/utils/pagination';

function AllLanding({navigation, searchWord, setItem}) {
  const [items, setItems] = useState([]);
  const [refetchSeries] = useLazyQuery(SEARCH_SERIE);
  const [refetchEvent] = useLazyQuery(SEARCH_EVENT);
  const [refetchCommunities] = useLazyQuery(SEARCH_COMMUNITIES);
  const [refetchCollection] = useLazyQuery(SEARCH_COLLECTIONS);
  const [refetchSession] = useLazyQuery(SEARCH_SESSIONS);

  const COLLECTION = useCallback(async () => {
    await refetchCollection({
      fetchPolicy: 'network-only',
      variables: {
        query: searchWord,
        paging: {
          limit: LIMIT,
          page: PAGE,
        },
      },
      onCompleted(data) {
        if (data.getSearchCollection.length > 0 && searchWord.length > 0) {
          setItems(data.getSearchCollection);
        }
      },
    });
  }, [refetchCollection, searchWord, setItem]);

  const SESSION = useCallback(async () => {
    await refetchSession({
      fetchPolicy: 'network-only',
      variables: {
        query: searchWord,
      },
      onCompleted(data) {
        if (data.getSearchSession.length > 0 && searchWord.length > 0) {
          setItems(data.getSearchSession);
        }
      },
    });

    return true;
  }, [refetchEvent, searchWord, setItem]);

  const COMMUNITY = useCallback(async () => {
    await refetchCommunities({
      fetchPolicy: 'network-only',
      variables: {
        query: searchWord,
        paging: {
          limit: LIMIT,
          page: PAGE,
        },
      },
      onCompleted(data) {
        if (data.getSearchTeam.length > 0 && searchWord.length > 0) {
          setItems(data.getSearchTeam);
        }
      },
    });

    return true;
  }, [refetchCommunities, searchWord, setItem]);

  const EVENT = useCallback(async () => {
    await refetchEvent({
      fetchPolicy: 'network-only',
      variables: {
        query: searchWord,
      },
      onCompleted(data) {
        if (data.getSearchEvent.length > 0 && searchWord.length > 0) {
          setItems(data.getSearchEvent);
        }
      },
    });

    return true;
  }, [refetchEvent, searchWord, setItem]);

  const SERIES = useCallback(async () => {
    await refetchSeries({
      fetchPolicy: 'network-only',
      variables: {
        query: searchWord,
      },
      onCompleted(data) {
        if (data.getSearchSerie.length > 0 && searchWord.length > 0) {
          setItems(data.getSearchSerie);
        }
      },
    });

    return true;
  }, [refetchSeries, searchWord, setItem]);

  useEffect(() => {
    if (searchWord.length === 0 || searchWord.length === 1) {
      setItem([]);
    } else {
      SESSION();
      COLLECTION();
      SERIES();
      EVENT();
      COMMUNITY();
    }
  }, [COLLECTION, COMMUNITY, EVENT, SERIES, searchWord, setItem]);

  return (
    <Sessions navigation={navigation} category="all" searchWord={searchWord} setItems={setItems} itemsSearch={items} />
  );
}

export default AllLanding;
