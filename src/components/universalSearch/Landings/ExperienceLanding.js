import React,{useCallback, useEffect,useState} from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import Sessions from 'src/components/HomeExperiences';
import { useLazyQuery } from '@apollo/client';
import { SEARCH_EVENT, SEARCH_COLLECTIONS, SEARCH_SERIE, SEARCH_SESSIONS } from 'src/graphql/queries/universalSearch';
import { LIMIT, PAGE } from 'src/utils/pagination';
function ExperienceLanding({ navigation, currentCategory, searchWord, setItem, itemsSearch }) {
    const [items, setItems] = useState([])
    const [refetchSeries] = useLazyQuery(SEARCH_SERIE);
    const [refetchEvent] = useLazyQuery(SEARCH_EVENT);
    const [refetchSession] = useLazyQuery(SEARCH_SESSIONS);

    const EVENT = useCallback(async () => {
        await refetchEvent({
            fetchPolicy: 'network-only',
            variables: {
                query: searchWord,
            },
            onCompleted(data) {
                if (data.getSearchEvent.length > 0 && searchWord.length > 0) {
                    setItems(data.getSearchEvent);
                }
            },
        });

        return true;
    }, [refetchEvent, searchWord, setItem]);

    const SESSION= useCallback(async () => {
        await refetchSession({
            fetchPolicy: 'network-only',
            variables: {
                query: searchWord,
            },
            onCompleted(data) {
                if (data.getSearchSession.length > 0 && searchWord.length > 0) {
                    setItems(data.getSearchSession);
                }
            },
        });

        return true;
    }, [refetchEvent, searchWord, setItem]);

    const SERIES = useCallback(async () => {
        await refetchSeries({
            fetchPolicy: 'network-only',
            variables: {
                query: searchWord,
            },
            onCompleted(data) {
                if (data.getSearchSerie.length > 0 && searchWord.length > 0) {
                    setItems(data.getSearchSerie);
                }
            },
        });

        return true;
    }, [refetchSeries, searchWord, setItem]);

    useEffect(() => {
        if (searchWord.length === 0) {
            setItem([]);
        } else {

            SERIES();
            EVENT();
            SESSION()

        }
    }, [EVENT, SERIES,SESSION, searchWord]);

    return (
        <Sessions
            navigation={navigation}
            category={'experience'}
            searchWord={searchWord}
            setItems={setItems}
            itemsSearch={items}
        />
    );
}

export default ExperienceLanding;