import React from 'react';
import {Text, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import Fonts from 'src/constants/Fonts';

const options = [
  {key: 'All', body: 'all'},
  {key: 'Collections', body: 'collection'},
  {key: 'Experiences', body: 'experience'},
  {key: 'Communities', body: 'comunity'},
];

const styles = StyleSheet.create({
  menu: {
    flexDirection: 'row',
    flex: 1,
    width: '110%',
    position: 'absolute',
    bottom: 0,
    paddingLeft:'7%'
   
  },
  item: {
    fontFamily: Fonts.avenir.fontFamily,
    fontSize: 14,
    fontWeight: '700',
   
    color: 'background: rgba(255, 255, 255, 0.5)',
  },
  itemSelected: {
    fontFamily: Fonts.avenir.fontFamily,
    fontSize: 14,
    fontWeight: '700',
    color: 'background: rgba(255, 255, 255, 1)',
  },
  wrap: {
    width: 'auto',
    paddingHorizontal: 10,
    
  },
  wrapActive: {
    borderBottomColor: '#ffffff',
    borderBottomWidth: 1,
    width: 'auto',
    paddingHorizontal: 10,
    paddingBottom: 1,
  },
});

function BottomMenu({setCurrentCategory, itemSelected}) {
  return (
    <ScrollView
      style={styles.menu}
      contentContainerStyle={{justifyContent: 'center', alignItems: 'center'}}
      horizontal
      showsHorizontalScrollIndicator={false}>
      {options.map((op, index) => (
        <TouchableOpacity
          key={`${Math.random() + index}`}
          style={itemSelected.includes(op.body) ? styles.wrapActive : styles.wrap}
          onPress={() => setCurrentCategory(op.body)}>
          <Text style={itemSelected.includes(op.body) ? styles.itemSelected : styles.item}>{op.key}</Text>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}

export default BottomMenu;
