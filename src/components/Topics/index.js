import React, {useRef, useEffect, useState, useCallback, useContext} from 'react';
import {StyleSheet, Image, ActivityIndicator} from 'react-native';
import {useQuery} from '@apollo/client';
import styled from 'styled-components/native';
import logo from 'assets/images/logos/Scoby_Final_Logos/ScobyDude_preferred_logo/scoby_dude.png';
import {GET_TOPICS, GET_USER_PROFILE} from 'src/graphql/queries/profile';
import colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import ModalHeader from 'src/screens/Profile/components/ModalHeader';
import {GlobalContext} from 'src/containers/global';
import {TOPICS_TEXT, TOPICS_TITTLE} from 'src/constants/Texts';
import Steps from 'src/components/Steps';
import CheckboxForm from './newCheckboxForm';
import NewLargeButton from '../NewLargeButton';
import NextButton from '../NextButton';
import BackButton from '../BackButtonSecond';
import withSafeAreaWithoutMenu from '../withSafeAreaWithoutMenu';

const Container = styled.View({
  flex: 1,
  backgroundColor: `${colors.blueBackgroundSession}`,
});

const LoadingContainer = styled.View({
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
});

const HeaderContainer = styled.View({
  width: '100%',
  backgroundColor: colors.blueBackgroundSession,
  marginHorizontal: 24,
});

const ScrollView = styled.ScrollView({
  flex: 1,
  marginHorizontal: 24,
});
const ContainerStep = styled.View({
  flexDirection: 'row',
  width: '80%',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
});
const TextDesc = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 16,
  color: 'white',
  width: '100%',
  marginBottom: '8px',
  lineHeight: '22px',
});

const TextTitle = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  color: colors.white,
  lineHeight: '32px',
  marginBottom: 4,
});

const ButtonWrapper = styled.View({
  flexDirection: 'row',
  marginBottom: 60,
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: '0px 60px',
  display: 'flex',
  width: '100%',
});

const styles = StyleSheet.create({
  logo: {
    width: '20%',
    height: '20%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  scroll: {
    paddingBottom: 12,
  },
});

function Topics({
  value,
  description,
  title,
  onBoarding,
  save,
  profile,
  showTitle,
  navigation,
  showSubtitle,
  signUp = false,
  showLogo,
  saving = false,
}) {
  const [topic, setTopic] = useState('');
  const [topicCurrent, setTopicCurrent] = useState('[]');
  const [disableTopic, setDisableTopic] = useState(true);
  const [isDisabledDone, setDisableDone] = useState(true);
  const topicsRef = useRef(null);
  const user = useQuery(GET_USER_PROFILE);
  const {data, loading, error} = useQuery(GET_TOPICS);
  // const ContextoCreateUsuario = React.createContext();
  const {setTopics} = useContext(GlobalContext);

  const handleSubmit = useCallback(() => {
    if (!isDisabledDone) {
      const currentTopics = topicsRef.current.getTopics();
      save(currentTopics);
    }
  }, [isDisabledDone]);

  const next = useCallback(() => {
    const currentTopics = topicsRef.current.getTopics();
    setTopics(currentTopics);
    save(currentTopics);
  }, [save]);

  useEffect(() => {
    if (value) {
      const onlyIdArr = value.map((i) => i.id);
      setTopic(JSON.stringify(onlyIdArr));
    }
  }, [value]);

  useEffect(() => {
    if (topic === topicCurrent) {
      setDisableDone(true);
    } else if (topicCurrent === '[]') {
      setDisableDone(true);
    } else {
      setDisableDone(false);
    }
  }, [topicCurrent, topic]);

  if (loading) {
    return (
      <LoadingContainer>
        <ActivityIndicator color={colors.white} size="large" />
      </LoadingContainer>
    );
  }

  if (error || !data) {
    return <></>;
  }

  const ButtomContainer = () => (
    <ButtonWrapper>
      <NewLargeButton disabled={disableTopic || saving} loading={saving} onPress={next} title="NEXT" />
    </ButtonWrapper>
  );

  return (
    <Container>
      {showLogo && <Image source={logo} style={styles.logo} />}
      {profile ? (
        <ModalHeader title={title} onPressDone={handleSubmit} disabled={isDisabledDone} />
      ) : (
        <HeaderContainer>
          {showTitle && !onBoarding && <TextTitle>{title || 'Select topics'}</TextTitle>}
          {onBoarding && <TextTitle>{TOPICS_TITTLE}</TextTitle>}
          {showSubtitle && !onBoarding && (
            <TextDesc>
              {description ||
                'Help us make your Scoby experience better! \nPick interesting topics and our AI will do the rest.'}
            </TextDesc>
          )}
          {onBoarding && (
            <ContainerStep>
              <TextDesc>{TOPICS_TEXT}</TextDesc>
              <Steps FirstValue={2} SecondValue={7} />
            </ContainerStep>
          )}
        </HeaderContainer>
      )}
      <ScrollView scrollEventThrottle={16} contentContainerStyle={styles.scroll}>
        {data ? (
          <CheckboxForm
            signUp={signUp}
            setTopicCurrent={setTopicCurrent}
            ref={topicsRef}
            data={data}
            user={user}
            setDisableTopic={setDisableTopic}
            background_color={colors.blueBackgroundSession}
            textBoxColor={colors.white}
          />
        ) : null}
      </ScrollView>
      {!profile ? !onBoarding && ButtomContainer() : null}
      {onBoarding && (
        <ButtonWrapper>
          <BackButton navigation={navigation} />
          <NextButton
            onPress={() => {
              if (typeof save === 'function') {
                save();
              } else {
                navigation.navigate('UploadIconCover');
              }
            }}
            disabled={disableTopic || saving}
          />
        </ButtonWrapper>
      )}
    </Container>
  );
}

export default withSafeAreaWithoutMenu(Topics);
