import React from 'react';
import styled from 'styled-components/native';
import Fonts, {avenir} from 'src/constants/Fonts';
import InviteFollowersTab from 'src/components/InviteFollowers/InviteFollowersTab';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import colors from 'src/constants/Colors';
import {FOLLOWERS, TEAMS} from 'src/constants/Texts';
import InviteTeamsTab from 'src/components/InviteFollowers/InviteTeamsTab';

const Title = styled.Text({
  ...Fonts.avenir,
  fontSize: 18,
  color: colors.white,
  paddingVertical: 12,
});

const PaddingWrapper = styled.View({
  paddingHorizontal: 24,
});

const Tabs = createMaterialTopTabNavigator();

const tabStyle = () => ({
  tabBarStyle: {
    backgroundColor: colors.blueBackgroundSession,
  },
  tabBarLabelStyle: {
    ...avenir,
    fontSize: 14,
    textTransform: 'capitalize',
  },
  tabBarIndicatorStyle: {
    backgroundColor: colors.white,
  },
  tabBarIndicatorContainerStyle: {
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 1,
  },
  tabBarPressColor: colors.darkBlue,
  tabBarInactiveTintColor: colors.borderGrey,
  tabBarActiveTintColor: colors.white,
});

const InviteFollowers = ({
  subTitle,
  getFollowerUsers,
  UsersTeamsToInviteNft,
  setUsersTeamsToInviteNft,
  nftsRequired,
  selectedFollowers,
  setSelectedFollowers,
  selectedTeamsFollowers,
  setSelectedTeamsFollowers,
  isAllSelectedTeams,
  setIsAllSelectedTeams,
  avatarBorderColor,
  followersQuery,
  setFollowersQuery,
  setQuery,
  query
  // navigation
}) => (
  <>
    <PaddingWrapper>
      <Title>{subTitle}</Title>
    </PaddingWrapper>
    <Tabs.Navigator initialRouteName="followers" screenOptions={tabStyle}>
      <Tabs.Screen
        name="followers"
        options={{
          tabBarLabel: FOLLOWERS,
        }}>
        {({navigation}) => (
          <PaddingWrapper>
            <InviteFollowersTab
              navigation={navigation}
              getFollowerUsers={getFollowerUsers}
              selectedFollowers={selectedFollowers}
              setSelectedFollowers={setSelectedFollowers}
              avatarBorderColor={avatarBorderColor}
              followersQuery={followersQuery}
              setFollowersQuery={setFollowersQuery}
            />
          </PaddingWrapper>
        )}
      </Tabs.Screen>
      <Tabs.Screen
        name="teams"
        options={{
          tabBarLabel: TEAMS,
        }}>
        {() => (
          <PaddingWrapper>
            <InviteTeamsTab
              setQuery={setQuery}
              query={query}
              nftsRequired={nftsRequired}
              UsersTeamsToInviteNft={UsersTeamsToInviteNft}
              setUsersTeamsToInviteNft={setUsersTeamsToInviteNft}
              selectedTeamsFollowers={selectedTeamsFollowers}
              setSelectedTeamsFollowers={setSelectedTeamsFollowers}
              isAllSelectedTeams={isAllSelectedTeams}
              setIsAllSelectedTeams={setIsAllSelectedTeams}
              avatarBorderColor={avatarBorderColor}
            />
          </PaddingWrapper>
        )}
      </Tabs.Screen>
    </Tabs.Navigator>
  </>
);

export default InviteFollowers;
