import React, {useCallback, useEffect, useState} from 'react';
import SearchInput from 'src/components/SearchInput';
import {
  ALL_FOLLOWERS,
  NEW_SESSION_INVITE_FOLLOWERS_SELECT,
  NO_FOLLOWERS_1,
  NO_FOLLOWERS_2,
  NO_FOLLOWERS_3,
  NO_FOLLOWERS_4,
  NO_FOLLOWERS_INVITE,
  SUBTITLE_SHARE_SESSION,
} from 'src/constants/Texts';
import colors from 'src/constants/Colors';
import {TouchableOpacity} from 'react-native';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {CheckIco} from 'assets/ico';
import {
  Avatar,
  ButtonConfirm,
  EmptyFollowersContainer,
  EmptyFollowersText,
  EmptyTitle,
  FollowersContainer,
  ListSubText,
  ListText,
  SearchPanelLink,
  SuggestedText,
  Username,
  Wrapper,
} from 'src/components/InviteFollowers/InviteFollowers.styled';
import {InviteAll} from 'src/components/InviteFollowers/components/InviteAllElement';

const InviteFollowersTab = ({selectedFollowers, getFollowerUsers, setSelectedFollowers, avatarBorderColor}) => {
  const [allFollowersCheckBox, setAllFollowersCheckBox] = useState(false);
  const [followersQuery, setFollowersQuery] = useState('');

  const selectFollowers = (idFollower) => {
    let followersArray = [...selectedFollowers];

    if (selectedFollowers.includes(idFollower)) {
      followersArray = followersArray.filter((i) => i !== idFollower);
    } else {
      followersArray.push(idFollower);
    }

    setSelectedFollowers(followersArray);
  };

  const selectAllFollowers = useCallback(() => {
    setAllFollowersCheckBox(!allFollowersCheckBox);
    const followersArray = [];

    if (!allFollowersCheckBox) {
      getFollowerUsers.data.forEach((item) => followersArray.push(item.id));
    }

    setSelectedFollowers(followersArray);
  }, [allFollowersCheckBox, getFollowerUsers, setSelectedFollowers, setAllFollowersCheckBox]);

  const arrayImgAvatar = [];
  const [data, setData] = useState([]);

  if (getFollowerUsers && getFollowerUsers.data && getFollowerUsers.data.length > 0) {
    getFollowerUsers.data.forEach((item) => arrayImgAvatar.push(item));
  } else if (getFollowerUsers.data.length !== 0) {
    getFollowerUsers.forEach((item) => arrayImgAvatar.push(item));
  }
  useEffect(() => {
    const checkAllFollowersSelect = () => {
      const followersIdArray = getFollowerUsers.data.reduce((totalArray, item) => {
        totalArray.push(item.id);
        return totalArray;
      }, []);

      if (followersIdArray.every((id) => selectedFollowers.includes(id))) {
        setAllFollowersCheckBox(true);
      } else {
        setAllFollowersCheckBox(false);
      }
    };
    checkAllFollowersSelect();
  }, [selectedFollowers, getFollowerUsers, setAllFollowersCheckBox]);

  useEffect(() => {
    if (getFollowerUsers && getFollowerUsers.data && getFollowerUsers.data.length > 0) {
      setData(getFollowerUsers.data);
    } else if (getFollowerUsers.data.length !== 0) {
      setData(getFollowerUsers);
    }
  }, [getFollowerUsers]);

  const renderItemFollowers = (item) => {
    const showRight = selectedFollowers.length > 0 && selectedFollowers.find((e) => e === item.id) !== undefined;

    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 24,
        }}
        key={item.id}
        activeOpacity={1}
        onPress={() => selectFollowers(item.id)}>
        <Avatar source={item.avatar ? {uri: item.avatar} : avatarSrc} />
        <Username>
          <ListText>{item.fullName || `@${item.username}`}</ListText>
          <ListSubText>{`@${item.username} ${item.location ? ` • ${item.location}` : ''}`}</ListSubText>
        </Username>
        {showRight ? <CheckIco width="20" height="20" fill={colors.pink} stroke={colors.white} /> : <ButtonConfirm />}
      </TouchableOpacity>
    );
  };

  const HeaderComponent = () => (
    <>
      {InviteAll(allFollowersCheckBox, selectAllFollowers, arrayImgAvatar, avatarBorderColor, ALL_FOLLOWERS)}
      <SuggestedText>{NEW_SESSION_INVITE_FOLLOWERS_SELECT}</SuggestedText>
    </>
  );

  const handleSearch = (text) => {
    setFollowersQuery(text);
    const formattedText = text.toLowerCase();
    const regExp = new RegExp(formattedText, 'g');
    setData(
      arrayImgAvatar.filter(
        ({username, fullName}) => regExp.test(username.toLowerCase()) || regExp.test(fullName.toLowerCase()),
      ),
    );
  };
  return (
    <Wrapper>
      <SearchInput
        value={followersQuery}
        onChangeText={handleSearch}
        autoCorrect={false}
        style={{width: '100%', alignSelf: 'center'}}
      />
      {data.length > 0 ? (
        <FollowersContainer
          keyExtractor={(item) => item.id.toString()}
          ListHeaderComponent={HeaderComponent}
          renderItem={({item}) => renderItemFollowers(item)}
          data={data}
        />
      ) : (
        <EmptyFollowersContainer>
          <EmptyTitle>{SUBTITLE_SHARE_SESSION}</EmptyTitle>
          <EmptyFollowersText>
            {NO_FOLLOWERS_INVITE}
            {NO_FOLLOWERS_1}
            <SearchPanelLink>{NO_FOLLOWERS_2}</SearchPanelLink>
            {NO_FOLLOWERS_3}
            {NO_FOLLOWERS_4}
          </EmptyFollowersText>
        </EmptyFollowersContainer>
      )}
    </Wrapper>
  );
};

export default InviteFollowersTab;
