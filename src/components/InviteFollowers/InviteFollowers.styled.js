import styled from 'styled-components/native';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';

export const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  overflow: 'hidden',
});

export const EmptyTitle = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 24,
  color: colors.white,
  paddingVertical: 12,
});

export const Teams = styled.FlatList({
  flex: 1,
  marginHorizontal: 24,
});

export const SuggestedText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 15,
  color: colors.white,
  paddingTop: 16,
  paddingBottom: 24,
});

export const TextAllFollow = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: colors.white,
  flex: 1,
  paddingHorizontal: 24,
});

export const Avatar = styled.Image({
  width: 45,
  height: 45,
  resizeMode: 'cover',
  borderRadius: 40,
});

export const AvatarAllFollow = styled.Image(({borderColor}) => ({
  width: 45,
  height: 45,
  resizeMode: 'cover',
  borderRadius: 40,
  borderWidth: 2,
  marginTop: 8,
  borderColor: borderColor || colors.blueBackgroundSession,
}));

export const ButtonConfirm = styled.View({
  width: 20,
  height: 20,
  borderRadius: 40,
  alignSelf: 'center',
  borderColor: colors.white,
  borderWidth: 1,
});

export const ListText = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: colors.white,
});

export const ListSubText = styled.Text({
  ...Fonts.avenir,
  fontSize: 13,
  color: colors.white,
});

export const Username = styled.View({
  flexDirection: 'column',
  flex: 1,
  paddingHorizontal: 24,
});

export const InviteAllRow = styled.TouchableOpacity({
  alignItems: 'center',
  flexDirection: 'row',
});

export const FollowersContainer = styled.FlatList({
  width: '100%',
});

export const EmptyFollowersContainer = styled.View({
  justifyContent: 'center',
  alignItems: 'center',
  paddingBottom: 24,
});

export const EmptyFollowersText = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: colors.white,
  textAlign: 'center',
});

export const SearchPanelLink = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: colors.pink,
});
