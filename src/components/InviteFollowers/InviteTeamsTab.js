import React, {useCallback} from 'react';

import {TouchableOpacity} from 'react-native';
import colors from 'src/constants/Colors';
import {
  Avatar,
  ButtonConfirm,
  EmptyFollowersContainer,
  EmptyFollowersText,
  EmptyTitle,
  ListText,
  SuggestedText,
  Username,
} from 'src/components/InviteFollowers/InviteFollowers.styled';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {CheckIco} from 'assets/ico';

import TeamsList from 'src/components/TeamsList/TeamsListForInvite';
import {useLazyGetTeamsInvite} from 'src/utils/hook/useLazyGetTeamsInvite';
import {
  ALL_TEAMS,
  NEW_SESSION_INVITE_TEAMS_SELECT,
  NO_SEARCH_FOUND,
  NO_TEAMS_FOUND,
  NO_TEAMS_INVITE,
} from 'src/constants/Texts';
import {InviteAll} from 'src/components/InviteFollowers/components/InviteAllElement';

const InviteTeamsTab = ({
  selectedTeamsFollowers,
  setSelectedTeamsFollowers,
  avatarBorderColor,
  isAllSelectedTeams,
  setIsAllSelectedTeams,
  setUsersTeamsToInviteNft,
  UsersTeamsToInviteNft,
  nftsRequired,
  setQuery,
  query
}) => {
  const selectTeam = (team) => {
    let teams = [...selectedTeamsFollowers];

    if (selectedTeamsFollowers.includes(team.id)) {
      teams = teams.filter((i) => i !== team.id);
    } else {
      teams.push(team.id);
    }
    setIsAllSelectedTeams(false);
    setSelectedTeamsFollowers(teams);
  };

  const selectAllTeams = useCallback(() => {
    setIsAllSelectedTeams(!isAllSelectedTeams);
  }, [isAllSelectedTeams, setIsAllSelectedTeams]);

  const arrayImgAvatar = [];

  if (UsersTeamsToInviteNft && UsersTeamsToInviteNft.data && UsersTeamsToInviteNft.data.length > 0) {
    UsersTeamsToInviteNft.data.forEach((item) => arrayImgAvatar.push(item));
  } else if (UsersTeamsToInviteNft.data.length !== 0) {
    UsersTeamsToInviteNft.forEach((item) => arrayImgAvatar.push(item));
  }

  const HeaderComponent = () => (
    <>
      {InviteAll(isAllSelectedTeams, selectAllTeams, arrayImgAvatar, avatarBorderColor, ALL_TEAMS)}
      <SuggestedText>{NEW_SESSION_INVITE_TEAMS_SELECT}</SuggestedText>
    </>
  );

  const renderItem = ({item}) => {
    const showRight =
      selectedTeamsFollowers.length > 0 && selectedTeamsFollowers.find((e) => e === item.id) !== undefined;
    return (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 24,
        }}
        key={item.id}
        activeOpacity={1}
        onPress={() => selectTeam(item)}>
        <Avatar source={item.avatar ? {uri: item.avatar} : avatarSrc} />
        <Username>
          <ListText>{item.name}</ListText>
        </Username>
        {showRight || isAllSelectedTeams ? (
          <CheckIco width="20" height="20" fill={colors.pink} stroke={colors.white} />
        ) : (
          <ButtonConfirm />
        )}
      </TouchableOpacity>
    );
  };

  const NoTeamsComponent = () => (
    <EmptyFollowersContainer>
      <EmptyTitle>{NO_TEAMS_INVITE}</EmptyTitle>
    </EmptyFollowersContainer>
  );

  const NoResultsComponent = () => (
    <EmptyFollowersContainer>
      <EmptyTitle>{NO_TEAMS_FOUND}</EmptyTitle>
      <EmptyFollowersText>{NO_SEARCH_FOUND}</EmptyFollowersText>
    </EmptyFollowersContainer>
  );

  return (
    <TeamsList
      query={query}
      setQuery={setQuery}
      setUsersTeamsToInviteNft={setUsersTeamsToInviteNft}
      nftsRequired={nftsRequired}
      UsersTeamsToInviteNft={UsersTeamsToInviteNft}
      limit={20}
      renderItem={renderItem}
      useLazyQueryHook={useLazyGetTeamsInvite}
      objectName="UsersTeamsToInviteNft"
      emptyDataComponent={NoTeamsComponent}
      emptySearchResultsComponent={NoResultsComponent}
      ListHeaderComponent={HeaderComponent}

    />
  );
};

export default InviteTeamsTab;
