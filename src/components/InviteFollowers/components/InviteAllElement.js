import React, {useMemo} from 'react';
import {
  AvatarAllFollow,
  ButtonConfirm,
  InviteAllRow,
  TextAllFollow,
} from 'src/components/InviteFollowers/InviteFollowers.styled';
import {StyleSheet, View} from 'react-native';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {CheckIco} from 'assets/ico';
import colors from 'src/constants/Colors';

export const InviteAll = (isAllSelected, setAllSelected, arrayImgAvatar, avatarBorderColor, inviteAllText) =>
  useMemo(
    () => (
      <InviteAllRow activeOpacity={1} onPress={() => setAllSelected((value) => !value)}>
        <View
          style={{
            ...StyleSheet.absoluteFillObject,
            marginTop: -8,
            marginLeft: 8,
          }}>
          {arrayImgAvatar.length > 1 && arrayImgAvatar[1].avatar ? (
            <AvatarAllFollow source={{uri: arrayImgAvatar[1].avatar}} borderColor={avatarBorderColor} />
          ) : (
            <AvatarAllFollow source={avatarSrc} borderColor={avatarBorderColor} />
          )}
        </View>
        {arrayImgAvatar.length > 0 && arrayImgAvatar[0].avatar ? (
          <AvatarAllFollow source={{uri: arrayImgAvatar[0].avatar}} borderColor={avatarBorderColor} />
        ) : (
          <AvatarAllFollow source={avatarSrc} borderColor={avatarBorderColor} />
        )}
        <TextAllFollow>{inviteAllText}</TextAllFollow>
        {isAllSelected ? (
          <CheckIco width="20" height="20" fill={colors.pink} stroke={colors.white} />
        ) : (
          <ButtonConfirm />
        )}
      </InviteAllRow>
    ),
    [arrayImgAvatar, avatarBorderColor, setAllSelected],
  );
