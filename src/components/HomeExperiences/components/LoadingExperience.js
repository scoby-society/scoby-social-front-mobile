import React from 'react';
import {ActivityIndicator, View} from 'react-native';

const LoadingExperience = () => (
  <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
    <ActivityIndicator size="large" color="#fff" />
  </View>
);

export default LoadingExperience;
