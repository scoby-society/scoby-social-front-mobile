import React, {useRef, useCallback, useMemo} from 'react';
import styled from 'styled-components';
import {Dimensions, Animated, View} from 'react-native';
import SessionsItem from 'src/components/Sessions/Sessions';
import SerieItem from 'src/components/Series';
import Colors from 'src/constants/Colors';
import EventItem from '../../Event/EventItem';
import { CardHomeProject } from 'src/screens/CreateProject/components/CardHomeProject';

const {width} = Dimensions.get('window');
const CIRCLE_SIZE_INACTIVE = 10;
const MARGIN_CIRCLE = 10;

const Wrapper = styled.View({
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
});

const colorArray = [Colors.navBarPurple, Colors.navBarPurple, Colors.white, Colors.navBarPurple, Colors.navBarPurple];

const Indicator = ({scrollX, data}) => {
  return (
    <View style={{flexDirection: 'row', marginRight: 10,}}>
      {data.map((_, i) => {
        const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
        const scale = scrollX.interpolate({
          inputRange,
          outputRange: [0.8, 1.8, 0.8],
          extrapolate: 'clamp',
        });
        const backgroundColor = scrollX.interpolate({
          inputRange: [(i - 2) * width, (i - 1) * width, i * width, (i + 1) * width, (i + 2) * width],
          outputRange: colorArray.map((bg) => bg),
        });
        const borderColor = scrollX.interpolate({
          inputRange: [(i - 1) * width, i * width, (i + 1) * width],
          outputRange: ['#fff', '#000', '#fff'],
        });

        const outputRange = data.map((_, index, da) =>
          index === i
            ? 1
            : i > index
            ? Math.abs(i - index - (da.length - 1)) / (da.length - 1)
            : Math.abs(i - index + (da.length - 1)) / (da.length - 1),
        );
        const opacity = scrollX.interpolate({
          inputRange: data.map((_, index) => index * width),
          outputRange,
        });
        const borderWidth = scrollX.interpolate({
          inputRange: [(i - 1) * width, i * width, (i + 1) * width],
          outputRange: [0, 1.5, 0],
        });
        return (
          <Animated.View
            key={`indicator-${i}-${Math.random()}`}
            style={{
              height: CIRCLE_SIZE_INACTIVE,
              width: CIRCLE_SIZE_INACTIVE,
              borderRadius: 5,
              backgroundColor,
              borderColor,
              borderWidth,
              opacity,
              marginBottom: MARGIN_CIRCLE + 2,
              marginHorizontal: width / data.length / 3,
              transform: [
                {
                  scale,
                },
              ],
            }}
          />
        );
      })}
    </View>
  );
};

const ExperienceHorizontalScroll = ({itemRender, handleRefresh, handleEndReached, scrollView, loading = false}) => {
  const scrollX = useRef(new Animated.Value(0)).current;

  const getSessions = useCallback((item) => item?.filter((i) => !i.finishedAt), []);

  const keyExtractorExperience = useCallback((item) => `${item.id}-feeditem-${Math.random()}`, []);

  const renderCards = ({item}) => {
    if (true) {
      
    //  return <CardHomeProject item={item} miniCard={''} category={'projects'} />;
    }

    if (item.schedule) {
      return <SerieItem enable={true} series={item} />;
    }

    if (item.start) {
      return <CardHomeProject/>;
    }

    
  };
  const memorizedValueExperience = useMemo(() => renderCards, [itemRender]);
  return (
    <Wrapper>
      <Animated.FlatList
        ref={scrollView}
        onScroll={Animated.event([{nativeEvent: {contentOffset: {x: scrollX}}}], {useNativeDriver: false})}
        scrollEventThrottle={32}
        horizontal
        pagingEnabled
        bounces={false}
        showsHorizontalScrollIndicator={false}
        data={getSessions(itemRender)}
        extraData={itemRender}
        renderItem={memorizedValueExperience}
        keyExtractor={keyExtractorExperience}
        initialNumToRender={20}
        onRefresh={handleRefresh}
        onEndReached={handleEndReached}
        refreshing={loading}
        endReached
      />
      {getSessions(itemRender).length > 1 && <Indicator scrollX={scrollX} data={getSessions(itemRender)} />}
    </Wrapper>
  );
};

export default ExperienceHorizontalScroll;
