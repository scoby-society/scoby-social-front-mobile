import React, { useRef, useEffect, useState, useMemo, useCallback } from 'react'
import {Alert} from 'react-native'
import { useLazyQuery } from '@apollo/client'
import styled from 'styled-components'
import ExperienceHorizontalScroll from '../components/ExperienceHorizontalScroll'
import {PAGE, LIMIT} from 'src/utils/pagination'
import { GET_SERIES } from 'src/graphql/queries/series'

const Wrapper=styled.View({
    flex:1,
    justifyContent:'center',
    alignItems:'center'
})

const SeriesScroll=({setLoading})=>{
    const [page, setPage]=useState(PAGE)
    const ScrollView=useRef(null)
    const [refetch, {data:{getSeriePaging}={getSeriePaging:[]}, fetchMore, loading}]=useLazyQuery(GET_SERIES,{
        pollInterval:10000,
        variables:{
            limit:LIMIT,
            page:PAGE
        }
    })

    const series=useMemo(()=>getSeriePaging,[getSeriePaging])

    useEffect(()=>{
        setLoading({loading, length:series.length})
    },[loading, series.length])

    useEffect(()=>{
        refetch({
            variables:{
                paging:{
                    limit:LIMIT,
                    page:PAGE
                }
            }
        })
        }, [refetch]
    )

    const handleRefresh = useCallback(() => {
        setPage(1);
        refetch({
          variables: {
            paging: {
              limit:LIMIT,
              page: 1,
            },
          },
        });
        ScrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
      }, [refetch]);
     
     const handleEndReached = useCallback(() => {
       setPage(page + 1);
       fetchMore({
         variables: {
           paging: {
             limit:LIMIT,
             page: page + 1,
           },
         },
         onError(e) {
           Alert.alert('Error', e.message);
         },
       });
     }, [fetchMore, page]);

    if(series.length===0){
        return null
    }

    return (
        <Wrapper>
            <ExperienceHorizontalScroll
                itemRender={series}
                handleEndReached={handleEndReached}
                handleRefresh={handleRefresh}
                scrollView={ScrollView}
                loading={loading}
            />
        </Wrapper>
    )
}

export default SeriesScroll