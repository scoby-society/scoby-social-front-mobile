import React, {useContext, useEffect, useState, useMemo, useCallback, useRef} from 'react';
import {useLazyQuery, useMutation} from '@apollo/client';
import {Alert} from 'react-native';
import styled from 'styled-components';
import {PAGE, LIMIT} from 'src/utils/pagination';
import {GET_LIVE_EVENTS} from 'src/graphql/queries/events';
import {FollowersContext} from 'src/containers/followers';
import {END_SESSION} from 'src/graphql/mutations/session';
import ExperienceHorizontalScroll from '../components/ExperienceHorizontalScroll';

const Wrapper = styled.View({
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
});

const EventLiveScroll = ({setLoading}) => {
  const [page, setPage] = useState(PAGE);
  const {currentUserProfile} = useContext(FollowersContext);
  const [endSession] = useMutation(END_SESSION);
  const ScrollView = useRef(null);
  const [refetch, {data: {getLiveEventsPaging} = {getLiveEventsPaging: []}, fetchMore, loading}] = useLazyQuery(
    GET_LIVE_EVENTS,
    {
      pollInterval: 10000,
      variables: {
        limit: LIMIT,
        page: PAGE,
      },
    },
  );

  const event = useMemo(
    () =>
      getLiveEventsPaging.filter((session) => {
        if (session.ownerUser.id === currentUserProfile.id) {
          endSession({
            variables: {
              sessionId: session.session.id,
              userId: currentUserProfile.id,
              currentUserId: currentUserProfile.id,
            },
          });
          return false;
        }
        return true;
      }),
    [endSession, getLiveEventsPaging, currentUserProfile.id],
  );

  useEffect(() => {
    setLoading({loading, length: event.length});
  }, [loading, event.length, setLoading]);

  useEffect(() => {
    refetch({
      variables: {
        paging: {
          limit: LIMIT,
          page: PAGE,
        },
      },
    });
  }, [refetch]);

  const handleRefresh = useCallback(() => {
    setPage(1);
    refetch({
      variables: {
        paging: {
          limit: LIMIT,
          page: 1,
        },
      },
    });
    ScrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
  }, [refetch]);

  const handleEndReached = useCallback(() => {
    setPage(page + 1);
    fetchMore({
      variables: {
        paging: {
          limit: LIMIT,
          page: page + 1,
        },
      },
      onError(e) {
        Alert.alert('Error', e.message);
      },
    });
  }, [fetchMore, page]);

  if (event.length === 0) {
    return null;
  }

  return (
    <Wrapper>
      <ExperienceHorizontalScroll
        itemRender={event}
        handleEndReached={handleEndReached}
        handleRefresh={handleRefresh}
        scrollView={ScrollView}
      />
    </Wrapper>
  );
};

export default EventLiveScroll;
