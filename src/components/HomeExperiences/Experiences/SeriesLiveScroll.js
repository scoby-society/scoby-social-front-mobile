import React, { useContext, useEffect, useState, useMemo, useCallback, useRef } from 'react'
import {Alert} from 'react-native'
import { useLazyQuery, useMutation } from '@apollo/client'
import styled from 'styled-components'
import ExperienceHorizontalScroll from '../components/ExperienceHorizontalScroll'
import {PAGE, LIMIT} from 'src/utils/pagination'
import { GET_LIVE_SERIES} from 'src/graphql/queries/series'
import { FollowersContext } from 'src/containers/followers'
import { END_SESSION } from 'src/graphql/mutations/session'

const Wrapper=styled.View({
    flex:1,
    justifyContent:'center',
    alignItems:'center'
})

const SeriesLiveScroll=({setLoading})=>{
    const [page, setPage]=useState(PAGE)
    const {currentUserProfile}=useContext(FollowersContext)
    const [endSession]=useMutation(END_SESSION)
    const ScrollView=useRef(null)
    const [refetch, {data:{getLiveSeriePaging}={getLiveSeriePaging:[]}, fetchMore, loading}]=useLazyQuery(GET_LIVE_SERIES,{
        pollInterval:10000,
        variables:{
            limit:LIMIT,
            page:PAGE
        }
    })

    const series=useMemo(()=>getLiveSeriePaging.filter((session) => {
        if (session.ownerUser.id === currentUserProfile.id) {
          endSession({
            variables: {
              sessionId: session.session.id,
              userId: currentUserProfile.id,
              currentUserId: currentUserProfile.id,
            },
          });
          return false;
        }
        return true;
    }),[endSession,getLiveSeriePaging,currentUserProfile.id])

    useEffect(()=>{
        setLoading({loading, length:series.length})
    },[loading, series.length])

    useEffect(()=>{
        refetch({
            variables:{
                paging:{
                    limit:LIMIT,
                    page:PAGE
                }
            }
        })
        }, [refetch]
    )

    const handleRefresh = useCallback(() => {
       setPage(1);
       refetch({
         variables: {
           paging: {
             limit:LIMIT,
             page: 1,
           },
         },
       });
       ScrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
     }, [refetch]);
    
    const handleEndReached = useCallback(() => {
      setPage(page + 1);
      fetchMore({
        variables: {
          paging: {
            limit:LIMIT,
            page: page + 1,
          },
        },
        onError(e) {
          Alert.alert('Error', e.message);
        },
      });
    }, [fetchMore, page]);

    if(series.length===0){
        return null
    }

    return (
        <Wrapper>
            <ExperienceHorizontalScroll
                itemRender={series}
                handleEndReached={handleEndReached}
                handleRefresh={handleRefresh}
                scrollView={ScrollView}
            />
        </Wrapper>
    )
}

export default SeriesLiveScroll