import React, {useContext, useEffect, useState, useMemo, useCallback, useRef} from 'react';
import {Alert} from 'react-native';
import {useLazyQuery, useMutation} from '@apollo/client';
import {GET_LIVE_SESSIONS} from 'src/graphql/queries/session';
import styled from 'styled-components';
import {PAGE, LIMIT} from 'src/utils/pagination';
import {FollowersContext} from 'src/containers/followers';
import {END_SESSION} from 'src/graphql/mutations/session';
import ExperienceHorizontalScroll from '../components/ExperienceHorizontalScroll';

const Wrapper = styled.View({
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
});

const SessionScroll = ({setLoading,data}) => {
  const [page, setPage] = useState(PAGE);
  const {currentUserProfile} = useContext(FollowersContext);
  const [endSession] = useMutation(END_SESSION);
  const ScrollView = useRef(null);
  const [refetch, {data: {getLiveSessionsPaging = []} = {getLiveSessionsPaging: []}, fetchMore, loading}] =
    useLazyQuery(GET_LIVE_SESSIONS, {
      pollInterval: 10000,
      variables: {
        limit: LIMIT,
        page: PAGE,
      },
    });

  const session = useMemo(
    () =>
      getLiveSessionsPaging.filter((sessionItem) => {
        if (sessionItem.ownerUser.id === currentUserProfile.id) {
          endSession({
            variables: {
              sessionId: sessionItem.id,
              userId: currentUserProfile.id,
              currentUserId: currentUserProfile.id,
            },
          });
          return false;
        }
        return true;
      }),
    [endSession, data, currentUserProfile],
  );

  useEffect(() => {
    setLoading({loading, length: session.length});
  }, [loading, session.length]);

  useEffect(() => {
    refetch({
      variables: {
        paging: {
          limit: LIMIT,
          page: PAGE,
        },
      },
    });
  }, [refetch]);

  const handleRefresh = useCallback(() => {
    setPage(1);
    refetch({
      variables: {
        paging: {
          limit: LIMIT,
          page: 1,
        },
      },
    });
    ScrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
  }, [refetch]);

  const handleEndReached = useCallback(() => {
    setPage(page + 1);
    fetchMore({
      variables: {
        paging: {
          limit: LIMIT,
          page: page + 1,
        },
      },
      onError(e) {
        Alert.alert('Error', e.message);
      },
    });
  }, [fetchMore, page]);

  if (session.length === 0) {
    return null;
  }

  return (
    <Wrapper>
      <ExperienceHorizontalScroll
        itemRender={session}
        handleEndReached={handleEndReached}
        handleRefresh={handleRefresh}
        scrollView={ScrollView}
      />
    </Wrapper>
  );
};

export default SessionScroll;
