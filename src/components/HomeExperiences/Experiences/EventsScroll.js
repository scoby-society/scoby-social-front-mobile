import React, {useRef,useEffect, useMemo, useState, useCallback } from 'react'
import {Alert} from 'react-native'
import { useLazyQuery } from '@apollo/client'
import styled from 'styled-components'
import ExperienceHorizontalScroll from '../components/ExperienceHorizontalScroll'
import {PAGE, LIMIT} from 'src/utils/pagination'
import { GET_EVENTS } from 'src/graphql/queries/events'

const Wrapper=styled.View({
    flex:1,
    justifyContent:'center',
    alignItems:'center'
})

const EventScroll=({setLoading,data})=>{
    const [page, setPage]=useState(PAGE)
    const ScrollView=useRef(null)
    const [refetch, {data:{getEventsPaging}={getEventsPaging:[]}, fetchMore, loading}]=useLazyQuery(GET_EVENTS,{
        pollInterval:10000,
        variables:{
            limit:LIMIT,
            page:PAGE
        }
    })

    const events=useMemo(()=>getEventsPaging,[getEventsPaging])

    useEffect(()=>{
        setLoading({loading, length:events.length})
    },[loading, events.length])

    useEffect(()=>{
        refetch({
            variables:{
                paging:{
                    limit:LIMIT,
                    page:PAGE
                }
            }
        })
        }, [refetch]
    )

    const handleRefresh = useCallback(() => {
        setPage(1);
        refetch({
          variables: {
            paging: {
              limit:LIMIT,
              page: 1,
            },
          },
        });
        ScrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
      }, [refetch]);
     
     const handleEndReached = useCallback(() => {
       setPage(page + 1);
       fetchMore({
         variables: {
           paging: {
             limit:LIMIT,
             page: page + 1,
           },
         },
         onError(e) {
           Alert.alert('Error', e.message);
         },
       });
     }, [fetchMore, page]);

    if(events.length===0){
        return null
    }

    return (
        <Wrapper>
            <ExperienceHorizontalScroll
                itemRender={data}
                handleEndReached={handleEndReached}
                handleRefresh={handleRefresh}
                scrollView={ScrollView}
                loading={loading}
            />
        </Wrapper>
    )
}

export default EventScroll