import React, {useCallback, useState} from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import {tabsHeight} from 'src/navigation/tabs';
import {CardHomeProject} from 'src/screens/CreateProject/components/CardHomeProject';

export default function Sessions({navigation, itemsSearch}) {
  const [loading] = useState(false);

  const DisplayProjects = useCallback(
    () => (
      <FlatList
        data={itemsSearch || []}
        style={{flex: 1}}
        refreshing={false}
        showsVerticalScrollIndicator={false}
        endReached
        keyExtractor={(item) => `${item.id}-${Math.random()}`}
        renderItem={(item) => <CardHomeProject dato={item} navigation={navigation} />}
        numColumns={1}
      />
    ),
    [itemsSearch],
  );

  return (
    <View
      style={{
        marginBottom: tabsHeight,
        height: '90%',
        paddingBottom: 40,
        paddingTop: 10,
      }}>
      <DisplayProjects />
      {loading && <ActivityIndicator size="large" color="red" style={{top: -30}} />}
    </View>
  );
}
