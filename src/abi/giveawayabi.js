export const giveawayabi = {
  version: '0.1.0',
  name: 'dev_give_away',
  instructions: [
    {
      name: 'initializeOwner',
      accounts: [
        {
          name: 'accountUser',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'platform',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'userBump',
          type: 'u32',
        },
      ],
    },
    {
      name: 'initializepdasol',
      accounts: [
        {
          name: 'userPdaAcct',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump',
          type: 'u8',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'initializestatepda',
      accounts: [
        {
          name: 'statepda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'depositTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump',
          type: 'u8',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'initialisetokenpda',
      accounts: [
        {
          name: 'tokenpda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'statepda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'mint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'depositTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump1',
          type: 'u8',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'sendsolpda',
      accounts: [
        {
          name: 'userPdaAcct',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump',
          type: 'u8',
        },
        {
          name: 'amount',
          type: 'u64',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'sendtokenpda',
      accounts: [
        {
          name: 'tokenpda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'statepda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'mint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'depositTokenAccount',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump1',
          type: 'u8',
        },
        {
          name: 'bump2',
          type: 'u8',
        },
        {
          name: 'amount',
          type: 'u64',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'claimsol',
      accounts: [
        {
          name: 'userPdaAcct',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'winner',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump',
          type: 'u8',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'initialisewinner',
      accounts: [
        {
          name: 'tokenpda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'statepda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'walletToDepositTo',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'reciever',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'mint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'associatedTokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump1',
          type: 'u8',
        },
        {
          name: 'bump2',
          type: 'u8',
        },
        {
          name: 'amount',
          type: 'u64',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'sendtokenwinner',
      accounts: [
        {
          name: 'tokenpda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'statepda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'walletToDepositTo',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: false,
          isSigner: true,
        },
        {
          name: 'depositTokenAccount',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'reciever',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'associatedTokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump1',
          type: 'u8',
        },
        {
          name: 'bump2',
          type: 'u8',
        },
        {
          name: 'amount',
          type: 'u64',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'cancelSol',
      accounts: [
        {
          name: 'userPdaAcct',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: false,
          isSigner: true,
        },
        {
          name: 'ownerpda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump',
          type: 'u8',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'initialiseSenderToken',
      accounts: [
        {
          name: 'tokenpda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'statepda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'walletToDepositTo',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: true,
          isSigner: true,
        },
        {
          name: 'mint',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'associatedTokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump1',
          type: 'u8',
        },
        {
          name: 'bump2',
          type: 'u8',
        },
        {
          name: 'amount',
          type: 'u64',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
    {
      name: 'canceltoken',
      accounts: [
        {
          name: 'tokenpda',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'statepda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'walletToDepositTo',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'sender',
          isMut: true,
          isSigner: false,
        },
        {
          name: 'owner',
          isMut: false,
          isSigner: true,
        },
        {
          name: 'depositTokenAccount',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'ownerpda',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'systemProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'rent',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'associatedTokenProgram',
          isMut: false,
          isSigner: false,
        },
        {
          name: 'tokenProgram',
          isMut: false,
          isSigner: false,
        },
      ],
      args: [
        {
          name: 'bump1',
          type: 'u8',
        },
        {
          name: 'bump2',
          type: 'u8',
        },
        {
          name: 'amount',
          type: 'u64',
        },
        {
          name: 'msgId',
          type: 'string',
        },
      ],
    },
  ],
  accounts: [
    {
      name: 'AccountUser',
      type: {
        kind: 'struct',
        fields: [
          {
            name: 'ownerId',
            type: 'publicKey',
          },
          {
            name: 'platformId',
            type: 'publicKey',
          },
        ],
      },
    },
    {
      name: 'UserPdaAcct',
      type: {
        kind: 'struct',
        fields: [
          {
            name: 'amount',
            type: 'u64',
          },
        ],
      },
    },
    {
      name: 'State',
      type: {
        kind: 'struct',
        fields: [
          {
            name: 'bump',
            type: 'u8',
          },
          {
            name: 'amount',
            type: 'u64',
          },
        ],
      },
    },
  ],
};
