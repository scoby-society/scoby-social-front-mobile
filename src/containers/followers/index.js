import React, {createContext, useEffect, useState} from 'react';

export const defaultFollowersContext = {
  currentUserProfile: {id: null},
  setCurrentUserProfile: () => {},
  notifications: 0,
  setNotifications: () => {},
  channels: null,
  setChannels: () => {},
  currentCategory: 'all',
  setCurrentCategory: () => {},
};

export const FollowersContext = createContext(defaultFollowersContext);

export default function FollowersProvider({children, setProfile}) {
  const [currentUserProfile, setCurrentUserProfile] = useState(defaultFollowersContext.currentUserProfile);
  const [notifications, setNotifications] = useState(defaultFollowersContext.notifications);
  const [channels, setChannels] = useState(defaultFollowersContext.channels);
  const [currentCategory, setCurrentCategory] = useState(defaultFollowersContext.currentCategory);

  useEffect(() => {
    setCurrentUserProfile(setProfile);
  }, [setProfile]);

  return (
    <FollowersContext.Provider
      value={{
        currentUserProfile,
        setCurrentUserProfile,
        notifications,
        setNotifications,
        channels,
        setChannels,
        currentCategory,
        setCurrentCategory,
      }}>
      {children}
    </FollowersContext.Provider>
  );
}
