import React, {createContext, useState, useEffect, useRef} from 'react';

export const defaultGlobalContext = {
  isLogged: false,
  initialLink: null,
  setIsLogged: () => {},
  setInitialLink: () => {},
  topics: [],
  setTopics: () => {},
  userId: null,
  walletKey: null,
  setWalletKey: () => {},
  referredId: null,
  hostWallet: null,
};

export const GlobalContext = createContext(defaultGlobalContext);

export default function GlobalProvider({children}) {
  const [isLogged, setIsLogged] = useState(defaultGlobalContext.isLogged);
  const [initialLink, setInitialLink] = useState(defaultGlobalContext.initialLink);
  const [id, setId] = useState(defaultGlobalContext.userId);
  const [walletKey, setWalletKey] = useState(defaultGlobalContext.walletKey);
  const [topics, setTopics] = useState(defaultGlobalContext.topics);
  const [referredId, setReferredId] = useState(defaultGlobalContext.referredId);
  const host = useRef(null);

  useEffect(() => {
    if (initialLink && initialLink.url) {
      const link = initialLink.url;
      const linkParts = link.split('/');
      setReferredId(linkParts[linkParts.length - 1]);
    }
  }, [initialLink]);

  return (
    <GlobalContext.Provider
      value={{
        isLogged,
        initialLink,
        setIsLogged,
        setInitialLink,
        id,
        setId,
        topics,
        setTopics,
        referredId,
        walletKey,
        setWalletKey,
        host,
      }}>
      {children}
    </GlobalContext.Provider>
  );
}
