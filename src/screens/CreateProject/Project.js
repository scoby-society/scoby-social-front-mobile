import React, {useState} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import styled from 'styled-components';
import withSafeArea from 'src/components/withSafeArea';
import {
  TITLE_NATIVE_PROJECT,
  SUBTITLE_NATIVE_PROJECT,
  TITLE_INCLUDED_PROJECT,
  SUBTITLE_INCLUDED_PROJECT,
  TEXT_BUTTON_DEPLOYED_PROJECT,
  TEXT_BUTTON_INITIATING_PROJECT,
  TEXT_BUTTON_OFFICIAL_PROJECT,
  TEXT_BUTTON_COLLECTOR_PROJECT,
  DEPLOYED_BUTTON_PROJECT,
  INITIATING_BUTTON_PROJECT,
  OFFICIAL_BUTTON_PROJECT,
  COLLECTOR_BUTTON_PROJECT,
} from 'src/constants/Texts';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import BackButton from 'src/components/BackButton';

const Style = StyleSheet.create({
  containerButton: {
    marginBottom: '10%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
  goBack: {
    zIndex: 1,
  },
  containerChildOne: {
    marginTop: 15,
    marginBottom: 10,
  },
  containerChildtwo: {
    marginBottom: 10,
  },
  container: {
    flex: 1,
    paddingHorizontal: '7%',
  },
  icons: {
    width: 120,
    height: 120,
  },
  mainContainer: {
    width: '100%',
    height: '100%',
  },
});
const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  width: '100%',
});

const SubTitle = styled.Text(({experience = 16}) => ({
  marginTop: 10,
  marginBottom: 15,
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
}));
const Text = styled.Text(({experience = 12}) => ({
  width: 150,
  marginTop: 10,
  marginBottom: 15,
  ...Fonts.avenir,
  fontSize: experience,
  textAlign: 'center',
}));
const ProjectScreen = ({navigation}) => {
  const [typeProject, setTypeProject] = useState('');
  const handleChangeTypeProject = (type) => {
    setTypeProject(type);
  };
  const handleRedirect = () => {
    navigation.navigate('CreationProjectScreen');
  };

  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <ScrollView style={Style.container}>
        <View style={Style.containerChildOne}>
          <Title>{TITLE_NATIVE_PROJECT}</Title>
          <SubTitle>{SUBTITLE_NATIVE_PROJECT}</SubTitle>
          <View style={Style.containerButton}>
            <View>
              <RegularButton
                onPress={() => handleChangeTypeProject('initiating')}
                title={INITIATING_BUTTON_PROJECT}
                style={[
                  Style.regularBtn,
                  typeProject === 'initiating'
                    ? {backgroundColor: colors.newPink}
                    : {backgroundColor: colors.disabledPink},
                ]}
              />
              <Text>{TEXT_BUTTON_INITIATING_PROJECT}</Text>
            </View>
            <View>
              <RegularButton
                onPress={() => handleChangeTypeProject('deployed')}
                title={DEPLOYED_BUTTON_PROJECT}
                style={[
                  Style.regularBtn,
                  typeProject === 'deployed'
                    ? {backgroundColor: colors.newPink}
                    : {backgroundColor: colors.disabledPink},
                ]}
              />
              <Text>{TEXT_BUTTON_DEPLOYED_PROJECT}</Text>
            </View>
          </View>
        </View>
        <View style={Style.containerChildtwo}>
          <Title>{TITLE_INCLUDED_PROJECT}</Title>
          <SubTitle>{SUBTITLE_INCLUDED_PROJECT}</SubTitle>
          <View style={Style.containerButton}>
            <View>
              <RegularButton
                onPress={() => handleChangeTypeProject('official')}
                title={OFFICIAL_BUTTON_PROJECT}
                style={[
                  Style.regularBtn,
                  typeProject === 'official'
                    ? {backgroundColor: colors.newPink}
                    : {backgroundColor: colors.disabledPink},
                ]}
              />
              <Text>{TEXT_BUTTON_OFFICIAL_PROJECT}</Text>
            </View>
            <View>
              <RegularButton
                onPress={() => handleChangeTypeProject('collector')}
                title={COLLECTOR_BUTTON_PROJECT}
                style={[
                  Style.regularBtn,
                  typeProject === 'collector'
                    ? {backgroundColor: colors.newPink}
                    : {backgroundColor: colors.disabledPink},
                ]}
              />
              <Text>{TEXT_BUTTON_COLLECTOR_PROJECT}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={Style.containerButton}>
        <RegularButton
          onPress={() => navigation.goBack()}
          title="Back"
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton
          disabled={typeProject === ''}
          onPress={() => handleRedirect(navigation)}
          title="Next"
          style={[
            Style.regularBtn,
            typeProject === '' ? {backgroundColor: colors.disabledPink} : {backgroundColor: colors.newPink},
          ]}
        />
      </View>
    </View>
  );
};

export default withSafeArea(ProjectScreen);
