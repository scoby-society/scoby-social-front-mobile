import React, {useContext, useMemo, useState} from 'react';
import {StyleSheet, View, ActivityIndicator, FlatList} from 'react-native';
import styled from 'styled-components';
import withSafeArea from 'src/components/withSafeArea';
import {TITLE_COLLECTION_PROJECT, SUBTITLE_COLLECTION_PROJECT} from 'src/constants/Texts';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import BackButton from 'src/components/BackButton';
import {useQuery} from '@apollo/client';
import {GET_NFT_BY_ID} from 'src/graphql/queries/Spores';
import {FollowersContext} from 'src/containers/followers';
import {CardCollection} from './components/CardCollection';

const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  marginTop: '5%',
  width: '100%',
  paddingHorizontal: '7%',
});
const SubTitle = styled.Text(({experience = 16}) => ({
  marginTop: '5%',
  marginBottom: 15,
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
  paddingHorizontal: '7%',
}));

const Style = StyleSheet.create({
  WrapperCard: {
    alignSelf: 'center',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkedIcon: {
    alignSelf: 'center',
  },
  columnContentCard: {
    flexDirection: 'column',
  },
  contentCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '5%',
  },
  containerButton: {
    marginBottom: '20%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
  },
  icons: {
    width: 120,
    height: 120,
  },
  mainContainer: {
    width: '100%',
    height: '100%',
  },
});

const CollectionProjectScreen = ({navigation, route}) => {
  const {params} = route;
  const {projectOneparams} = params;
  const {currentUserProfile} = useContext(FollowersContext);
  const [checked, setChecked] = useState(projectOneparams?.nft || []);
  const parseNftAsIdArray = () => {
    const nftIds = checked.map((e) => e.id);
    return nftIds;
  };
 
  const handleNext = () => {
    navigation.navigate('PrivacyProjectScreen', {...params, checked: parseNftAsIdArray()});
  };

  const {data: {getNftByUser} = {getNftByUser: []}, loading} = useQuery(GET_NFT_BY_ID, {
    variables: {
      id: currentUserProfile.id,
    },
  });

  const allUserNft = useMemo(() => {
    const filterMembership = getNftByUser
      .filter((a) => a.symbol === 'SCOBYNYC')
      .sort((a, b) => {
        if (a.serialNumber > b.serialNumber) {
          return 1;
        }
        if (a.serialNumber < b.serialNumber) {
          return -1;
        }
        return 0;
      });

    const filterOthers = getNftByUser
      .filter((a) => a.symbol !== 'SCOBYNYC')
      .sort((a, b) => {
        if (a.symbol < b.symbol) {
          return -1;
        }
        if (a.symbol > b.symbol) {
          return 1;
        }
        return 0;
      });

    return [...filterMembership, ...filterOthers];
  }, [getNftByUser]);

  if (loading) {
    return (
      <View style={Style.loadingContainer}>
        <ActivityIndicator size="large" color={colors.white} />
      </View>
    );
  }

  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <Title>{TITLE_COLLECTION_PROJECT}</Title>
      <SubTitle>{SUBTITLE_COLLECTION_PROJECT}</SubTitle>
      <FlatList
        contentContainerStyle={Style.WrapperCard}
        style={{flex: 1}}
        data={allUserNft}
        keyExtractor={(item) => `${item.id}-${Math.random()}`}
        renderItem={({item}) => <CardCollection item={item} checked={checked} setChecked={setChecked} />}
        numColumns={2}
      />
      <View style={Style.containerButton}>
        <RegularButton
          title="Back"
          onPress={() => navigation.goBack()}
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton
          title="Next"
          onPress={() => handleNext()}
          disabled={checked === null}
          style={[
            Style.regularBtn,
            checked === null ? {backgroundColor: colors.disabledPink} : {backgroundColor: colors.newPink},
          ]}
        />
      </View>
    </View>
  );
};

export default withSafeArea(CollectionProjectScreen);
