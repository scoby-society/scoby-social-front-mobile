import React from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import styled from 'styled-components';
import withSafeArea from 'src/components/withSafeArea';
import {TITLE_INVITE_FOLKS_PROJECT, SUBTITLE_INVITE_FOLKS_PROJECT} from 'src/constants/Texts';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import BackButton from 'src/components/BackButton';
const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  marginTop: '5%',
  width: '100%',
  paddingHorizontal: '7%',
});
const SubTitle = styled.Text(({experience = 16}) => ({
  marginTop: '5%',
  marginBottom: 15,
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
  paddingHorizontal: '7%',
}));
// eslint-disable-next-line arrow-body-style
const InviteFolksProjectScreen = ({navigation}) => {
  const Style = StyleSheet.create({
    checkedIcon: {
      alignSelf: 'center',
    },
    columnContentCard: {
      flexDirection: 'column',
    },
    contentCard: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop:'5%'
    },
    containerButton: {
      marginBottom: '10%',
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    regularBtn: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: 125,
      height: 45,
      alignSelf: 'center',
      borderRadius: 8,
      marginTop: 12,
      ...Fonts.avenir,
      fontWeight: 'bold',
    },
    container: {
      flex: 1,
    },
    icons: {
      width: 120,
      height: 120,
    },
    mainContainer: {
      width: '100%',
      height: '100%',
    },
  });
  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <ScrollView style={Style.container}>
        <Title>{TITLE_INVITE_FOLKS_PROJECT}</Title>
        <SubTitle>{SUBTITLE_INVITE_FOLKS_PROJECT}</SubTitle>
      </ScrollView>
      <View style={Style.containerButton}>
        <RegularButton
          title={'Back'}
          onPress={() => navigation.goBack()}
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton title={'Next'} style={[Style.regularBtn, {backgroundColor: colors.newPink}]} />
      </View>
    </View>
  );
};

export default withSafeArea(InviteFolksProjectScreen);