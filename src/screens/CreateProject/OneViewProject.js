import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, View, Text, ActivityIndicator, Alert, FlatList} from 'react-native';
import coverImage from 'assets/images/profile/Cover.png';
import styled from 'styled-components';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import {TEXT_MEMBER_PROJECT} from 'src/constants/Texts';
import {HeartRoundedIcon, AddRoundedIcon, ClosePurpleIcon, Dots} from 'assets/svg';
import withSafeArea from 'src/components/withSafeArea';
import BackButton from 'src/components/BackButton';
import {useMutation, useQuery} from '@apollo/client';
import {END_PROJECT, JOIN_PROJECT, LEAVE_PROJECT} from 'src/graphql/mutations/projects';
import {GET_ALL_PROJECT, GET_PROJECT_BY_ID} from 'src/graphql/queries/projects';
import RegularButton from 'src/components/RegularButton';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import NftItem from 'src/components/Nfts/nftItem';
import CommunityCard from 'src/components/HomeCards/CommunityCard';
import ManageBottoms from './components/ManageBottoms';
import EventItem from 'src/components/Event/EventItem';
import SessionsItem from 'src/components/Sessions/Sessions';
import SeriesItem from 'src/components/Series/SeriesItem';
import ChannelItem from 'src/components/Channel/ChannelItem';

const ContainPrimaryOwner = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  position: 'relative',
  bottom: 35,
});

const Card = styled.View(({suscribeProject}) => ({
  backgroundColor: colors.blueCardProject,
  zIndex: -1,
  width: '370px',
  height: suscribeProject ? '560px' : '540px',
  borderRadius: 40,
  marginHorizontal: '5%',
  alignSelf: 'center',
  position: 'relative',
  flex: 1,
  marginVertical: '20px',
}));
const ContainMenuItem = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-around',
  position: 'relative',
  bottom: '3%',
  marginTop: '2%',
});
const TitleCard = styled.Text(({pass}) => ({
  paddingHorizontal: pass ? '7%' : '4%',
  ...Fonts.avenirBold,
  lineHeight: '20px',
  fontSize: 16,
  color: colors.white,
  marginTop: '10%',
  width: pass ? '100%' : '250px',
}));
const MenuItemBottom = styled.TouchableOpacity({
  ...Fonts.avenirBold,
  lineHeight: '13px',
  fontSize: 13,
  color: colors.white,
});
const TextMenuItem = styled.Text({
  ...Fonts.avenirBold,
  lineHeight: '13px',
  fontSize: 13,
  color: colors.white,
});
const SubText = styled.Text(({experience = 10}) => ({
  color: colors.white,
  paddingHorizontal: '4%',
  width: '220px',
  marginTop: 5,
  ...Fonts.avenir,
  fontSize: experience,
  textAlign: 'left',
}));
const SubTextMember = styled.Text(({experience = 10}) => ({
  color: colors.white,
  width: '170px',
  marginTop: 5,
  ...Fonts.avenir,
  fontSize: experience,
  textAlign: 'left',
}));
const AvatarImage = styled.Image(() => ({
  marginTop: '4%',
  alignSelf: 'center',
  width: '337px',
  height: '325px',
  borderRadius: 40,
}));
const TextHeartIcon = styled.Text({
  marginTop: '4%',
  marginBottom: '15%',
  ...Fonts.ttNormsRegular,
  fontSize: '10px',
  lineHeight: '10px',
  color: colors.white,
});
const TextAddIcon = styled.Text({
  textAlign: 'center',
  marginTop: '4%',
  width: '140px',
  ...Fonts.avenir,
  fontSize: '8px',
  color: colors.white,
  letterSpacing: '0.5px',
});
const ModalOptionsOwner = styled.View({
  height: '90%',
  width: '32%',
  position: 'absolute',
  right: 5,
  top: 5,
  zIndex: 2,
  borderRadius: 16,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  paddingLeft: 10,
  backgroundColor: '#FFFFFFDB',
});
const ModalOptionsSuscribe = styled.View({
  height: '50%',
  width: '32%',
  position: 'absolute',
  right: 5,
  top: 5,
  zIndex: 2,
  borderRadius: 16,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  paddingLeft: 10,
  backgroundColor: '#FFFFFFDB',
});
const AbandonBtn = styled.TouchableOpacity({
  width: '100%',
  position: 'absolute',
  left: 10,
  bottom: 25,
});
const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: 12,
  top: 12,
});
const BlockBtn = styled.TouchableOpacity({
  marginTop: '2%',
  minWidth: '100%',
  position: 'absolute',
  left: 10,
  bottom: 15,
});

const RemoveBtn = styled.TouchableOpacity({
  marginTop: '2%',
  minWidth: '100%',
  position: 'absolute',
  left: 10,
  bottom: 60,
});

const EditBtn = styled.TouchableOpacity({
  marginTop: '2%',
  minWidth: '100%',
  position: 'absolute',
  left: 10,
  bottom: 38,
});

const Divider = styled.View({
  borderBottomColor: '#6536BB4D',
  borderBottomWidth: 1,
  width: '90%',
});
const ReportBtn = styled.TouchableOpacity({
  width: '100%',
  position: 'absolute',
  left: 10,
  bottom: 82,
});
const BgImage = styled.ImageBackground({
  width: '100%',
  height: 144,
  flex: 1,
  alignSelf: 'flex-end',
  alignItems: 'flex-end',
});
const ContainIconBanner = styled.View({
  flexDirection: 'row',
  marginTop: '2%',
  marginRight: '4%',
});

const Avatar = styled.TouchableOpacity({
  width: 60,
  height: 60,
  marginTop: -28,
  marginLeft: 25,
  borderRadius: 48,
  borderColor: colors.white,
  backgroundColor: colors.black,
  alignItems: 'center',
  justifyContent: 'center',
  overflow: 'hidden',
  borderWidth: 3,
});

const UserImage = styled.Image({
  width: '100%',
  height: '100%',
  opacity: 1,
});

const JoinedAvatar = styled.Image({width: 26, height: 26, opacity: 1, resizeMode: 'cover', borderRadius: 40});
const JoinedAvatar2 = styled.Image({
  width: 26,
  height: 26,
  opacity: 1,
  resizeMode: 'cover',
  borderRadius: 40,
});

const PurpleCircle = styled.View({
  width: 26,
  height: 26,
  borderRadius: 50,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: colors.purpleBackgroundHalfOpacity,
});

const ThirdMemberAvatar = styled.View({
  width: 18,
  height: 18,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: colors.pinkMagenta,
  borderRadius: 50,
});

const JoinedWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: 3,
});

const MoreMembersText = styled.Text({
  color: '#fff',
});
const TextMember = styled.Text({...Fonts.avenirSemiBold, color: colors.white, fontSize: 12, alignSelf: 'center'});
const OneViewProjectScreen = ({navigation, route}) => {
  const {params} = route;
  const {projectId} = params;
  const [show, setShow] = useState(false);
  const [showItem, setShowItem] = useState('experiences');
  const Style = StyleSheet.create({
    loadingContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    container: {
      flex: 1,
    },
    mainContainer: {
      flex:1,
    },
    vectorIcon: {
      marginLeft: '2%',
      display: 'flex',
      alignSelf: 'center',
    },
    iconsBanner: {
      width: 5,
      height: 5,
      marginRight: 0.5,
      marginLeft: 0.5,
    },
    icons: {
      width: 32,
      height: 32,
    },
    contentPrimary: {
      paddingRight: '5%',
      position: 'relative',
      bottom: 50,
      alignItems: 'center',
    },
    contentPrimarySuscribe: {
      right: 90,
      paddingRight: '5%',
      position: 'relative',
      bottom: 35,
      alignItems: 'center',
    },
    contentPrimaryNotSuscribe: {
      paddingRight: '5%',
      position: 'relative',
      bottom: 35,
      alignItems: 'center',
    },
    spaceLeftContainPrimary: {
      marginTop: '3%',
    },
    ContainSubCardSecondary: {
      marginTop: '5%',
      flexDirection: 'column',
      alignContent: 'center',
    },
    spaceMemberContain: {
      flexDirection: 'row',
      marginBottom: '2%',
    },
    spaceTextSecondaryCardOne: {
      flexDirection: 'row',
      marginTop: '1%',
      marginLeft: '4%',
    },
    spaceTextSecondaryCardTwo: {
      flexDirection: 'row',
      marginTop: '1%',
      marginLeft: '1%',
    },
    regularBtn: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: 105,
      height: 30,
      alignSelf: 'center',
      marginTop: '20%',
      fontSize: 9,
      lineHeight: 10,
      fontWeight: 'bold',
      color: colors.white,
      borderRadius: 10,
    },
    containDescription: {
      marginLeft: '17%',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop: '2%',
    },
    ContainSubTextVectorReverse: {
      flexDirection: 'column',
      marginLeft: '17%',
    },
    containFatherSubCardSecondary: {
      display: 'flex',
      alignSelf: 'center',
    },
  });
  const {data: {getProjectById = []} = {getProjectById: []}, loading} = useQuery(GET_PROJECT_BY_ID, {
    variables: {
      projectId,
    },
    fetchPolicy: 'network-only',
   
   
  
  
  
  });
    
  const [joinProject] = useMutation(JOIN_PROJECT, {
    refetchQueries: [{query: GET_PROJECT_BY_ID, variables: {projectId: getProjectById?.id}}],
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  useEffect(()=>{
    console.log(getProjectById?.suscribeUsers)
  },[getProjectById])

  const [leaveProject] = useMutation(LEAVE_PROJECT, {
    refetchQueries: [{query: GET_PROJECT_BY_ID, variables: {projectId: getProjectById?.id}}],
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const projectLeave = async (id) => {
    await leaveProject({
      variables: {
        projectId: id,
      },
    });
    setShow(false);
  };

  const projectJoin = async (id) => {
    await joinProject({
      variables: {
        projectId: id,
      },
    });
  };

  const [endProject] = useMutation(END_PROJECT, {
    refetchQueries: [{query: GET_ALL_PROJECT}],
    onCompleted() {
      navigation.replace('MainTabs', {screen: 'Home'});
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });
  const deleteProject = async (id) => {
    await endProject({
      variables: {
        projectId: id,
      },
    });
  };
  const handleAdd = (getProjectByIdUser) => {
    navigation.replace('ProjectStack', {
      screen: 'EnhanceProjectScreen',
      params: {projectOneparams: getProjectByIdUser},
    });
  };

  if (loading) {
    return (
      <View style={Style.loadingContainer}>
        <ActivityIndicator size="large" color={colors.white} />
      </View>
    );
  }

  if(!loading){
    return (
      <View style={Style.mainContainer}>
        <BackButton style={{marginTop: '2%', position:'absolute'}} onPress={() => navigation.replace('MainTabs', {screen: 'Home'})} />
        <ScrollView style={Style.container}>
          <BgImage
            source={
              getProjectById.coverImage !== null || getProjectById.coverImage !== ''
                ? {uri: getProjectById.coverImage}
                : coverImage
            }
            resizeMode="cover">
            {((!show && getProjectById.ownerProject) || getProjectById.suscribeProject) && (
              <ContainIconBanner>
                <Dots onPress={() => setShow(true)} />
              </ContainIconBanner>
            )}
            {show && getProjectById.ownerProject && (
              <ModalOptionsOwner>
                <CloseButton onPress={() => setShow(false)}>
                  <ClosePurpleIcon style={{width: 12, height: 12}} />
                </CloseButton>
                <ReportBtn onPress={() => null}>
                  <Text style={{color: colors.purple}}>Invite</Text>
                  <Divider />
                </ReportBtn>
                <EditBtn onPress={() => navigation.navigate('CreationProjectScreen', {projectOneparams: getProjectById})}>
                  <Text style={{color: colors.purple}}>Edit</Text>
                  <Divider />
                </EditBtn>
                <RemoveBtn onPress={() => {}}>
                  <Text style={{color: colors.purple}}>Remove</Text>
                  <Divider />
                </RemoveBtn>
                <BlockBtn onPress={() => deleteProject(getProjectById.id)}>
                  <Text style={{color: colors.purple}}>Delete</Text>
                  <Divider />
                </BlockBtn>
              </ModalOptionsOwner>
            )}
            {show && getProjectById.suscribeProject && (
              <ModalOptionsSuscribe>
                <CloseButton onPress={() => setShow(false)}>
                  <ClosePurpleIcon style={{width: 12, height: 12}} />
                </CloseButton>
                <AbandonBtn onPress={() => projectLeave(getProjectById?.id)}>
                  <Text style={{color: colors.purple}}>Abandon Project</Text>
                </AbandonBtn>
              </ModalOptionsSuscribe>
            )}
          </BgImage>
          <Card suscribeProject={getProjectById?.suscribeProject}>
            <AvatarImage
              source={
                getProjectById.tile !== null || getProjectById.tile !== '' ? {uri: getProjectById.tile} : coverImage
              }
              resizeMode="cover"
            />
            {getProjectById.ownerProject === true && (
              <>
                <View style={{flexDirection: 'row', justifyContent: 'flex-start', marginBottom: '5%'}}>
                  <View style={{flexDirection: 'column', marginLeft: '5%', marginTop: '1%'}}>
                    <TextMember>Members joined</TextMember>
                    <JoinedWrapper
                      style={{
                        transform: [
                          {
                            translateX:
                              getProjectById?.suscribeUsers.length <= 3 ? getProjectById?.suscribeUsers.length * 3 : 9,
                          },
                        ],
                      }}>
                      {getProjectById?.suscribeUsers.length <= 0 && <TextMember>Not members joined yet</TextMember>}
                      {getProjectById?.suscribeUsers.length > 0 && (
                        <JoinedAvatar
                          source={
                            getProjectById?.suscribeUsers[0].avatar
                              ? {uri: getProjectById?.suscribeUsers[0].avatar}
                              : avatarSrc
                          }
                        />
                      )}
                    
                      {getProjectById?.suscribeUsers.length > 1 && (
                        
                        <JoinedAvatar2
                          source={
                            getProjectById?.suscribeUsers[1].avatar
                              ? {uri: getProjectById?.suscribeUsers[1].avatar}
                              : avatarSrc
                          }
                          style={{transform: [{translateX: -9}]}}
                        />
                      )}
                      {getProjectById?.suscribeUsers.length > 2 && (
                        <PurpleCircle style={{transform: [{translateX: -18}]}}>
                          <ThirdMemberAvatar>
                            <MoreMembersText>
                              +{getProjectById?.suscribeUsers.length > 11 ? 9 : getProjectById?.suscribeUsers.length - 2}
                            </MoreMembersText>
                          </ThirdMemberAvatar>
                        </PurpleCircle>
                      )}
                    </JoinedWrapper>
                  </View>
                </View>
                <ContainPrimaryOwner>
                  <View style={Style.spaceLeftContainPrimary}>
                    <TitleCard pass={getProjectById.ownerProject} ellipsizeMode="tail" numberOfLines={1}>
                      {getProjectById.name}
                    </TitleCard>
                    <SubText ellipsizeMode="tail" numberOfLines={4}>
                      {getProjectById.description}
                    </SubText>
                  </View>
                  <View style={Style.contentPrimary}>
                    <HeartRoundedIcon style={Style.icons} />
                    <TextHeartIcon>Save to Favorites</TextHeartIcon>
                    <>
                      <AddRoundedIcon onPress={() => handleAdd(getProjectById)} style={Style.icons} />
                      <TextAddIcon>Add Experiences, Communities and Collections to this Project</TextAddIcon>
                    </>
                  </View>
                </ContainPrimaryOwner>
              </>
            )}
            {getProjectById.ownerProject === false && (
              <>
                <View style={Style.spaceMemberContain}>
                  <Avatar activeOpacity={1}>
                    <UserImage
                      source={getProjectById?.user ? {uri: getProjectById?.user?.avatar} : coverImage}
                      style={{resizeMode: 'cover'}}
                    />
                  </Avatar>
                  <View
                    style={{flexDirection: 'column', marginLeft: '2%', marginTop: '1%', width: 'auto', height: 'auto'}}>
                    <TextMember>Members joined</TextMember>
                    <JoinedWrapper
                      style={{
                        transform: [
                          {
                            translateX:
                              getProjectById?.suscribeUsers.length <= 3 ? getProjectById?.suscribeUsers.length * 3 : 9,
                          },
                        ],
                      }}>
                      {getProjectById?.suscribeUsers.length <= 0 && <TextMember>Not members joined yet</TextMember>}
                      {getProjectById?.suscribeUsers.length > 0 && (
                        <JoinedAvatar
                          source={
                            getProjectById?.suscribeUsers[0].avatar
                              ? {uri: getProjectById?.suscribeUsers[0].avatar}
                              : avatarSrc
                          }
                        />
                      )}
                      {getProjectById?.suscribeUsers.length > 1 && (
                        <JoinedAvatar2
                          source={
                            getProjectById?.suscribeUsers[1].avatar
                              ? {uri: getProjectById?.suscribeUsers[1].avatar}
                              : avatarSrc
                          }
                          style={{transform: [{translateX: -9}]}}
                        />
                      )}
                      {getProjectById?.suscribeUsers.length > 2 && (
                        <PurpleCircle style={{transform: [{translateX: -18}]}}>
                          <ThirdMemberAvatar>
                            <MoreMembersText>
                              +{getProjectById?.suscribeUsers.length > 11 ? 9 : getProjectById?.suscribeUsers.length - 2}
                            </MoreMembersText>
                          </ThirdMemberAvatar>
                        </PurpleCircle>
                      )}
                    </JoinedWrapper>
                  </View>
                </View>
                <ContainPrimaryOwner>
                  <View style={Style.spaceLeftContainPrimary}>
                    <TitleCard pass={getProjectById.ownerProject} ellipsizeMode="tail" numberOfLines={1}>
                      {getProjectById.name}
                    </TitleCard>
                    <SubText ellipsizeMode="tail" numberOfLines={4}>
                      {getProjectById.description}
                    </SubText>
                    {getProjectById.suscribeProject && (
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          marginTop: '5%',
                          paddingHorizontal: '4%',
                        }}>
                        <SubTextMember>{TEXT_MEMBER_PROJECT}</SubTextMember>
                        <ManageBottoms
                          navigation={navigation}
                          suscribeUsers={getProjectById?.suscribeUsers}
                          projects={getProjectById}
                        />
                      </View>
                    )}
                  </View>
                  <View
                    style={
                      getProjectById?.suscribeProject ? Style.contentPrimarySuscribe : Style.contentPrimaryNotSuscribe
                    }>
                    <HeartRoundedIcon style={Style.icons} />
                    <TextHeartIcon>Save to Favorites</TextHeartIcon>
                    {!getProjectById.suscribeProject && (
                      <RegularButton
                        onPress={() => {
                          projectJoin(getProjectById.id);
                        }}
                        disabled={!!getProjectById.suscribeProject}
                        custom
                        font="bold"
                        title="Join Project"
                        style={[
                          Style.regularBtn,
                          getProjectById.suscribeProject === false
                            ? {backgroundColor: colors.newPink}
                            : {backgroundColor: colors.disabledPink},
                        ]}
                      />
                    )}
                  </View>
                </ContainPrimaryOwner>
              </>
            )}
          </Card>
          <ContainMenuItem>
            <MenuItemBottom onPress={() => setShowItem('experiences')}>
              <TextMenuItem>Experiences</TextMenuItem>
            </MenuItemBottom>
            <MenuItemBottom onPress={() => setShowItem('community')}>
              <TextMenuItem>Communities</TextMenuItem>
            </MenuItemBottom>
            <MenuItemBottom onPress={() => setShowItem('collection')}>
              <TextMenuItem>Collections</TextMenuItem>
            </MenuItemBottom>
          </ContainMenuItem>
          {showItem === 'collection' && getProjectById?.nft?.map((item, index) => {
            return <NftItem item={{...item, ownerUser: {...getProjectById?.user}}} isFeed key={`${Math.random()+index}`} />
          })}
          {showItem === 'community' && getProjectById?.team?.map((item, index) => {
            return <CommunityCard item={{...item, ownerUser: {...getProjectById?.user}}} key={`${Math.random()+index}`} />
          })}
          {showItem === 'experiences' && getProjectById?.experience?.map((item, index) => {
                if(item.typeExperience === 'events'){
                  return <EventItem event={{...item, ownerUser: {...getProjectById?.user}}} enable key={`${Math.random()+index}`}/>
                }
                if(item.typeExperience === 'sessions'){
                  return <SessionsItem session={{...item, ownerUser: {...getProjectById?.user}}} key={`${Math.random()+index}`}/>
                }
                if(item.typeExperience === 'series'){
                  return <SeriesItem enable series={{...item, ownerUser: {...getProjectById?.user}}} />
                }
                if(item.typeExperience === 'channels'){
                  return <ChannelItem enable series={{...item, ownerUser: {...getProjectById?.user}}} />
                }
                return null
              }
            )
          }
        </ScrollView>
      </View>
    );
  }
};
export default withSafeArea(OneViewProjectScreen);
