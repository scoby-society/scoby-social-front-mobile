import React, {useState, useCallback, useEffect} from 'react';
import {StyleSheet, ScrollView, View, Alert} from 'react-native';
import {requestCameraPermission} from 'src/utils/permission/allPermission';
import ScobyImage from 'assets/images/ScobyAvatar.png';
import ImagePicker from 'react-native-image-crop-picker';
import styled from 'styled-components';
import withSafeArea from 'src/components/withSafeArea';
import {
  TITLE_DECORATE_PROJECT,
  TITLE_BACKGROUND_PROJECT,
  SUBTITLE_BACKGROUND_PROJECT,
  CAMERA,
  GALLERY,
  PHOTO_CHOOSE,
  FONT_TITLE_AVATAR_PROJECT,
} from 'src/constants/Texts';
import EditableHeaderWithImage from 'src/screens/Profile/components/EditableHeaderWithImage';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import BackButton from 'src/components/BackButton';
import PromptModal from 'src/components/Modal/PromptModal';
// const ContainerButton = styled.View(({height}) => {(
//   minHeight:height,
//   flexDirection: 'row',
//   justifyContent: 'space-around',
//   marginBottom: 20,
// )});
const TopContent = styled.TouchableOpacity({
  marginTop: '3%',
  width: 317,
  height: 305,
  borderWidth: 4,
  borderRadius: 40,
  borderColor: colors.transparent,
  backgroundColor: colors.transparent,
  alignSelf: 'center',
  overflow: 'hidden',
});
const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  marginTop: '5%',
  width: '100%',
  paddingHorizontal: '7%',
  marginBottom: '5%',
});
const Card = styled.View({
  backgroundColor: colors.blueCardProject,
  width: '348px',
  height: '530px',
  borderRadius: 40,
  marginHorizontal: '5%',
  marginVertical: '5%',
  position: 'relative',
  bottom: '8%',
  alignSelf: 'center',
});
const ContainerText = styled.View({
  position: 'absolute',
  width: '100%',
  height: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  opacity: 0.7,
});
const AvatarImage = styled.Image(() => ({
  backgroundColor: colors.white,
  width: '100%',
  height: '100%',
}));
const TapText = styled.Text({
  fontWeight: '400',
  lineHeight: '25px',
  ...Fonts.avenir,
  color: colors.blueBackgroundSession,
  fontSize: 20,
});
const TitleCard = styled.Text({
  fontWeight: '750',
  paddingHorizontal: '6%',
  ...Fonts.avenirBold,
  lineHeight: '20px',
  fontSize: 16,
  color: colors.white,
  marginTop: '10%',
  width: '100%',
});
const SubText = styled.Text(({experience = 12}) => ({
  width: '80%',
  paddingHorizontal: '6%',
  marginTop: 5,
  ...Fonts.avenir,
  fontSize: experience,
  textAlign: 'left',
  color: colors.white,
}));
const DecorateProjectScreen = ({navigation, route}) => {
  const {params} = route;
  const {name, description, projectOneparams} = params;
  const [avatar, setAvatar] = useState(projectOneparams ? projectOneparams.tile : null);
  const [cover, setCover] = useState(projectOneparams ? projectOneparams.coverImage : null);

  const [modalVisible, setModalVisible] = useState(undefined);
  const Style = StyleSheet.create({
    containerButton: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: '20%',
    },
    regularBtn: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: 125,
      height: 45,
      alignSelf: 'center',
      borderRadius: 8,
      ...Fonts.avenir,
      fontWeight: 'bold',
    },
    container: {
      flex: 1,
    },
    icons: {
      width: 120,
      height: 120,
    },
    mainContainer: {
      paddingTop: '2%',
      width: '100%',
      height: '100%',
    },
  });
  const setImage = useCallback(
    (response, bg) => {
      if (!response.didCancel && !response.errorCode && !response.errorMessage) {
        if (bg) {
          setAvatar(response.path);
        }
      } else if (response.errorMessage) {
        Alert.alert('Image Error', response.errorMessage);
      } else {
        Alert.alert('Error', 'Wrong');
      }
    },
    [setAvatar],
  );

  const handleNext = () => {
    navigation.navigate('CollectionProjectScreen', {...params, avatar, cover});
  };

  const chooseImage = useCallback(
    (bg, type) => {
      if (type === 'CAMERA') {
        ImagePicker.openCamera({
          compressImageMaxHeight: bg ? 600 : 1000,
          compressImageMaxWidth: bg ? 1000 : 1000,
          mediaType: 'photo',
          useFrontCamera: true,
          cropperCircleOverlay: !bg,
          freeStyleCropEnabled: true,
          cropping: true,
          forceJpg: true,
        }).then((response) => setImage(response, bg));
      }

      if (type === 'GALERY') {
        ImagePicker.openPicker({
          compressImageMaxHeight: bg ? 600 : 1000,
          compressImageMaxWidth: bg ? 1000 : 1000,
          mediaType: 'photo',
          cropperCircleOverlay: !bg,
          freeStyleCropEnabled: true,
          cropping: true,
          forceJpg: true,
        }).then((response) => setImage(response, bg));
      }
    },
    [setImage],
  );

  const handleChangeCover = useCallback(
    (type) => {
      setModalVisible(undefined);
      setTimeout(() => chooseImage(true, type), 500);
    },
    [chooseImage],
  );

  const handleChangeAvatar = useCallback(
    (type) => {
      setModalVisible(undefined);
      setTimeout(() => chooseImage(false, type), 500);
    },
    [chooseImage],
  );

  const handleChoise = useCallback(
    (type) => {
      modalVisible === 'COVER' ? handleChangeCover(type) : handleChangeAvatar(type);
    },
    [modalVisible, handleChangeAvatar],
  );

  useEffect(() => {
    requestCameraPermission();
  }, []);

  const hideModal = useCallback(
    (state) => {
      setModalVisible(state ? 'AVATAR' : undefined);
    },
    [setModalVisible],
  );

  return (
    <View style={Style.mainContainer}>
      <PromptModal
        visible={modalVisible !== undefined}
        setVisible={hideModal}
        text={PHOTO_CHOOSE}
        leftButtonText={CAMERA}
        rightButtonText={GALLERY}
        onLeftButtonPress={() => {
          handleChoise('CAMERA');
          setModalVisible(undefined);
        }}
        onRightButtonPress={() => {
          handleChoise('GALERY');
          setModalVisible(undefined);
        }}
      />
      <BackButton onPress={() => navigation.goBack()} />
      <ScrollView style={Style.container}>
        <Title>{TITLE_DECORATE_PROJECT}</Title>
        <EditableHeaderWithImage
          cover={cover}
          setCover={setCover}
          hasAvatar
          text={TITLE_BACKGROUND_PROJECT}
          subText={SUBTITLE_BACKGROUND_PROJECT}
          customSize={true}
        />
        <Card>
          <TopContent activeOpacity={1} onPress={() => setModalVisible('COVER')}>
            <AvatarImage source={avatar ? {uri: avatar} : ScobyImage} resizeMode="cover" />
            <ContainerText>
              <TapText>{FONT_TITLE_AVATAR_PROJECT}</TapText>
            </ContainerText>
          </TopContent>
          <TitleCard>{name}</TitleCard>
          <SubText>{description}</SubText>
        </Card>
      </ScrollView>
        <View style={Style.containerButton}>
          <RegularButton
            onPress={() => navigation.goBack()}
            title="Back"
            style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
          />
          <RegularButton
            title="Next"
            disabled={!!(cover === null || avatar === null)}
            onPress={() => handleNext(navigation)}
            style={[
              Style.regularBtn,
              cover === null || avatar === null
                ? {backgroundColor: colors.disabledPink}
                : {backgroundColor: colors.newPink},
            ]}
          />
        </View>
    </View>
  );
};

export default withSafeArea(DecorateProjectScreen);
