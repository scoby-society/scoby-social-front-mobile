import React, {useState} from 'react';
import {StyleSheet, ScrollView, View, Linking} from 'react-native';
import withSafeArea from 'src/components/withSafeArea';
import styled from 'styled-components';
import {
  TITLE_ENHANCE_PROJECT,
  TITLE_EXPERIENCE_ENHANCE_PROJECT,
  SUBTITLE_EXPERIENCE_ENHANCE_PROJECT,
  TITLE_COMMUNITY_ENHANCE_PROJECT,
  SUBTITLE_COMMUNITY_ENHANCE_PROJECT,
  TITLE_COLLECTIONS_ENHANCE_PROJECT,
  SUBTITLE_COLLECTIONS_ENHANCE_PROJECT,
  CREATOR_PROMPT_TEXT,
  CREATOR_APPLY_NOW,
  CREATOR_HOST_SESSION,
  SCOBY_PLUS,
} from 'src/constants/Texts';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import PromptModal from 'src/components/Modal/PromptModal';
import RegularButton from 'src/components/RegularButton';

const Style = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '7%',
  },
  icons: {
    width: 120,
    height: 120,
  },
  mainContainer: {
    width: '100%',
    height: '94%',
  },
  containerButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
});

const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  marginTop: '10%',
  width: '100%',
});

const SubTitle = styled.Text(({experience = 14}) => ({
  marginTop: 10,
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
}));

const CadenceTitle = styled.Text(({experience = 20}) => ({
  ...Fonts.avenirBold,
  fontSize: experience,
  color: colors.white,
}));

const Cadence = styled.TouchableOpacity(({type}) => ({
  height: 160,
  borderBottomColor: '#FFFFFF40',
  borderBottomWidth: type === 'middle' ? 1 : 0,
  borderTopColor: '#FFFFFF40',
  borderTopWidth: type === 'middle' ? 1 : 0,
  justifyContent: 'center',
}));

const CadenceTextDirection = styled.View({
  flexDirection: 'column',
  width: '80%',
});

const ContainerSelected = styled.View(({state}) => ({
  backgroundColor: state ? colors.primaryPurpleColor : colors.blueBackgroundSession,
  borderRadius: 20,
  paddingHorizontal: 5,
  paddingVertical: 5,
  width: '100%',
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'space-around',
}));

const EnhanceProjectScreen = ({navigation, route}) => {
  const {params} = route;
  const [experience, setExperience] = useState('');
  const {unTitleWeight, titleWeight} = {unTitleWeight: 20, titleWeight: 23};
  const {unSubTitleWeight, subTitleWeight} = {unSubTitleWeight: 16, subTitleWeight: 18};
  const [modalVisible, setModalVisible] = useState(false);

  const handlerToNameSession = () => {
    if (experience === 'experience') {
      navigation.navigate('CreateSession', {...params});
    }
    if (experience === 'community') {
      navigation.navigate('NameTeam', {...params});
    }
    if (experience === 'collections') {
      navigation.navigate('CollectionProjectScreen', {...params});
    }
    // if(experience === 'programs'){

    //   navigation.navigate('CreateNewProgram',{...params});

    // }
  };

  return (
    <View style={Style.mainContainer}>
      <PromptModal
        visible={modalVisible}
        setVisible={setModalVisible}
        text={CREATOR_PROMPT_TEXT}
        leftButtonText={CREATOR_HOST_SESSION}
        rightButtonText={CREATOR_APPLY_NOW}
        onLeftButtonPress={async () => {
          setModalVisible(false);
          navigation.navigate('SessionName');
        }}
        onRightButtonPress={() => {
          setModalVisible(false);
          Linking.openURL(SCOBY_PLUS);
        }}
      />
      <ScrollView style={Style.container}>
        <Title>{TITLE_ENHANCE_PROJECT}</Title>
        <Cadence
          type="middle"
          onPress={() => {
            setExperience('experience');
          }}>
          <ContainerSelected state={experience === 'experience'}>
            <CadenceTextDirection>
              <CadenceTitle experience={experience === 'experience' ? titleWeight : unTitleWeight}>
                {TITLE_EXPERIENCE_ENHANCE_PROJECT}
              </CadenceTitle>
              <SubTitle experience={experience === 'experience' ? subTitleWeight : unSubTitleWeight}>
                {SUBTITLE_EXPERIENCE_ENHANCE_PROJECT}
              </SubTitle>
            </CadenceTextDirection>
          </ContainerSelected>
        </Cadence>
        <Cadence
          type="middle"
          onPress={() => {
            setExperience('community');
          }}>
          <ContainerSelected state={experience === 'community'}>
            <CadenceTextDirection>
              <CadenceTitle experience={experience === 'community' ? titleWeight : unTitleWeight}>
                {TITLE_COMMUNITY_ENHANCE_PROJECT}
              </CadenceTitle>
              <SubTitle experience={experience === 'community' ? subTitleWeight : unSubTitleWeight}>
                {SUBTITLE_COMMUNITY_ENHANCE_PROJECT}
              </SubTitle>
            </CadenceTextDirection>
          </ContainerSelected>
        </Cadence>
        <Cadence
          onPress={() => {
            setExperience('collections');
          }}>
          <ContainerSelected state={experience === 'collections'}>
            <CadenceTextDirection>
              <CadenceTitle experience={experience === 'collections' ? titleWeight : unTitleWeight}>
                {TITLE_COLLECTIONS_ENHANCE_PROJECT}
              </CadenceTitle>
              <SubTitle experience={experience === 'collections' ? subTitleWeight : unSubTitleWeight}>
                {SUBTITLE_COLLECTIONS_ENHANCE_PROJECT}
              </SubTitle>
            </CadenceTextDirection>
          </ContainerSelected>
        </Cadence>
        {/* <Cadence
          
          onPress={() => {
           
            setExperience('programs');
          }}>
          <ContainerSelected state={experience === 'programs'}>
            <CadenceTextDirection>
              <CadenceTitle experience={experience === 'programs' ? titleWeight : unTitleWeight}>
                {TITLE_PROGRAMS_ENHANCE_PROJECT}
              </CadenceTitle>
              <SubTitle experience={experience === 'programs' ? subTitleWeight : unSubTitleWeight}>
                {SUBTITLE_PROGRAMS_ENHANCE_PROJECT}
              </SubTitle>
            </CadenceTextDirection>
          </ContainerSelected>
        </Cadence> */}
        <View style={Style.containerButton}>
          <RegularButton
            onPress={() => navigation.replace('MainTabs', {screen: 'Home'})}
            title="Back"
            style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
          />
          <RegularButton
            title="Next"
            onPress={handlerToNameSession}
            disabled={experience === ''}
            style={[
              Style.regularBtn,
              experience === '' ? {backgroundColor: colors.disabledPink} : {backgroundColor: colors.newPink},
            ]}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default withSafeArea(EnhanceProjectScreen);
