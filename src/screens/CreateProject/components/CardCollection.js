import React, {useMemo} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import styled from 'styled-components';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import coverImage from 'assets/images/profile/Cover.png';
import {CheckBoxIcon, UnCheckCirculeIcon} from 'assets/svg';

const Card = styled.View({
  backgroundColor: colors.blueBackgroundSession,
  borderColor: colors.blueCardProject,
  borderWidth: 2,
  width: '175px',
  height: '262px',
  borderRadius: 30,
  marginVertical: '2%',
  marginHorizontal: '2%',
});
const AvatarImage = styled.Image(() => ({
  width: '155px',
  height: '155px',
  borderRadius: 22,
}));
const TopContent = styled.View({
  marginTop: '3%',
  borderWidth: 4,
  borderColor: colors.transparent,
  backgroundColor: colors.transparent,
  alignSelf: 'center',
});
export const CardCollection = ({item, checked, setChecked}) => {
  const Style = StyleSheet.create({
    TextTitleCard: {
      ...Fonts.avenir,
      lineHeight: 20,
      fontSize: 13,
      color: colors.white,
      opacity: checked ? 1 : 0.8,
      marginTop: '18%',
      width: 100,
    },
    TextTitleCard2: {
      fontWeight: '750',
      ...Fonts.avenir,
      lineHeight: 16,
      fontSize: 11,
      color: colors.white,
      opacity: checked ? 1 : 0.8,
      marginTop: '17%',
    },
    SubTitleCard: {
      lineHeight: 30,
      ...Fonts.avenir,
      fontSize: 12,
      color: colors.usernameGrey,
      position: 'relative',
      bottom: 6,
      opacity: checked ? 1 : 0.8,
      marginLeft: '3%',
    },
    checkedIcon: {
      alignSelf: 'center',
    },
    columnContentCard: {
      flexDirection: 'column',
    },
    contentCard: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: '5%',
      width: '90%',
    },
    containerButton: {
      marginBottom: '10%',
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    regularBtn: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: 125,
      height: 45,
      alignSelf: 'center',
      borderRadius: 8,
      marginTop: 12,
      ...Fonts.avenir,
      fontWeight: 'bold',
    },
    container: {
      flex: 1,
    },
    icons: {
      width: 120,
      height: 120,
    },
    mainContainer: {
      width: '100%',
      height: '100%',
    },
  });

  const unSelect = () => {
    const unCheck = checked.filter((e) => e.id !== item.id);
    setChecked(unCheck);
  };

  const select = () => {
    const isCollection = checked.filter((e) => e.symbol === item.symbol).length > 0;

    if (isCollection) {
      const eraseItem = checked.filter((e) => e.symbol !== item.symbol);
      setChecked([...eraseItem, item]);
    } else {
      setChecked((e) => [...e, item]);
    }
  };

  const isSelected = useMemo(() => {
    const getItem = checked.filter((e) => e.id === item.id);
    if (getItem.length > 0) {
      return true;
    }
    return false;
  }, [checked, item.id]);

  return (
    <Card>
      <TopContent>
        <AvatarImage
          source={item?.urlImage !== null || item?.urlImage !== '' ? {uri: item?.urlImage} : coverImage}
          resizeMode="cover"
        />
        <View style={Style.contentCard}>
          <View style={Style.columnContentCard}>
            <Text style={Style.TextTitleCard} numberOfLines={1}>
              {item?.name}
            </Text>
            <Text style={Style.SubTitleCard}>{`#${item?.serialNumber}`}</Text>
          </View>
          <View style={Style.columnContentCard}>
            <Text style={Style.TextTitleCard2}>Select</Text>
            <View style={Style.checkedIcon}>
              {isSelected ? (
                <CheckBoxIcon style={Style.checkedIcon} onPress={unSelect} />
              ) : (
                <UnCheckCirculeIcon onPress={select} />
              )}
            </View>
          </View>
        </View>
      </TopContent>
    </Card>
  );
};
