import React from 'react';
import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';
import coverImage from 'assets/images/profile/Cover.png';
import styled from 'styled-components';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import CommunityCard from 'src/components/HomeCards/CommunityCard';
import EventItem from 'src/components/Event/EventItem';
import NftItem from 'src/components/Nfts/nftItem';
import ChannelItem from 'src/components/Channel/ChannelItem';
import FeedItem from 'src/components/Sessions/Sessions';
import SerieHome from 'src/components/Series';

const ContainPrimary = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  zIndex: -1,
});
const Card = styled.View({
  backgroundColor: colors.blueCardProject,
  marginTop: 30,
  width: '360px',
  paddingBottom: 20,
  borderRadius: 40,
  marginHorizontal: '5%',
  alignSelf: 'center',
  position: 'relative',
  bottom: '4%',
  flex: 1,
  marginVertical: '5%',
  zIndex: -1000,
});
const TitleCard = styled.Text({
  paddingHorizontal: '7%',
  ...Fonts.avenirBold,
  lineHeight: '20px',
  fontSize: 16,
  color: colors.white,
  marginTop: '8%',
  maxWidth: '250px',
  
});
const SubText = styled.Text(({experience = 10}) => ({
  color: colors.white,
  paddingHorizontal: '5%',
  width: '360px',
  marginTop: '10%',
  ...Fonts.avenir,
  fontSize: experience,
  textAlign: 'left',
}));
const AvatarImage = styled.Image(() => ({
  marginTop: '5%',
  alignSelf: 'center',
  width: '315px',
  height: '315px',
  borderRadius: 40,
}));
export const CardHomeProject = ({dato: {item}, navigation}) => {
  const category = item.typeData !== undefined ? item.typeData : '';

  const Style = StyleSheet.create({
    vectorIcon: {
      marginLeft: '2%',
      display: 'flex',
      alignSelf: 'center',
    },
    iconsBanner: {
      width: 5,
      height: 5,
      marginRight: 0.5,
      marginLeft: 0.5,
    },
    icons1: {
      width: 32,
      height: 32,
      marginRight: '6%',
      left: -70,
    },
    icons2: {
      width: 32,
      height: 32,
      left: -60,
    },
    contentPrimary: {
      flexDirection: 'row',
      position: 'relative',
      right: 60,
      bottom: 18,
      alignItems: 'baseline',
      justifyContent: 'space-around',
    },
    spaceLeftContainPrimary: {
      marginTop: '3%',
    },
    ContainSubCardSecondary: {
      marginTop: '5%',
      flexDirection: 'column',
      alignContent: 'center',
    },
    spaceTextSecondaryCardOne: {
      flexDirection: 'row',
      marginTop: '1%',
      marginLeft: '4%',
    },
    spaceTextSecondaryCardTwo: {
      flexDirection: 'row',
      marginTop: '1%',
      marginLeft: '1%',
    },
    regularBtn: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      width: 43,
      height: 20,
      alignSelf: 'center',
      marginTop: '12%',
      fontSize: 10,
      lineHeight: 10,
      fontWeight: 'bold',
      color: colors.white,
    },
    containDescription: {
      marginLeft: '17%',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop: '2%',
    },
    ContainSubTextVectorReverse: {
      flexDirection: 'column',
      marginLeft: '17%',
    },
    containFatherSubCardSecondary: {
      display: 'flex',
      alignSelf: 'center',
    },
    cardWrapper: {
      backgroundColor: 'red',
      width: 100,
      height: 50,
    },
    textBtn: {
      ...Fonts.avenir,
      fontWeight: '700',
      fontSize: 12,
      color: colors.white,
    },
    btn: {
      width: 87,
      height: 27,
      backgroundColor: '#CD068E',
      textAlign: 'center',
      justifyContent: 'center',
      alignItems: 'center',
      top: 30,
      right: 40,
      borderRadius: 6,
      position: 'absolute',
    },
    wrap: {
      flexDirection: 'row',
    },
  });

  const handleNext = (id) => {
    navigation.replace('ProjectStack', {screen: 'OneViewProjectScreen', params: {projectId: id}});
  };

  const DisplayExperiences = ({itemExperience}) => {
    if (itemExperience.typeData.includes('experience')) {
      if (itemExperience.typeExperience.includes('series')) {
        return <SerieHome series={{...itemExperience}} enable />;
      }
      if (itemExperience.typeExperience.includes('event')) {
        return <EventItem event={itemExperience} enable />;
      }
      if (itemExperience.typeExperience.includes('channels')) {
        return <ChannelItem channel={itemExperience} enable />;
      }
      if (itemExperience.typeExperience.includes('sessions')) {
        return <FeedItem session={itemExperience} enable />;
      }
      return <></>;
    }
    return <></>;
  };

  const ButtomCards = ({itemDisplay}) => {
    if (itemDisplay.typeData !== undefined) {
      return (
        <>
          {itemDisplay.typeData.includes('collections') && itemDisplay.nft.length > 0 && (
            <NftItem item={itemDisplay.nft[0]} isFeed />
          )}
          {itemDisplay.typeData.includes('comunity') && <CommunityCard item={itemDisplay} />}
          <DisplayExperiences itemExperience={itemDisplay} />
        </>
      );
    }
    // eslint-disable-next-line no-underscore-dangle
    if (itemDisplay.__typename.includes('SeriesObject')) {
      return <SerieHome series={item} homeFeed enable />;
    }
    return <EventItem event={itemDisplay} enable />;
  };

  return (
    <Card>
      {!category.includes('collections') && item.projects !== null && item.projects !== undefined ? (
        <AvatarImage
          source={item.tile !== null || item.projects.tile !== '' ? {uri: item.projects.tile} : coverImage}
          resizeMode="cover"
        />
      ) : (
        <AvatarImage
          source={item.tile !== null || item.tile !== '' ? {uri: item.tile} : coverImage}
          resizeMode="cover"
        />
      )}
      <ContainPrimary>
        <View style={Style.spaceLeftContainPrimary}>
          {!category.includes('collections') && item.projects !== null && item.projects !== undefined ? (
            <>
              <View style={Style.wrap}>
                <TitleCard>{item.projects.name}</TitleCard>
                <TouchableOpacity
                  style={Style.btn}
                  onPress={() =>
                    category.includes('collections') ? handleNext(item?.id) : handleNext(item?.projects?.id)
                  }>
                  <Text style={Style.textBtn}>Check it out</Text>
                </TouchableOpacity>
              </View>
              <SubText ellipsizeMode="tail" numberOfLines={4}>
                {item.projects.description.slice(0, 120)}...
              </SubText>
            </>
          ) : (
            <>
              <TitleCard>{item.name}</TitleCard>
              <TouchableOpacity
                style={Style.btn}
                onPress={() =>
                  category.includes('collections') ? handleNext(item?.id) : handleNext(item?.projects?.id)
                }>
                <Text style={Style.textBtn}>Check it out</Text>
              </TouchableOpacity>
              <SubText ellipsizeMode="tail" numberOfLines={4}>
                {item.description.slice(0, 120)}...
              </SubText>
            </>
          )}
        </View>
        <View style={Style.contentPrimary} />
      </ContainPrimary>
      <ButtomCards itemDisplay={item} />
    </Card>
  );
};
