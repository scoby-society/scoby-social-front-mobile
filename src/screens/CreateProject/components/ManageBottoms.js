import React, {useCallback, useMemo} from 'react';
import {Alert, StyleSheet} from 'react-native';
import styled from 'styled-components';
import ShareSvg from 'assets/svg/ShareCurrentColor.svg';
import Invite from 'assets/svg/InviteCurrentColor.svg';
import Share from 'react-native-share';
import {UNKNOWN_ERROR_TEXT} from 'src/constants/Texts';
import {DYNAMIC_LINKS_DEFAULTS, DYNAMIC_LINKS_HEADERS, DYNAMIC_LINKS_SHORTENER_URL} from 'src/constants/Variables';
import firebase from '@react-native-firebase/app';
import lodash from 'lodash';
import Fonts from 'src/constants/Fonts';
import Colors from 'src/constants/Colors';

const JoinedWrapper = styled.View(({suscribeUsers}) => ({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  marginVertical: suscribeUsers ? 3 : 70,
}));

const Text = styled.Text({...Fonts.avenirSemiBold, color: Colors.white, fontSize: 12});

const Btn = styled.TouchableOpacity({
  width:74,
  height:30,
  borderRadius: 8,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal: 10,
  paddingVertical: 6,
  backgroundColor: 'rgba(255, 255, 255, 0.3)',
  borderColor: 'rgba(255, 255, 255, 0.8)',
  borderWidth: 0.5,
  marginHorizontal: 4,
});

const styles = StyleSheet.create({
  btnIcon: {color: Colors.white, width: 18, height: 18, marginRight: 6},
  btnText: {fontSize: 14},
});

const ManageBottoms = ({navigation, suscribeUsers, projects}) => {
  const {id} = projects;
  const buildSeriesLink = async () => {
    try {
      const body = JSON.stringify(
        lodash.merge(DYNAMIC_LINKS_DEFAULTS, {
          dynamicLinkInfo: {
            link: `${DYNAMIC_LINKS_DEFAULTS.dynamicLinkInfo.domainUriPrefix}/projects/${id}`,
          },
        }),
      );
      const response = await fetch(`${DYNAMIC_LINKS_SHORTENER_URL}?key=${firebase.app().options.apiKey}`, {
        method: 'POST',
        headers: DYNAMIC_LINKS_HEADERS,
        body,
      });
      const json = await response.json();
      return json.shortLink;
    } catch (e) {
      Alert.alert('Error', e.message);
      return null;
    }
  };

  const shareUrlProfile = useCallback(async () => {
    const url = await buildSeriesLink();
    if (url) {
      Share.open({
        message: `Join me on Scoby and let’s have a Meaningful Conversation!`,
        url,
      });
    } else {
      Alert.alert(UNKNOWN_ERROR_TEXT);
    }
  }, []);

  const handleInvite = () => {
    // if (typeof setVisible === 'function') {
    //   setVisible();
    // }
    navigation.navigate('InviteUsers', {sessionParams: {experience: 'projectsInvite', projects}});
  };

//   const setEnable=useMemo(()=>{
//     if(isOwner){
//       return false
//     }else{
//       return !isAvailable
//     }
//   },[])

  return (
    <JoinedWrapper suscribeUsers={suscribeUsers}>
        
      <Btn onPress={handleInvite}>
        <Invite width={18} heigth={18} style={styles.btnIcon} />
        <Text style={styles.btnText}>Invite</Text>
      </Btn>
      <Btn  onPress={shareUrlProfile}>
        <ShareSvg width={18} heigth={18} style={styles.btnIcon} />
        <Text style={styles.btnText}>Share</Text>
      </Btn>
    </JoinedWrapper>
  );
};
export default ManageBottoms;