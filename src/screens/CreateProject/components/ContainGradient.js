import React from 'react';
import {StyleSheet} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from 'src/constants/Colors';
const styles = StyleSheet.create({
  leftGradient: {
    borderColor: colors.white,
    width: 253,
    height: 126,
    borderRadius: 15,
    alignSelf: 'center',
  },
});

const ContainGradient = ({children}) => (
  <LinearGradient
    start={{
      x: 0.16,
      y: 0.16,
    }}
    end={{
      x: 0.16,
      y: 0.80,
    }}
    locations={[-0.55, 0.45, 0.85]}
    colors={['#6E67F62B', '#F4C1DF2B', '#EA87C22B']}
    style={styles.leftGradient}>
    {children}
  </LinearGradient>
);

export default ContainGradient;
