import React, {useContext} from 'react';
import {ActivityIndicator, Alert, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import {CloseWhiteIco} from 'assets/svg';
import EventItem from 'src/components/Event/EventItem';
import HeaderWithImage from 'src/components/HeaderWithImage';
import {useMutation, useQuery} from '@apollo/client';
import {EVENTS_ERROR_LOADING, EVENTS_ERROR_TITLE, EVENTS_NON_EXISTENT} from 'src/constants/Texts';
import {GET_EVENT_BY_ID} from 'src/graphql/queries/events';
import {FollowersContext} from 'src/containers/followers';
import {GET_FOLLOWERS} from 'src/graphql/queries/followers';
import {DELETE_GIVEAWAY, JOIN_GIVEAWAY, LEAVE_GIVEAWAY} from 'src/graphql/mutations/giveaway';
import {GET_EVENT_AND_GIVEAWAY} from 'src/graphql/queries/giveaway';
import GiveawayBtn from 'src/components/Event/GiveawayBtn';
import Fonts from 'src/constants/Fonts';
import {Header, TitleHeaderText} from './components/Header';
import confirmBox from '../Giveaways/components/ConfirmBox';
import GiveawayCollection from './components/CollectionCard';
import CommunityCard from './components/CommunityCard';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  marginTop: 50,
});

const CloseButton = styled.TouchableOpacity({
  left: 0,
  margin: 0,
});

const EventsContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const LoadingContainer = styled.View({
  flex: 1,
  height: '100%',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  alignSelf: 'center',
  marginBottom: 100,
  backgroundColor: colors.blueBackgroundSession,
});

const styles = StyleSheet.create({
  icon: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginRight: 20,
  },
  scroll: {
    paddingBottom: 0,
  },
  btnContainer: {
    marginLeft: '62%',
    marginRight: '3%',
    marginTop: -10,
    marginBottom: 20,
  },
});

const EventsLandingView = ({navigation, route, info}) => {
  const {currentUserProfile} = useContext(FollowersContext);
  const [deleteGiveAway] = useMutation(DELETE_GIVEAWAY);
  const [joinGiveAway] = useMutation(JOIN_GIVEAWAY);
  const [leaveGiveAway] = useMutation(LEAVE_GIVEAWAY);

  const eventsID = route?.params?.id;
  const id = eventsID || info;
  const {data, refetch} = useQuery(GET_EVENT_AND_GIVEAWAY, {
    variables: {idEvent: id},
    // fetchPolicy: 'network-only',
  });
  const {
    loading,
    data: {getEventbyId = []} = [],
    error,
  } = useQuery(GET_EVENT_BY_ID, {
    variables: {id},
    fetchPolicy: 'network-only',
  });

  const {data: {getFollowerUsers} = {getFollowerUsers: {data: []}}} = useQuery(GET_FOLLOWERS, {
    variables: {
      paging: {
        limit: 100,
        page: 1,
      },
      userId: currentUserProfile.id,
    },
  });

  if (loading) {
    return (
      <LoadingContainer>
        <ActivityIndicator size="large" color={colors.white} />
      </LoadingContainer>
    );
  }

  const getErrorAlert = () => {
    const msg = error ? EVENTS_ERROR_LOADING : EVENTS_NON_EXISTENT;
    Alert.alert(EVENTS_ERROR_TITLE, msg, [
      {
        text: 'Ok',
        onPress: () => {
          navigation.goBack();
        },
      },
    ]);
    return null;
  };

  const givawayId = data?.getEventAndGiveAway?.giveAway?.id;
  const checkfinishedGiveAway = data?.getEventAndGiveAway?.giveAway?.finishedAt;
  const suscribeUsers = data?.getEventAndGiveAway?.giveAway?.suscribeUsers;
  const ownerUser = data?.getEventAndGiveAway?.event?.ownerUser;
  const getUserInsuscribe = suscribeUsers?.find((item) => item.id === currentUserProfile.id);

  const finishedGiveAway = async () => {
    try {
      const response = await deleteGiveAway({variables: {id: givawayId}});
      if (response?.data?.deleteGiveAway?.id) {
        return Alert.alert('GiveAway successFully deleted');
      }
    } catch (err) {
      return Alert.alert('Giveway is finished');
    }
  };
  /* const addGiveaway = () => {
    const giveawayDetails = {EventId: id};
    navigation.navigate('CreateGiveaways', giveawayDetails);
  }; */

  const addAsParticipant = async () => {
    await joinGiveAway({variables: {id: givawayId}});
    refetch(
      {idEvent: id},
      // fetchPolicy: 'network-only',
    );
    return Alert.alert('You have Entered the Giveaway!');
  };

  const confirmJoinParticipant = async () => {
    confirmBox({onSuccess: addAsParticipant, text: 'you want to join the giveaway'});
  };

  const handleJoinSession = (session) => {
    const useJoinSession = {
      hostName: session.ownerUser.username,
      sessionName: session.title,
      sessionDescription: session.description,
      id: session.id,
      vonageSessionId: session.vonageSessionToken,
      userId: currentUserProfile.id,
      followers: getFollowerUsers && getFollowerUsers.data,
    };
    navigation.navigate('JoinSession', {params: useJoinSession});
  };

  const leaveGiveAwayOnsuccess = async () => {
    await leaveGiveAway({variables: {id: givawayId}});
    refetch(
      {idEvent: id},
      // fetchPolicy: 'network-only',
    );
    Alert.alert('you successfully  leave the giveaway');
  };

  const leaveGiveAwayAsParticipant = () => {
    confirmBox({onSuccess: leaveGiveAwayOnsuccess, text: 'you want to leave the giveaway'});
  };

  // const openGiveawaySimulation = () => {
  //   const params = {id: eventsID};
  //   navigation.navigate('GiveawayMockScreen', params);
  // };

  if (error || !getEventbyId) {
    return getErrorAlert();
  }
  return (
    <Wrapper>
      <ScrollView contentContainerStyle={styles.scroll}>
        <CloseButton onPress={() => navigation.goBack()}>
          <CloseWhiteIco />
        </CloseButton>
        <Header>
          <TitleHeaderText>{getEventbyId.title}</TitleHeaderText>
        </Header>
        <HeaderWithImage avatar={getEventbyId.avatar} backgroundImage={getEventbyId.backgroundImage} />

        {givawayId && !checkfinishedGiveAway && !getUserInsuscribe && currentUserProfile?.id !== ownerUser?.id && (
          <TouchableOpacity onPress={confirmJoinParticipant}>
            <View>
              <View style={styles.btnContainer}>
                <GiveawayBtn icon="plus">
                  <Text style={{color: '#FFFFFF'}}>Join Giveaway </Text>
                </GiveawayBtn>
              </View>
            </View>
          </TouchableOpacity>
        )}

        {givawayId && currentUserProfile?.id !== ownerUser?.id && getUserInsuscribe && (
          <TouchableOpacity onPress={leaveGiveAwayAsParticipant}>
            <View>
              <View style={styles.btnContainer}>
                <GiveawayBtn>
                  <Text style={{color: '#FFFFFF'}}>Leave Giveaway </Text>
                </GiveawayBtn>
              </View>
            </View>
          </TouchableOpacity>
        )}

        {/* {!givawayId && currentUserProfile?.id === ownerUser?.id && (
          <TouchableOpacity onPress={addGiveaway}>
            <View>
              <View style={styles.btnContainer}>
                <GiveawayBtn icon="plus">
                  <Text style={{color: '#FFFFFF'}}>Add Giveaway </Text>
                </GiveawayBtn>
              </View>
            </View>
          </TouchableOpacity>
        )} */}

        {/* <TouchableOpacity onPress={() => openGiveawaySimulation()}>
          <View>
            <View style={styles.btnContainer}>
              <GiveawayBtn>
                <Text style={{color: '#FFFFFF', textAlign: 'center'}}> Go Live </Text>
              </GiveawayBtn>
            </View>
          </View>
        </TouchableOpacity> */}

        {currentUserProfile?.id === ownerUser?.id && givawayId && (
          <TouchableOpacity onPress={() => finishedGiveAway()}>
            <View>
              <View style={styles.btnContainer}>
                <GiveawayBtn>
                  <Text style={{color: '#FFFFFF'}}>Cancel Giveaway </Text>
                </GiveawayBtn>
              </View>
            </View>
          </TouchableOpacity>
        )}
        <EventsContainer>
          <EventItem enable={false} event={getEventbyId} navigation={navigation} onJoinSession={handleJoinSession} />
        </EventsContainer>

        {!!givawayId && (
          <>
            {data?.getEventAndGiveAway?.giveAway?.community.length > 0 && (
              <View style={{margin: 10}}>
                <Text style={{color: '#ffffff'}}>
                  To enter the giveaway, you must be a member of all of these communities{' '}
                </Text>
                <TitleText style={{fontSize: 24, color: '#ffffff', lineHeight: 32}}>Community &nbsp;</TitleText>

                {data?.getEventAndGiveAway?.giveAway?.community?.map((item) => (
                  <CommunityContainer>
                    <CommunityCard data={item} />
                  </CommunityContainer>
                ))}
              </View>
            )}
          </>
        )}
        {!!givawayId && (
          <>
            {data?.getEventAndGiveAway?.giveAway?.collections.length > 0 && (
              <View style={{margin: 10}}>
                <Text style={{color: '#ffffff'}}>
                  To enter the giveaway, you must be holder of an NFT in all of these Collections{' '}
                </Text>
                <TitleText style={{fontSize: 24, color: '#ffffff', lineHeight: 32}}>Collections &nbsp;</TitleText>

                {data?.getEventAndGiveAway?.giveAway?.collections?.map((item) => (
                  <CollectionContainer style={{margin: 0}}>
                    <GiveawayCollection navigation={navigation} data={item} enable={false} event="" />
                  </CollectionContainer>
                ))}
                <EmptyContainer />
              </View>
            )}
          </>
        )}
      </ScrollView>
    </Wrapper>
  );
};

export default EventsLandingView;

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 18,
  lineHeight: '18px',
  color: colors.white,
});

const CommunityContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
  padding: 0,
});

const CollectionContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const EmptyContainer = styled.View({
  width: '100%',
  height: 80,
  alignSelf: 'center',
});
