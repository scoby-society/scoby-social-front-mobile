/* eslint-disable no-console */
/* eslint-disable camelcase */
import React, {useContext, useState, useEffect} from 'react';

import {ScrollView, StyleSheet, Text, TouchableOpacity, View, Alert, Linking} from 'react-native';
import {useMutation, useQuery} from '@apollo/client';
import {subscribe} from 'graphql';

import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import HeaderWithImage from 'src/components/HeaderWithImage';
import BackButton from 'src/components/BackButton';
import EventItem from 'src/components/Event/EventItem';
import GiveawayBtn from 'src/components/Event/GiveawayBtn';
import Fonts from 'src/constants/Fonts';
import {GET_EVENT_AND_GIVEAWAY, GET_NFT_ID} from 'src/graphql/queries/giveaway';
import {DELETE_GIVEAWAY, LEAVE_GIVEAWAY} from 'src/graphql/mutations/giveaway';
import {FollowersContext} from 'src/containers/followers';

import {GIVEAWAY_PROGRAM_ID} from 'src/config/env';
import {giveawayabi} from 'src/abi/giveawayabi';
import AsyncStorage from '@react-native-community/async-storage';
import bs58 from 'bs58';
import nacl from 'tweetnacl';
import {getOrCreateAssociatedTokenAccount, TOKEN_PROGRAM_ID, ASSOCIATED_TOKEN_PROGRAM_ID} from '@solana/spl-token';
import * as anchor from '../../../lib/anchor/index';
import {Header, TitleHeaderText} from './components/Header';
import CommunityCard from './components/CommunityCard';
import GiveawayCollection from './components/CollectionCard';
import confirmBox from '../Giveaways/components/ConfirmBox';

const GIVEAWAY_PROGRAM = new anchor.web3.PublicKey(GIVEAWAY_PROGRAM_ID);

const encryptPayload = (payload, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');
  const nonce = nacl.randomBytes(24);
  const encryptedPayload = nacl.box.after(Buffer.from(JSON.stringify(payload)), nonce, sharedSecret);
  return [nonce, encryptedPayload];
};

// const decryptPayload = (data, nonce, sharedSecret) => {
//   if (!sharedSecret) throw new Error('missing shared secret');
//   const decryptedData = nacl.box.open.after(bs58.decode(data), bs58.decode(nonce), sharedSecret);
//   if (!decryptedData) {
//     throw new Error('Unable to decrypt data');
//   }
//   return JSON.parse(Buffer.from(decryptedData).toString('utf8'));
// };

const onTransactionRedirectLink = 'scoby://phantom/signAndSendTransaction';
const buildUrl = (path, params) => `phantom://v1/${path}?${params.toString()}`;

let deepLinkingListener = null;
let hasStarted = false;

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  marginTop: 24,
});

const EventsContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const styles = StyleSheet.create({
  icon: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginRight: 20,
  },
  scroll: {
    paddingBottom: 0,
  },
  btnContainer: {
    marginLeft: '62%',
    marginRight: '3%',
    marginTop: -10,
    marginBottom: 20,
  },
  arrowicon: {
    marginBottom: 30,
    marginLeft: 10,
  },
});

const EventsScreen = ({navigation, route}) => {
  const {events} = route?.params;
  const {currentUserProfile} = useContext(FollowersContext);
  const [deleteGiveAway] = useMutation(DELETE_GIVEAWAY);
  const [leaveGiveAway] = useMutation(LEAVE_GIVEAWAY);
  const [mintAddress, setMintAddress] = useState('');
  const [nftID, setNftID] = useState(0);

  const {data, refetch} = useQuery(GET_EVENT_AND_GIVEAWAY, {
    fetchPolicy: 'network-only',
    variables: {idEvent: events?.id},
    onCompleted(resultdata) {
      if (resultdata.getEventAndGiveAway.giveAway) {
        if (resultdata.getEventAndGiveAway.giveAway.giveawayType !== 'sol') {
          setNftID(resultdata.getEventAndGiveAway.giveAway.nfts[0]);
        }
      }
    },
  });

  const {getNFTByID} = useQuery(GET_NFT_ID, {
    fetchPolicy: 'network-only',
    variables: {
      id: nftID,
    },
    onCompleted({getSporeDsById}) {
      setMintAddress(getSporeDsById.contractAdress);
    },
  });

  const giveaway = data?.getEventAndGiveAway?.giveAway;
  const givawayId = data?.getEventAndGiveAway?.giveAway?.id;
  const givawayData = data?.getEventAndGiveAway?.giveAway?.finishedAt;
  const giveawayType = data?.getEventAndGiveAway?.giveAway?.giveawayType;
  // const suscribeUsers = data?.getEventAndGiveAway?.event?.suscribeUsers;
  const ownerUser = data?.getEventAndGiveAway?.event?.ownerUser;
  const finishedDateEvent = data?.getEventAndGiveAway?.event?.finishedAt;
  // const getUserInsuscribe = suscribeUsers?.includes(currentUserProfile.id);

  const processTransaction = async () => {
    if (!hasStarted) {
      return;
    }
    hasStarted = false;
    try {
      const response = await deleteGiveAway({variables: {id: givawayId}});
      if (response?.data?.deleteGiveAway?.id) {
        refetch({idEvent: events?.id});
        return Alert.alert('GiveAway successfully cancel');
      }
    } catch (err) {
      refetch({idEvent: events?.id});
      return Alert.alert('Giveway is finished');
    }
  };

  const handleDeepLink = ({url}) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        const urlData = new URL(url);
        const params = urlData.searchParams;
        if (params.get('errorCode')) {
          console.log('Error is on eventscreen ', params.get('errorCode'));
          // return;
        }
        processTransaction();
      }
    });
  };

  useEffect(() => {
    if (deepLinkingListener) {
      deepLinkingListener.remove();
    }
    deepLinkingListener = Linking.addEventListener('url', handleDeepLink);
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      refetch({idEvent: events?.id});
    });
    return unsubscribe;
  }, [navigation]);

  const finishedGiveAway = async () => {
    hasStarted = true;

    const phantomWalletPublicKey = await AsyncStorage.getItem('public_key');
    const userWallet = new anchor.web3.PublicKey(phantomWalletPublicKey);

    const conn = new anchor.web3.Connection(anchor.web3.clusterApiUrl('devnet'));
    const randWallet = anchor.web3.Keypair.generate();
    const confirmOption = {commitment: 'finalized', preflightCommitment: 'finalized', skipPreflight: false};
    const provider = new anchor.AnchorProvider(conn, randWallet, confirmOption);
    const program = new anchor.Program(giveawayabi, GIVEAWAY_PROGRAM, provider);
    const transaction = new anchor.web3.Transaction();
    const msg_id = events?.id.toString();

    if (giveawayType === 'sol') {
      const [user_pda_sol, user_bump_sol] = await anchor.web3.PublicKey.findProgramAddress(
        [userWallet.toBuffer(), Buffer.from(msg_id)],
        GIVEAWAY_PROGRAM,
      );

      const [user_pda_ac] = await anchor.web3.PublicKey.findProgramAddress([userWallet.toBuffer()], GIVEAWAY_PROGRAM);

      transaction.add(
        await program.methods
          .cancelSol(user_bump_sol, msg_id)
          .accounts({
            userPdaAcct: user_pda_sol,
            sender: userWallet,
            owner: userWallet,
            ownerpda: user_pda_ac,
            systemProgram: anchor.web3.SystemProgram.programId,
          })
          .instruction(),
      );
    } else {
      await getNFTByID();
      const tokenaccount = await getOrCreateAssociatedTokenAccount(conn, userWallet, mintAddress, userWallet);
      const mint = new anchor.web3.PublicKey(mintAddress);
      const amount = data?.getEventAndGiveAway?.giveaway?.nfts.length;

      const [user_pda_state, bump1] = await anchor.web3.PublicKey.findProgramAddress(
        [userWallet.toBuffer(), tokenaccount.address.toBuffer(), Buffer.from(msg_id), Buffer.from('state')],
        GIVEAWAY_PROGRAM,
      );

      const [usertokenpda, bump2] = await anchor.web3.PublicKey.findProgramAddress(
        [userWallet.toBuffer(), tokenaccount.address.toBuffer(), Buffer.from(msg_id)],
        GIVEAWAY_PROGRAM,
      );

      const [user_pda_ac] = await anchor.web3.PublicKey.findProgramAddress([userWallet.toBuffer()], GIVEAWAY_PROGRAM);

      const wallet_to_deposit_to = await getOrCreateAssociatedTokenAccount(conn, userWallet, mint, userWallet);

      transaction.add(
        await program.methods
          .canceltoken(bump1, bump2, new anchor.BN(amount), msg_id)
          .accounts({
            tokenpda: usertokenpda,
            statepda: user_pda_state,
            walletToDepositTo: wallet_to_deposit_to.address,
            sender: userWallet,
            owner: userWallet,
            depositTokenAccount: tokenaccount.address,
            ownerpda: user_pda_ac,
            systemProgram: anchor.web3.SystemProgram.programId,
            rent: anchor.web3.SYSVAR_RENT_PUBKEY,
            associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
            tokenProgram: TOKEN_PROGRAM_ID,
          })
          .signers([])
          .instruction(),
      );
    }

    transaction.feePayer = userWallet;
    const anyTransaction = transaction;
    anyTransaction.recentBlockhash = (await conn.getLatestBlockhash()).blockhash;

    const serializedTransaction = anyTransaction.serialize({
      requireAllSignatures: false,
    });

    const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
    const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
    const dappPublicKey = await AsyncStorage.getItem('dapp_public_key');
    const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));
    const publicSession = await AsyncStorage.getItem('public_session');

    const payload = {
      session: publicSession,
      transaction: bs58.encode(serializedTransaction),
    };

    const [nonce, encryptedPayload] = encryptPayload(payload, sharedSecretDapp);

    const params = new URLSearchParams({
      dapp_encryption_public_key: dappPublicKey,
      nonce: bs58.encode(nonce),
      redirect_link: onTransactionRedirectLink,
      payload: bs58.encode(encryptedPayload),
    });
    const url = buildUrl('signAndSendTransaction', params);

    Linking.openURL(url);
  };

  const addGiveaway = () => {
    const giveawayDetails = {EventId: events?.id};
    navigation.navigate('CreateGiveaways', giveawayDetails);
  };

  const leaveGiveAwayaOnsuccess = async () => {
    try {
      await leaveGiveAway({variables: {id: givawayId}});
      // eslint-disable-next-line no-empty
    } catch (error) {}
  };

  const leaveGiveAwayAsParticipant = () => {
    confirmBox({onSuccess: leaveGiveAwayaOnsuccess, text: 'you want to leave the giveaway'});
  };

  const cancelGiveAway = async () => {
    confirmBox({onSuccess: finishedGiveAway, text: 'you want to cancel the giveaway'});
  };

  const checkGiveAway = !giveaway && currentUserProfile?.id === ownerUser?.id;
  const checkAnother = giveaway && currentUserProfile?.id === ownerUser?.id && !!givawayData && !finishedDateEvent;
  return (
    <>
      <Wrapper>
        <ScrollView contentContainerStyle={styles.scroll}>
          <Header>
            <TitleHeaderText>{events.title}</TitleHeaderText>
          </Header>
          <HeaderWithImage avatar={events.avatar} backgroundImage={events.backgroundImage} />

          {givawayId && currentUserProfile?.id !== ownerUser?.id && subscribe.includes(currentUserProfile?.id) && (
            <TouchableOpacity onPress={leaveGiveAwayAsParticipant}>
              <View>
                <View style={styles.btnContainer}>
                  <GiveawayBtn icon="plus">
                    <Text style={{color: '#FFFFFF'}}>Leave Giveaway </Text>
                  </GiveawayBtn>
                </View>
              </View>
            </TouchableOpacity>
          )}

          {checkGiveAway && (
            <TouchableOpacity onPress={addGiveaway}>
              <View>
                <View style={styles.btnContainer}>
                  <GiveawayBtn icon="plus">
                    <Text style={{color: '#FFFFFF'}}>Add Giveaway</Text>
                  </GiveawayBtn>
                </View>
              </View>
            </TouchableOpacity>
          )}

          {checkAnother && (
            <TouchableOpacity onPress={addGiveaway}>
              <View>
                <View style={styles.btnContainer}>
                  <GiveawayBtn icon="plus">
                    <Text style={{color: '#FFFFFF'}}>Add Giveaway</Text>
                  </GiveawayBtn>
                </View>
              </View>
            </TouchableOpacity>
          )}

          {/* <TouchableOpacity onPress={() => openGiveawaySimulation()}>
            <View>
              <View style={styles.btnContainer}>
                <GiveawayBtn>
                  <Text style={{color: '#FFFFFF', textAlign: 'center'}}> Go Live </Text>
                </GiveawayBtn>
              </View>
            </View>
          </TouchableOpacity> */}
          {/* 
          {(!giveaway && currentUserProfile?.id === ownerUser?.id) ||
          (giveaway && currentUserProfile?.id === ownerUser?.id && !!givawayData && !finishedDateEvent) ? (
            
            ) : (
              <View />
            )} */}

          {/* {(!givawayId && currentUserProfile?.id === ownerUser?.id) ||
            (givawayId && currentUserProfile?.id === ownerUser?.id && givawayData && !finishedDateEvent) (
              <TouchableOpacity onPress={addGiveaway}>
                <View>
                  <View style={styles.btnContainer}>
                    <GiveawayBtn icon="plus">
                      <Text style={{color: '#FFFFFF'}}>Add Giveaway </Text>
                    </GiveawayBtn>
                  </View>
                </View>
              </TouchableOpacity>
            ) : ( ))} */}

          {currentUserProfile?.id === ownerUser?.id && givawayId && !givawayData && (
            <>
              <TouchableOpacity onPress={() => cancelGiveAway()}>
                <View>
                  <View style={styles.btnContainer}>
                    <GiveawayBtn>
                      <Text style={{color: '#FFFFFF'}}>Cancel Giveaway </Text>
                    </GiveawayBtn>
                  </View>
                </View>
              </TouchableOpacity>
            </>
          )}

          <EventsContainer>
            <EventItem navigation={navigation} enable={false} event={events} />
          </EventsContainer>
          {!!givawayId && (
            <>
              {data?.getEventAndGiveAway?.giveAway?.community.length > 0 && (
                <View style={{margin: 10}}>
                  <Text style={{color: '#ffffff'}}>
                    To enter the giveaway, you must be a member of all of these communities{' '}
                  </Text>
                  <TitleText style={{fontSize: 24, color: '#ffffff', lineHeight: 32}}>Community &nbsp;</TitleText>

                  {data?.getEventAndGiveAway?.giveAway?.community?.map((item) => (
                    <CommunityContainer>
                      <CommunityCard data={item} />
                    </CommunityContainer>
                  ))}
                </View>
              )}
            </>
          )}
          {!!givawayId && (
            <>
              {data?.getEventAndGiveAway?.giveAway?.collections.length > 0 && (
                <View style={{margin: 10}}>
                  <Text style={{color: '#ffffff'}}>
                    To enter the giveaway, you must be holder of an NFT in all of these Collections{' '}
                  </Text>
                  <TitleText style={{fontSize: 24, color: '#ffffff', lineHeight: 32}}>Collections &nbsp;</TitleText>

                  {data?.getEventAndGiveAway?.giveAway?.collections?.map((item) => (
                    <CollectionContainer style={{margin: 0}}>
                      <GiveawayCollection navigation={navigation} data={item} enable={false} event="" />
                    </CollectionContainer>
                  ))}
                  <EmptyContainer />
                </View>
              )}
            </>
          )}
        </ScrollView>
      </Wrapper>
      <BackButton onPress={() => navigation.goBack()} />
    </>
  );
};

export default EventsScreen;

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 18,
  lineHeight: '18px',
  color: colors.white,
});

const CommunityContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
  padding: 0,
});

const CollectionContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const EmptyContainer = styled.View({
  width: '100%',
  height: 80,
  alignSelf: 'center',
});

/* const EventsScreen = ({navigation, route}) => {
    const {events} = route.params;

    return (
      <>
        <Wrapper>
          <ScrollView contentContainerStyle={styles.scroll}>
            <Header>
              <TitleHeaderText>{events.title}</TitleHeaderText>
            </Header>
            <HeaderWithImage avatar={events.avatar} backgroundImage={events.backgroundImage} />
            <EventsContainer>
              <EventItem navigation={navigation} enable={false} event={events} />
            </EventsContainer>
          </ScrollView>
        </Wrapper>
        <BackButton onPress={() => navigation.goBack()} />
      </>
    );
  };

  export default EventsScreen;
*/
