import React from 'react';
import {StyleSheet, View} from 'react-native';
import styled from 'styled-components/native';
import Colors from 'src/constants/Colors';
import Fonts, {avenirBold} from 'src/constants/Fonts';
import avatarSrc from '../../../../assets/images/profile/avatarPlaceholder.png';

const TeamCardWrapper = styled.View({
  backgroundColor: Colors.white,
  borderRadius: 8,
  padding: 12,
  marginTop: 20,
  marginBottom: 10,
  position: 'relative',
});

const TeamNameWrapper = styled.View({
  width: '60%',
});

const TeamNameText = styled.Text({
  ...avenirBold,
  fontSize: 14,
  color: Colors.black,
});

const TeamCardBodyWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  marginBottom: 22,
});

const TeamOwnerWrapper = styled.View({
  alignItems: 'center',
  marginLeft: 6,
});

const TeamAvatarOwnerWrapper = styled.View({
  borderRadius: 50,
  borderWidth: 3,
  borderColor: Colors.newPink,
});

const TeamOwnerFullName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 17,
  fontWeight: 'bold',
  color: Colors.violetColor,
  marginTop: 6,
});

const TeamOwnerName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  color: Colors.violetColor,
  marginTop: 6,
});

const TeamAvatarOwner = styled.Image({
  width: 80,
  height: 80,
  borderRadius: 50,
  resizeMode: 'cover',
});

const TeamCardDesc = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  maxWidth: '55%',
  flexGrow: 1,
  marginTop: 2,
  color: Colors.black,
});

const TeamCardDescContainer = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  maxWidth: '55%',
  flexGrow: 1,
  flexDirection: 'column',
  marginTop: 2,
  marginBottom: 15,
  color: Colors.black,
});

const BottomWrapper = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
});

const TopicsWrapper = styled.View({
  flexDirection: 'row',
  flexWrap: 'wrap',
  maxWidth: '70%',
});

const Topic = styled.View({
  flexDirection: 'row',
  backgroundColor: Colors.violetColorSelfOpacity,
  borderRadius: 5,
  marginHorizontal: 4,
  marginVertical: 2,
  paddingHorizontal: 6,
  paddingVertical: 6,
  minWidth: 62,
});

const TopicText = styled.Text({
  marginLeft: 2,
});

const CommunityCard = ({data}) => {
  const styles = StyleSheet.create({
    cardContainer: {
      width: '100%',
      backgroundColor: '#0A0846',
      borderRadius: 30,
      padding: 7,
    },
    cardBottom: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'relative',
      marginTop: 10,
    },
    gotoCommunityCardsButton: {
      height: 26,
      width: 120,
      justifyContent: 'center',
      borderRadius: 5,
      color: 'white',
      marginTop: 5,
    },
    gotoText: {
      width: 120,
      paddingLeft: 2,
    },
  });

  return (
    <View style={styles.cardContainer}>
      <TeamCardWrapper>
        <TeamNameWrapper style={styles.teamNameWrapper}>
          <TeamNameText style={styles.teamNameText}> {data.name} </TeamNameText>
        </TeamNameWrapper>
        <TeamCardBodyWrapper style={styles.teamCardBodyWrapper}>
          <TeamOwnerWrapper style={styles.teamOwnerWrapper}>
            <TeamAvatarOwnerWrapper style={styles.teamAvatarOwnerWrapper}>
              <TeamAvatarOwner source={data.avatar ? {uri: data.avatar} : avatarSrc} style={styles.teamAvatarOwner} />
            </TeamAvatarOwnerWrapper>
            <TeamOwnerFullName style={styles.teamOwnerFullName}>{data.ownerUser.fullName}</TeamOwnerFullName>
            <TeamOwnerName style={styles.teamOwnerName}>@{data.ownerUser.username}</TeamOwnerName>
          </TeamOwnerWrapper>
          <TeamCardDescContainer>
            <TeamCardDesc style={styles.teamCardDesc}>{data.description}</TeamCardDesc>
          </TeamCardDescContainer>
        </TeamCardBodyWrapper>
        <BottomWrapper>
          <TopicsWrapper style={styles.topicsWrapper}>
            {data.topics?.map(({icon, name: topicName}) => (
              <Topic style={styles.topic}>
                <TopicText style={styles.topicText}>
                  {icon} {topicName}
                </TopicText>
              </Topic>
            ))}
          </TopicsWrapper>
        </BottomWrapper>
      </TeamCardWrapper>
    </View>
  );
};

export default CommunityCard;
