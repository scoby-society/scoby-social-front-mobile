import React from 'react';
import {StyleSheet, View, ImageBackground} from 'react-native';
import styled from 'styled-components/native';
import Colors from 'src/constants/Colors';
import Fonts, {avenirBold} from 'src/constants/Fonts';
import avatarSrc from '../../../../assets/images/profile/avatarPlaceholder.png';
import collectionsSrc from '../../../../assets/images/collections.png';

const bgimage = require('../../../../assets/images/bgimage.png');

const CollectionCard = ({data}) => {
  const styles = StyleSheet.create({
    cardContainer: {
      width: '100%',
      backgroundColor: 'transparent',
      borderRadius: 30,
      paddingTop: 20,
      margin: 0,
    },
    cardBottom: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'relative',
      marginTop: 10,
    },
    image: {
      flex: 1,
      justifyContent: 'center',
    },
    gotoCollectionCardsButton: {
      height: 26,
      width: 110,
      justifyContent: 'center',
      borderRadius: 5,
      color: 'white',
    },
    gotoText: {
      width: 110,
      paddingLeft: 2,
    },
  });
  return (
    <View style={styles.cardContainer}>
      <ImageBackground source={bgimage} resizeMode="cover" style={styles.image}>
        <TeamCardWrapper>
          <TeamCardBodyWrapper style={styles.teamCardBodyWrapper}>
            <TeamOwnerWrapper style={styles.teamOwnerWrapper}>
              <TeamAvatarOwnerWrapper style={styles.teamAvatarOwnerWrapper}>
                <TeamAvatarOwner
                  source={data.avatarCreator ? {uri: data.avatarCreator} : collectionsSrc}
                  style={styles.teamAvatarOwner}
                />
                <TeamAvatarOwnerPic
                  source={data.cardProjectAvatar ? {uri: data.cardProjectAvatar} : avatarSrc}
                  style={styles.TeamAvatarOwnerPic}
                />
              </TeamAvatarOwnerWrapper>
            </TeamOwnerWrapper>
            <TeamNameWrapper style={styles.teamNameWrapper}>
              <TeamNameText style={styles.teamNameText}>{data?.title} </TeamNameText>
              <TeamCardDesc style={styles.teamCardDesc}>{data?.cardProjectDescription}</TeamCardDesc>
            </TeamNameWrapper>
          </TeamCardBodyWrapper>
        </TeamCardWrapper>
      </ImageBackground>
    </View>
  );
};

export default CollectionCard;

const TeamCardWrapper = styled.View({
  borderRadius: 8,
  padding: 12,
  marginTop: 20,
  marginBottom: 10,
  position: 'relative',
});

const TeamNameWrapper = styled.View({
  width: '60%',
});

const TeamNameText = styled.Text({
  ...avenirBold,
  fontSize: 14,
  color: Colors.white,
});

const TeamCardBodyWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  marginBottom: 22,
});

const TeamOwnerWrapper = styled.View({
  alignItems: 'center',
  marginLeft: 6,
});

const TeamAvatarOwnerWrapper = styled.View({
  borderRadius: 15,
  borderWidth: 0,
  borderColor: Colors.newPink,
  position: 'relative',
});

const TeamAvatarOwner = styled.Image({
  width: 113,
  height: 115,
  borderRadius: 15,
  resizeMode: 'cover',
});

const TeamAvatarOwnerPic = styled.Image({
  width: 35,
  height: 35,
  borderRadius: 15,
  resizeMode: 'cover',
  position: 'absolute',
  bottom: -10,
  left: -10,
});

const TeamCardDesc = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  maxWidth: '100%',
  flexGrow: 1,
  marginTop: 2,
  color: Colors.white,
});
