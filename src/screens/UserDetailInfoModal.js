import React, {useCallback, useContext, useEffect, useMemo, useState} from 'react';
import {Alert, Linking, Platform, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useQuery} from '@apollo/client';
import {useNavigation} from '@react-navigation/core';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {CloseWhiteIco, Dots} from 'assets/svg';
import openLink from 'src/utils/hook/openLink';
import styled from 'styled-components/native';
import Colors from 'src/constants/Colors';
import {GET_NFTS_BY_USER, GET_USER_PROFILE} from 'src/graphql/queries/profile';
import {statusBarHeight} from 'src/components/withSafeArea';
import {GlobalContext} from 'src/containers/global';
import coverImage from 'assets/images/profile/Cover.png';
import {WebsiteIco, LocationIco} from 'assets/ico';
import MyFollowersContent from 'src/screens/Profile/components/MyFollowersContent';
import {ACTIVITY_KEYS} from 'src/screens/Activity/ActivityKeys';
import Geocoder from 'react-native-geocoding';
import Fonts from 'src/constants/Fonts';
import Follow from 'src/components/Follow';
import RegularButton from 'src/components/RegularButton';
import {FollowersContext} from 'src/containers/followers';
import BlockReportContainer from 'src/components/BlockReportContainer';
import {useBlock} from 'src/hooks/blockAndReportHooks/useBlock';
import {IS_BLOCKED} from 'src/constants/Texts';
import {kitty} from 'src/config/api';
import 'react-native-get-random-values';
import 'react-native-url-polyfill/auto';
import ProfileList from 'src/navigation/profileList';

const Wrapper = styled.View({
  flex: 1,
  backgroundColor: Colors.blueBackgroundSession,
  paddingTop: statusBarHeight,
});

const TextViewWalletTitle = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.white,
  width: '80%',
  fontSize: 10,
  textAlign: 'center',
});

const TextViewWallet = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.white,
  fontSize: 12,
  textAlign: 'center',
});

const TextViewSubtitle = styled.Text({
  ...Fonts.avenir,
  color: Colors.white,
  fontSize: 10,
  width: '80%',
  marginVertical: 5,
  textAlign: 'center',
});

const ViewWallet = styled.TouchableOpacity({
  paddingHorizontal: 10,
  paddingVertical: 5,
  backgroundColor: Colors.newPink,
  borderRadius: 5,
  width: 100,
  height: 25,
});

const ViewWalletContainer = styled.View({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-end',
  alignItems: 'center',
  width: '65%',
});

const ActivityIndicator = styled.ActivityIndicator({});

const ButtonWrapper = styled.View({
  flex: 1,
});

const FollowButton = styled(Follow)({
  marginRight: 4,
  alignItems: 'center',
  justifyContent: 'center',
});

const MessageButton = styled(RegularButton)({
  marginLeft: 4,
  alignItems: 'center',
  justifyContent: 'center',
});

const AvatarImage = styled.Image`
  height: 100%;
  width: 100%;
  border-radius: 48px;
`;

const Actions = styled.View({
  paddingTop: 20,
  flexDirection: 'row',
});

const MainContent = styled.View({
  marginHorizontal: 24,
});

const TitleText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 16,
  lineHeight: '21px',
  color: Colors.white,
  marginVertical: 8,
});

const ContentText = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  lineHeight: '21px',
  color: Colors.regularText,
});

const Block = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  flex: 1,
});

const BlockSeparator = styled.View({
  paddingHorizontal: 8,
});

const Separator = styled.View({
  width: 0.5,
  height: 20,
  backgroundColor: '#e5eaf2',
});

const BlockContainer = styled.View({
  flexDirection: 'row',
  marginTop: 16,
});

const LocationText = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  color: Colors.lightPurple,
  flex: 1,
  paddingLeft: 8,
});

const OpenLinkBtn = styled.TouchableOpacity({
  width: '100%',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  left: 0,
});

const HeaderText = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.white,
  fontSize: 16,
  textAlign: 'center',
});

const TopRow = styled.View({
  flexDirection: 'row',
  height: 60,
  alignItems: 'center',
  justifyContent: 'center',
});

const Row = styled.View({
  flexDirection: 'row',
  marginVertical: 5,
  width: '100%',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const TopContent = styled.View`
  flex-direction: column;
  height: 144px;
  align-items: flex-end;
`;

const BgImage = styled.ImageBackground`
  position: absolute;
  top: 0px;
  width: 100%;
  height: 144px;
`;

const Avatar = styled.TouchableOpacity({
  width: 96,
  height: 96,
  marginTop: -48,
  marginLeft: 16,
  borderWidth: 4,
  borderRadius: 48,
  borderColor: Colors.blueBackgroundSession,
  backgroundColor: Colors.black,
  alignItems: 'center',
  justifyContent: 'center',
  overflow: 'hidden',
});

const styles = StyleSheet.create({
  textBlock: {
    ...Fonts.avenirBold,
    color: 'white',
    fontSize: 16,
    alignSelf: 'center',
    textAlign: 'center',
    width: '80%',
    marginTop: 50,
  },
  blockBtn: {
    color: 'white',
    borderRadius: 4,
    width: 118,
    height: 27,
    backgroundColor: '#6536BB',
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 10,
    right: 30,
  },
  buttonPurple: {
    maxWidth: '50%',
    height: 32,
    alignSelf: 'flex-end',
    marginBottom: 0,
    marginTop: 10,
    backgroundColor: Colors.purple,
  },
});

const MainContentWrapper = styled.View({
  width: '100%',
  marginTop: '15px',
  flexDirection: 'row',
  justifyContent: 'space-between',
});

const MainContentItem = styled.View({
  flex: 1,
});

export default function UserDetailInfoModal({info, onClose, navigation, route, nonBlink}) {
  const externalProvidedUserID = route?.params?.id;
  // const fade = route?.params?.fade;
  const id = externalProvidedUserID || info;
  const safeNavigation = useNavigation();
  const {currentUserProfile} = useContext(FollowersContext);
  const [show, setShow] = useState(false);
  const {
    loading,
    data: {getUserProfile} = {getUserProfile: {}},
    error,
  } = useQuery(GET_USER_PROFILE, {variables: {id: parseFloat(id, 10)}});
  const profileOfUser = getUserProfile;
  const {setInitialLink} = useContext(GlobalContext);
  const [blocked, setBlocked] = useState(false);
  const {isBlocked, unblockUser} = useBlock();
  const {
    data: {getNftByUser = []} = [],
    loading: {loadingNft},
  } = useQuery(GET_NFTS_BY_USER, {
    variables: {id},
  });

  useEffect(() => {
    setInitialLink(null);
    setBlocked(isBlocked(id));

    if (error) {
      Alert.alert(error.message);
      if (externalProvidedUserID) {
        navigation.navigate('MainTabs');
      }
    }
  }, [error, externalProvidedUserID, navigation, setInitialLink, id, isBlocked]);

  const openGps = useCallback(
    (lat, lng) => {
      const scheme = Platform.select({
        ios: 'maps:0,0?q=',
        android: 'geo:0,0?q=',
      });
      const latLng = `${lat},${lng}`;
      const label = profileOfUser ? `${profileOfUser.username}` : '';
      const url = Platform.select({
        ios: `${scheme}${label}@${latLng}`,
        android: `${scheme}${latLng}(${label})`,
      });

      Linking.openURL(url);
    },
    [profileOfUser],
  );

  const geocode = useCallback(
    (address) => {
      Geocoder.from(address)
        .then((json) => {
          const {location} = json.results[0].geometry;
          openGps(location.lat, location.lng);
        })
        .catch(() => {});
    },
    [openGps],
  );

  const handleClose = useCallback(() => {
    if (typeof onClose === 'function') {
      onClose();
    } else if (navigation.canGoBack()) {
      navigation.goBack();
    } else if (externalProvidedUserID) {
      navigation.reset({index: 0, routes: [{name: 'MainTabs', screen: 'Profile'}]});
    } else {
      navigation.reset({index: 0, routes: [{name: 'MainTabs', screen: 'Home'}]});
    }
  }, [externalProvidedUserID, navigation, onClose]);

  const navigateToChat = async (profile) => {
    const {id: userid} = profile || {};
    const channel = await kitty.createChannel({type: 'DIRECT', members: [{username: userid}]});
    if (profile) navigation.navigate(ACTIVITY_KEYS.PRIVATE_CHAT_KITTY, {channel: channel.channel});
    handleClose();
  };

  const getHoldingNft = useMemo(() => {
    let stringNft;
    getNftByUser.forEach((item, index) => {
      if (index > 2) {
        return;
      }
      if (!stringNft) {
        stringNft = item.name;
      } else {
        stringNft = `${stringNft}, ${item.name}`;
      }
    });
    return stringNft;
  }, [getNftByUser]);

  const getHeader = () => (
    <>
      <TopRow>
        <CloseButton onPress={handleClose}>
          <CloseWhiteIco />
        </CloseButton>
        <HeaderText>{profileOfUser.fullName || profileOfUser.username}</HeaderText>
      </TopRow>
      <TopContent>
        <BgImage
          source={profileOfUser.backgroundImage ? {uri: profileOfUser.backgroundImage} : coverImage}
          style={{resizeMode: 'stretch'}}
        />
        {/* <Flag role={profileOfUser.role.toLowerCase()} /> */}
        {!show && <Dots onPress={() => setShow(!show)} style={{marginRight: 10}} />}
        <BlockReportContainer
          show={show}
          setShow={setShow}
          user={profileOfUser}
          navigation={navigation}
          close={onClose}
          isBlockedUser={blocked}
        />
      </TopContent>
      <Avatar activeOpacity={nonBlink ? 1 : 0.2}>
        <AvatarImage source={profileOfUser.avatar ? {uri: profileOfUser.avatar} : avatarSrc} resizeMode="cover" />
      </Avatar>
      <MyFollowersContent navigation={safeNavigation} userId={profileOfUser.id} />

      <MainContent>
        <MainContentWrapper>
          <MainContentItem>
            <Row>
              <TitleText>{profileOfUser ? `@${profileOfUser.username}` : ''}</TitleText>
              {profileOfUser.publicKey && !blocked && (
                <ViewWalletContainer>
                  <TextViewWalletTitle numberOfLines={2}>
                    {`Join ${profileOfUser.username} in these amazing projects!`}
                  </TextViewWalletTitle>
                  {getNftByUser.length > 0 && (
                    <TextViewSubtitle>{`${profileOfUser.username} is spreading ${getHoldingNft}...`}</TextViewSubtitle>
                  )}
                  <ViewWallet
                    onPress={() => {
                      navigation.navigate('UserNftList', {nft: getNftByUser, user: getUserProfile});
                    }}>
                    {loading && <ActivityIndicator size="small" color={Colors.white} />}
                    {!loading && <TextViewWallet>Check it out</TextViewWallet>}
                  </ViewWallet>
                </ViewWalletContainer>
              )}
            </Row>
            {profileOfUser?.bio ? <ContentText>{profileOfUser ? profileOfUser.bio : ''}</ContentText> : null}
          </MainContentItem>
          {blocked && (
            <MainContentItem>
              <TouchableOpacity style={styles.blockBtn} onPress={unblockUser}>
                <Text>Unblock</Text>
              </TouchableOpacity>
            </MainContentItem>
          )}
        </MainContentWrapper>
        {profileOfUser.location || profileOfUser.website ? (
          <BlockContainer>
            {profileOfUser.location ? (
              <Block>
                {/* TODO: need add action for opening location on map */}
                <OpenLinkBtn onPress={() => geocode(profileOfUser.location)}>
                  <LocationIco />
                  <LocationText numberOfLines={1}>{profileOfUser.location}</LocationText>
                </OpenLinkBtn>
              </Block>
            ) : null}
            {profileOfUser.location && profileOfUser.website ? (
              <BlockSeparator>
                <Separator />
              </BlockSeparator>
            ) : null}
            {profileOfUser.website ? (
              <Block>
                <OpenLinkBtn onPress={() => openLink(profileOfUser.website.toLowerCase())}>
                  <WebsiteIco />
                  <LocationText numberOfLines={1}>{profileOfUser.website.toLowerCase()}</LocationText>
                </OpenLinkBtn>
              </Block>
            ) : null}
          </BlockContainer>
        ) : null}
        {profileOfUser.id !== currentUserProfile.id && !blocked && (
          <Actions>
            <ButtonWrapper>
              <FollowButton user={profileOfUser} navigation={safeNavigation} />
            </ButtonWrapper>
            <ButtonWrapper>
              <MessageButton onPress={() => navigateToChat(profileOfUser)} active title="Message" />
            </ButtonWrapper>
          </Actions>
        )}
      </MainContent>
    </>
  );

  return (
    <Wrapper>
      {loading || error || !profileOfUser || loadingNft ? (
        <ActivityIndicator color={Colors.white} size="large" />
      ) : (
        <>
          {getHeader()}
          {!blocked ? <ProfileList id={id} /> : <Text style={styles.textBlock}>{IS_BLOCKED}</Text>}
        </>
      )}
    </Wrapper>
  );
}
