import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {royaltiesText} from 'src/constants/Texts';
import styled from 'styled-components/native';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#170E4D',
    width: '97%',
    height: 210,
    zIndex: 10,
    alignSelf: 'center',
    borderRadius: 20,
    paddingLeft: 10,
    paddingRight: 10,
    marginBottom: 60,
    paddingTop: 5,
  },
  wrapper2: {
    width: '97%',
    height: 200,
    zIndex: 10,
    alignSelf: 'center',
    borderRadius: 20,
    paddingLeft: 10,
    marginBottom: 20,
  },
  cardSectionOneM: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 5,
    width: '60%',
  },
  cardSectionOneN: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 5,
    width: '100%',
  },
  cardSectionTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',

    marginTop: 50,
    marginBottom: 5,
  },
  member: {
    flexDirection: 'column',

    width: 150,
  },
  textSection: {
    flexDirection: 'row',
  },
  title: {
    color: 'white',
    fontWeight: '700',
    // textDecorationColor:'underline'
  },
  username: {
    color: 'white',
    fontWeight: '700',
    alignSelf: 'center',
    textDecorationLine: 'underline',
  },
  nickname: {
    color: 'white',
    fontSize: 11,
    alignSelf: 'center',
  },
  wrap: {
    flexDirection: 'row',
  },
  rank: {
    color: 'white',
    alignSelf: 'center',
  },
  symbol: {
    color: 'white',
    fontWeight: '700',
  },
  coin: {
    color: 'white',
    marginRight: 5,
  },
});

const Avatar = styled.Image(() => ({
  width: '50px',
  height: '50px',
  borderRadius: 100,
  alignSelf: 'center',
  marginRight: 10,
}));

function Royalty({rank, type, userName, nickName, avatar, total, creator, connector, price, id}) {
  const isPar = (numero) => numero % 2 === 0;

  const navigation = useNavigation();
  const goToProfile = () => {
    navigation.navigate('ForeignUserProfile', {id});
  };

  function getImgSource(avatarUser) {
    return avatarUser ? {uri: avatarUser} : avatarSrc;
  }

  return (
    <View style={!isPar(rank) ? styles.wrapper : styles.wrapper2}>
      <View style={type === 'my' ? styles.cardSectionOneN : styles.cardSectionOneM}>
        <View>
          <Text style={styles.title}>Rank</Text>
          <Text style={styles.rank}>{rank}</Text>
        </View>
        <View style={styles.member}>
          <Text style={styles.title}>Member</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Avatar source={getImgSource(avatar)} />
            <View>
              <TouchableOpacity onPress={() => goToProfile()}>
                <Text style={styles.username}>{userName}</Text>
              </TouchableOpacity>
              <Text style={styles.nickname}>@{nickName}</Text>
            </View>
          </View>
        </View>

        {type === 'my' && (
          <View>
            <Text>Network</Text>
            <View style={styles.wrap}>
              <View style={{marginRight: 30}}>
                <Text>Guests</Text>
                <Text>13,421</Text>
              </View>
              <View>
                <Text>Guests</Text>
                <Text>13,421</Text>
              </View>
            </View>
          </View>
        )}
      </View>
      <View style={styles.cardSectionTwo}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.title}>{royaltiesText.TOTAL_ROYALTIES}</Text>
          <View style={styles.textSection}>
            <Text style={styles.coin}>{total}</Text>
            <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
          </View>
          <View style={styles.textSection}>
            <Text style={styles.coin}>
              {royaltiesText.DOLLAR_SIGN}
              {parseFloat(total * price).toFixed(2)}
            </Text>
            <Text style={styles.symbol}>{royaltiesText.USD}</Text>
          </View>
        </View>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.title}>{royaltiesText.CREATOR_ROYALTIES}</Text>
          <View style={styles.textSection}>
            <Text style={styles.coin}>{creator}</Text>
            <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
          </View>
          <View style={styles.textSection}>
            <Text style={styles.coin}>
              {royaltiesText.DOLLAR_SIGN}
              {parseFloat(creator * price).toFixed(2)}
            </Text>
            <Text style={styles.symbol}>{royaltiesText.USD}</Text>
          </View>
        </View>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.title}>{royaltiesText.CONNECTOR_ROYALTIES}</Text>
          <View style={styles.textSection}>
            <Text style={styles.coin}>{connector}</Text>
            <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
          </View>
          <View style={styles.textSection}>
            <Text style={styles.coin}>
              {royaltiesText.DOLLAR_SIGN}
              {parseFloat(connector * price).toFixed(2) === 'NaN' ? 0 : parseFloat(connector * price).toFixed(2)}
            </Text>
            <Text style={styles.symbol}>{royaltiesText.USD}</Text>
          </View>
        </View>
      </View>
      {type.includes('my') && <Text style={styles.title}>Memberships:</Text>}
    </View>
  );
}

export default Royalty;
