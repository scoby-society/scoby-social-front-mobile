import React from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';
// import styled from "@emotion/styled";
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import {royaltiesText} from 'src/constants/Texts';
import Medalla1 from 'assets/images/medalla1.png';
import Medalla2 from 'assets/images/medalla2.png';
import Medalla3 from 'assets/images/medalla3.png';

const Avatar = styled.Image((props) => ({
  width: props.size,
  height: props.size,
  borderRadius: 100,
  alignSelf: 'center',
  zIndex:1000,
  marginBottom: -20,
}));

const Medalla = styled.Image(() => ({
  position: 'absolute',
  right: 10,
}));

const styles = StyleSheet.create({
  username: {
    color: 'white',
    fontWeight: '700',
    alignSelf: 'center',
    textDecorationLine: 'underline',
  },
  nickname: {
    color: 'white',
    fontSize: 11,
    alignSelf: 'center',
  },
  tr: {
    color: 'white',
    alignSelf: 'center',
    fontWeight: '700',
    marginTop: 20,
  },
  textSection: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  symbol: {
    fontWeight: '700',
    color: 'white',
    marginLeft: 5,
  },
  coin: {
    color: 'white',
    marginRight: 10,
  },
  linearGradient: {
    width: '90%',
    borderRadius: 10,
    paddingLeft: 2,
    paddingRight: 2,
  },
  height: {
    height: 200,
  },
  height2: {
    height: 190,
  },
  height3: {
    height: 180,
  },
});

const one = ['rgba(255, 255, 255, 0.08)', 'rgba(255, 255, 255, 0.50)', 'rgba(255, 255, 255, 0.08)'];
const two = ['rgba(255, 255, 255, 0.08)', 'rgba(255, 255, 255, 0.25)', 'rgba(255, 255, 255, 0.08)'];
const three = ['rgba(255, 255, 255, 0.08)', 'rgba(255, 255, 255, 0.15)', 'rgba(255, 255, 255, 0.08)'];

function Card({position, data, price}) {

  const navigation = useNavigation();
  const goToProfile = (id) => {
    navigation.navigate('ForeignUserProfile', {id});
  };
  return (
    <View style={{flex: 1, flexDirection: 'column', width: '20%', justifyContent:'center' ,alignItems:'center', marginTop:-30}}>
      <Avatar source={{uri: data.img}} size={position.includes('1') ? '60px' : '50px'} />
      <LinearGradient
        colors={position === '1' ? one : position === '2' ? two : three}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0}}
        style={[
          styles.linearGradient,
          position === '1' ? styles.height : position === '2' ? styles.height2 : styles.height3,
        ]}>
        {position.includes('1') ? (
          <Medalla source={Medalla1} />
        ) : position.includes('2') ? (
          <Medalla source={Medalla2} />
        ) : (
          <Medalla source={Medalla3} />
        )}

        <TouchableOpacity style={{marginTop: 30}} onPress={() => goToProfile(data.id)}>
          <Text style={styles.username}>{data.userName}</Text>
        </TouchableOpacity>
        <Text style={styles.nickname}>@{data.nickName}</Text>

        <Text style={styles.tr}>{royaltiesText.TOTAL_ROYALTIES}</Text>
        <View style={styles.textSection}>
          <Text style={{color: '#fff'}}>{data.total}</Text>
          <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
        </View>
        <View style={styles.textSection}>
          <Text style={{color: '#fff'}}>${parseFloat(data.total * price).toFixed(2)}</Text>
          <Text style={styles.symbol}>{royaltiesText.USD}</Text>
        </View>
      </LinearGradient>
    </View>
  );
}

export default Card;
