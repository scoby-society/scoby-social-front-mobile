import {useQuery} from '@apollo/client';
import React, {useContext, useEffect, useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {FollowersContext} from 'src/containers/followers';
import styled from 'styled-components/native';
import {NETWORK_ROYALTIES, MY_NETWORK_DATA} from '../../graphql/queries/royalties';
import Card from './components/Card';
import Royalty from './components/Royalty';
import axios from 'react-native-axios';
import Fonts from 'src/constants/Fonts';
import {royaltiesText} from 'src/constants/Texts';
import LoadingExperience from 'src/components/HomeExperiences/components/LoadingExperience';
import BackButton from 'src/components/BackButton';
import {useNavigation} from '@react-navigation/native';

const CardsWrapper = styled.View({
  width: '95%',
  height: '361px',
  backgroundColor: '#1B1054',
  borderRadius: 12,
  paddingLeft: 10,
  paddingRight: 10,
  alignSelf: 'center',
  alignItems: 'center',
  marginBottom: 50,
});

const MyButton = styled.TouchableOpacity((props) => ({
  backgroundColor: props.color,
  borderRadius: 7,
  left: 30,
  justifyContent: 'center',
  alignItems: 'center',
  paddingHorizontal:10,
  paddingVertical:5,
  marginLeft: 5,
  marginVertical:10
}));

const styles = StyleSheet.create({
  text: {
    ...Fonts.goudy,
    fontSize: 30,
    color: 'white',
    marginLeft: 30,
  },
  wrapper: {
    height: '100%',
    paddingTop: 50,
  },
  textWrapper: {
    flexDirection: 'row',
    width: '100%',
  },
  cards: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
    width: '100%',
  },
  wrap: {
    width: 125,

    left: -15,
  },
  textSection: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  title: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '700',
    marginBottom: 20,
    marginTop: 20,
  },
  textBtnActive: {
    color: 'white',
    textAlign: 'center',
    fontWeight: '700',
  },
  textBtnOff: {
    color: '#9389B5',
    textAlign: 'center',
    fontWeight: '700',
  },
  btnSect: {
    flexDirection: 'row',
  },
  subTitle: {
    fontSize: 20,
    color: 'white',
    fontFamily: Fonts.avenir.fontFamily,
    fontWeight: '700',
    marginLeft: 10,
    marginBottom: 20,
  },
  symbol: {
    color: 'white',
    fontWeight: '700',
  },
  coin: {
    color: 'white',
    marginRight: 5,
  },
});
const COINAPI =
  'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=solana&order=market_cap_desc&per_page=100&page=1&sparkline=false';
const Royalties = () => {
  const {currentUserProfile, token} = useContext(FollowersContext);
  const [firstPlaceN, setFirstPlaceN] = useState({});
  const [secondPlaceN, setSecondPlaceN] = useState({});
  const [thirdPlaceN, setThirdPlaceN] = useState({});
  const [firstPlaceM, setFirstPlaceM] = useState({});
  const [secondPlaceM, setSecondPlaceM] = useState({});
  const [thirdPlaceM, setThirdPlaceM] = useState({});
  const [updateNetwork, setUpdateNetwork] = useState(0);
  const [updateMyNetwork, setUpdateMyNetwork] = useState(0);
  const [solana, setSolana] = useState({});
  const [networkData, setNetworkData] = useState([]);
  const [myNetworkData, setMyNetworkData] = useState([]);
  const [completedN, setCompletedN] = useState(false);
  const [completedM, setCompletedM] = useState(false);
  const navigation = useNavigation();
  const [type, setType] = useState('network');
  const [networkRoyalties, setNetworkRoyalties] = useState({
    totalRoyalties: 0,
    creatorRoyalties: 0,
    connectorRoyalties: 0,
  });
  const [myNetworkRoyalties, setMyNetworkRoyalties] = useState({
    totalRoyalties: 0,
    creatorRoyalties: 0,
    connectorRoyalties: 0,
  });

  const {data: {getMemberNetworkById} = {}} = useQuery(MY_NETWORK_DATA, {
    fetchPolicy: 'network-only',
    variables: {
      id: currentUserProfile.id,
    },
    onCompleted(data) {
      setMyNetworkData(data.getMemberNetworkById);
      setCompletedM(true);
      setFirstPlaceM({
        userName: data.getMemberNetworkById[0]?.ownerUser?.username,
        nickName: data.getMemberNetworkById[0]?.ownerUser?.fullName,
        img: data.getMemberNetworkById[0]?.ownerUser?.avatar,
        total: data.getMemberNetworkById[0]?.totalRoyalties,
      });
      setSecondPlaceM({
        userName: data.getMemberNetworkById[1]?.ownerUser?.username,
        nickName: data.getMemberNetworkById[1]?.ownerUser?.fullName,
        img: data.getMemberNetworkById[1]?.ownerUser?.avatar,
        total: data.getMemberNetworkById[1]?.totalRoyalties,
      });
      setThirdPlaceM({
        userName: data.getMemberNetworkById[2]?.ownerUser?.username,
        nickName: data.getMemberNetworkById[2]?.ownerUser?.fullName,
        img: data.getMemberNetworkById[2]?.ownerUser?.avatar,
        total: data.getMemberNetworkById[2]?.totalRoyalties,
      });
    },
  });

  const {data: {getSocialNetworks} = {}} = useQuery(NETWORK_ROYALTIES, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    fetchPolicy: 'network-only',
    onCompleted(data) {
      setCompletedN(true);

      setNetworkData(data.getSocialNetworks);
      setFirstPlaceN({
        id: data.getSocialNetworks[0].ownerUser.id,
        userName: data.getSocialNetworks[0]?.ownerUser?.username,
        nickName: data.getSocialNetworks[0]?.ownerUser?.fullName,
        img: data.getSocialNetworks[0]?.ownerUser?.avatar,
        total: data.getSocialNetworks[0]?.totalRoyalties,
      });
      setSecondPlaceN({
        id: data.getSocialNetworks[0].ownerUser.id,
        userName: data.getSocialNetworks[1]?.ownerUser?.username,
        nickName: data.getSocialNetworks[1]?.ownerUser?.fullName,
        img: data.getSocialNetworks[1]?.ownerUser?.avatar,
        total: data.getSocialNetworks[1]?.totalRoyalties,
      });
      setThirdPlaceN({
        id: data.getSocialNetworks[0].ownerUser.id,
        userName: data.getSocialNetworks[2]?.ownerUser?.username,
        nickName: data.getSocialNetworks[2]?.ownerUser?.fullName,
        img: data.getSocialNetworks[2]?.ownerUser?.avatar,
        total: data.getSocialNetworks[2]?.totalRoyalties,
      });
    },
  });

  useEffect(() => {
    updateNetwork === 0 &&
      getSocialNetworks?.map((user) => {
        setUpdateNetwork(1);
        setNetworkRoyalties({
          totalRoyalties: (networkRoyalties.totalRoyalties +=
            user.totalRoyalties === null ? 0 : parseFloat(user.totalRoyalties)),
          creatorRoyalties: (networkRoyalties.creatorRoyalties +=
            user.creatorRoyalties === null ? 0 : parseFloat(user.creatorRoyalties)),
          conectorRoyalties: (networkRoyalties.conectorRoyalties +=
            user.connectorRoyalties === null ? 0 : parseFloat(user.connectorRoyalties)),
        });
        return null;
      });
  }, [getSocialNetworks, networkRoyalties, updateNetwork]);

  useEffect(() => {
    updateMyNetwork === 1 &&
      getMemberNetworkById?.map((user, i) => {
        setUpdateMyNetwork(1);
        setMyNetworkRoyalties({
          totalRoyalties: (myNetworkRoyalties.totalRoyalties +=
            user.totalContribution === null ? 0 : user.totalContribution),
          creatorRoyalties: (myNetworkRoyalties.creatorRoyalties += user.asCreator === null ? 0 : user.asCreator),
          conectorRoyalties: (myNetworkRoyalties.conectorRoyalties += user.asConnector === null ? 0 : user.asConnector),
        });
        return null;
      });
  }, [getMemberNetworkById, myNetworkRoyalties, updateMyNetwork]);

  useEffect(() => {
    axios.get(COINAPI).then((res) => {
      setSolana(res.data[0]);
    });
  }, []);

  if (!completedN && type.includes('network')) {
    return <LoadingExperience />;
  } else if (!completedM && type.includes('my')) {
    return <LoadingExperience />;
  }

  return (
    <View style={{height: '100%'}}>
      <ScrollView style={styles.wrapper}>
        <BackButton navigation={navigation} />
        <Text style={styles.text}>{royaltiesText.ROYALTIES}</Text>
        <View style={styles.btnSect}>
          <MyButton
            color={type.includes('network') ? '#6536BB' : '#492894'}
            onPress={() => setType('network')}>
            <Text style={type.includes('network') ? styles.textBtnActive : styles.textBtnOff}>
              {royaltiesText.NETWORK_ROYALTIES}
            </Text>
          </MyButton>
          <MyButton
            color={type.includes('my') ? '#6536BB' : '#492894'}
            onPress={() => setType('my')}>
            <Text style={type.includes('my') ? styles.textBtnActive : styles.textBtnOff}>
              {royaltiesText.MY_ROYALTIES}
            </Text>
          </MyButton>
        </View>
        <CardsWrapper>
          <View style={styles.cards}>
            <Card
              position={'2'}
              data={type.includes('network') ? secondPlaceN : secondPlaceM}
              price={solana.current_price}
            />
            <Card
              position={'1'}
              data={type.includes('network') ? firstPlaceN : firstPlaceM}
              price={solana.current_price}
            />
            <Card
              position={'3'}
              data={type.includes('network') ? thirdPlaceN : thirdPlaceM}
              price={solana.current_price}
            />
          </View>

          <View style={styles.textWrapper}>
            <View style={styles.wrap}>
              <Text style={styles.title}>{royaltiesText.TOTAL_ROYALTIES_EARNED}</Text>
              <View style={styles.textSection}>
                <Text style={styles.coin}>
                  {type.includes('network') ? networkRoyalties.totalRoyalties : myNetworkRoyalties.totalRoyalties}
                </Text>
                <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
              </View>
              <View style={styles.textSection}>
                <Text style={styles.coin}>
                  {royaltiesText.DOLLAR_SIGN}
                  {type.includes('network')
                    ? parseFloat(networkRoyalties.totalRoyalties * solana.current_price).toFixed(2)
                    : parseFloat(myNetworkRoyalties.totalRoyalties * solana.current_price).toFixed(2)}
                </Text>
                <Text style={styles.symbol}>{royaltiesText.USD}</Text>
              </View>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.title}>{royaltiesText.CREATOR_ROYALTIES_EARNED}</Text>
              <View style={styles.textSection}>
                <Text style={styles.coin}>
                  {type.includes('network') ? networkRoyalties.creatorRoyalties : myNetworkRoyalties.creatorRoyalties}
                </Text>
                <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
              </View>
              <View style={styles.textSection}>
                <Text style={styles.coin}>
                  {royaltiesText.DOLLAR_SIGN}
                  {type.includes('network')
                    ? parseFloat(networkRoyalties.creatorRoyalties * solana.current_price).toFixed(2)
                    : parseFloat(myNetworkRoyalties.creatorRoyalties * solana.current_price).toFixed(2)}
                </Text>
                <Text style={styles.symbol}>{royaltiesText.USD}</Text>
              </View>
            </View>
            <View style={styles.wrap}>
              <Text style={styles.title}>{royaltiesText.CONNECTOR_ROYALTIES_EARNED}</Text>
              <View style={styles.textSection}>
                <Text style={styles.coin}>
                  {type.includes('network')
                    ? networkRoyalties.connectorRoyalties
                    : myNetworkRoyalties.connectorRoyalties}
                </Text>
                <Text style={styles.symbol}>{royaltiesText.SOL}</Text>
              </View>
              <View style={styles.textSection}>
                <Text style={styles.coin}>
                  {royaltiesText.DOLLAR_SIGN}
                  {type.includes('network') &&
                    !isNaN( parseFloat(networkRoyalties.connectorRoyalties * solana.current_price).toFixed(2))
                    ? parseFloat(networkRoyalties.connectorRoyalties * solana.current_price).toFixed(2)
                    : 0}
                </Text>
                <Text style={styles.symbol}>{royaltiesText.USD}</Text>
              </View>
            </View>
          </View>
        </CardsWrapper>

        <Text style={styles.subTitle}>{royaltiesText.ROYALTIES}</Text>

        {type.includes('network')
          ? networkData.map((data, index) => {
              if (parseInt(data.totalRoyalties) > 0) {
               
                return (
                  <Royalty
                    rank={index + 1}
                    type={type}
                    userName={data.ownerUser?.username}
                    nickName={data.ownerUser?.fullName}
                    avatar={data.ownerUser.avatar}
                    total={data.totalRoyalties}
                    creator={data.creatorRoyalties}
                    connector={data.conectorRoyalties}
                    price={solana.current_price}
                    id={data.ownerUser.id}
                    key={index}
                  />
                );
              }
            })
          : myNetworkData.map((data, index) => {
              if (parseInt(data.totalRoyalties) > 0) {
                return (
                  <Royalty
                    rank={index + 1}
                    type={type}
                    userName={data.ownerUser?.username}
                    nickName={data.ownerUser?.fullName}
                    //  avatar={data.ownerUser?.avatar}
                    total={data.totalRoyalties}
                    creator={data.creatorRoyalties}
                    connector={data.conectorRoyalties}
                    price={solana.current_price}
                    key={index}
                  />
                );
              }
            })}
      </ScrollView>
    </View>
  );
};

export default Royalties;
