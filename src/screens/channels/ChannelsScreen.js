import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import HeaderWithImage from 'src/components/HeaderWithImage';
import BackButton from 'src/components/BackButton';
import ChannelItem from 'src/components/Channel/ChannelItem';
import {Header, TitleHeaderText} from './components/Header';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  marginTop: 24,
});

const ChannelsContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const styles = StyleSheet.create({
  icon: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginRight: 20,
  },
  scroll: {
    paddingBottom: 0,
  },
});

const ChannelsScreen = ({navigation, route}) => {
  const {channel} = route.params;

  return (
    <>
      <Wrapper>
        <ScrollView contentContainerStyle={styles.scroll}>
          <BackButton onPress={() => navigation.goBack()} />
          <Header>
            <TitleHeaderText>{channel.title}</TitleHeaderText>
          </Header>
          <HeaderWithImage avatar={channel.avatar} backgroundImage={channel.backgroundImage} />
          <ChannelsContainer>
            <ChannelItem navigation={navigation} enable={false} channel={channel} />
          </ChannelsContainer>
        </ScrollView>
      </Wrapper>
    </>
  );
};

export default ChannelsScreen;
