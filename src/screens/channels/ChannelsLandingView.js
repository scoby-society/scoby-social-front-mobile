import React from 'react';
import {ActivityIndicator, Alert, ScrollView, StyleSheet} from 'react-native';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import {CloseWhiteIco} from 'assets/svg';
import HeaderWithImage from 'src/components/HeaderWithImage';
import {useQuery} from '@apollo/client';
import {CHANNEL_ERROR_LOADING, CHANNEL_ERROR_TITLE, CHANNEL_NON_EXISTENT} from 'src/constants/Texts';
import ChannelItem from 'src/components/Channel/ChannelItem';
import {GET_CHANNEL_BY_ID} from 'src/graphql/queries/channel';
import {Header, TitleHeaderText} from './components/Header';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  marginTop: 50,
});

const CloseButton = styled.TouchableOpacity({
  left: 0,
  margin: 0,
});

const ChannelsContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const LoadingContainer = styled.View({
  flex: 1,
  height: '100%',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  alignSelf: 'center',
  marginBottom: 100,
  backgroundColor: colors.blueBackgroundSession,
});

const styles = StyleSheet.create({
  icon: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginRight: 20,
  },
  scroll: {
    paddingBottom: 0,
  },
});

const ChannelsLandingView = ({navigation, route, info}) => {
  const ChannelsID = route?.params?.id;
  const id = ChannelsID || info;
  const {
    loading,
    data: {getChatById = []} = [],
    error,
  } = useQuery(GET_CHANNEL_BY_ID, {
    variables: {id},
    fetchPolicy: 'network-only',
  });

  if (loading) {
    return (
      <LoadingContainer>
        <ActivityIndicator size="large" color={colors.white} />
      </LoadingContainer>
    );
  }

  const getErrorAlert = () => {
    const msg = error ? CHANNEL_ERROR_LOADING : CHANNEL_NON_EXISTENT;
    Alert.alert(CHANNEL_ERROR_TITLE, msg, [
      {
        text: 'Ok',
        onPress: () => {
          navigation.goBack();
        },
      },
    ]);
    return null;
  };

  if (error || !getChatById) {
    return getErrorAlert();
  }
  return (
    <Wrapper>
      <ScrollView contentContainerStyle={styles.scroll}>
        <CloseButton onPress={() => navigation.goBack()}>
          <CloseWhiteIco />
        </CloseButton>
        <Header>
          <TitleHeaderText>{getChatById.title}</TitleHeaderText>
        </Header>
        <HeaderWithImage avatar={getChatById.avatar} backgroundImage={getChatById.backgroundImage} />
        <ChannelsContainer>
          <ChannelItem enable={false} channel={getChatById} navigation={navigation} />
        </ChannelsContainer>
      </ScrollView>
    </Wrapper>
  );
};

export default ChannelsLandingView;
