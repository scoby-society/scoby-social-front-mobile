import 'react-native-get-random-values';
import DeepLinking from 'react-native-deep-linking';
import 'react-native-url-polyfill/auto';
import {Buffer} from 'buffer';
import fonts, {avenir} from 'src/constants/Fonts';

import React, {useCallback, useContext, useEffect, useState} from 'react';
import {StyleSheet, Image, Dimensions, Text, Linking, TouchableOpacity, SafeAreaView, View, Alert} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import {
  WALLET_CONNECTED_HEADER,
  WALLET_CONNECT_HEADER,
  WALLET_CONNECT_SUBHEADER,
  CONNECT_WALLET,
  PHANTOM_SUGGEST,
  PHANTOM_GOOGLE_PLAY,
  WALLET_DISCONNECT_MESSAGE,
  WALLET_DISCONNECT_CONFIRM,
  WALLET_CONNECTED_HEADER_REGISTER,
  WALLET_CONNECTED_SUBHEADER,
  ERROR_CONNECT_WALLET,
} from 'src/constants/Texts';
import NewLargeButton from 'src/components/NewLargeButton';
import logo from 'assets/images/logos/Scoby_Final_Logos/ScobyDude_preferred_logo/scoby_dude.png';
import {PhantomLogo} from 'assets/svg';

import nacl from 'tweetnacl';
import bs58 from 'bs58';
import {shortendPublicKey} from 'src/utils/helpers';
import AsyncStorage from '@react-native-community/async-storage';
import {GlobalContext} from 'src/containers/global';
import BottomModal from 'src/components/BottomModal';
import {CLUSTER} from 'src/config/env';
import {VERIFY_PUBLIC_KEY} from 'src/graphql/mutations/auth';
import {useMutation} from '@apollo/client';
import LoadingExperience from 'src/components/HomeExperiences/components/LoadingExperience';
import {UPDATE_PROFILE} from 'src/graphql/mutations/profile';
import {GET_USER_PROFILE} from 'src/graphql/queries/profile';
import {FollowersContext} from 'src/containers/followers';
import BackButton from 'src/components/BackButton';

global.Buffer = global.Buffer || Buffer;
DeepLinking.addScheme('scoby://');

const onConnectRedirectLinkConnect = 'scoby://phantom/onConnect';

const buildUrl = (path, params) => `phantom://v1/${path}?${params.toString()}`;

const decryptPayload = (data, nonce, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');

  const decryptedData = nacl.box.open.after(bs58.decode(data), bs58.decode(nonce), sharedSecret);
  if (!decryptedData) {
    throw new Error('Unable to decrypt data');
  }
  return JSON.parse(Buffer.from(decryptedData).toString('utf8'));
};

const WalletConnectScreen = (props) => {
  const {width} = Dimensions.get('window');
  const {setWalletKey} = useContext(GlobalContext);
  const [loadingData, setLoadingData] = useState(false);
  const createUserProfile = props.route?.params?.createUserProfile;
  const id = props.route?.params?.id;
  const profile = props.route?.params?.profile;
  const onBoarding = props.route?.params?.onBoarding;
  const styles = StyleSheet.create({
    walletContainer: {
      flex: 1,
      justifyContent: 'space-between',
      flexDirection: 'column',
      padding: 25,
    },
    logo: {
      width: width * 0.2,
      height: width * 0.2,
      resizeMode: 'contain',
    },
    content: {
      padding: 24,
      flex: 1,
    },
    connect: {
      backgroundColor: colors.purple,
      width: 220,
      alignSelf: 'center',
      display: 'flex',
      flexDirection: 'row',
    },
    connect_text: {
      ...fonts.avenirSemiBold,
      fontSize: 20,
      color: colors.white,
    },
    next: {
      marginTop: 15,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
    },
    nextBtn: {
      width: 100,
      backgroundColor: colors.newPink,
      padding: 10,
      borderRadius: 10,
      textAlign: 'center',
    },
    nextBtnDisabled: {
      width: 100,
      backgroundColor: colors.disabledButton,
      padding: 10,
      borderRadius: 10,
      textAlign: 'center',
      color: colors.translucentBlack,
    },
    logoContainer: {
      position: 'relative',
    },
    skipBtn: {
      position: 'absolute',
      right: 0,
      top: 20,
    },
    mainContainer: {
      flex: 1,
    },
  });

  const [deepLink, setDeepLink] = useState('');
  const [dappKeyPair, setDappKeyPair] = useState(nacl.box.keyPair());
  const [sessionData, setSessionData] = useState('');
  const [phantomWalletPublicKey, setPhantomWalletPublicKey] = useState('');
  const [visible, setVisible] = useState(false);
  const {setCurrentUserProfile, currentUserProfile} = useContext(FollowersContext);

  const [updateUserProfile] = useMutation(UPDATE_PROFILE, {
    refetchQueries: [{query: GET_USER_PROFILE}],
    onError(e) {
      Alert.alert(e.message, ERROR_CONNECT_WALLET);
    },
    onCompleted(e) {
      setCurrentUserProfile(e.updateUserProfile);
    },
  });

  const [setVerifyUserPublicKey, {loading}] = useMutation(VERIFY_PUBLIC_KEY, {
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e, null, 4));
    },
  });

  const saveData = async (key) => {
    await setVerifyUserPublicKey({
      variables: {
        publicKey: key?.public_key,
      },
      onCompleted: async ({verifyUserPublicKey}) => {
        await AsyncStorage.setItem('token', verifyUserPublicKey.authorizationToken);
        if (key?.public_key) {
          await AsyncStorage.setItem('public_key', key?.public_key);
          await AsyncStorage.setItem('public_session', key?.public_session);
          await AsyncStorage.setItem('phantom_encryption_public_key', key?.phantom_encryption_public_key);
          await AsyncStorage.setItem('dapp_secret_key', key?.dapp_secret_key);
          await AsyncStorage.setItem('dapp_public_key', key?.dapp_public_key);
        }

        setWalletKey(key?.public_key);
        if (props.route.params.from === 'register') {
          props.navigation.navigate('TopicsScreen', {onBoarding, createUserProfile, id, profile});
        } else if (props.route.params.from === 'login') {
          props.navigation.navigate('MainTabs', {
            screen: 'Profile',
          });
        } else {
          props.navigation.goBack();
        }
      },
    });
  };

  const handleSave = useCallback(
    async (key) => {
      // eslint-disable-next-line no-console
      if (key?.public_key !== currentUserProfile?.publicKey) {
        await updateUserProfile({
          variables: {
            profile: {
              publicKey: key?.public_key,
            },
          },
          onCompleted() {
            saveData(key);
          },
        });
      } else if (key?.public_key !== null) {
        saveData(key);
      } else if (key?.public_key === null) {
        await updateUserProfile({
          variables: {
            profile: {
              publicKey: key?.public_key,
            },
          },
        });
      }
    },
    [updateUserProfile],
  );

  const handleDeepLink = ({url}) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        DeepLinking.evaluateUrl(url);
        setDeepLink(url);
      }
    });
  };

  useEffect(() => {
    DeepLinking.addRoute('/phantom/onConnect');
    DeepLinking.addRoute('/phantom/onDisconnect');
    (async () => {
      const initialUrl = await Linking.getInitialURL();
      const publicKey = await AsyncStorage.getItem('public_key');
      const publicSession = await AsyncStorage.getItem('public_session');
      if (publicKey) {
        setSessionData(publicSession);
        setPhantomWalletPublicKey(publicKey);
      }
      if (initialUrl) {
        setDeepLink(initialUrl);
      }
    })();
    const deepLinkingListener = Linking.addEventListener('url', handleDeepLink);
    return () => {
      if (deepLinkingListener) {
        deepLinkingListener.remove();
      }
    };
  }, []);

  const handleNext = useCallback(
    async (data) => {
      handleSave(data);
    },
    [setVerifyUserPublicKey, phantomWalletPublicKey],
  );

  // handle inbounds links
  useEffect(() => {
    if (!deepLink) return;

    const url = new URL(deepLink);
    const params = url.searchParams;

    if (params.get('errorCode')) {
      return;
    }
    if (/onConnect/.test(url.pathname)) {
      const sharedSecretDapp = nacl.box.before(
        bs58.decode(params.get('phantom_encryption_public_key')),
        dappKeyPair.secretKey,
      );

      const connectData = decryptPayload(params.get('data'), params.get('nonce'), sharedSecretDapp);
      const connectedData = {
        public_key: connectData.public_key,
        public_session: connectData.session,
        phantom_encryption_public_key: params.get('phantom_encryption_public_key'),
        dapp_secret_key: bs58.encode(dappKeyPair.secretKey),
        dapp_public_key: bs58.encode(dappKeyPair.publicKey),
      };
      setSessionData(connectData.session);
      setPhantomWalletPublicKey(connectData.public_key);
      handleNext(connectedData);
    }
  }, [deepLink]);

  const handleConnect = async () => {
    if (sessionData.length > 0) {
      setVisible(true);
    } else {
      const params = new URLSearchParams({
        dapp_encryption_public_key: bs58.encode(dappKeyPair.publicKey),
        cluster: CLUSTER,
        app_url: 'https://phantom.app',
        redirect_link: onConnectRedirectLinkConnect,
        key: nacl.randomBytes(24),
      });

      const url = buildUrl('connect', params);
      Linking.openURL(url);
    }
  };

  const handingOpenPhantomUrl = useCallback(() => {
    Linking.openURL(PHANTOM_GOOGLE_PLAY);
  }, []);

  const onRequestClose = () => {
    setVisible(false);
  };

  const onDisconnect = async () => {
    setLoadingData(true);
    await AsyncStorage.removeItem('public_key');
    await AsyncStorage.removeItem('public_session');
    await AsyncStorage.removeItem('phantom_encryption_public_key');
    await AsyncStorage.removeItem('dapp_secret_key');
    await AsyncStorage.removeItem('dapp_public_key');
    setSessionData('');
    setPhantomWalletPublicKey('');
    setVisible(false);
    setDappKeyPair(nacl.box.keyPair());
    setWalletKey(null);
    await handleSave(null);
    setLoadingData(false);
  };

  const skip = async () => {
    if (props.route.params.from === 'register') {
      props.navigation.navigate('TopicsScreen', {onBoarding, createUserProfile, id});
    } else if (props.route.params.from === 'login') {
      props.navigation.navigate('MainTabs', {
        screen: 'Profile',
      });
    } else {
      props.navigation.replace('MainTabs', {
        screen: 'Home',
      });
    }
  };

  if (loading || loadingData) {
    return <LoadingExperience />;
  }

  return (
    <SafeAreaView style={styles.mainContainer}>
      <Wrapper keyboardShouldPersistTaps="handled" contentContainerStyle={styles.content}>
        {props.route.params.from === 'user' && <BackButton navigation={props.navigation} />}
        <View style={styles.walletContainer}>
          <View>
            {props.route.params.from === 'register' && (
              <HeaderTextSub>{WALLET_CONNECTED_HEADER_REGISTER}</HeaderTextSub>
            )}

            <View style={[styles.logoContainer, {marginTop: props.route.params.from !== 'register' ? 60 : 0}]}>
              <LogoContainer>
                <Image source={logo} style={styles.logo} />
              </LogoContainer>
              {props.route.params.from !== 'user' && (
                <View style={styles.skipBtn}>
                  <TouchableOpacity onPress={skip}>
                    <DescriptionText>Skip</DescriptionText>
                  </TouchableOpacity>
                </View>
              )}
            </View>

            {phantomWalletPublicKey !== '' && <HeaderText>{WALLET_CONNECTED_HEADER}</HeaderText>}
            {phantomWalletPublicKey !== '' && <SubtitleText>{WALLET_CONNECTED_SUBHEADER}</SubtitleText>}

            {phantomWalletPublicKey === '' && <HeaderText>{WALLET_CONNECT_HEADER}</HeaderText>}
            {phantomWalletPublicKey === '' && <SubtitleText>{WALLET_CONNECT_SUBHEADER}</SubtitleText>}

            <NewLargeButton style={styles.connect} onPress={handleConnect}>
              {phantomWalletPublicKey !== '' ? (
                <Text style={styles.connect_text}>{shortendPublicKey(phantomWalletPublicKey)}</Text>
              ) : (
                <Text style={styles.connect_text}>{CONNECT_WALLET}</Text>
              )}
            </NewLargeButton>
          </View>
          <View>
            <PhantomBox>
              <CenteredSubtitle>{PHANTOM_SUGGEST}</CenteredSubtitle>
              <NewLargeButton style={styles.connect} onPress={handingOpenPhantomUrl}>
                <PhantomLogo
                  width="35"
                  style={{
                    marginRight: 10,
                  }}
                />
                <Text style={styles.connect_text}>Phantom</Text>
              </NewLargeButton>
            </PhantomBox>
          </View>
        </View>

        <BottomModal visible={visible} onRequestClose={onRequestClose} opaque>
          <Text>{WALLET_DISCONNECT_MESSAGE}</Text>
          <NewLargeButton onPress={onDisconnect} title={WALLET_DISCONNECT_CONFIRM} />
        </BottomModal>
      </Wrapper>
    </SafeAreaView>
  );
};

export default WalletConnectScreen;
const Wrapper = styled(KeyboardAwareScrollView)({
  backgroundColor: colors.blueBackgroundSession,
  flex: 1,
});
const LogoContainer = styled.View({
  alignItems: 'center',
});

const HeaderText = styled.Text({
  ...fonts.goudy,
  paddingTop: 30,
  paddingBottom: 8,
  color: colors.white,
  fontSize: 24,
  lineHeight: '32px',
  textAlign: 'center',
});

const HeaderTextSub = styled.Text({
  ...fonts.goudy,
  paddingTop: 50,
  paddingBottom: 30,
  color: colors.white,
  fontSize: 16,
  textAlign: 'center',
});

const SubtitleText = styled.Text({
  ...fonts.avenir,
  fontSize: 16,
  color: colors.white,
  textAlign: 'center',
});

const PhantomBox = styled.View({
  padding: '30px 30px 5px',
  background: 'rgba(255, 255, 255, 0.04)',
  borderRadius: '10px',
});
const CenteredSubtitle = styled.Text({
  ...fonts.avenir,
  fontSize: 16,
  color: colors.white,
  textAlign: 'center',
});

const DescriptionText = styled.Text({
  avenir,
  fontSize: 14,
  color: colors.white,
  textAlign: 'right',
});
