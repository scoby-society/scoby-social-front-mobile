import React from 'react';
import {View} from 'react-native';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import styled from 'styled-components/native';

const Text=styled.Text({
    ...Fonts.avenir,
    color:'#fff',
    fontSize:18
})

const TitleText=styled.Text({
    ...Fonts.goudy,
    color:'#fff',
    fontSize:20,
    textAlign:'center',
    marginBottom:40,
})

const SelectLoginOption=({navigation})=>{
    return(
        <View style={{justifyContent:'center', alignContent:'center', flex:1}}>
            <TitleText>Select Login Mode</TitleText>
            <RegularButton 
                style={{display:'flex', justifyContent:'center', alignItems:'center'}}
                onPress={()=>navigation.navigate('LoginWithPhone')}
            >
                <Text>Traditional Login</Text>
            </RegularButton>
            <RegularButton 
                style={{marginTop:30, display:'flex', justifyContent:'center', alignItems:'center'}}
                onPress={()=>navigation.navigate('WalletConnect')}
            >
                <Text>Wallet Login</Text>
            </RegularButton>
        </View>
    );
}

export default SelectLoginOption