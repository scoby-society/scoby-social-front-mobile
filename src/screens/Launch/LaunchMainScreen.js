import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput,ScrollView } from 'react-native';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import styled from 'styled-components';
import PriceOfProject from './components/PriceOfProject';
import UploadImg from './components/UploadImg';


const Label = styled.Text(({ experience = 14 }) => ({
    marginLeft: 5,
    fontWeight: '750',
    paddingTop: '13%',
    ...Fonts.avenir,
    fontSize: experience,
    color: Colors.white,
}));

function LaunchMainScreen({ }) {
    const [hasFocusName, setFocusName] = useState(false);
    const [name, setName] = useState('')
    const [symbol, setSymbol] = useState('')
    const [description, setDescription] = useState('')

    const [hasFocusDescription, setFocusDescription] = useState(false);

    const handleChangeName = (e) => {
        setName(e);
    };

    const handleChangeSymbol = (e) => {
        setSymbol(e);
    };

    return (
        <ScrollView style={styles.wrap}>
            <Text style={styles.title}>Launch your Social NFT Project</Text>
            <Text style={styles.info}>The information you provide will be written on the Solana blockchain and cannot be changed later.</Text>

            <Label>Name of Project</Label>
            <TextInput
                onFocus={() => setFocusName(true)}
                onBlur={() => setFocusName(false)}
                style={hasFocusName ? styles.inputFocus : styles.input}
                onChangeText={(e) => handleChangeName(e)}
                value={name}
                placeholder="Name of Project"
                placeholderTextColor={Colors.greySession}
                maxLength={32}
            />
            <Text style={styles.limit}>*Limit 32 Characters</Text>

            <Label>Symbol</Label>
            <TextInput
                onFocus={() => setFocusName(true)}
                onBlur={() => setFocusName(false)}
                style={hasFocusName ? styles.inputFocus : styles.input}
                onChangeText={(e) => handleChangeSymbol(e)}
                value={symbol}
                placeholder="Symbol"
                placeholderTextColor={Colors.greySession}
                maxLength={8}
            />
            <Text style={styles.limit}>*Limit 8 Characters</Text>

            <Label>Description</Label>
            <Text style={styles.info}>This is where you'll explain your project in detail, including what holders of this Project Pass will receive.</Text>
            <TextInput
                onFocus={() => setFocusDescription(true)}
                onBlur={() => setFocusDescription(false)}
                style={hasFocusDescription ? styles.inputDescriptionFocus : styles.inputDescription}
                onChangeText={(e) =>
                    inputTextUpdate(e, setDescription, setDescriptionCounter, description, descriptionCounter)
                }
                value={description}
                placeholder="Description"
                placeholderTextColor={Colors.greySession}
                maxLength={277}
                multiline
                textAlignVertical="top"
                numberOfLines={5}
                blurOnSubmit
            />
            
            <Label>Description</Label>
            <Text style={styles.info}>This is where you'll explain your project in detail, including what holders of this Project Pass will receive.</Text>
            <UploadImg/>
            <PriceOfProject/>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    wrap: {
        height: '100%',
        flex:1,
        paddingLeft: 37,
        paddingRight: 37,
        paddingTop: 72
    },
    title: {
        ...Fonts.goudy,
        color: 'white',
        fontSize: 24,
        fontWeight: '500',
        marginTop: 100,
        marginBottom: 20

    },
    info: {
        ...Fonts.avenir,
        fontSize: 12,
        marginTop: 0
    },
    input: {
        height: 50,
        ...Fonts.avenir,
        padding: 16,
        marginTop: 5,
        marginBottom: -15,
        color: Colors.white,
        backgroundColor: Colors.transparent,
        borderRadius: 15,
        borderColor: '#FFFFFF54',
        borderWidth: 1,
        marginVertical: 24,
    },
    inputFocus: {
        height: 50,
        ...Fonts.avenir,
        padding: 16,
        marginTop: 5,
        marginBottom: -15,
        color: Colors.white,
        backgroundColor: '#FFFFFF0A',
        borderRadius: 15,
        borderColor: '#FFFFFF80',
        borderWidth: 1,
        marginVertical: 24,
    },
    limit: {
        ...Fonts.avenir,
        fontWeight: '500',
        fontSize: 10,
        color: '#9f9f9f',
        top: 20,
        left: 10
    },
    inputDescription: {
        ...Fonts.avenir,
        padding: 16,
        paddingTop: 16,
        marginTop: 5,
        minHeight: 96,
        color: Colors.white,
        backgroundColor: Colors.transparent,
        borderRadius: 15,
        borderColor: '#FFFFFF54',
        borderWidth: 1.5,
    },
    inputDescriptionFocus: {
        ...Fonts.avenir,
        padding: 16,
        paddingTop: 16,
        marginTop: 5,
        minHeight: 96,
        color: Colors.white,
        backgroundColor: '#FFFFFF0A',
        borderRadius: 15,
        borderColor: '#FFFFFF80',
        borderWidth: 1.5,
    },
})

export default LaunchMainScreen;