import { Img } from 'assets/svg';
import React,{useState,useCallback, useEffect} from 'react';
import { View,StyleSheet,Text, TouchableOpacity,Image } from 'react-native';
import PromptModal from 'src/components/Modal/PromptModal';
import { CAMERA, GALLERY, PHOTO_CHOOSE } from 'src/constants/Texts';
import ImagePicker from 'react-native-image-crop-picker';

function UploadImg(props) {
    const [modalVisible, setModalVisible] = useState(undefined);
    const [avatar, setAvatar]= useState('')

    const chooseImage = useCallback(
        (bg, type) => {
          if (type === 'CAMERA') {
            ImagePicker.openCamera({
              compressImageMaxHeight: bg ? 600 : 1000,
              compressImageMaxWidth: bg ? 1000 : 1000,
              mediaType: 'photo',
              useFrontCamera: true,
              cropperCircleOverlay: !bg,
              freeStyleCropEnabled: true,
              cropping: true,
              forceJpg: true,
            }).then((response) => setImage(response, bg));
          }
    
          if (type === 'GALERY') {
            ImagePicker.openPicker({
             // compressImageMaxHeight: bg ? 600 : 1000,
             // compressImageMaxWidth: bg ? 1000 : 1000,
              mediaType: 'photo',
              cropperCircleOverlay: !bg,
              freeStyleCropEnabled: true,
              cropping: true,
              forceJpg: true,
            }).then((response) => setImage(response, bg));
          }
        },
        [setImage],
      );

      const hideModal = useCallback(
        (state) => {
          setModalVisible(state ? 'AVATAR' : undefined);
        },
        [setModalVisible],
      );

      const handleChoise = useCallback(
        (type) => {
          modalVisible === 'COVER' ? handleChangeCover(type) : handleChangeAvatar(type);
        },
        [modalVisible, handleChangeAvatar],
      );
    
      const setImage = useCallback(

        (response, bg) => {
            console.log(bg)
          if (!response.didCancel && !response.errorCode && !response.errorMessage) {
            if (true) {
              setAvatar(response.path);
            }
          } else if (response.errorMessage) {
            Alert.alert('Image Error', response.errorMessage);
          } else {
            Alert.alert('Error', 'Wrong');
          }
        },
        [setAvatar],
      );

      const handleChangeAvatar = useCallback(
        (type) => {
          setModalVisible(undefined);
          setTimeout(() => chooseImage(false, type), 500);
        },
        [chooseImage],
      );

      useEffect(()=>{
        console.log(avatar)
      },[avatar])


    return (
       
       <>
        <PromptModal
        visible={modalVisible !== undefined}
        setVisible={hideModal}
        text={PHOTO_CHOOSE}
        leftButtonText={CAMERA}
        rightButtonText={GALLERY}
        onLeftButtonPress={() => {
          handleChoise('CAMERA');
          setModalVisible(undefined);
        }}
        onRightButtonPress={() => {
          handleChoise('GALERY');
          setModalVisible(undefined);
        }}
      />



         <View style={styles.wrapImg}>
            {avatar.length===0?
                <Img/>
                :
                <Image 
                    source={{uri:avatar}}
                    style={styles.img}
                
                />
            }
        </View>

        <TouchableOpacity style={styles.btn} onPress={()=>setModalVisible(!modalVisible)}>
            <Text>Upload Image</Text>
        </TouchableOpacity>
       
       </>
    );
}

const styles=StyleSheet.create({
    wrapImg:{
        width:'100%',
        height:176,
        borderColor: '#FFFFFF54',
        borderWidth:1,
        borderRadius:10,
        marginBottom:100,
        justifyContent:'center',
        alignItems:'center'
    },
    btn:{
        width:107,
        height:42,
        backgroundColor:'#cd068e',
        top:-90,
        alignSelf:'center',
        borderRadius:8,
        alignItems:'center',
        justifyContent:'center',
        
    },
    img:{
        height:'90%',
        width:'60%'
    }
})

export default UploadImg;