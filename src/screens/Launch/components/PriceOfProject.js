import React from 'react';
import { View,Text,StyleSheet } from 'react-native';
import Fonts from 'src/constants/Fonts';

function PriceOfProject(props) {
    return (
        <View style={styles.wrap}>
            <Text style={styles.title}>Price of Project Pass</Text>
        </View>
    );
}

const styles=StyleSheet.create({
    title:{
        ...Fonts.avenir,
        color:'white',
        fontWeight:'700',
        
    },
    wrap:{
        height:100
    }
})

export default PriceOfProject;