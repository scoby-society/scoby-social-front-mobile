import React, {useEffect, useState} from 'react';
import {ActivityIndicator, ScrollView, StyleSheet} from 'react-native';
import SerieHome from 'src/components/Series';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import HeaderWithImage from 'src/components/HeaderWithImage';
import BackButton from 'src/components/BackButton';
import {useLazyQuery} from '@apollo/client';
import {GET_USER_SERIES} from 'src/graphql/queries/profile';
import {Header, TitleHeaderText} from './components/Header';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  marginTop: 24,
});

const SeriesContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const LoadingContainer = styled.View({
  flex: 1,
  height: '100%',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  alignSelf: 'center',
  marginBottom: 100,
  backgroundColor: colors.blueBackgroundSession,
});

const styles = StyleSheet.create({
  icon: {
    width: 50,
    height: 50,
    alignSelf: 'center',
    marginRight: 20,
  },
  scroll: {
    paddingBottom: 0,
  },
});

const SeriesScreen = ({navigation, route}) => {
  const {series} = route.params;
  const [currentSeries, setCurrentSeries] = useState(series);

  const [getUserSeries, {loading}] = useLazyQuery(GET_USER_SERIES, {
    onCompleted(data) {
      setCurrentSeries(data);
    },
  });

  useEffect(() => {
    if (!currentSeries) {
      getUserSeries();
    }
  }, []);

  if (loading) {
    return (
      <LoadingContainer>
        <ActivityIndicator size="large" color={colors.white} />
      </LoadingContainer>
    );
  }

  return (
    <>
      <Wrapper>
        <ScrollView contentContainerStyle={styles.scroll}>
          <Header>
            <TitleHeaderText>{series.seriesName}</TitleHeaderText>
          </Header>
          <HeaderWithImage avatar={series.avatar} backgroundImage={series.backgroundImage} />
          <SeriesContainer>
            <SerieHome series={series} navigation={navigation} />
          </SeriesContainer>
        </ScrollView>
      </Wrapper>
      <BackButton onPress={() => navigation.goBack()} />
    </>
  );
};

export default SeriesScreen;
