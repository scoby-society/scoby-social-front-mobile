import React, {useCallback, useContext, useEffect, useRef, useState} from 'react';
import {StyleSheet, Animated, Alert, View, Dimensions} from 'react-native';
import Share from 'react-native-share';
import {VerifiedIco, ShareIco} from 'assets/ico';
import {CogIcon} from 'assets/svg';
import HeaderWithImage from 'src/components/HeaderWithImage';
import NewLargeButton from 'src/components/NewLargeButton';
import {statusBarHeight} from 'src/components/withSafeArea';
import lodash from 'lodash';
import colors from 'src/constants/Colors';
import openLink from 'src/utils/hook/openLink';
import firebase from '@react-native-firebase/app';
import {
  PROFILE_HEADER,
  PROFILE_BUTTON_EDIT,
  PROFILE_LINK_TITLE_TEXT,
  PROFILE_LINK_TEXT,
  UNKNOWN_ERROR_TEXT,
} from 'src/constants/Texts';
import Fonts from 'src/constants/Fonts';
import {FollowersContext} from 'src/containers/followers';
import {tabsHeight} from 'src/navigation/tabs';
import styled from 'styled-components/native';

import 'react-native-get-random-values';
import 'react-native-url-polyfill/auto';
import {DYNAMIC_LINKS_DEFAULTS, DYNAMIC_LINKS_HEADERS, DYNAMIC_LINKS_SHORTENER_URL} from 'src/constants/Variables';
import Toast from 'react-native-root-toast';
import ProfileList from 'src/navigation/profileList';
import {Header, HeaderLeftButton, HeaderRightButton, HeaderTitleText} from './components/Header';
import MyFollowersContent from './components/MyFollowersContent';

const Container = styled.View({
  backgroundColor: colors.blueBackgroundSession,
  height: '100%',
});

const FullName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 18,
  lineHeight: '26px',
  color: colors.white,
});

const ContentText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  paddingBottom: 16,
  color: colors.white,
});

const WebsiteText = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  color: '#ec008c',
});

const OpenLinkBtn = styled.TouchableOpacity`
  flex-direction: row;
  padding-bottom: 12px;
  margin-bottom: 6px;
`;

const SettingsButton = styled.TouchableOpacity({
  position: 'absolute',
  right: 0,
  top: 0,
  padding: 16,
});

const DetailsContainer = styled.TouchableOpacity({
  backgroundColor: colors.blueBackgroundSession,
  paddingHorizontal: 24,
  zIndex: -2,
});

const HorizontalBottons = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
});

const MainProfileInfoContainer = styled.View({
  zIndex: -1,
});

const NameLinkWrapper = styled.View({
  marginVertical: 20,
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'row',
});

const LinkContainer = styled.View({
  maxWidth: '50%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
});

const LinkText = styled.Text`
  color: #ffffff;
  margin-bottom: ${(props) => (props.title ? '4px' : '0px')};
  font-weight: ${(props) => (props.title ? 700 : 400)};
  line-height: 15px;
  font-size: ${(props) => (props.title ? '12px' : '9px')};
  text-align: center;
`;

const ShareProfileButtonText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 12,
  textDecoration: 'underline #fff',
  textAlign: 'center',
  marginRight: 8,
  color: colors.white,
});

const ShareProfileButton = styled.TouchableOpacity`
  padding-horizontal: 10px;
  padding-vertical: 5px;
  margin-top: 4px;
  color: ${colors.white};
  background-color: #cd068e;
  border-radius: 8px;
  align-items: center;
  flex-direction: row;
  justify-content: center;
`;

const HorizontalProfileInfo = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
});

const VerticalProfileInfo = styled.View({
  flexDirection: 'column',
});

const {height, width} = Dimensions.get('screen');

export default function ProfileScreen({navigation}) {
  const {currentUserProfile} = useContext(FollowersContext);
  const {id, username, fullName, avatar, website, bio, backgroundImage /* role */} = currentUserProfile;
  const [showHeader, setShowHeader] = useState(true);
  const [headerSize, setHeaderSize] = useState(0);
  const HideHeader = useRef(new Animated.Value(0)).current;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const ScrollUp = () => {
    Animated.timing(HideHeader, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true,
    }).start();
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const ScrollDown = () => {
    Animated.timing(HideHeader, {
      toValue: -(headerSize + 10),
      duration: 500,
      useNativeDriver: true,
    }).start();
  };

  const buildProfileLink = useCallback(async (idUser) => {
    try {
      const body = JSON.stringify(
        lodash.merge(DYNAMIC_LINKS_DEFAULTS, {
          dynamicLinkInfo: {
            link: `${DYNAMIC_LINKS_DEFAULTS.dynamicLinkInfo.domainUriPrefix}/user/${idUser}`,
          },
        }),
      );

      const response = await fetch(`${DYNAMIC_LINKS_SHORTENER_URL}?key=${firebase.app().options.apiKey}`, {
        method: 'POST',
        headers: DYNAMIC_LINKS_HEADERS,
        body,
      });
      const json = await response.json();
      return json.shortLink;
    } catch (error) {
      return null;
    }
  }, []);

  const shareUrlProfile = useCallback(async () => {
    const url = await buildProfileLink(id);
    if (url) {
      Share.open({
        message: `Join me on Scoby and let’s have a Meaningful Conversation!`,
        url,
      })
        .catch((e) => {
          // eslint-disable-next-line no-console
          console.log(e);
        })
        .then((e) => {
          const response = `${e?.app}`;
          if (response?.includes('CopyToPasteboard')) {
            Toast.show('Link was copy', {
              duration: Toast.durations.LONG,
              position: Toast.positions.BOTTOM,
              shadow: true,
              animation: true,
              hideOnPress: true,
            });
          }
        });
    } else {
      Alert.alert(UNKNOWN_ERROR_TEXT);
    }
  }, [buildProfileLink, id]);

  const shareButtonText = `scoby.one/${username}`;

  useEffect(() => {
    if (showHeader) {
      ScrollUp();
    } else {
      ScrollDown();
    }
  }, [showHeader, ScrollDown, ScrollUp]);

  const handleSettingsOpen = () => {
    navigation.navigate('Settings');
  };

  const styles = StyleSheet.create({
    scroll: {
      paddingBottom: tabsHeight + statusBarHeight,
      justifyContent: 'center',
      minHeight: '80%',
    },
    button: {
      height: 32,
      width: '50%',
      marginBottom: 0,
      marginTop: 0,
      paddingTop: 0,
    },
    buttonPurple: {
      height: 32,
      width: '45%',
      marginBottom: 0,
      marginTop: 0,
      paddingTop: 0,
      backgroundColor: colors.purple,
    },
    buttonPurpleDisable: {
      height: 32,
      width: '45%',
      marginBottom: 0,
      marginTop: 0,
      paddingTop: 0,
      backgroundColor: colors.purpleBackgroundHalfOpacity,
    },
    activeText: {
      fontWeight: 'bold',
    },
  });

  const MainProfileInfo = () => (
    <MainProfileInfoContainer>
      <HeaderWithImage avatar={avatar} backgroundImage={backgroundImage} nonBlink />
      <SettingsButton onPress={handleSettingsOpen}>
        <CogIcon />
      </SettingsButton>
      <MyFollowersContent navigation={navigation} />
    </MainProfileInfoContainer>
  );

  const MainProfileInfoUser = () => (
    <DetailsContainer activeOpacity={1} onPress={() => setShowHeader(true)}>
      <HorizontalProfileInfo>
        <VerticalProfileInfo>
          <NameLinkWrapper>
            <FullName>
              {fullName ? `${fullName}` : `@${username}`} {currentUserProfile.publicKey && <VerifiedIco />}
            </FullName>
          </NameLinkWrapper>
          {bio && bio.length > 0 ? <ContentText style={{maxWidth: (width - 20) / 2}}>{bio || ''}</ContentText> : null}
          {website && website.length ? (
            <OpenLinkBtn onPress={() => openLink(website)}>
              <WebsiteText>{website.toLowerCase()}</WebsiteText>
            </OpenLinkBtn>
          ) : null}
        </VerticalProfileInfo>
        <LinkContainer>
          <LinkText title>{PROFILE_LINK_TITLE_TEXT}</LinkText>
          <LinkText>{PROFILE_LINK_TEXT}</LinkText>
          <ShareProfileButton large active onPress={() => shareUrlProfile()}>
            <ShareProfileButtonText>{shareButtonText}</ShareProfileButtonText>
            <ShareIco />
          </ShareProfileButton>
        </LinkContainer>
      </HorizontalProfileInfo>
      <HorizontalBottons>
        <NewLargeButton
          style={styles.buttonPurple}
          onPress={() => navigation.navigate('EditProfile')}
          title={PROFILE_BUTTON_EDIT}
        />
      </HorizontalBottons>
    </DetailsContainer>
  );

  return (
    <>
      <Container>
        <Animated.View
          style={{
            height: height + 160,
            transform: [
              {
                translateY: HideHeader,
              },
            ],
          }}>
          <View
            onLayout={(event) => {
              setHeaderSize(Math.round(event.nativeEvent.layout.height));
            }}>
            <Header>
              <HeaderLeftButton />
              <HeaderTitleText>{PROFILE_HEADER}</HeaderTitleText>
              <HeaderRightButton />
            </Header>
            <MainProfileInfo />
          </View>
          <MainProfileInfoUser />
          <ProfileList setShowHeader={setShowHeader} />
        </Animated.View>
      </Container>
    </>
  );
}
