import React, {useEffect, useState} from 'react';
import styled from 'styled-components/native';
import CommonSwitch from 'src/components/Switch';
import {HIDE_SPORES, SHOW_SPORES} from 'src/graphql/mutations/NFT';
import {useMutation} from '@apollo/client';

const Switch = styled(CommonSwitch)({
  width: 20,
});
const Card = styled.View`
  justify-content: space-around;
  align-items: center;
  width: 190px;
  height: auto;
  background-color: rgba(180, 180, 220, 0.2);
  border: 1px solid rgba(150, 150, 255, 0.5);
  border-radius: 30px;
  margin: 10px 5px;
`;
const Text = styled.Text`
  color: white;
  margin-right: 10px;
  margin-left: 10px;
  font-family: Avenir;
  font-size: 12px;
`;

const SubTittle = styled.Text`
  color: rgba(100, 100, 100, 1);
  font-size: 10px;
`;
const Row = styled.View`
  display: flex;
  flex-direction: row;
  margin-bottom: 8px;
`;
const Left = styled.View`
  display: flex;
  justify-content: flex-start;
  width: 85%;
  margin-bottom: 8px;
`;
const Image = styled.Image`
  margin-top: 8px;
  width: 170px;
  height: 170px;
  border-radius: 22px;
`;

const CardNFT = ({nft, GeneralState}) => {
  const [isEnabled, setIsEnabled] = useState(nft.isVisible);
  const [showSpores] = useMutation(SHOW_SPORES);
  const [hideSpores] = useMutation(HIDE_SPORES);

  const toggleSwitch = () => {
    setIsEnabled(isEnabled !== true);
    if (isEnabled === true) {
      hideSpores({
        variables: {
          idSpores: nft.id,
        },
      });
    }

    if (isEnabled === false) {
      showSpores({
        variables: {
          idSpores: nft.id,
        },
      });
    }
  };

  useEffect(() => {
    setIsEnabled(GeneralState);
    if (GeneralState === false) {
      hideSpores({
        variables: {
          idSpores: nft.id,
        },
      });
    }

    if (GeneralState === true) {
      showSpores({
        variables: {
          idSpores: nft.id,
        },
      });
    }
  }, [GeneralState]);

  useEffect(() => {
    setIsEnabled(nft?.isVisible);
  }, [nft]);

  return (
    <>
      <Card>
        <Image source={{uri: nft.urlImage}} />
        <Left>
          <SubTittle>{nft.name}</SubTittle>
        </Left>
        <Row>
          <Text>Hide</Text>
          <Text>Show</Text>
        </Row>

        <Row>
          <Switch
            circleSize={12}
            switchLeftPx={2.5}
            switchRightPx={2.2}
            barHeight={20}
            containerStyle={{border: 0}}
            switchWidthMultiplier={3}
            backgroundActive="rgba(190, 239, 0, 0.65)"
            backgroundInactive="rgba(255, 74, 81, 0.65)"
            thumbColor="rgb(200,200,200)"
            setValue={toggleSwitch}
            value={isEnabled}
          />
        </Row>
      </Card>
    </>
  );
};
export default CardNFT;
