/* eslint-disable no-use-before-define */
import {useLazyQuery} from '@apollo/client';
// import { useNavigation } from '@react-navigation/native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {ActivityIndicator, Alert, StyleSheet, Dimensions} from 'react-native';
import FilterInput from 'src/components/FilterInput';
import {ProfileTeamCard} from 'src/components/TeamCard';
import colors from 'src/constants/Colors';
import {avenirBold} from 'src/constants/Fonts';
import {NO_TEAMS, UNKNOWN_ERROR_TEXT} from 'src/constants/Texts';
import {GET_USERS_TEAMS} from 'src/graphql/queries/team';
import styled from 'styled-components/native';

const windowHeight = Dimensions.get('window').height;

const Container = styled.View({
  width: '100%',
  height: '100%',
  marginRight: 10,
});

const InputContainer = styled.View({
  width: '90%',
  marginTop: 10,
  marginLeft: 'auto',
  marginRight: 'auto',
});

const TeamList = styled.FlatList({
  height: windowHeight,
  flexGrow: 0,
  marginLeft: 10,
  marginRight: 10,
});

const EmptyStateContainer = styled.View({
  flex: 1,
  alignItems: 'center',
});

const EmptyStateText = styled.Text({
  ...avenirBold,
  color: colors.greySession,
  marginTop: '20%',
  fontSize: 22,
});

export const Teams = ({setShowHeader}) => {
  const [query, setQuery] = useState('');
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const scrollView = useRef();
  const limit = 5;

  const [refetch, {data: {getUsersTeams} = {getUsersTeams: {data: []}}, fetchMore, networkStatus, loading, error}] =
    useLazyQuery(GET_USERS_TEAMS, {
      pollInterval: 10000,
      fetchPolicy: 'network-only',
      variables: {
        paging: {
          limit,
          page,
        },
        query,
      },
      onCompleted(data) {
        setTotal(data.getUsersTeams.paging.total);
        setIsLoading(false);
      },
      onError(e) {
        Alert.alert(UNKNOWN_ERROR_TEXT, e.message);
      },
      notifyOnNetworkStatusChange: true,
    });

  const handleEndReached = useCallback(() => {
    if (getUsersTeams?.data?.length || total > 0) {
      setPage(page + 1);
      fetchMore({
        variables: {
          paging: {
            limit,
            page: page + 1,
          },
          query,
        },
        fetchPolicy: 'network-only',
      });
    }
  }, [fetchMore, page, query, total, getUsersTeams?.data?.length]);

  const handleRefresh = useCallback(() => {
    setPage(1);

    refetch({
      fetchPolicy: 'no-cache',
      variables: {
        paging: {
          limit,
          page: 1,
        },
        query,
      },
    });
    scrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
  }, [query, refetch]);

  useEffect(() => {
    scrollView.current?.scrollToOffset({x: 0, y: 0, animated: true});
    const timeout = setTimeout(
      () => {
        setPage(1);
        refetch({
          variables: {
            paging: {
              limit,
              page: 1,
            },
            query,
          },
        });
      },
      query ? 400 : 0,
    );

    return () => {
      clearTimeout(timeout);
    };
  }, [query, refetch]);

  useEffect(() => {
    setTotal(getUsersTeams?.paging?.total);
  }, [getUsersTeams?.paging?.total]);

  /* useEffect(() => {
    const unsubscribeFocus = navigation.addListener('focus', () => {
      refetch({
        variables: {
          paging: {
            limit,
            page: 1,
          },
          query,
        },
      });
      setQuery('');
      setIsLoading(false);
      setPage(1);
    });

    return unsubscribeFocus;
  }, [navigation, refetch]); */

  // const isEmptyState = data.getUsersTeams.data.length === 0;
  const isAnimating = (loading || getUsersTeams?.data?.length || total > 0) && !error;
  const isIndicatorShown = isLoading || networkStatus === 4;

  const EmptyComponent = () =>
    isIndicatorShown ? (
      <ActivityIndicator size="large" color={colors.white} />
    ) : (
      <EmptyStateContainer>
        <EmptyStateText>{NO_TEAMS}</EmptyStateText>
      </EmptyStateContainer>
    );

  const keyExtractor = ({id}) => `team-card${id}-${Math.random()}`;

  const renderItem = (team) => (
    <ProfileTeamCard
      name={team.item.name}
      ownerUser={team.item.ownerUser}
      members={team.item.members}
      teamId={team.item.id}
      membersAllowedToInvite={team.item.membersAllowedToInvite}
      description={team.item.description}
      topics={team.item.topics}
    />
  );

  return (
    <Container>
      <InputContainer>
        <FilterInput value={query} onChangeText={setQuery} autoCorrect={false} onFocus={() => setShowHeader(false)} />
      </InputContainer>
      <TeamList
        EmptyComponent={<EmptyComponent />}
        ref={scrollView}
        onScroll={() => setShowHeader(false)}
        data={getUsersTeams?.data || []}
        renderItem={renderItem}
        onEndReached={handleEndReached}
        onEndReachedThreshold={0.2}
        refreshing={false}
        keyExtractor={keyExtractor}
        onRefresh={handleRefresh}
        showsVerticalScrollIndicator={false}
        endReached
        keyboardShouldPersistTaps="handled"
        keyboardDismissMode="interactive"
        contentContainerStyle={styles.scroll}
        ListFooterComponent={
          loading ? <ActivityIndicator size="large" color={colors.white} animating={isAnimating} /> : null
        }
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  scroll: {
    minHeight: windowHeight - 400,
    paddingBottom: 60,
  },
});
