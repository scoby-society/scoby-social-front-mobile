import React, {useContext, useMemo} from 'react';
import {FlatList} from 'react-native';
import {GET_USER_SERIES, GET_USER_EVENTS} from 'src/graphql/queries/profile';
import {FollowersContext} from 'src/containers/followers';
import SerieHome from 'src/components/Series';
import EventItem from 'src/components/Event/EventItem';
import {GET_FOLLOWERS} from 'src/graphql/queries/followers';
import {useQuery} from '@apollo/client';
import {GET_CHANNELS} from 'src/graphql/queries/channel';
import ChannelItem from 'src/components/Channel/ChannelItem';

const ExperiencesForeignUser = ({navigation, id, handleClose}) => {
  const {currentUserProfile} = useContext(FollowersContext);

  const {data: {getUserSeries = []} = []} = useQuery(GET_USER_SERIES, {
    variables: {id: parseFloat(id, 10)},
    fetchPolicy: 'network-only',
  });

  const {data: {getUserEvents = []} = []} = useQuery(GET_USER_EVENTS, {
    variables: {id},
    fetchPolicy: 'network-only',
  });

  const {data: {getAllChatExperience = []} = []} = useQuery(GET_CHANNELS, {
    fetchPolicy: 'network-only',
  });

  const allChat = useMemo(
    () => getAllChatExperience.filter((chat) => chat.ownerUser.id === id),
    [getAllChatExperience, id],
  );

  const {data: {getFollowerUsers} = {getFollowerUsers: {data: []}}} = useQuery(GET_FOLLOWERS, {
    variables: {
      paging: {
        limit: 100,
        page: 1,
      },
      userId: currentUserProfile.id,
    },
  });

  const data = useMemo(() => {
    const events = getUserEvents?.filter((e) => e.eventType !== 'Secret');
    const series = getUserSeries?.filter((e) => e.serieType !== 'Secret');
    return [...series, ...events, ...allChat];
  }, [allChat, getUserEvents, getUserSeries]);

  const openSeries = (series) => {
    handleClose();
    navigation.navigate('SeriesLandingView', {id: series.id});
  };

  const openEvents = (events) => {
    handleClose();
    navigation.navigate('EventLandingView', {id: events.id});
  };

  const openChannel = (channel) => {
    handleClose();
    navigation.navigate('ChannelLandingView', {id: channel.id});
  };

  const handleJoinSession = (session) => {
    const useJoinSession = {
      hostName: session.ownerUser.username,
      sessionName: session.title,
      sessionDescription: session.description,
      id: session.id,
      vonageSessionId: session.vonageSessionToken,
      userId: currentUserProfile.id,
      followers: getFollowerUsers && getFollowerUsers.data,
    };
    navigation.navigate('JoinSession', {params: useJoinSession});
  };

  const renderItem = ({item}) => {
    if (item.schedule) {
      return (
        <SerieHome
          series={item}
          enable
          openSerie={openSeries}
          navigation={navigation}
          onJoinSession={handleJoinSession}
          setVisible={handleClose}
        />
      );
    }
    if (item.eventType) {
      return (
        <EventItem
          event={item}
          navigation={navigation}
          enable
          openEvent={openEvents}
          onJoinSession={handleJoinSession}
          setVisible={handleClose}
        />
      );
    }
    if (item.chatType) {
      return (
        <ChannelItem channel={item} navigation={navigation} enable openChannel={openChannel} setVisible={handleClose} />
      );
    }
  };

  return <FlatList data={data} renderItem={renderItem} keyExtractor={(item) => `${item.id}`} />;
};

export default ExperiencesForeignUser;
