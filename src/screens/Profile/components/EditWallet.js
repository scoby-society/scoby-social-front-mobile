import React, {useCallback, useState} from 'react';
import {Modal} from 'react-native';
import styled from 'styled-components';
import Input from 'src/components/Input';
import {statusBarHeight} from 'src/components/withSafeArea';
import {PROFILE_EDIT_SUBTITLE_PUBLICKEY} from 'src/constants/Texts';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import ModalHeader from './ModalHeader';
import ListItem from './ListItem';

const ModalWrapper = styled.View({
  flex: 1,
  paddingTop: statusBarHeight,
  backgroundColor: Colors.blueBackgroundSession,
});

const ModalContent = styled.View({
  paddingHorizontal: 30,
});

const SubTitle = styled.Text({
  ...Fonts.avenir,
  color: Colors.white,
  width: '90%',
  alignSelf: 'center',
  fontSize: 15,
  marginVertical: 20,
});

const EditWallet = ({value, setProfile}) => {
  const [publicKey, setPublicKey] = useState(value || '');
  const [visible, setVisible] = useState(false);

  const onSave = () => {
    setProfile('publicKey', publicKey);
    setVisible(false);
  };

  const handleOpen = useCallback(() => setVisible(true), []);
  const handleClose = useCallback(() => {
    setPublicKey(value || '');
    setVisible(false);
  }, [value]);

  return (
    <>
      <ListItem title="Wallet Address" onPress={handleOpen} value={publicKey} />
      <Modal visible={visible} onRequestClose={handleClose} transparent statusBarTranslucent animationType="slide">
        <ModalWrapper>
          <ModalHeader
            title="Wallet Address"
            onPressBack={handleClose}
            onPressDone={onSave}
            disabled={value === publicKey}
          />
          <SubTitle>{PROFILE_EDIT_SUBTITLE_PUBLICKEY}</SubTitle>
          <ModalContent>
            <Input value={publicKey} onChangeText={setPublicKey} maxLength={500} />
          </ModalContent>
        </ModalWrapper>
      </Modal>
    </>
  );
};

export default EditWallet;
