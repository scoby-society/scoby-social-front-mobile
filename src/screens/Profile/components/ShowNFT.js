import React, {useState, useContext} from 'react';
import styled from 'styled-components/native';
import BackButton from 'src/components/BackButton';
import {goudy} from 'src/constants/Fonts';
import {Text} from 'react-native';
import {useQuery} from '@apollo/client';
import {GET_NFT_BY_ID} from 'src/graphql/queries/Spores';
import {FollowersContext} from 'src/containers/followers';
import SkipButton from 'src/components/SkipButton';
import Steps from 'src/components/Steps';
import NextButton from 'src/components/NextButton';
import BackButtonSecond from 'src/components/BackButtonSecond';
import {RadioButton} from 'react-native-paper';
import {GlobalContext} from 'src/containers/global';
import CardNFT from './CardNFT';
import Colors from 'src/constants/Colors';

const Tittle = styled.Text`
color:white;
font-size:22px;
font-family:${goudy.fontFamily}
margin-bottom:25px;
`;
const SubTittle = styled.Text`
  color: white;
  font-size: 12px;
  font-family: avenir;
`;
const Wrapper = styled.View`
  flex: 1;
  flex-direction: column;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;
const ContainerHeader = styled.View`
  margin-top: 10px;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  height: 100px;
  width: 75%;
  margin-bottom: 50px;
`;
const ButtonBack = styled(BackButton)({});

const TextButton = styled.Text`
  color: white;
  font-size: 14px;
  font-family: 'Avenir';
`;
const Row = styled.View`
  flex-direction: row;
`;
const ScrollContainer = styled.ScrollView({});
const ContainerCard = styled.View`
  display: flex;
  flex: 1;
  flex-wrap: wrap;
  justify-content: center;
  flex-direction: row;
  width: 100%;
`;
const ContainerButton = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const ButtonSave = styled.TouchableOpacity`
  width: 107px;
  height: 42px;
  margin-bottom: 30px;
  margin-top: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  background: #cd068e;
`;

const Container = styled.View({
  marginTop: 30,
  display: 'flex',
  justifyContent: 'space-between',
  flexDirection: 'row',
  alignItems: 'center',
});
const ButtonWrapper = styled.View({
  flexDirection: 'row',
  marginBottom: 60,
  alignItems: 'center',
  justifyContent: 'space-between',
  padding: '0px 60px',
  display: 'flex',
  width: '100%',
});
const ContainerText = styled.View({
  width: '100%',

  marginTop: 200,
  marginBottom: 200,
});
const Column = styled.View({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  margin: '0px 10px',
});
const ShowNFT = ({navigation, route}) => {
  const {setIsLogged} = useContext(GlobalContext);
  const onBoarding = route?.params?.onBoarding;
  const [ShowAll, setShowAll] = useState();
  const {currentUserProfile} = useContext(FollowersContext);
  const {
    data: {getNftByUser = []} = [],
    loading: {loadingSpores},
  } = useQuery(GET_NFT_BY_ID, {
    pollInterval: 5000,
    variables: {
      id: currentUserProfile.id,
    },
  });

  const handleClick = (n) => {
    setShowAll(n);
  };

  const Skip = () => {
    navigation.navigate('HostSelectionScreen');
  };
  return (
    <>
      <Container>
        <ButtonBack onPress={navigation.goBack} />
        <SkipButton skip={Skip} />
      </Container>

      <ScrollContainer>
        <Wrapper>
          <ContainerHeader>
            <Tittle>{onBoarding ? 'Include your NFTs' : 'Show your NFTs on Scoby'}</Tittle>
            <Container style={{marginBottom: 20, width: '100%'}}>
              <SubTittle>
                {onBoarding
                  ? 'Select the NFTs you would like to include.'
                  : 'Unlock Experiences, Connections and Community.'}
                <Steps FirstValue={4} SecondValue={7} />
              </SubTittle>
            </Container>

            {getNftByUser.length > 0 && (
              <Row style={{width: '100%', justifyContent: 'center'}}>
                <Column>
                  <Text>Hide All</Text>
                  <RadioButton
                    uncheckedColor="rgba(134, 133, 158, 1)"
                    color="rgba(236, 0, 140, 1)"
                    value="second"
                    status={ShowAll === false ? 'checked' : 'unchecked'}
                    onPress={() => handleClick(false)}
                  />
                </Column>

                <Column>
                  <Text>Show All</Text>
                  <RadioButton
                    uncheckedColor="rgba(134, 133, 158, 1)"
                    color="rgba(236, 0, 140, 1)"
                    value="second"
                    status={ShowAll === true ? 'checked' : 'unchecked'}
                    onPress={() => handleClick(true)}
                  />
                </Column>
              </Row>
            )}
          </ContainerHeader>
          <ContainerCard>
            {!loadingSpores &&
              getNftByUser?.map((nft, index) => (
                <CardNFT key={`${index}-${Math.random()}`} GeneralState={ShowAll} nft={nft} />
              ))}
            {!getNftByUser.length > 0 && (
              <ContainerText>
                <Text style={{textAlign: 'center', color: Colors.white}}>You don't have any NFTs in</Text>
                <Text style={{textAlign: 'center', color: Colors.white}}>this wallet! We'll have to</Text>
                <Text style={{textAlign: 'center', color: Colors.white}}>do something about that.</Text>
              </ContainerText>
            )}
          </ContainerCard>
        </Wrapper>
        <ContainerButton>
          {!onBoarding && (
            <ButtonSave onPress={Skip}>
              <TextButton>Save</TextButton>
            </ButtonSave>
          )}
          {onBoarding && (
            <ButtonWrapper>
              <BackButtonSecond navigation={navigation} />
              <NextButton onPress={Skip} />
            </ButtonWrapper>
          )}
        </ContainerButton>
      </ScrollContainer>
    </>
  );
};
export default ShowNFT;
