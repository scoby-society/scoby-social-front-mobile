import React, {useContext, useMemo} from 'react';
import {FlatList, View} from 'react-native';
import colors from 'src/config/theme/colors';
import Fonts from 'src/constants/Fonts';
import {PROFILE_SUBTITLE_TO_SHARE_1, PROFILE_SUBTITLE_TO_SHARE_2, PROFILE_TITLE_TO_SHARE} from 'src/constants/Texts';
import styled from 'styled-components/native';
import {FollowersContext} from 'src/containers/followers';
import SerieHome from 'src/components/Series';
import {useQuery} from '@apollo/client';
import {GET_USER_EVENTS, GET_USER_SERIES} from 'src/graphql/queries/profile';
import EventItem from 'src/components/Event/EventItem';
import {GET_CHANNELS} from 'src/graphql/queries/channel';
import ChannelItem from 'src/components/Channel/ChannelItem';

const SeriesContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
  zIndex: 2,
});

const LoadingContainer = styled.View({
  flex: 1,
  height: '100%',
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: colors.blueBackgroundSession,
});

const DetailsContainer = styled.View({
  paddingHorizontal: 24,
  zIndex: -2,
});

const TitleText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 16,
  textAlign: 'center',
  color: colors.white,
  marginVertical: 16,
});

const SmallText = styled.Text({
  ...Fonts.avenir,
  textAlign: 'center',
  fontSize: 14,
  lineHeight: '20px',
  color: '#9094A2',
});

export const SharedExperiences = ({navigation, setShowHeader}) => {
  const {currentUserProfile} = useContext(FollowersContext);

  const {data: {getUserSeries = []} = [], loading} = useQuery(GET_USER_SERIES, {
    fetchPolicy: 'network-only',
  });

  const {
    data: {getUserEvents = []} = [],
    loading: {loadingEvents},
  } = useQuery(GET_USER_EVENTS, {
    fetchPolicy: 'network-only',
  });
  const {
    data: {getAllChatExperience = []} = [],
    loading: {loadingChannels},
  } = useQuery(GET_CHANNELS, {
    fetchPolicy: 'network-only',
  });

  const allChat = useMemo(
    () => getAllChatExperience.filter((chat) => chat.ownerUser.id === currentUserProfile.id),
    [getAllChatExperience, currentUserProfile],
  );

  const footer = () => (
    <DetailsContainer>
      <TitleText>{PROFILE_TITLE_TO_SHARE}</TitleText>
      <SmallText>{PROFILE_SUBTITLE_TO_SHARE_1}</SmallText>
      <SmallText>{PROFILE_SUBTITLE_TO_SHARE_2}</SmallText>
    </DetailsContainer>
  );

  const openSeries = (series) => {
    const temp = {...series, landingView: true};
    navigation.navigate('SeriesLandingPage', {series: temp});
  };

  const openEvents = (events) => {
    navigation.navigate('EventsLandingPage', {events});
  };

  const openChannel = (channel) => {
    navigation.navigate('ChannelsLandingPage', {channel});
  };

  const setKey = (item) => `serieCard-${item.id}-${Math.random()}`;

  const renderItem = ({item}) => {
    if (item.schedule) {
      return <SerieHome series={item} enable openSerie={openSeries} navigation={navigation} />;
    }
    if (item.eventType) {
      return <EventItem event={item} navigation={navigation} enable openEvent={openEvents} />;
    }
    if (item.chatType) {
      return <ChannelItem channel={item} navigation={navigation} enable openChannel={openChannel} />;
    }
  };

  return (
    <View style={{flex: 1}}>
      {loading || loadingEvents || loadingChannels ? (
        <LoadingContainer>{/* <ActivityIndicator color={colors.white} size="large" /> */}</LoadingContainer>
      ) : (
        <SeriesContainer>
          <FlatList
            onScroll={() => setShowHeader(false)}
            ListFooterComponent={footer()}
            ListFooterComponentStyle={{marginBottom: 200, zIndex: 1}}
            renderItem={renderItem}
            data={[...getUserSeries, ...getUserEvents, ...allChat]}
            keyExtractor={setKey}
          />
        </SeriesContainer>
      )}
    </View>
  );
};
