import React, {useCallback, useContext} from 'react';
import {Alert} from 'react-native';
import {StackActions} from '@react-navigation/native';
import Share from 'react-native-share';
import firebase from '@react-native-firebase/app';
import lodash from 'lodash';
import styled from 'styled-components/native';

import {FollowersContext} from 'src/containers/followers';
import colors from 'src/config/theme/colors';
import Fonts from 'src/constants/Fonts';
import {ShareIco} from 'assets/ico';
import {SHARE_TITLE, SHARE_TEXT, UNKNOWN_ERROR_TEXT} from 'src/constants/Texts';
import {DYNAMIC_LINKS_DEFAULTS, DYNAMIC_LINKS_HEADERS, DYNAMIC_LINKS_SHORTENER_URL} from 'src/constants/Variables';
import {GlobalContext} from 'src/containers/global';
import OnBoardingWrapper from 'src/screens/SignUp/components/OnBoardingWrapper';

const ShareProfileButton = styled.TouchableOpacity`
  padding: 8px;
  color: ${colors.white};
  background-color: #cd068e;
  border-radius: 64px;
  align-items: center;
  flex-direction: row;
  width: 250px;
  justify-content: center;
  margin-top: 20;
`;

const ShareProfileButtonText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  textAlign: 'center',
  marginRight: 16,
  color: colors.white,
});

const ShareText = styled.Text({
  color: colors.white,
  fontSize: 16,
  marginTop: 60,
});

const Sharing = ({navigation}) => {
  const {currentUserProfile} = useContext(FollowersContext);
  const {setIsLogged} = useContext(GlobalContext);
  const {id, username} = currentUserProfile;
  async function buildProfileLink(ids) {
    try {
      const body = JSON.stringify(
        lodash.merge(DYNAMIC_LINKS_DEFAULTS, {
          dynamicLinkInfo: {
            link: `${DYNAMIC_LINKS_DEFAULTS.dynamicLinkInfo.domainUriPrefix}/user/${ids}`,
          },
        }),
      );
      const response = await fetch(`${DYNAMIC_LINKS_SHORTENER_URL}?key=${firebase.app().options.apiKey}`, {
        method: 'POST',
        headers: DYNAMIC_LINKS_HEADERS,
        body,
      });
      const json = await response.json();
      return json.shortLink;
    } catch (error) {
      return null;
    }
  }
  const shareUrlProfile = useCallback(async () => {
    const url = await buildProfileLink(id);
    if (url) {
      Share.open({
        message: `Join me on Scoby and let’s have a Meaningful Conversation!`,
        url,
      });
    } else {
      Alert.alert(UNKNOWN_ERROR_TEXT);
    }
  }, [id]);

  const shareButtonText = `scoby.one/${username}`;

  const skip = () => {
    setIsLogged(true);
    navigation.dispatch(StackActions.popToTop());
  };

  return (
    <OnBoardingWrapper navigation={navigation} skipStep={skip} title={SHARE_TITLE} subTitle={SHARE_TEXT} currStep={7}>
      <ShareText>Deep Link to Profile</ShareText>
      <ShareProfileButton large active onPress={() => shareUrlProfile()}>
        <ShareProfileButtonText>{shareButtonText}</ShareProfileButtonText>
        <ShareIco />
      </ShareProfileButton>
    </OnBoardingWrapper>
  );
};

export default Sharing;
