import {useNavigation} from '@react-navigation/native';
import React,{useState} from 'react';
import {StyleSheet, ScrollView, View, TextInput, Text} from 'react-native';
import BackButton from 'src/components/BackButton';
import colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import styled from 'styled-components';
import SelectCollectionCard from './components/SelectCollectionCard';
import RegularButton from 'src/components/RegularButton';

const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  marginLeft: '7%',
  color: colors.white,
  marginTop: '5%',
  width: '100%',
});
const Label = styled.Text(({experience = 14}) => ({
  height: 60,
  marginLeft: '7%',
  paddingTop: '10%',
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
}));

const collections = [
  {
    name: 'Wave',
    num: '#3254',
    img: 'https://png.pngtree.com/thumb_back/fw800/background/20190528/pngtree-gold-marble-texture-background-image_115783.jpg',
  },
  {
    name: 'Abstract Pink',
    num: '#777',
    img: 'https://img.freepik.com/free-vector/pink-color-watercolor-background_125540-1216.jpg',
  },
  {
    name: 'Abstract',
    num: '#666',
    img: 'https://png.pngtree.com/thumb_back/fw800/background/20190528/pngtree-gold-marble-texture-background-image_115783.jpg',
  },
  {
    name: 'Sad',
    num: '#000',
    img: 'https://png.pngtree.com/thumb_back/fw800/background/20190528/pngtree-gold-marble-texture-background-image_115783.jpg',
  }
];

function SelectCollection({navigation, route}) {
  //const navigation = useNavigation();
  const {params} = route;
  const [name, setName] = useState(params?.projectOneparams ? params?.projectOneparams?.name : '');
  const [description, setDescription] = useState(params?.projectOneparams ? params?.projectOneparams?.description : '');
  const handleNext = () => {
    navigation.navigate('PrivacySettings', {...params, name, description});
  };
  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />

      <ScrollView style={Style.container}>
        <Title>Sellect Collections</Title>
        <Label>Restrict access to holders of NFTs in these collections.</Label>
        <View style={Style.cardSection}>
          {collections.map(collection=>{
            return(
                <SelectCollectionCard item={collection}/>
            )
          })

          }
          
        </View>
      </ScrollView>
      <View style={Style.containerButton}>
        <RegularButton
          onPress={() => cleanFields()}
          title="Back"
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton
          title="Next"
          onPress={() => handleNext()}
         // disabled={!!(name === '' || description === '')}
          style={[
            Style.regularBtn,
            name === '' || description === ''
              ? {backgroundColor: colors.disabledPink}
              : {backgroundColor: colors.newPink},
          ]}
        />
      </View>
    </View>
  );
}

const Style = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: '100%',
    paddingTop:15
  },
  container: {
    flex: 1,
  },
  cardSection: {
    paddingLeft: '3%',
    paddingRight: '3%',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  containerButton: {
    marginBottom: '2%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
});

export default SelectCollection;
