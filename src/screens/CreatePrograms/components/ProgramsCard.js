import React from 'react';
import {StyleSheet, ScrollView, View, Text, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

function ProgramsCard(props) {
    return (
        <View style={styles.card}>
            <LinearGradient
          start={{x:0.1, y: 0}}
          end={{x: 0.5, y: 1}}
          colors={['#4FBEFF', '#412fb2']}
          style={styles.rightSide}>
                <Text>right</Text>


          </LinearGradient>
           
            <View style={styles.leftSide}><Text>left</Text></View>
        </View>
    );
}

const styles=StyleSheet.create({
    rightSide:{
        width:167,
        height:259,
        backgroundColor:'blue',
        borderTopLeftRadius:6,
        borderBottomLeftRadius:6


    },
    leftSide:{
        width:167,
        height:259,
        backgroundColor:'white',
        borderTopRightRadius:6,
        borderBottomRightRadius:6
    },
    card:{
        flexDirection:'row',
        alignSelf:'center'
    }
})

export default ProgramsCard;