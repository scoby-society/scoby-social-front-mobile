import React, {useState} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Fonts from 'src/constants/Fonts';
import CheckBox from '@react-native-community/checkbox';

const styles = StyleSheet.create({
  card: {
    width: 175,
    height: 262,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: ' rgba(255, 255, 255, 0.2)',
    marginTop: 30,
  },
  avatar: {
    width: 155,
    height: 155,
    borderRadius: 22,
    marginTop: 10,
    alignSelf: 'center',
  },
  sectionOne: {
    flexDirection: 'row',
    justifyContent: 'space-between',

    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },
  textOne: {
    ...Fonts.ttNormsBold,
    color: '#FFFFFF',
    fontSize: 13,
  },
  textTwo: {
    ...Fonts.avenir,
    fontSize: 9,
    color: '#C5C5C5',
  },
  textThree: {
    ...Fonts.avenir,
    color: '#FFFFFF',
  },
});

function SelectCollectionCard({item}) {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);

  return (
    <View style={styles.card}>
      <Image
        style={styles.avatar}
        source={{
          uri: item.img,
        }}
      />
      <View style={styles.sectionOne}>
        <View style={styles.wrap}>
          <Text style={styles.textOne}>{item.name}</Text>
          <Text style={styles.textTwo}>{item.num}</Text>
        </View>
        <View style={styles.wrap}>
          <Text style={styles.textThree}>Select</Text>
          <CheckBox
            style={styles.box}
            disabled={false}
            value={toggleCheckBox}
            onValueChange={(newValue) => setToggleCheckBox(newValue)}
          />
        </View>
      </View>
    </View>
  );
}

export default SelectCollectionCard;
