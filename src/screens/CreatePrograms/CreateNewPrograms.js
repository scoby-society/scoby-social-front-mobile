import React, {useState, useMemo} from 'react';
import {StyleSheet, ScrollView, View, TextInput, Text} from 'react-native';
import styled from 'styled-components';
import withSafeArea from 'src/components/withSafeArea';
import {TITLE_CREATION_PROJECT, LABEL_NAME_PROJECT, LABEL_DESCRIPTION_PROJECT} from 'src/constants/Texts';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import BackButton from 'src/components/BackButton';

const Style = StyleSheet.create({
  containerButton: {
    marginBottom: '25%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    paddingHorizontal: '7%',
  },
  icons: {
    width: 120,
    height: 120,
  },
  mainContainer: {
    width: '100%',
    height: '100%',
  },
  input: {
    height: 50,
    ...Fonts.avenir,
    padding: 16,
    marginTop: 5,
    marginBottom: -15,
    color: colors.white,
    backgroundColor: colors.transparent,
    borderRadius: 15,
    borderColor: '#FFFFFF54',
    borderWidth: 1,
    marginVertical: 24,
  },
  inputFocus: {
    height: 50,
    ...Fonts.avenir,
    padding: 16,
    marginTop: 5,
    marginBottom: -15,
    color: colors.white,
    backgroundColor: '#FFFFFF0A',
    borderRadius: 15,
    borderColor: '#FFFFFF80',
    borderWidth: 1,
    marginVertical: 24,
  },
  inputDescription: {
    ...Fonts.avenir,
    padding: 16,
    paddingTop: 16,
    marginTop: 5,
    minHeight: 96,
    color: colors.white,
    backgroundColor: colors.transparent,
    borderRadius: 15,
    borderColor: '#FFFFFF54',
    borderWidth: 1.5,
  },
  inputDescriptionFocus: {
    ...Fonts.avenir,
    padding: 16,
    paddingTop: 16,
    marginTop: 5,
    minHeight: 96,
    color: colors.white,
    backgroundColor: '#FFFFFF0A',
    borderRadius: 15,
    borderColor: '#FFFFFF80',
    borderWidth: 1.5,
  },
  textViewStyle: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: colors.transparent,
  },
  textStyle: {
    ...Fonts.avenir,
    color: colors.greySession,
    textAlign: 'left',
    padding: 8,
  },
});
const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  marginTop: '5%',
  width: '100%',
});
const Label = styled.Text(({experience = 14}) => ({
  height: 60,
  marginLeft: 5,
  paddingTop: '10%',
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
}));
const CreationNewPrograms = ({navigation, route}) => {
  const {params} = route;
  const [hasFocusDescription, setFocusDescription] = useState(false);
  const [hasFocusName, setFocusName] = useState(false);
  const [descriptionCounter, setDescriptionCounter] = useState(
    params?.projectOneparams ? Number(params?.projectOneparams?.description.length) : 0,
  );
  const [name, setName] = useState(params?.projectOneparams ? params?.projectOneparams?.name : '');
  const [description, setDescription] = useState(params?.projectOneparams ? params?.projectOneparams?.description : '');
  const calculation = useMemo(() => 277 - descriptionCounter, [descriptionCounter]);
  const handleChangeName = (e) => {
    setName(e);
  };
  const handleNext = () => {
    navigation.navigate('SelectCollection', {...params, name, description});
  };
  const cleanFields = () => {
    navigation.goBack();
  };
  const inputTextUpdate = (name, updateText, updateCounter, text, counter) => {
    const nameInput = name || '';
    updateText(nameInput);
    if (text && text.length > nameInput.length) {
      if (counter > 0) {
        updateCounter(name.length);
      }
    } else if (text && text.length < nameInput.length) {
      updateCounter(name.length);
    } else {
      updateCounter(0);
    }
  };
  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <ScrollView style={Style.container}>
        <Title>Create a new program</Title>
        <Label>Name your program and add a description</Label>
        <TextInput
          onFocus={() => setFocusName(true)}
          onBlur={() => setFocusName(false)}
          style={hasFocusName ? Style.inputFocus : Style.input}
          onChangeText={(e) => handleChangeName(e)}
          value={name}
          placeholder="Name of Program"
          placeholderTextColor={colors.greySession}
          maxLength={40}
        />
        <Label>{LABEL_DESCRIPTION_PROJECT}</Label>
        <TextInput
          onFocus={() => setFocusDescription(true)}
          onBlur={() => setFocusDescription(false)}
          style={hasFocusDescription ? Style.inputDescriptionFocus : Style.inputDescription}
          onChangeText={(e) =>
            inputTextUpdate(e, setDescription, setDescriptionCounter, description, descriptionCounter)
          }
          value={description}
          placeholder="Description"
          placeholderTextColor={colors.greySession}
          maxLength={277}
          multiline
          textAlignVertical="top"
          numberOfLines={5}
          blurOnSubmit
        />
        <View style={Style.textViewStyle}>
          <Text style={Style.textStyle}>{`*${calculation} character`}</Text>
        </View>
      </ScrollView>
      <View style={Style.containerButton}>
        <RegularButton
          onPress={() => cleanFields()}
          title="Back"
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton
          title="Next"
          onPress={() => handleNext()}
          disabled={!!(name === '' || description === '')}
          style={[
            Style.regularBtn,
            name === '' || description === ''
              ? {backgroundColor: colors.disabledPink}
              : {backgroundColor: colors.newPink},
          ]}
        />
      </View>
    </View>
  );
};

export default withSafeArea(CreationNewPrograms);
