import React, {useState} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import BackButton from 'src/components/BackButton';
import colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import styled from 'styled-components';
import {RadioButton} from 'react-native-paper';
import RegularButton from 'src/components/RegularButton';

const Title = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  marginLeft: '7%',
  color: colors.white,
  marginTop: '5%',
  width: '100%',
});
const MiniTitle = styled.Text({
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 15,
  marginLeft: '7%',
  color: colors.white,
  marginTop: '5%',
  width: '100%',
});
const Label = styled.Text(({experience = 14}) => ({
  height: 60,
  marginLeft: '7%',
  paddingTop: '10%',
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
}));

const Style = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: '100%',
    paddingTop: 15,
  },
  container: {
    flex: 1,
  },
  sectionOne: {},
  sectionTwo: {},
  item: {
    flexDirection: 'row',
    marginLeft: '10%',
    marginRight: '10%',
    justifyContent: 'space-between',
  },
  itemTitle: {
    ...Fonts.avenir,
    fontSize: 15,
    fontWeight: '700',
    color: 'white',
  },
  itemText: {
    ...Fonts.avenir,
    fontSize: 12,
    fontWeight: '500',
    width: 300,
  },
  radio: {
    backgroundColor: 'red',
  },
  containerButton: {
    marginBottom: '2%',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
});

function PrivacySettings({navigation, route}) {
  const {params} = route;

  const [name] = useState(params?.projectOneparams ? params?.projectOneparams?.name : '');
  const [description] = useState(params?.projectOneparams ? params?.projectOneparams?.description : '');
  const handleNext = () => {
    navigation.navigate('SetTopics', {...params, name, description});
  };
  const [checked, setChecked] = React.useState('1');
  const [checked2, setChecked2] = React.useState('4');
  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <View style={Style.container}>
        <Title>Privacy Settings</Title>
        <Label>Set the access and visibility for your Program.</Label>

        <View style={Style.sectionOne}>
          <MiniTitle>Who can join?</MiniTitle>
          <View style={Style.item}>
            <View style={Style.wrap}>
              <Text style={Style.itemTitle}>Open</Text>
              <Text style={Style.itemText}>Anyone can join the Program.</Text>
            </View>

            <RadioButton
              value="1"
              status={checked === '1' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('1')}
              color={checked === '1' ? '#EC008C' : ''}
            />
          </View>
          <View style={Style.item}>
            <View style={Style.wrap}>
              <Text style={Style.itemTitle}>Closed</Text>
              <Text style={Style.itemText}>Only holders of the included collections and their invited guests.</Text>
            </View>

            <RadioButton
              value="2"
              status={checked === '2' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('2')}
              color={checked === '2' ? '#EC008C' : 'gray'}
            />
          </View>
          <View style={Style.item}>
            <View style={Style.wrap}>
              <Text style={Style.itemTitle}>Exclusive</Text>
              <Text style={Style.itemText}>Only holders of the included collections.</Text>
            </View>

            <RadioButton
              value="3"
              status={checked === '3' ? 'checked' : 'unchecked'}
              onPress={() => setChecked('3')}
              color={checked === '3' ? '#EC008C' : ''}
            />
          </View>
        </View>

        <View style={Style.sectionTwo}>
          <MiniTitle>Who can see your program?</MiniTitle>

          <View style={Style.item}>
            <View style={Style.wrap}>
              <Text style={Style.itemTitle}>Listed</Text>
              <Text style={Style.itemText}>Anyone can see you your Program.</Text>
            </View>

            <RadioButton
              value="4"
              status={checked2 === '4' ? 'checked' : 'unchecked'}
              onPress={() => setChecked2('4')}
              color={checked2 === '4' ? '#EC008C' : ''}
            />
          </View>

          <View style={Style.item}>
            <View style={Style.wrap}>
              <Text style={Style.itemTitle}>Unlisted</Text>
              <Text style={Style.itemText}>Only those included or invited can see your Program.</Text>
            </View>

            <RadioButton
              value="5"
              status={checked2 === '5' ? 'checked' : 'unchecked'}
              onPress={() => setChecked2('5')}
              color={checked2 === '5' ? '#EC008C' : ''}
            />
          </View>
        </View>
      </View>
      <View style={Style.containerButton}>
        <RegularButton
          onPress={() => navigation.goBack()}
          title="Back"
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton
          title="Next"
          onPress={() => handleNext()}
          // disabled={!!(name === '' || description === '')}
          style={[
            Style.regularBtn,
            name === '' || description === ''
              ? {backgroundColor: colors.disabledPink}
              : {backgroundColor: colors.newPink},
          ]}
        />
      </View>
    </View>
  );
}

export default PrivacySettings;
