import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Topics from 'src/components/Topics';
import BackButton from 'src/components/BackButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import styled from 'styled-components';

const Title = styled.Text({
    ...Fonts.goudy,
    lineHeight: '32px',
    fontSize: 28,
    marginLeft: '7%',
    color: colors.white,
    marginTop: '5%',
    width: '100%',
  });
  const Label = styled.Text(({experience = 14}) => ({
    height: 60,
    marginLeft: '7%',
    paddingTop: '10%',
    ...Fonts.avenir,
    fontSize: experience,
    color: colors.white,
  }));

function TopicsPrograms({navigation, params}) {
  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <Title>Set the Topics</Title>
        <Label>Let everyone know what this Program is all about</Label>
      <View style={Style.container}>
      <Topics onBoarding={true} navigation={navigation}/>
      </View>
    </View>
  );
}

const Style = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: '100%',
    paddingTop: 15,
  },
  container: {
    flex: 1,
  },
});

export default TopicsPrograms;
