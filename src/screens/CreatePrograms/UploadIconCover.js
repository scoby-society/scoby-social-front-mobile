import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import BackButton from 'src/components/BackButton';
import styled from 'styled-components';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import EditableHeaderWithImage from '../Profile/components/EditableHeaderWithImage';
import ProgramsCard from './components/ProgramsCard';

const Title = styled.Text({
    ...Fonts.goudy,
    lineHeight: '32px',
    fontSize: 28,
    marginLeft: '7%',
    color: colors.white,
    marginTop: '5%',
    width: '100%',
});

function UploadIconCover({ navigation, params }) {
    return (
        <View style={Style.mainContainer}>
            <BackButton onPress={() => navigation.goBack()} />

            <View style={Style.container}>
                <Title>Upload an Icon and Cover Photo</Title>

                <EditableHeaderWithImage

                />
                <ProgramsCard/>
            </View>
        </View>
    );
}

const Style = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: '100%',
        paddingTop: 15,
    },
    container: {
        flex: 1,
    },
});

export default UploadIconCover;