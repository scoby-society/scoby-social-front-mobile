/* eslint-disable no-use-before-define */
import {useMutation, useQuery} from '@apollo/client';
import React, {useContext, useEffect, useMemo, useState} from 'react';
import {ActivityIndicator, StyleSheet, View, Alert, TouchableOpacity} from 'react-native';
import {BackgroundImage} from 'react-native-elements/dist/config';
import CloseNormal from 'assets/svg/closeNormal.svg';
import LinearGradient from 'react-native-linear-gradient';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {GET_TEAM} from 'src/graphql/queries/team';
import styled from 'styled-components';
import {useNavigation} from '@react-navigation/core';
import {FollowersContext} from 'src/containers/followers';
import {TEAM_LEAVE_MODAL_TEXT, UNKNOWN_ERROR_TEXT} from 'src/constants/Texts';
import {checkAvatar} from 'src/utils/helpers';
import {CogIcon} from 'assets/svg';
import {ACCEPT_INVITE, LEAVE_TEAM, REQUEST_MEMBERSHIP} from 'src/graphql/mutations/team';
import {TEAM_TYPES} from 'src/constants/Variables';
import {BlurView} from '@react-native-community/blur';
import PromptModal from 'src/components/Modal/PromptModal';
import {DefaultTeamCard} from 'src/components/TeamCard';
import {GET_TEAMS} from 'src/graphql/queries/profile';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';

const Wrapper = styled.View({flex: 1, zIndex: -3});
const Cover = styled.View({});
const TeamType = styled.Text({...Fonts.avenirBold, color: Colors.white, fontSize: 14, paddingLeft: 6});
const AvatarWrapper = styled.View({
  width: 100,
  height: 100,
  position: 'absolute',
  bottom: -50,
  left: 15,
  borderRadius: 50,
  borderWidth: 6,
  borderColor: Colors.blueBackgroundSession,
});
const AvatarImg = styled.Image({width: '100%', height: '100%', resizeMode: 'cover', borderRadius: 50});

const Content = styled.View({padding: 12});

const ContentHeader = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'flex-end',
  width: '100%',
  minHeight: 56,
});

const MembersWrapper = styled.TouchableOpacity({
  alignItems: 'center',
  alignSelf: 'center',
  width: '40%',
});

const MembersAvatarWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'center',
  paddingHorizontal: 12,
});

const LeaveButton = styled.TouchableOpacity({
  backgroundColor: 'rgba(101, 54, 187, 0.8)',
  width: 88,
  height: 28,
  position: 'absolute',
  right: 5,
  top: 5,
  zIndex: 10,
  borderRadius: 8,
  alignItems: 'center',
  justifyContent: 'center',
});

const MembersText = styled.Text({color: Colors.grey, ...Fonts.avenir, marginBottom: 6});

const MemberItem = styled.Image({width: 30, height: 30, borderRadius: 50});

const MoreMembersText = styled.Text({...Fonts.avenir, color: Colors.white, fontSize: 13});

const ActionButton = styled.TouchableOpacity({
  width: '30%',
  height: 40,
  backgroundColor: Colors.newPink,
  alignSelf: 'flex-end',
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'center',
  marginHorizontal: 6,
  borderRadius: 8,
  zIndex: 100,
});

const BluredActionButton = styled(ActionButton)({
  position: 'absolute',
  top: 254,
  right: 10,
});

const ActionButtonText = styled.Text({
  ...Fonts.avenirSemiBold,
  color: Colors.white,
  fontSize: 14,
  textAlign: 'center',
});

const TeamName = styled.Text({
  ...Fonts.goudy,
  color: Colors.white,
  fontSize: 26,
  alignSelf: 'center',
  textAlign: 'center',
  paddingVertical: 12,
  width: '70%',
});

const SettingsButton = styled.TouchableOpacity({
  position: 'absolute',
  right: 0,
  top: 0,
  padding: 16,
  zIndex: 100000,
});

const TeamScreen = ({route}) => {
  const {id: teamId} = route?.params;
  const [isActionLoading, setIsActionLoading] = useState(false);

  const {
    data: {getTeam} = [],
    loading,
    refetch,
  } = useQuery(GET_TEAM, {
    fetchPolicy: 'network-only',
    variables: {teamId: Number(teamId)},
    onError: () => {
      setIsActionLoading(false);
      Alert.alert(UNKNOWN_ERROR_TEXT);
    },
  });

  const team = useMemo(() => getTeam, [getTeam]);

  const [accept, {loading: isAcceptInProcess}] = useMutation(ACCEPT_INVITE, {
    onCompleted() {
      refetch();
      setIsActionLoading(false);
    },
    onError: () => Alert.alert(UNKNOWN_ERROR_TEXT),
  });

  const [leave, {loading: isLeaveInProcess}] = useMutation(LEAVE_TEAM, {
    onCompleted() {
      refetch();
      setIsActionLoading(false);
    },
    onError: () => Alert.alert(UNKNOWN_ERROR_TEXT),
  });

  const [request, {loading: isMembershipRequesting}] = useMutation(REQUEST_MEMBERSHIP, {
    refetchQueries: [{query: GET_TEAM, variables: {teamId: Number(teamId)}}, {query: GET_TEAMS}],
    onCompleted() {
      refetch();
      setIsActionLoading(false);
    },
    onError: () => {
      Alert.alert(UNKNOWN_ERROR_TEXT);
    },
  });

  const [isBlur, setIsBlur] = useState(false);
  const [isLeaveModalShown, setIsLeaveModalShown] = useState(false);

  const navigator = useNavigation();
  const {currentUserProfile} = useContext(FollowersContext);

  const acceptedMembers = useMemo(() => team?.members.filter((user) => user.isAccepted), [team?.members]);

  const memberAvatarWrapperStyles = {
    transform: [{translateX: team?.members?.length > 4 ? 16 : team?.members?.length * 2}],
  };

  const invitedUsers = useMemo(() => team?.members?.map(({user}) => user?.id), [team?.members]);

  const isOwner = currentUserProfile?.id === team?.ownerUser?.id;

  const isMemberAccepted = useMemo(
    () => team?.members?.find(({user}) => user.id === currentUserProfile?.id)?.isAccepted,
    [currentUserProfile, team?.members],
  );

  const isMemberInvitedToTeam = useMemo(
    () => !!team?.members?.find(({user}) => user.id === currentUserProfile?.id),
    [currentUserProfile, team?.members],
  );

  const isMemberInPendingList = useMemo(
    () => !!team?.pendingUsers?.find(({id}) => id === currentUserProfile?.id),
    [currentUserProfile.id, team?.pendingUsers],
  );

  const actionBtnTextMap = {
    owner: 'Host a session',
    newbie: 'Accept',
    pending: 'Pending for review',
    stranger: team?.teamType === TEAM_TYPES.OPEN ? 'Join' : 'Apply',
    existed: 'Host a session',
    existedAndCanHost: 'Host a session',
  };

  const teamIdVariables = useMemo(() => ({variables: {teamId: Number(teamId)}}), [teamId]);

  const onLeaveTeam = () => leave(teamIdVariables);

  const requestToJoin = () => request(teamIdVariables);

  const actionBtnActionMap = {
    owner: () => goToSessionCreation(),
    newbie: () => accept(teamIdVariables),
    existed: () => null,
    pending: () => null,
    stranger: () => requestToJoin(),
    existedAndCanHost: () => goToSessionCreation(),
  };

  const disabledBtnStateMap = {
    owner: false,
    newbie: false,
    existed: true,
    pending: true,
    stranger: false,
    existedAndCanHost: false,
  };

  const userStatus = useMemo(() => {
    if (isOwner) return 'owner';
    if (isMemberInPendingList) return 'pending';
    if (!isMemberAccepted && isMemberInvitedToTeam) return 'newbie';
    if (isMemberAccepted && team?.membersAllowedToHost) return 'existedAndCanHost';
    if (isMemberAccepted && !team?.membersAllowedToHost) return 'existed';

    return 'stranger';
  }, [isMemberAccepted, isMemberInPendingList, isMemberInvitedToTeam, isOwner, team?.membersAllowedToHost]);

  const isMemberWithoutAccess = ['stranger', 'pending'].includes(userStatus);

  useEffect(() => {
    setIsBlur(team?.teamType === TEAM_TYPES.EXCLUSIVED && isMemberWithoutAccess);
  }, [team?.teamType, isOwner, isMemberWithoutAccess]);

  useEffect(() => {
    if (team) {
      setIsActionLoading(false);
    }
  }, [team]);

  const isLoading = (isMembershipRequesting || isAcceptInProcess || loading || isActionLoading) && !isLeaveInProcess;
  const isLeaveLoading = useMemo(
    () => loading || isActionLoading || isLeaveInProcess,
    [loading, isActionLoading, isLeaveInProcess],
  );

  const goToTeamMembers = () => {
    navigator.navigate('TeamMembers', {members: acceptedMembers});
  };
  const goToTeamSettings = () => navigator.navigate('TeamSettings', {team});
  const goBack = () => {
    if (navigator.canGoBack) {
      navigator.goBack();
    } else {
      navigator.reset({index: 0, routes: [{name: 'MainTabs', screen: 'Home'}]});
    }
  };

  const goToSessionCreation = () =>
    navigator.navigate('CreateSession', {
      screen: 'SessionName',
      params: {
        experience: 'SessionName',
        invitedUsers,
        followers: invitedUsers,
        isTeamsMembersInvited: true,
        teamInvitation: [],
        followersList: acceptedMembers?.map(({user}) => user),
        isPrivate: true,
      },
    });

  const onActionButtonPress = async () => {
    setIsActionLoading(true);
    await actionBtnActionMap[userStatus]();
  };

  const toggleLeaveModal = () => setIsLeaveModalShown((prev) => !prev);

  const handleLeaveTeam = () => {
    toggleLeaveModal();
    onLeaveTeam();
  };

  if (loading || !team) return <ActivityIndicator size="large" style={styles.activityIndicator} />;

  const isButtonDisabled = acceptedMembers?.length === 0 && isOwner ? true : disabledBtnStateMap[userStatus];

  return (
    <>
      <Wrapper>
        <Cover>
          <View style={styles.header}>
            <TouchableOpacity onPress={goBack}>
              <CloseNormal width={56} height={56} style={styles.closeIcon} />
            </TouchableOpacity>
            <TeamName>{team.name}</TeamName>
          </View>
          <BackgroundImage source={{uri: team?.backgroundImage}} resizeMode="cover" style={styles.bgImage}>
            {isOwner && (
              <SettingsButton onPress={goToTeamSettings}>
                <CogIcon />
              </SettingsButton>
            )}
            {!isOwner && isMemberAccepted && (
              <LeaveButton onPress={toggleLeaveModal}>
                {isLeaveLoading ? <ActivityIndicator /> : <ActionButtonText>Leave Community</ActionButtonText>}
              </LeaveButton>
            )}
            <LinearGradient
              colors={[Colors.chatGradientStart, Colors.transparent]}
              start={{x: 0, y: 0.75}}
              end={{x: 1, y: 0.25}}
              style={styles.teamTypeGradient}>
              <TeamType>{getTeam.teamType}</TeamType>
            </LinearGradient>
            <AvatarWrapper>
              <AvatarImg source={{uri: team?.avatar}} />
            </AvatarWrapper>
          </BackgroundImage>
        </Cover>
        <Content>
          <ContentHeader>
            <>
              {acceptedMembers?.length ? (
                <MembersWrapper onPress={goToTeamMembers}>
                  <MembersText>Members</MembersText>
                  <MembersAvatarWrapper style={memberAvatarWrapperStyles}>
                    <MemberItem source={checkAvatar(acceptedMembers[0].user)} />
                    {acceptedMembers.length > 1 && (
                      <MemberItem style={styles.secondMemberItem} source={checkAvatar(acceptedMembers[1].user)} />
                    )}
                    {acceptedMembers.length > 2 && (
                      <MemberItem style={styles.thirdMemberItem} source={checkAvatar(acceptedMembers[2].user)} />
                    )}
                    {acceptedMembers.length > 3 && (
                      <MemberItem style={styles.fourthMemberItem} source={checkAvatar(acceptedMembers[3].user)} />
                    )}
                    {acceptedMembers.length > 4 && (
                      <View style={styles.moreMembersOuter}>
                        <View style={styles.moreMembersInner}>
                          <MoreMembersText>+{acceptedMembers.length - 4}</MoreMembersText>
                        </View>
                      </View>
                    )}
                  </MembersAvatarWrapper>
                </MembersWrapper>
              ) : (
                <MembersWrapper disabled onPress={() => {}}>
                  <MembersText>No members</MembersText>
                </MembersWrapper>
              )}
              <ActionButton
                onPress={onActionButtonPress}
                disabled={isButtonDisabled}
                style={isButtonDisabled && styles.disabledBtn}>
                <ActionButtonText>{actionBtnTextMap[userStatus]}</ActionButtonText>
              </ActionButton>
            </>
          </ContentHeader>
          <DefaultTeamCard
            ownerUser={team.ownerUser}
            members={team.members}
            teamId={teamId}
            membersAllowedToInvite={team.membersAllowedToInvite}
            description={team.description}
            topics={team.topics}
          />
        </Content>
        {isBlur && (
          <>
            <BlurView
              style={styles.blurStyles}
              reducedTransparencyFallbackColor="gray"
              blurType="light"
              blurAmount={15}
            />
            <LinearGradient
              colors={[Colors.chatGradientStart, Colors.transparent]}
              start={{x: 0, y: 0.75}}
              end={{x: 1, y: 0.25}}
              style={styles.bluredTeamTypeGradient}>
              <TeamType>{team.teamType}</TeamType>
            </LinearGradient>
            <TouchableOpacity onPress={goBack} style={styles.closeIconBlured}>
              <CloseNormal width={56} height={56} style={styles.closeIcon} />
            </TouchableOpacity>
            <BluredActionButton
              onPress={onActionButtonPress}
              disabled={disabledBtnStateMap[userStatus]}
              style={disabledBtnStateMap[userStatus] && styles.disabledBtn}>
              {isLoading ? <ActivityIndicator /> : <ActionButtonText>{actionBtnTextMap[userStatus]}</ActionButtonText>}
            </BluredActionButton>
          </>
        )}
        <PromptModal
          visible={isLeaveModalShown}
          setVisible={setIsLeaveModalShown}
          text={TEAM_LEAVE_MODAL_TEXT}
          leftButtonText="Yes"
          rightButtonText="No"
          onLeftButtonPress={handleLeaveTeam}
          onRightButtonPress={toggleLeaveModal}
        />
      </Wrapper>
    </>
  );
};

const styles = StyleSheet.create({
  bgImage: {width: '100%', height: 170, justifyContent: 'space-between'},
  teamTypeGradient: {width: '25%', height: '12%', flexDirection: 'row', alignItems: 'center'},
  bluredTeamTypeGradient: {
    width: 80,
    height: 20,
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    top: 50,
    left: 0,
  },
  secondMemberItem: {transform: [{translateX: -8}]},
  thirdMemberItem: {transform: [{translateX: -16}]},
  fourthMemberItem: {transform: [{translateX: -24}]},
  iconShare: {color: Colors.shareBtnText},
  moreMembersOuter: {
    width: 30,
    height: 30,
    borderRadius: 50,
    backgroundColor: Colors.pinkMagentaTranslucent,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    transform: [{translateX: -32}],
  },
  header: {flexDirection: 'row', alignItems: 'center'},
  closeIcon: {color: Colors.white, alignSelf: 'flex-start'},
  closeIconBlured: {color: Colors.white, position: 'absolute', top: 0, left: 0},
  moreMembersInner: {
    width: 24,
    height: 24,
    borderRadius: 50,
    backgroundColor: Colors.newPink,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabledBtn: {backgroundColor: Colors.disabledButton},
  activityIndicator: {alignSelf: 'center'},
  blurStyles: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 0,
  },
});

export default withSafeAreaWithoutMenu(TeamScreen);
