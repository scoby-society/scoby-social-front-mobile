import React from 'react';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import BoxScreen from 'src/screens/VonageBoxScreen/components/BoxScreen';

const JoinVonageBox = ({navigation, route}) => {
  const paramsRoutes = route.params;

  return (
    <BoxScreen
      session={paramsRoutes.paramsSession}
      navigation={navigation}
      enableMic={false}
      enableCam={false}
      frontCam={false}
      isGuest
    />
  );
};

export default withSafeAreaWithoutMenu(JoinVonageBox, {drawUnderStatusBar: true});
