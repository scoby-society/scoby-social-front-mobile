import React from 'react';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import BoxScreen from 'src/screens/VonageBoxScreen/components/BoxScreen';

const CreateVonageBox = ({navigation, route}) => {
  const paramsSession = route.params;

  return <BoxScreen session={paramsSession?.paramsSession} navigation={navigation} isGuest={false} />;
};

export default withSafeAreaWithoutMenu(CreateVonageBox, {drawUnderStatusBar: true});
