import React, {useCallback, useState} from 'react';
import {Alert, StyleSheet} from 'react-native';
import {FollowersContext} from 'src/containers/followers';
import {useMutation, useQuery} from '@apollo/client';
import {GET_FOLLOWERS_BY_NFT} from 'src/graphql/queries/followers';
import styled from 'styled-components/native';
import {
  NEW_EVENT_INVITE_FOLLOWERS_TITLE,
  NEW_SERIE_INVITE_FOLLOWERS_TITLE,
  NEW_SESSION_INVITE_FOLLOWERS_TITLE,
  TITLE_POPULATE_SESSION,
  SUBTITLE_SESSION_PROJECT,
  TITLE_INVITE_FOLKS_PROJECT,
} from 'src/constants/Texts';
import InviteFollowers from 'src/components/InviteFollowers/InviteFollowers';
import NewLargeButton from 'src/components/NewLargeButton';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import LadingSerie from 'src/components/LoadingSerie';
import {CREATE_SERIE, INVITE_SERIE} from 'src/graphql/mutations/serie';
import {ReactNativeFile} from 'apollo-upload-client';
import {GET_USER_EVENTS, GET_USER_SERIES} from 'src/graphql/queries/profile';
import {CREATE_EVENT, INVITE_EVENT} from 'src/graphql/mutations/event';
import {GET_TEAM_USERS_TO_INVITE_BY_NFT} from 'src/graphql/queries/team';
import {INVITE_PROJECT, CREATE_PROJECT, EDIT_PROJECT} from 'src/graphql/mutations/projects';
import {CREATE_CHANNEL} from 'src/graphql/mutations/channel';
import {GET_PROJECT_BY_ID} from 'src/graphql/queries/projects';
import {HOME_PROJECT_FEED} from 'src/graphql/queries/universalSearch';
import {kitty} from 'src/config/api';
import BackButton from 'src/components/BackButton';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';

const Wrapper = styled.View({
  flex: 1,
  flexDirection: 'column',
  backgroundColor: Colors.blueBackgroundSession,
  paddingTop: 24,
});

const PaddingWrapper = styled.View({
  paddingHorizontal: 24,
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  lineHeight: '32px',
  color: Colors.white,
  marginBottom: 4,
});

const InviteFollowersContainer = styled.View({
  flex: 1,
});

const Buttons = styled.View({
  flexDirection: 'row',
});

function buildFile(uri) {
  return new ReactNativeFile({
    uri,
    name: 'image.jpg',
    type: 'image/jpeg',
  });
}

function mapMembers(member) {
  return {username: member};
}

const ShareSession = ({navigation, route}) => {
  const {currentUserProfile} = React.useContext(FollowersContext);
  const [selectedFollowers, setSelectedFollowers] = useState([]);
  const [selectedTeamsFollowers, setSelectedTeamsFollowers] = useState([]);
  const [isAllSelectedTeams, setIsAllSelectedTeams] = useState(false);
  const [getFollowerUsersNft, setGetFollowerUsersNft] = useState({data: []});
  const [UsersTeamsToInviteNft, setUsersTeamsToInviteNft] = useState({data: []});
  const [teamsQuery, setTeamsQuery] = useState('');

  const [query, setQuery] = useState('');
  const {params} = route;
  const {sessionParams, projectParams} = params;
  const {projectOneparams} = projectParams || route?.params?.sessionParams?.params || [];

  const {experience} = sessionParams;
  useQuery(GET_FOLLOWERS_BY_NFT, {
    variables: {
      nftsRequired: sessionParams?.require || sessionParams?.events?.nftsRequired || [],
      paging: {
        limit: 100,
        page: 1,
      },
      userId: currentUserProfile.id,
    },
    onCompleted({getFollowerUsersNft: {data}}) {
      if (sessionParams?.holder > 0) {
        data.map((i) => {
          if (i.isNft) setSelectedFollowers((j) => [...j, i.id]);
          return null;
        });
      }
      if (
        sessionParams?.require?.length > 0 ||
        sessionParams?.events?.nftsRequired?.length > 0 ||
        sessionParams?.series?.nftsRequired?.length > 0
      ) {
        setGetFollowerUsersNft({data: data.filter((i) => i.isNft === true)});
      } else {
        setGetFollowerUsersNft({data});
      }
    },
  });

  useQuery(GET_TEAM_USERS_TO_INVITE_BY_NFT, {
    fetchPolicy: 'network-only',
    pollInterval: 500,
    variables: {
      nftsRequired: sessionParams?.require || sessionParams?.events?.nftsRequired || [],
      paging: {
        limit: 20,
        page: 1,
      },
      query,
    },
    onCompleted({getUsersTeamsToInviteNft: {data}}) {
      if (
        sessionParams?.require?.length > 0 ||
        sessionParams?.events?.nftsRequired?.length > 0 ||
        sessionParams?.series?.nftsRequired?.length > 0
      ) {
        setUsersTeamsToInviteNft({data: data.filter((i) => i.isNft === true)});
      } else {
        setUsersTeamsToInviteNft({data});
      }
    },
  });

  const [createSerie, {loading}] = useMutation(CREATE_SERIE, {
    refetchQueries: [{query: GET_USER_SERIES}],
    onCompleted() {
      navigation.replace('MainTabs', {
        screen: 'Profile',
      });
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [createProject, {loading: loadingProject}] = useMutation(CREATE_PROJECT, {
    refetchQueries: [
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: 100,
            page: 1,
          },
          category: 'all',
        },
      },
    ],
    onCompleted() {
      navigation.replace('MainTabs', {screen: 'Home'});
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [editProject, {loading: loadingProjectEdit}] = useMutation(EDIT_PROJECT, {
    refetchQueries: [
      {query: GET_PROJECT_BY_ID, variables: {projectId: sessionParams?.projects?.id || projectOneparams?.id}},
      {
        query: HOME_PROJECT_FEED,
        variables: {
          paging: {
            limit: 100,
            page: 1,
          },
          category: 'all',
        },
      },
    ],
    onCompleted(e) {
      const {id} = e?.editProject;
      navigation.replace('ProjectStack', {
        screen: 'OneViewProjectScreen',
        params: {projectId: id},
      });
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [createChannel, {loading: loadingChannel}] = useMutation(CREATE_CHANNEL, {
    onCompleted({createChat}) {
      kitty
        .createChannel({
          type: 'PUBLIC',
          name: sessionParams.Name,
          members: selectedFollowers.map((e) => mapMembers(e)),
          properties: {avatar: createChat.avatar, id: createChat.id},
        })
        .then(() =>
          navigation.replace('MainTabs', {
            screen: 'Profile',
          }),
        );
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [
    inviteUserSeries,
    {
      loading: {loadingInviteSerie},
    },
  ] = useMutation(INVITE_SERIE, {
    onCompleted() {
      navigation.navigate('Profile');
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [
    inviteUserEvent,
    {
      loading: {loadingInviteEvent},
    },
  ] = useMutation(INVITE_EVENT, {
    onCompleted() {
      navigation.navigate('Profile');
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [
    invitedUsersProjects,
    {
      loading: {loadingInviteProjects},
    },
  ] = useMutation(INVITE_PROJECT, {
    refetchQueries: [{query: GET_PROJECT_BY_ID, variables: {projectId: sessionParams?.projects?.id}}],
    onCompleted(e) {
      navigation.replace('ProjectStack', {
        screen: 'OneViewProjectScreen',
        params: {projectId: e.invitedUsersProjects.id},
      });
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const [createEvent, {loading: loadingEvent}] = useMutation(CREATE_EVENT, {
    refetchQueries: [{query: GET_USER_EVENTS}],
    onCompleted() {
      navigation.replace('MainTabs', {
        screen: 'Profile',
      });
    },
    onError(e) {
      Alert.alert('Error', e.message);
    },
  });

  const handleEvent = () => {
    createEvent({
      variables: {
        event: {
          day: sessionParams.day,
          description: sessionParams.Description,
          end: sessionParams.end,
          invitedUsers: selectedFollowers,
          start: sessionParams.start,
          title: sessionParams.Name,
          topics: sessionParams.topics,
          isTeamsMembersInvited: isAllSelectedTeams,
          teamInvitation: selectedTeamsFollowers,
          nft: sessionParams?.primary || null,
          nftsInviteUser: [],
          nftsRequired: sessionParams?.require || null,
          projectId: projectOneparams?.id,
        },
        avatar: buildFile(sessionParams.Avatar),
        backgroundImage: buildFile(sessionParams.Cover),
      },
    });
  };
  const handleSession = () => {
    const overAllSelectedFollowers = [...selectedFollowers, ...selectedTeamsFollowers];
    params.sessionParams.followers = [...new Set(overAllSelectedFollowers)];
    params.sessionParams.hostName = currentUserProfile.username;
    params.sessionParams.followersList = getFollowerUsersNft?.data;
    params.sessionParams.isTeamsMembersInvited = isAllSelectedTeams;
    params.sessionParams.teamInvitation = selectedTeamsFollowers;
    params.sessionParams.projectOneparams = projectOneparams;
    navigation.navigate('CreateSessionScreen', {sessionParams: params});
  };
  const handleSerie = async () => {
    await createSerie({
      variables: {
        serie: {
          calendarName: sessionParams.CalendarName,
          className: sessionParams.ClassName,
          description: sessionParams.Description,
          seriesName: sessionParams.Name,
          topics: sessionParams.topics,
          invitedUsers: selectedFollowers,
          isTeamsMembersInvited: isAllSelectedTeams,
          teamInvitation: selectedTeamsFollowers,
          nft: sessionParams?.primary || null,
          nftsInviteUser: [],
          nftsRequired: sessionParams?.require || null,
          projectId: projectOneparams?.id,
        },
        schedule: sessionParams.Schedule,
        avatar: buildFile(sessionParams.Avatar),
        backgroundImage: buildFile(sessionParams.Cover),
      },
    });
  };
  const handleProject = async () => {
    if (projectOneparams) {
      await editProject({
        variables: {
          projectId: projectOneparams?.id,
          projectEditObj: {
            name: projectParams?.name || projectOneparams?.name,
            description: projectParams?.description || projectOneparams?.description,
            join: projectParams.joinChoice || projectOneparams?.join,
            project: projectParams?.projectChoice || projectOneparams?.project,
            nft: projectParams?.checked || projectOneparams?.nft,
            invitedUsers: selectedFollowers || [],
            teamInvitation: selectedTeamsFollowers || [],
          },
          coverImage: buildFile(projectParams?.cover || projectOneparams?.coverImage),
          tile: buildFile(projectParams?.avatar || projectOneparams?.tile),
        },
      });
    }
    if (!projectOneparams) {
      await createProject({
        variables: {
          projectObj: {
            name: projectParams?.name,
            description: projectParams?.description,
            join: projectParams?.joinChoice,
            project: projectParams?.projectChoice,
            nft: projectParams?.checked,
            invitedUsers: selectedFollowers,
            teamInvitation: selectedTeamsFollowers,
          },
          coverImage: buildFile(projectParams.cover),
          tile: buildFile(projectParams.avatar),
        },
      });
    }
  };
  const handleChannel = async () => {
    await createChannel({
      variables: {
        avatar: buildFile(sessionParams.Avatar),
        backgroundImage: buildFile(sessionParams.Cover),
        chat: {
          description: sessionParams.Description,
          invitedUsers: selectedFollowers,
          isTeamsMembersInvited: isAllSelectedTeams,
          nft: sessionParams?.primary || null,
          nftsInviteUser: [],
          nftsRequired: sessionParams?.require || null,
          teamInvitation: selectedTeamsFollowers,
          title: sessionParams.Name,
          topics: sessionParams.topics,
          projectId: projectOneparams?.id,
        },
      },
    });
  };

  const handleInviteSerie = async () => {
    await inviteUserSeries({
      variables: {
        idSerie: sessionParams.series.id,
        invitedUsers: selectedFollowers,
        isTeamsMembersInvited: isAllSelectedTeams,
        teamInvitation: selectedTeamsFollowers,
      },
    });
  };

  const handleInviteEvent = async () => {
    await inviteUserEvent({
      variables: {
        idEvent: sessionParams.events.id,
        invitedUsers: selectedFollowers,
        isTeamsMembersInvited: isAllSelectedTeams,
        teamInvitation: selectedTeamsFollowers,
      },
    });
  };

  const handleInviteProject = async () => {
    await invitedUsersProjects({
      variables: {
        idProject: sessionParams.projects.id,
        invitedUsers: selectedFollowers,
        isTeamsMembersInvited: isAllSelectedTeams,
        teamInvitation: selectedTeamsFollowers,
      },
    });
  };

  const submit = () => {
    if (experience === 'session') {
      handleSession();
    }
    if (experience === 'serie') {
      handleSerie();
    }
    if (experience === 'seriesInvite') {
      handleInviteSerie();
    }
    if (experience === 'event') {
      handleEvent();
    }
    if (experience === 'eventInvite') {
      handleInviteEvent();
    }
    if (experience === 'projectsInvite') {
      handleInviteProject();
    }
    if (experience === 'channel') {
      handleChannel();
    }
    if (experience === 'project') {
      handleProject();
    }
  };

  const goBack = () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      navigation.reset({
        index: 0,
        routes: [{name: 'MainTabs'}],
      });
    }
  };

  const arrayImgAvatar = [];

  if (getFollowerUsersNft && getFollowerUsersNft.data && getFollowerUsersNft.data.length > 0) {
    getFollowerUsersNft.data.map((item) => arrayImgAvatar.push(item));
  }

  const styles = StyleSheet.create({
    buttonContainer: {
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    button: {
      flex: 1,
      marginTop: 24,
      marginBottom: 24,
    },
  });
  const setSubtitle = useCallback(() => {
    if (experience === 'session') {
      return NEW_SESSION_INVITE_FOLLOWERS_TITLE;
    }
    if (experience === 'serie') {
      return NEW_SERIE_INVITE_FOLLOWERS_TITLE;
    }
    if (experience === 'seriesInvite') {
      return NEW_SERIE_INVITE_FOLLOWERS_TITLE;
    }
    if (experience === 'event') {
      return NEW_EVENT_INVITE_FOLLOWERS_TITLE;
    }
    if (experience === 'eventInvite') {
      return NEW_EVENT_INVITE_FOLLOWERS_TITLE;
    }
    if (experience === 'projectsInvite') {
      return NEW_EVENT_INVITE_FOLLOWERS_TITLE;
    }
    if (experience === 'project') {
      return SUBTITLE_SESSION_PROJECT;
    }
  }, [experience]);

  const setTitle = useCallback(() => {
    if (experience !== 'project') {
      return TITLE_POPULATE_SESSION;
    }
    if (experience === 'project') {
      return TITLE_INVITE_FOLKS_PROJECT;
    }
  }, [experience]);

  return (
    <Wrapper>
      {experience === 'project' && <BackButton onPress={() => navigation.goBack()} />}
      <PaddingWrapper>
        <LadingSerie
          visible={
            loading ||
            loadingEvent ||
            loadingInviteEvent ||
            loadingInviteSerie ||
            loadingChannel ||
            loadingProject ||
            loadingProjectEdit ||
            loadingInviteProjects
          }
        />
        <TitleText>{setTitle()}</TitleText>
      </PaddingWrapper>
      <InviteFollowersContainer>
        <InviteFollowers
          nftsRequired={sessionParams?.require || sessionParams?.events?.nftsRequired || []}
          subTitle={setSubtitle()}
          getFollowerUsers={getFollowerUsersNft}
          setSelectedFollowers={setSelectedFollowers}
          UsersTeamsToInviteNft={UsersTeamsToInviteNft}
          setUsersTeamsToInviteNft={setUsersTeamsToInviteNft}
          selectedFollowers={selectedFollowers}
          selectedTeamsFollowers={selectedTeamsFollowers}
          setSelectedTeamsFollowers={setSelectedTeamsFollowers}
          isAllSelectedTeams={isAllSelectedTeams}
          setIsAllSelectedTeams={setIsAllSelectedTeams}
          avatarBorderColor={Colors.blueBackgroundSession}
          teamsQuery={teamsQuery}
          setTeamsQuery={setTeamsQuery}
          navigation={navigation}
          setQuery={setQuery}
          query={query}
        />
      </InviteFollowersContainer>
      <PaddingWrapper>
        <Buttons>
          <NewLargeButton large transparent noPadding title="Back" onPress={goBack} style={styles.button} />
          <NewLargeButton large noPadding title="Next" onPress={submit} style={styles.button} />
        </Buttons>
      </PaddingWrapper>
    </Wrapper>
  );
};

export default withSafeAreaWithoutMenu(ShareSession);
