/* eslint-disable no-use-before-define */
import {useNavigation} from '@react-navigation/core';
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import Dropdown from 'src/components/Dropdown';
import CommonSwitch from 'src/components/Switch';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {
  TEAM_SETTINGS_ALLOW_HOST,
  TEAM_SETTINGS_ALLOW_INVITE,
  TEAM_SETTINGS_PRIVATE_DESC,
  TEAM_SETTINGS_PUBLIC_DESC,
  TEAM_SETTINGS_SECRET_DESC,
  TEAM_SETTINGS_SUBTITLE,
  TEAM_SETTINGS_TITLE,
} from 'src/constants/Texts';
import styled from 'styled-components';
import {useMutation} from '@apollo/client';
import {UPDATE_TEAM} from 'src/graphql/mutations/team';
import {GET_TEAM} from 'src/graphql/queries/team';
import {GET_TEAMS} from 'src/graphql/queries/profile';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import TeamSettingsModal from './TeamSettingsModal';

const Wrapper = styled.View({
  flex: 1,
  flexGrow: 1,
  paddingHorizontal: 26,
  paddingTop: 24,
  height: '100%',
});

const ScrollableContent = styled.ScrollView({marginBottom: 80, marginTop: '5%'});

const Title = styled.Text({...Fonts.goudy, fontSize: 32, lineHeight: '32px', color: Colors.white, alignSelf: 'center'});

const ParamsWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
  alignSelf: 'center',
});

const ContentWrapper = styled.View({
  marginTop: '5%',
  marginBottom: 24,
});

const ParamsText = styled.Text({
  color: Colors.white,
  width: '75%',
  fontSize: 14,
  ...Fonts.avenirSemiBold,
  paddingVertical: 14,
});

const SubTitle = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 16,
  color: Colors.white,
  paddingVertical: 26,
  marginBottom: 14,
  alignSelf: 'center',
});

const TeamActionsWrapper = styled.View({
  paddingTop: 66,
});

const Buttons = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '100%',
  paddingHorizontal: 20,
  alignSelf: 'center',
  position: 'absolute',
  bottom: 25,
});

const Button = styled.TouchableOpacity({
  width: '40%',
  height: 55,
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 12,
});

const ButtonSingle = styled.TouchableOpacity({
  width: '40%',
  height: 55,
  flexDirection: 'row',
  alignItems: 'center',
  alignSelf: 'center',
  position: 'absolute',
  bottom: 25,
  justifyContent: 'center',
  borderRadius: 12,
  backgroundColor: Colors.newPink,
});

const ITEMS = [
  {label: 'Private', desc: TEAM_SETTINGS_PRIVATE_DESC},
  {label: 'Public', desc: TEAM_SETTINGS_PUBLIC_DESC},
  {label: 'Secret', desc: TEAM_SETTINGS_SECRET_DESC},
];

const ButtonText = styled.Text({...Fonts.avenir, fontSize: 16, color: Colors.grey});

const TeamSettings = ({route}) => {
  const [isAllowToInvite, setIsAllowToInvite] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isAllowToHost, setIsAllowToHost] = useState(false);
  const [items] = useState(ITEMS);
  const [value, setValue] = useState(items[0]);

  const {
    params: {sessionParams, team},
  } = route;

  const navigator = useNavigation();

  const [updateTeam] = useMutation(UPDATE_TEAM, {
    refetchQueries: [{query: GET_TEAM, variables: {teamId: Number(team?.id)}}, {query: GET_TEAMS}],
    onCompleted() {
      if (navigator.canGoBack) {
        navigator.goBack();
      } else {
        navigator.replace('TeamScreen', {id: team?.id});
      }
    },
  });

  const onMutate = async () => {
    if (!team) return;
    const updateTeamPayload = {
      membersAllowedToHost: isAllowToHost,
      membersAllowedToInvite: isAllowToInvite,
      teamId: team?.id,
    };

    await updateTeam({
      variables: {updateTeamPayload},
    });
  };

  useEffect(() => {
    if (team) {
      setIsAllowToHost(team.membersAllowedToHost);
      setIsAllowToInvite(team.membersAllowedToInvite);
    }
  }, [items, team, route]);

  const goBack = () => {
    if (team) {
      onMutate();
    } else {
      navigator.goBack();
    }
  };

  const goToEditTeamScreen = () => {
    navigator.navigate('NameTeam', {teamId: team?.id});
  };

  const toggleModal = () => setIsModalVisible((prev) => !prev);

  const handleNext = () => {
    const params = {...sessionParams, isAllowToInvite, isAllowToHost};
    navigator.navigate('InviteFollowers', {sessionParams: params});
  };

  return (
    <Wrapper>
      <ScrollableContent showsVerticalScrollIndicator={false}>
        <Title>Member Permissions</Title>
        <SubTitle>Set the permissions for community members.</SubTitle>
        <ContentWrapper>
          <ParamsWrapper>
            <ParamsText>
              Allow Members to invite others, who meet the participatoin guidelines you have set, to the community.
            </ParamsText>
            <CommonSwitch value={isAllowToInvite} setValue={setIsAllowToInvite} />
          </ParamsWrapper>
          <ParamsWrapper>
            <ParamsText>Allow Members to Host live community sessions.</ParamsText>
            <CommonSwitch value={isAllowToHost} setValue={setIsAllowToHost} />
          </ParamsWrapper>
        </ContentWrapper>
        {team && (
          <TeamActionsWrapper>
            <Button style={[styles.longButton, styles.whiteBtn]} onPress={goToEditTeamScreen}>
              <ButtonText style={styles.darkText}>Edit Community</ButtonText>
            </Button>
            <Button style={[styles.longButton, styles.redBtn]} onPress={toggleModal}>
              <ButtonText style={styles.lightText}>Delete Community</ButtonText>
            </Button>
          </TeamActionsWrapper>
        )}
      </ScrollableContent>
      {team ? (
        <ButtonSingle onPress={goBack}>
          <ButtonText>Back</ButtonText>
        </ButtonSingle>
      ) : (
        <Buttons>
          <Button onPress={goBack}>
            <ButtonText>Back</ButtonText>
          </Button>
          <Button onPress={handleNext} style={styles.pinkBtn}>
            <ButtonText>Next</ButtonText>
          </Button>
        </Buttons>
      )}
      <TeamSettingsModal teamId={team?.id} isModalVisible={isModalVisible} toggleModal={toggleModal} />
    </Wrapper>
  );
};

const styles = StyleSheet.create({
  pinkBtn: {backgroundColor: Colors.newPink},
  modalWrapper: {flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'},
  longButton: {width: '80%', height: 42, alignSelf: 'center'},
  whiteBtn: {backgroundColor: Colors.white, marginBottom: 24},
  redBtn: {backgroundColor: Colors.red},
  darkText: {color: Colors.blueBackgroundSession, fontSize: 18, ...Fonts.avenirSemiBold},
  lightText: {color: Colors.white, fontSize: 18, ...Fonts.avenirSemiBold},
});

export default withSafeAreaWithoutMenu(TeamSettings);
