import React, {useState} from 'react';
import NewLargeButton from 'src/components/NewLargeButton';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import styled from 'styled-components';
import NftListProfile from '../Nft/NftListProfile';

const Container = styled.View({
  flex: 1,
  justifyContent: 'space-between',
  alignItems: 'center',
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  color: Colors.white,
  margin: '60px 20px 0 20px',
});

const SkipText = styled.Text({
  ...Fonts.avenir,
  fontSize: 15,
  color: Colors.white,
});

const SkipView = styled.View({
  justifyContent: 'center',
  alignItems: 'flex-end',
  width: '90%',
  margin: 10,
});

const ButtonContainer = styled.View({
  margin: '0 20px 0 20px',
  flexDirection: 'row',
});

export const SelectNft = ({navigation, route}) => {
  const [holder, setHolder] = useState([]);
  const [require, setRequire] = useState([]);
  const [primary, setPrimary] = useState(null);
  const {params} = route;
  const {experience} = params;

  const nextStep = () => {
    navigation.navigate(experience, {...params, experience, holder, require, primary});
  };

  const SkipStep = () => {
    navigation.navigate(experience, {...params, experience, holder: null, require: null, primary: null});
  };

  return (
    <Container>
      <TitleText>Select NFTs to share in this Experience</TitleText>
      <SkipView>
        <SkipText onPress={SkipStep}>Skip</SkipText>
      </SkipView>
      <NftListProfile
        experience={experience}
        holder={holder}
        require={require}
        primary={primary}
        setHolder={setHolder}
        setPrimary={setPrimary}
        setRequire={setRequire}
      />
      <ButtonContainer>
        <NewLargeButton title="Back" transparent onPress={() => navigation.goBack()} flex />
        <NewLargeButton flex title="Next" onPress={nextStep} disabled={!primary} />
      </ButtonContainer>
    </Container>
  );
};
