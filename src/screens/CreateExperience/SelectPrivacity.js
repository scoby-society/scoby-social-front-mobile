import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import styled from 'styled-components';
import withSafeArea from 'src/components/withSafeArea';
import {
  CAN_JOIN_PRIVACY,
  CAN_PROJECT_PRIVACY,
  OPEN_PRIVACY_PROJECT,
  TEXT_OPEN_PRIVACY_PROJECT,
  CLOSED_PRIVACY_PROJECT,
  TEXT_CLOSED_PRIVACY_PROJECT,
  EXCLUSIVE_PRIVACY_PROJECT,
  TEXT_EXCLUSIVE_PRIVACY_PROJECT,
  LISTED_PRIVACY_PROJECT,
  TEXT_LISTED_PRIVACY_PROJECT,
  UNLISTED_PRIVACY_PROJECT,
  TEXT_UNLISTED_PRIVACY_PROJECT,
  TITLE_PRIVACY_PROJECT,
  SUBTITLE_PRIVACY_PROJECT,
} from 'src/constants/Texts';
import RegularButton from 'src/components/RegularButton';
import Fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import BackButton from 'src/components/BackButton';
import {UnCheckRoundedIcon, CheckRoundedIcon} from 'assets/svg';
import {ScrollView} from 'react-native-gesture-handler';

const Style = StyleSheet.create({
  containerButton: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginBottom: '20%',
  },
  regularBtn: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 125,
    height: 45,
    alignSelf: 'center',
    borderRadius: 8,
    marginTop: 12,
    ...Fonts.avenir,
    fontWeight: 'bold',
  },
  goBack: {
    zIndex: 1,
  },
  containerChildOne: {
    marginTop: 15,
    marginBottom: 10,
  },
  containerChildtwo: {
    marginBottom: 10,
  },
  container: {
    flex: 1,
    paddingHorizontal: '7%',
  },
  icons: {
    width: 120,
    height: 120,
  },
  mainContainer: {
    marginTop: '2%',
    width: '100%',
    height: '100%',
  },
});
const Title = styled.Text({
  marginTop: '2%',
  ...Fonts.goudy,
  lineHeight: '32px',
  fontSize: 28,
  color: colors.white,
  width: '100%',
});

const Title2 = styled.Text(({margin}) => ({
  marginTop: margin,
  ...Fonts.goudy,
  lineHeight: '17px',
  fontSize: 15,
  color: colors.white,
  width: '100%',
}));
const Title3 = styled.Text({
  letterSpacing: 1,
  marginTop: '2%',
  ...Fonts.ttNormsBold,
  lineHeight: '18px',
  fontSize: 15,
  color: colors.white,
  width: '100%',
});
const SubTitle = styled.Text(({experience = 13}) => ({
  marginTop: '6%',
  marginBottom: 15,
  lineHeight: '14px',
  ...Fonts.avenir,
  fontSize: experience,
  color: colors.white,
}));
const ContainBox = styled.View({
  marginTop: '3%',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  flexWrap: 'wrap',
});
const ContainText = styled.View({
  flexDirection: 'column',
});
const SubText = styled.Text(({experience = 12}) => ({
  width: 260,
  lineHeight: '14px',
  ...Fonts.avenir,
  fontSize: experience,
  textAlign: 'left',
  color: colors.white,
}));
const SelectPrivacity = ({navigation, route}) => {
  const {params} = route;
  const [type, setType] = useState('');
  const [visibility, setVisibility] = useState('');

  const handleGoToTopics = () => {
    let {params} = route;
    params = {...params, type, visibility};
    navigation.navigate('SelectTopics', {
      ...params,
    });
  };
  return (
    <View style={Style.mainContainer}>
      <BackButton onPress={() => navigation.goBack()} />
      <ScrollView style={Style.container}>
        <Title>{TITLE_PRIVACY_PROJECT}</Title>
        <SubTitle>{SUBTITLE_PRIVACY_PROJECT}</SubTitle>
        <Title2 margin="2%">{CAN_JOIN_PRIVACY}</Title2>
        <ContainBox>
          <ContainText>
            <Title3>{OPEN_PRIVACY_PROJECT}</Title3>
            <SubText>{TEXT_OPEN_PRIVACY_PROJECT}</SubText>
          </ContainText>
          {type === 'Open' ? (
            <CheckRoundedIcon
              onPress={() => {
                setType('');
              }}
            />
          ) : (
            <UnCheckRoundedIcon
              onPress={() => {
                setType('Open');
              }}
            />
          )}
        </ContainBox>
        <ContainBox>
          <ContainText>
            <Title3>{CLOSED_PRIVACY_PROJECT}</Title3>
            <SubText>{TEXT_CLOSED_PRIVACY_PROJECT}</SubText>
          </ContainText>
          {type === 'Closed' ? (
            <CheckRoundedIcon
              onPress={() => {
                setType('');
              }}
            />
          ) : (
            <UnCheckRoundedIcon
              onPress={() => {
                setType('Closed');
              }}
            />
          )}
        </ContainBox>
        <ContainBox>
          <ContainText>
            <Title3>{EXCLUSIVE_PRIVACY_PROJECT}</Title3>
            <SubText>{TEXT_EXCLUSIVE_PRIVACY_PROJECT}</SubText>
          </ContainText>
          {type === 'Exclusived' ? (
            <CheckRoundedIcon
              onPress={() => {
                setType('');
              }}
            />
          ) : (
            <UnCheckRoundedIcon
              onPress={() => {
                setType('Exclusived');
              }}
            />
          )}
        </ContainBox>
        <Title2 margin="12%">{CAN_PROJECT_PRIVACY}</Title2>
        <ContainBox>
          <ContainText>
            <Title3>{LISTED_PRIVACY_PROJECT}</Title3>
            <SubText>{TEXT_LISTED_PRIVACY_PROJECT}</SubText>
          </ContainText>
          {visibility === 'Listed' ? (
            <CheckRoundedIcon
              onPress={() => {
                setVisibility('');
              }}
            />
          ) : (
            <UnCheckRoundedIcon
              onPress={() => {
                setVisibility('Listed');
              }}
            />
          )}
        </ContainBox>
        <ContainBox>
          <ContainText>
            <Title3>{UNLISTED_PRIVACY_PROJECT}</Title3>
            <SubText>{TEXT_UNLISTED_PRIVACY_PROJECT}</SubText>
          </ContainText>
          {visibility === 'Unlisted' ? (
            <CheckRoundedIcon
              onPress={() => {
                setVisibility('');
              }}
            />
          ) : (
            <UnCheckRoundedIcon
              onPress={() => {
                setVisibility('Unlisted');
              }}
            />
          )}
        </ContainBox>
      </ScrollView>
      <View style={Style.containerButton}>
        <RegularButton
          onPress={() => navigation.goBack()}
          title="Back"
          style={[Style.regularBtn, {backgroundColor: 'transparent', borderColor: 'transparent'}]}
        />
        <RegularButton
          disabled={!!(type === '' || visibility === '')}
          onPress={() => handleGoToTopics(navigation)}
          title="Next"
          style={[
            Style.regularBtn,
            type === '' || visibility === ''
              ? {backgroundColor: colors.disabledPink}
              : {backgroundColor: colors.newPink},
          ]}
        />
      </View>
    </View>
  );
};

export default withSafeArea(SelectPrivacity);
