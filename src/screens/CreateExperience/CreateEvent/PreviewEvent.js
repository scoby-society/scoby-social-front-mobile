import React, {useContext, useState} from 'react';
import {Alert, ScrollView} from 'react-native';
import styled from 'styled-components/native';
import {FollowersContext} from 'src/containers/followers';
import colors from 'src/constants/Colors';
import {Header, TitleHeaderText} from 'src/screens/Series/components/Header';
import Button from 'src/components/NewLargeButton';
import {PREVIEW_SERIES_TITLE} from 'src/constants/Texts';
import EventItem from 'src/components/Event/EventItem';
import {useMutation} from '@apollo/client';
import {ReactNativeFile} from 'apollo-upload-client';
import {EDIT_EVENTS} from 'src/graphql/mutations/event';
import {GET_USER_EVENTS} from 'src/graphql/queries/profile';
import LadingSerie from 'src/components/LoadingSerie';
import EditableHeaderWithImage from '../../Profile/components/EditableHeaderWithImage';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  paddingTop: 40,
  justifyContent: 'space-between',
});

const EventContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
  paddingHorizontal: 24,
  alignSelf: 'center',
});

function buildFile(uri) {
  return new ReactNativeFile({
    uri,
    name: 'image.jpg',
    type: 'image/jpeg',
  });
}

const PreviewEvent = ({navigation, route}) => {
  const {params} = route;
  const {sessionParams} = params;
  const {currentUserProfile} = useContext(FollowersContext);
  const {username, fullName, avatar, bio} = currentUserProfile;
  const [currentevent] = useState({
    ownerUser: {
      username,
      fullName,
      bio,
      avatar,
    },
    topics: sessionParams.topicsObjects,
    description: sessionParams.Description,
    title: sessionParams.Name,
    start: sessionParams.start,
    end: sessionParams.end,
    day: sessionParams.day,
  });
  const [eventAvatar, seteventAvatar] = useState(sessionParams.event?.avatar);
  const [eventCover, seteventCover] = useState(sessionParams.event?.backgroundImage);

  const [editEvent, {loading}] = useMutation(EDIT_EVENTS, {
    refetchQueries: [{query: GET_USER_EVENTS}],
    onCompleted() {
      navigation.navigate('Profile');
    },
    onError(e) {
      Alert.alert('Error', `${e.message}`);
      navigation.navigate('Profile');
    },
  });

  const onSubmit = async () => {
    sessionParams.Avatar = eventAvatar;
    sessionParams.Cover = eventCover;
    if (sessionParams.event) {
      editEvent({
        variables: {
          avatar: buildFile(sessionParams.Avatar),
          backgroundImage: buildFile(sessionParams.Cover),
          id: sessionParams.event.id,
          event: {
            day: sessionParams.day,
            description: sessionParams.Description,
            end: sessionParams.end,
            start: sessionParams.start,
            title: sessionParams.Name,
            topics: sessionParams.topics,
          },
        },
      });
    } else {
      navigation.navigate('ShareSession', {sessionParams});
    }
  };

  return (
    <Wrapper>
      <LadingSerie visible={loading} />
      <ScrollView style={{flex: 1}}>
        <Header>
          <TitleHeaderText>{PREVIEW_SERIES_TITLE}</TitleHeaderText>
        </Header>
        <EditableHeaderWithImage
          avatar={eventAvatar}
          setAvatar={seteventAvatar}
          cover={eventCover}
          setCover={seteventCover}
        />
        <EventContainer>
          <EventItem event={currentevent} enable={false} />
        </EventContainer>
      </ScrollView>
      <ButtonContainer>
        <Button
          title="Back"
          flex
          transparent
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Button onPress={onSubmit} flex title="Next" disabled={!eventAvatar || !eventCover} />
      </ButtonContainer>
    </Wrapper>
  );
};

export default PreviewEvent;
