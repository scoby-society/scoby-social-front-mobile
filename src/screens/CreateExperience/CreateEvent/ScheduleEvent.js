import React, {useState} from 'react';
import {EVENT_SCHEDULE_SUBTITLE, TITLE_NAME_EVENT} from 'src/constants/Texts';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import DateTimeTouchable from 'src/components/Event/DateTimeTouchable';
import NewLargeButton from 'src/components/NewLargeButton';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import {Alert} from 'react-native';

const Wrapper = styled.View({flex: 1, marginTop: '20%', marginHorizontal: 24, justifyContent: 'space-between'});
const Container = styled.View({flex: 0.5});
const Title = styled.Text({
  ...Fonts.goudy,
  color: Colors.white,
  fontSize: 28,
  lineHeight: '32px',
});

const SubTitle = styled.Text({
  ...Fonts.avenir,
  color: Colors.white,
  fontSize: 16,
  marginTop: 20,
  lineHeight: '23px',
});

const DateContainer = styled.View({flexDirection: 'row', justifyContent: 'space-between', marginVertical: 30});

const ItemTitle = styled.Text({
  ...Fonts.avenirBold,
  color: Colors.white,
  fontSize: 16,
});

const HorizontalTimeContainer = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '100%',
  marginTop: 30,
});

const VerticalTimeContainer = styled.View({width: '40%', justifyContent: 'center', alignItems: 'center'});

const TimeText = styled.Text({
  ...Fonts.avenir,
  color: Colors.white,
  fontSize: 13,
  marginTop: 7,
});

const ButtonContainer = styled.View({flexDirection: 'row', width: '90%', flex: 0.1, alignItems: 'flex-end'});

const ScheduleEvent = ({navigation, route}) => {
  const {event, } = route?.params;
  const [date, setDate] = useState(event?.day);
  const [startTime, setStartTime] = useState(event?.start);
  const [endTime, setEndTime] = useState(event?.end);
  const [isVisible, setVisible] = useState(false);
  const [saveTo, setSaveTo] = useState(null);

  const confirmDate = (SelectedDate) => {
    if (saveTo === 'date') {
      setDate(moment(SelectedDate).format('yyyy-MM-DD'));
    }
    if (saveTo === 'startTime') {
      setStartTime(moment(SelectedDate).format('HH:mm:ss'));
    }
    if (saveTo === 'endTime') {
      setEndTime(moment(SelectedDate).format('HH:mm:ss'));
    }
    setVisible(null)
  };

  const hideDateModal = () => {
    setVisible(false);
  };

  const showModalDate = () => {
    setVisible('date');
    setSaveTo('date');
  };

  const showModalStartTime = () => {
    setVisible('time');
    setSaveTo('startTime');
  };

  const showModalEndTime = () => {
    setVisible('time');
    setSaveTo('endTime');
  };

  const handleGoToNextStep = () => {
    if (date && startTime && endTime) {
      navigation.navigate('EventName', {start: startTime, end: endTime, day: date, event, ...route?.params});
    } else {
      Alert.alert('Empty field', "The fields can't be empty");
    }
  };

  return (
    <Wrapper>
      <DateTimePickerModal
        isVisible={!!isVisible}
        mode={isVisible}
        date={new Date(Date.now())}
        onHide={hideDateModal}
        onCancel={hideDateModal}
        onConfirm={confirmDate}
      />
      <Container>
        <Title>{TITLE_NAME_EVENT}</Title>
        <SubTitle>{EVENT_SCHEDULE_SUBTITLE}</SubTitle>
        <DateContainer>
          <ItemTitle>Set Date</ItemTitle>
          <DateTimeTouchable
            onPress={showModalDate}
            DateTime={date ? moment(date, 'yyyy-MM-DD').format('MM/DD/yyyy') : null}
            type="date"
          />
        </DateContainer>
        <ItemTitle>Set Time</ItemTitle>
        <HorizontalTimeContainer>
          <VerticalTimeContainer>
            <DateTimeTouchable
              onPress={showModalStartTime}
              DateTime={startTime ? moment(startTime, 'HH:mm:ss').format('hh:mm a') : null}
              size="100%"
            />
            <TimeText>Start time</TimeText>
          </VerticalTimeContainer>
          <VerticalTimeContainer>
            <DateTimeTouchable
              onPress={showModalEndTime}
              DateTime={endTime ? moment(endTime, 'HH:mm:ss').format('hh:mm a') : null}
              size="100%"
            />
            <TimeText>End time</TimeText>
          </VerticalTimeContainer>
        </HorizontalTimeContainer>
      </Container>
      <ButtonContainer>
        <NewLargeButton
          title="Back"
          transparent
          onPress={() => {
            navigation.goBack();
          }}
          flex
        />
        <NewLargeButton flex title="Next" onPress={handleGoToNextStep} />
      </ButtonContainer>
    </Wrapper>
  );
};

export default ScheduleEvent;
