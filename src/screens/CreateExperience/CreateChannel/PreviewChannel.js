import React, {useContext, useState} from 'react';
import {Alert, ScrollView} from 'react-native';
import styled from 'styled-components/native';
import {FollowersContext} from 'src/containers/followers';
import colors from 'src/constants/Colors';
import {Header, TitleHeaderText} from 'src/screens/Series/components/Header';
import Button from 'src/components/NewLargeButton';
import {PREVIEW_SERIES_TITLE} from 'src/constants/Texts';
import {useMutation} from '@apollo/client';
import {ReactNativeFile} from 'apollo-upload-client';
import LadingSerie from 'src/components/LoadingSerie';
import ChannelItem from 'src/components/Channel/ChannelItem';
import {EDIT_CHANNEL} from 'src/graphql/mutations/channel';
import {GET_CHANNELS} from 'src/graphql/queries/channel';
import EditableHeaderWithImage from '../../Profile/components/EditableHeaderWithImage';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
  backgroundColor: colors.blueBackgroundSession,
  paddingTop: 40,
  justifyContent: 'space-between',
});

const ChannelContainer = styled.View({
  width: '100%',
  alignSelf: 'center',
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
  paddingHorizontal: 24,
  alignSelf: 'center',
});

function buildFile(uri) {
  return new ReactNativeFile({
    uri,
    name: 'image.jpg',
    type: 'image/jpeg',
  });
}

const PreviewChannel = ({navigation, route}) => {
  const {params} = route;
  const {sessionParams} = params;
  const {currentUserProfile} = useContext(FollowersContext);
  const {username, fullName, avatar, bio} = currentUserProfile;
  const [currentchannel] = useState({
    ownerUser: {
      username,
      fullName,
      bio,
      avatar,
    },
    topics: sessionParams.topicsObjects,
    description: sessionParams.Description,
    title: sessionParams.Name,
  });
  const [channelAvatar, setchannelAvatar] = useState(sessionParams.channel?.avatar);
  const [channelCover, setchannelCover] = useState(sessionParams.channel?.backgroundImage);

  const [editchannel, {loading}] = useMutation(EDIT_CHANNEL, {
    refetchQueries: [{query: GET_CHANNELS}],
    onCompleted() {
      navigation.navigate('Profile');
    },
    onError(e) {
      Alert.alert('Error', `${e.message}`);
      navigation.navigate('Profile');
    },
  });

  const onSubmit = async () => {
    sessionParams.Avatar = channelAvatar;
    sessionParams.Cover = channelCover;
    if (sessionParams.channel) {
      editchannel({
        variables: {
          avatar: buildFile(sessionParams.Avatar),
          backgroundImage: buildFile(sessionParams.Cover),
          id: sessionParams.channel.id,
          chat: {
            description: sessionParams.Description,
            title: sessionParams.Name,
            topics: sessionParams.topics,
          },
        },
      });
    } else {
      navigation.navigate('ShareSession', {sessionParams});
    }
  };

  return (
    <Wrapper>
      <LadingSerie visible={loading} />
      <ScrollView style={{flex: 1}}>
        <Header>
          <TitleHeaderText>{PREVIEW_SERIES_TITLE}</TitleHeaderText>
        </Header>
        <EditableHeaderWithImage
          avatar={channelAvatar}
          setAvatar={setchannelAvatar}
          cover={channelCover}
          setCover={setchannelCover}
        />
        <ChannelContainer>
          <ChannelItem channel={currentchannel} />
        </ChannelContainer>
      </ScrollView>
      <ButtonContainer>
        <Button
          title="Back"
          flex
          transparent
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Button onPress={onSubmit} flex title="Next" disabled={!channelAvatar || !channelCover} />
      </ButtonContainer>
    </Wrapper>
  );
};

export default PreviewChannel;
