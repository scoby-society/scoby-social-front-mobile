import React, {useContext, useCallback} from 'react';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {View, StyleSheet, FlatList, Image} from 'react-native';
import {FollowersContext} from 'src/containers/followers';
import {Divider, List} from 'react-native-paper';
import Colors from 'src/constants/Colors';
import styled from 'styled-components/native';
import Fonts from 'src/constants/Fonts';
import moment from 'moment';
import {ACTIVITY_KEYS} from './ActivityKeys';

const Img = styled.View({
  borderRadius: 50,
  justifyContent: 'center',
  alignItems: 'center',
});

const Text = styled.Text(({alignSelf = 'flex-start'}) => ({
  ...Fonts.avenir,
  color: Colors.greySession,
  marginRight: 8,
  alignSelf,
}));

const MessageContent = styled.View({
  flexDirection: 'column',
  justifyContent: 'space-between',
});

const Sender = styled.Text({
  ...Fonts.avenirSemiBold,
  color: Colors.white,
  marginBottom: 6,
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f5f5f5',
    flex: 1,
    width: '100%',
  },
  listTitle: {
    fontSize: 22,
    color: Colors.white,
  },
  listDescription: {
    color: Colors.white,
    fontSize: 16,
  },
  image: {
    width: 40,
    height: 40,
    borderRadius: 50,
  },
});

const ChatKity = ({navigation, channels, setChannels}) => {
  const {currentUserProfile} = useContext(FollowersContext);

  const filterUser = useCallback(
    (item) => item?.members?.filter((i) => parseInt(i.name, 10) !== currentUserProfile.id)[0],
    [currentUserProfile],
  );

  return (
    <>
      <View style={styles.container}>
        <FlatList
          data={channels}
          refreshing={false}
          onRefresh={() => {
            setChannels();
          }}
          style={{
            backgroundColor: Colors.blueBackgroundSession,
          }}
          contentContainerStyle={{
            paddingHorizontal: 20,
          }}
          keyExtractor={(item) => item.id.toString()}
          ItemSeparatorComponent={() => <Divider />}
          renderItem={({item}) => {
            const lastUserName = item?.lastReceivedMessage?.user?.displayName;
            return (
              <List.Item
                title={() => <Sender>{filterUser(item)?.displayName || item.displayName}</Sender>}
                left={() => (
                  <Img>
                    <Image
                      resizeMode="cover"
                      source={
                        filterUser(item)?.displayPictureUrl || item?.properties?.avatar
                          ? {uri: filterUser(item)?.displayPictureUrl ?? item?.properties?.avatar}
                          : avatarSrc
                      }
                      style={styles.image}
                    />
                  </Img>
                )}
                description={() => (
                  <MessageContent>
                    {item.type === 'PUBLIC' ? (
                      <Text numberOfLines={1}>
                        {lastUserName === currentUserProfile?.username ? 'you' : lastUserName}
                      </Text>
                    ) : null}
                    <Text numberOfLines={2}>{item?.lastReceivedMessage?.body}</Text>
                    <Text alignSelf="flex-end">{moment(item?.lastReceivedMessage?.createdTime)?.fromNow()}</Text>
                  </MessageContent>
                )}
                titleNumberOfLines={1}
                titleStyle={styles.listTitle}
                descriptionStyle={styles.listDescription}
                descriptionNumberOfLines={1}
                onPress={() => {
                  navigation.navigate(ACTIVITY_KEYS.PRIVATE_CHAT_KITTY, {channel: item});
                }}
              />
            );
          }}
        />
      </View>
    </>
  );
};

export default ChatKity;
