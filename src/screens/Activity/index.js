import React, {useEffect, useContext} from 'react';
import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';
import {useBlock} from 'src/hooks/blockAndReportHooks/useBlock';
import {FollowersContext} from 'src/containers/followers';
import {ACTIVITY_KEYS} from './ActivityKeys';
import ActivityScreen from './ActivityScreen';
import NewMessage from './NewMessage';

const Stack = createStackNavigator();

const options = {headerShown: false, headerBackTitleVisible: false, headerTitleStyle: {display: 'none'}};
const ActivityStack = ({channels, setChannels}) => {
  const {currentUserProfile} = useContext(FollowersContext);

  const {isBlocked} = useBlock();

  useEffect(() => {
    channels.map(async (channel, index) => {
      if (channel.members !== undefined) {
        const target = channel.members.filter((m) => parseInt(m.name, 10) !== currentUserProfile.id);

        if (isBlocked(target[0].name)) {
          channels.splice(index, 1);
        }
      }
      return true;
    });
  });
  return (
    <Stack.Navigator initialRouteName={ACTIVITY_KEYS.NOTIFICATIONS}>
      <Stack.Screen options={options} name={ACTIVITY_KEYS.NOTIFICATIONS}>
        {({navigation}) => <ActivityScreen navigation={navigation} channels={channels} setChannels={setChannels} />}
      </Stack.Screen>
      <Stack.Screen options={options} name={ACTIVITY_KEYS.NEW_MESSAGE} component={NewMessage} />
    </Stack.Navigator>
  );
};

export default ActivityStack;
