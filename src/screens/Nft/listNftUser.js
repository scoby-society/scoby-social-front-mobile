import React, {useContext, useEffect, useMemo, useState} from 'react';
import {ActivityIndicator, Alert, FlatList, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import BackButton from 'src/components/BackButton';
import NftItem from 'src/components/Nfts/nftItem';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {FollowersContext} from 'src/containers/followers';
import styled from 'styled-components/native';
import {shortendPublicKey} from 'src/utils/helpers';
import {GlobalContext} from 'src/containers/global';
import NewLargeButton from 'src/components/NewLargeButton';
import {useMintMembership} from 'src/utils/hook/mintMembership';
import {useGetNftFromWallet} from 'src/utils/hook/getNftFromWallet';
import {ProgressBar} from 'react-native-paper';

const Wrapper = styled.View({flex: 1, width: '100%'});
const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 22,
  marginHorizontal: 30,
  color: Colors.white,
});
const SubtitleText = styled.Text({
  ...Fonts.avenir,
  fontSize: 12,
  marginHorizontal: 30,
  color: Colors.white,
});

const Row = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginBottom: 10,
});

const Text = styled.Text({
  color: Colors.white,
});

const ListNftUser = ({navigation, route}) => {
  const user = route?.params?.user;
  const [pagging, setPagging] = useState(10);
  const [reloadNft, setReloadNft] = useState();
  const {currentUserProfile} = useContext(FollowersContext);
  const {nftDb, mintMembership, loadingTransation, loading, mintResult, resetMintResult} = useMintMembership(
    user || currentUserProfile,
  );

  const {loadData, loadingNft, progress, total} = useGetNftFromWallet(currentUserProfile, false);

  useEffect(() => {
    if (mintResult === 'success') {
      Alert.alert('Mint success', 'You Mint a Nft', [
        {
          text: 'ok',
          onPress: async () => {
            setReloadNft(true);
            resetMintResult();
            await setTimeout(async () => {
              await loadData();
              setReloadNft(false);
            }, 30000);
          },
        },
      ]);
    } else if (mintResult === 'error') {
      Alert.alert('Mint Error', 'Error minting Nft', [{text: 'ok', onPress: resetMintResult}]);
    }
  }, [mintResult]);

  const isCurrentUser = useMemo(() => {
    if (nftDb.length > 0) {
      return currentUserProfile.id === nftDb[0].user.id;
    }
    return false;
  }, [currentUserProfile.id, nftDb]);

  const userName = useMemo(() => (nftDb.length > 0 ? nftDb[0].user.username : null), [nftDb]);

  const gotoWallet = () => {
    navigation.navigate('WalletConnectUser', {from: 'user'});
  };

  const sortNft = useMemo(() => {
    const filterMembership = nftDb
      .filter((a) => a.symbol === 'SCOBYNYC')
      .sort((a, b) => {
        if (a.serialNft > b.serialNft) {
          return 1;
        }
        if (a.serialNft < b.serialNft) {
          return -1;
        }
        return 0;
      });
    const filterOthers = nftDb
      .filter((a) => a.symbol !== 'SCOBYNYC')
      .sort((a, b) => {
        if (a.symbol < b.symbol) {
          return -1;
        }
        if (a.symbol > b.symbol) {
          return 1;
        }
        return 0;
      });

    return [...filterMembership, ...filterOthers];
  }, [nftDb]);

  const paggingNft = useMemo(() => sortNft.slice(0, pagging), [pagging, sortNft]);

  const renderHeader = () => {
    if (!isCurrentUser) {
      return (
        <>
          <TitleText>{`${userName}'s Bag 💰`}</TitleText>
          <SubtitleText>{`Join ${userName} in these projects through the NFTs in his collections.`}</SubtitleText>
        </>
      );
    }
    return (
      <GlobalContext.Consumer>
        {({walletKey}) => (
          <>
            <Row>
              <TitleText>My Wallet</TitleText>
              <LinearGradient
                start={{x: 0, y: 0.3}}
                end={{x: 1, y: 0}}
                colors={['#cd068e', '#9e0f92', '#7a1794', '#5f1c96', '#501f98', '#4a2098']}
                style={{
                  padding: 5,
                  flexDirection: 'row',
                  alignItems: 'center',
                  borderRadius: 5,
                  marginRight: 30,
                }}>
                <TouchableOpacity onPress={gotoWallet}>
                  {walletKey === null && <Text>Connect</Text>}
                  {walletKey !== null && <Text>{shortendPublicKey(walletKey)}</Text>}
                </TouchableOpacity>
              </LinearGradient>
            </Row>
            <SubtitleText>
              This is a snapshot of the NFTs that were in your wallet last time we checked. You can refresh your Scoby
              wallet view any time.
            </SubtitleText>
          </>
        )}
      </GlobalContext.Consumer>
    );
  };

  const renderFooter = () => (
    <NewLargeButton
      title="More"
      disabled={sortNft.length === paggingNft.length}
      flex
      onPress={() => setPagging((e) => e + 10)}
    />
  );

  if (loading || loadingNft || reloadNft) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <BackButton style={{position: 'absolute'}} navigation={navigation} />
        <ActivityIndicator size="large" color="#fff" />
        {reloadNft && (
          <ProgressBar
            progress={progress / total ? progress / total : 0}
            color={Colors.white}
            style={{width: 200, height: 10, marginVertical: 20}}
          />
        )}
        {reloadNft && (
          <Text style={{color: '#fff'}}>
            {`${Number.isNaN(Math.round((progress * 100) / total)) ? 0 : Math.round((progress * 100) / total)}%`}
          </Text>
        )}
      </View>
    );
  }

  return (
    <Wrapper>
      <BackButton navigation={navigation} />
      <FlatList
        ListHeaderComponent={renderHeader}
        style={{flex: 1}}
        data={paggingNft}
        renderItem={({item}) => (
          <NftItem
            item={item}
            mintMembership={mintMembership}
            loadingTransation={loadingTransation}
            CurrentUser={isCurrentUser}
          />
        )}
        keyExtractor={(item) => `${item.name} ${Math.random()}`}
        ListFooterComponent={renderFooter}
      />
    </Wrapper>
  );
};

export default withSafeAreaWithoutMenu(ListNftUser);
