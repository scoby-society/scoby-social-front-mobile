import {useQuery} from '@apollo/client';
import React, {useContext} from 'react';
import {ActivityIndicator, FlatList} from 'react-native';
import NftItem from 'src/components/Nfts/nftItem';
import {GET_NFT_BY_ID} from 'src/graphql/queries/Spores';
import {FollowersContext} from 'src/containers/followers';

const NftListProfile = ({
  setShowHeader,
  id = null,
  experience = null,
  holder,
  require,
  primary,
  setHolder,
  setPrimary,
  setRequire,
}) => {
  const {currentUserProfile} = useContext(FollowersContext);

  const {
    data: {getNftByUser = []} = [],
    loading: {loadingSpores},
  } = useQuery(GET_NFT_BY_ID, {
    pollInterval: 5000,
    variables: {
      id: id || currentUserProfile.id,
    },
  });

  const renderItem = ({item}) => (
    <NftItem
      item={item}
      CurrentUser={!id}
      experience={experience}
      holder={holder}
      require={require}
      primary={primary}
      setHolder={setHolder}
      setPrimary={setPrimary}
      setRequire={setRequire}
    />
  );

  const HideHeader = () => {
    if (typeof setShowHeader === 'function') {
      setShowHeader(false);
    }
  };

  const KeyExtractor = (item) => `${item.id}-nft-${Math.random()}`;

  if (loadingSpores) {
    return <ActivityIndicator size="large" color="#fff" />;
  }

  return (
    <FlatList
      onScroll={HideHeader}
      data={getNftByUser}
      renderItem={renderItem}
      keyExtractor={KeyExtractor}
      contentContainerStyle={{
        paddingBottom: 300,
      }}
    />
  );
};

export default NftListProfile;
