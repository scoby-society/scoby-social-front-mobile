import React, {useCallback, useContext, useState} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {useMutation, useQuery} from '@apollo/client';
import styled from 'styled-components/native';
import {ActivityIndicator, Linking} from 'react-native';
import {SETTINGS_HEADER_TEXT, PRIVACY_URL, TERMS_URL} from 'src/constants/Texts';
import colors from 'src/constants/Colors';
import {FileIco} from 'assets/ico';
import {GET_USER_PROFILE} from 'src/graphql/queries/profile';
import {FollowersContext} from 'src/containers/followers';
import {GlobalContext} from 'src/containers/global';
import BackButton from 'src/components/BackButton';
import withSafeArea from 'src/components/withSafeArea';
import Fonts from 'src/constants/Fonts';
import NewLargeButton from 'src/components/NewLargeButton';
import {DELETE_ACCOUNT} from 'src/graphql/mutations/profile';
import PromptModal from 'src/components/Modal/PromptModal';
import NFT from '../../assets/images/nft.png';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: column;
  background-color: ${colors.blueBackgroundSession};
`;
const NFTIcon = styled.Image`
  width: 30px;
  height: auto;
  padding: 15px;
  margin: 0;
`;
const MainContent = styled.ScrollView`
  flex: 1;
  flex-direction: column;
  margin: 24px;
`;

const TitleText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 24,
  color: colors.white,
  marginTop: 32,
  marginBottom: 16,
});

const DescriptionText = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: '#9094A2',
  marginBottom: 48,
});

const SmallText = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: '#9094A2',
});

const StatusText = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: colors.greenTermsAccepted,
});

const WhiteText = styled.Text({
  ...Fonts.avenir,
  fontSize: 18,
  color: colors.white,
  paddingLeft: 16,
});

const Row = styled.View({
  flexDirection: 'row',
  marginTop: 24,
  alignItems: 'center',
  justifyContent: 'space-between',
});

const Item = styled.View({});

const WrapperLoading = styled.View({
  flex: 1,
  alignSelf: 'center',
  justifyContent: 'center',
  alignItems: 'center',
});

const ButtonRowNFT = styled.TouchableOpacity({
  flexDirection: 'row',
  marginBottom: '80px',
  alignItems: 'center',
});
const ButtonRow = styled.TouchableOpacity({
  flexDirection: 'row',
  marginBottom: '20px',
});
const SettingsScreen = ({navigation}) => {
  const {client} = useQuery(GET_USER_PROFILE, {fetchPolicy: 'network-only'});
  const {setIsLogged, setWalletKey} = useContext(GlobalContext);
  const {setCurrentUserProfile} = useContext(FollowersContext);
  const [isVisible, setIsVisible] = useState(false);

  const logout = useCallback(async () => {
    await client.cache.reset();
    await client.clearStore();
    setIsLogged(false);
    await AsyncStorage.removeItem('token');
    await AsyncStorage.removeItem('public_key');
    await AsyncStorage.removeItem('public_session');
    await AsyncStorage.removeItem('phantom_encryption_public_key');
    await AsyncStorage.removeItem('dapp_secret_key');
    await AsyncStorage.removeItem('dapp_public_key');
    setWalletKey(null);
    setCurrentUserProfile({id: null});
    setTimeout(() => {
      navigation.replace('LoginWithPhone');
    }, 200);
  }, [client, setCurrentUserProfile, setIsLogged]);

  const [deleteAccount, {loading}] = useMutation(DELETE_ACCOUNT, {
    onCompleted() {
      logout();
    },
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e, null, 4));
    },
  });

  const DeleteAccount = () => {
    deleteAccount();
  };

  if (loading) {
    return (
      <WrapperLoading>
        <ActivityIndicator size="large" color={colors.white} />
      </WrapperLoading>
    );
  }

  return (
    <>
      <PromptModal
        visible={isVisible}
        setVisible={setIsVisible}
        text="Are you sure you want to delete your account? This will remove all of your data from the app."
        leftButtonText="Delete"
        rightButtonText="Cancel"
        onLeftButtonPress={DeleteAccount}
        leftButtonBackground={colors.primaryPurpleColor}
        onRightButtonPress={() => setIsVisible(false)}
      />
      <BackButton onPress={navigation.goBack} />
      <Wrapper>
        <MainContent showsVerticalScrollIndicator={false}>
          <TitleText>{SETTINGS_HEADER_TEXT}</TitleText>
          <DescriptionText>Account management and privacy settings</DescriptionText>
          <SmallText>NFT</SmallText>
          <Row>
            <Item>
              <ButtonRowNFT onPress={() => navigation.navigate('ShowNFT')}>
                <NFTIcon source={NFT} />
                <WhiteText>Show/Hide</WhiteText>
              </ButtonRowNFT>
            </Item>
          </Row>
          <SmallText>Legal</SmallText>
          <Row>
            <Item>
              <ButtonRow onPress={() => Linking.openURL(TERMS_URL)}>
                <FileIco />
                <WhiteText>Terms of Use</WhiteText>
              </ButtonRow>
            </Item>
            <StatusText>Accepted</StatusText>
          </Row>
          <Row>
            <Item>
              <ButtonRow onPress={() => Linking.openURL(PRIVACY_URL)}>
                <FileIco />
                <WhiteText>Privacy Policy</WhiteText>
              </ButtonRow>
            </Item>
            <StatusText>Accepted</StatusText>
          </Row>
          <NewLargeButton onPress={logout} title="Log out" />
          <NewLargeButton
            onPress={() => setIsVisible(true)}
            style={{
              backgroundColor: colors.blueBackgroundSession,
              borderColor: colors.red,
              borderWidth: 1,
              alignSelf: 'center',
            }}
            width="50%"
            height="40px"
            title="Delete account"
          />
        </MainContent>
      </Wrapper>
    </>
  );
};

export default withSafeArea(SettingsScreen);
