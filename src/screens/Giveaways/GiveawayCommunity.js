/* eslint-disable */
/* eslint-disable radix */
/* eslint-disable no-console */
import {useLazyQuery, useMutation} from '@apollo/client';
import React, {useState, useEffect, useCallback} from 'react';
import {Alert, SafeAreaView, StyleSheet, Text, View, Linking} from 'react-native';

import {RadioButton} from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NewLargeButton from 'src/components/NewLargeButton';
import withSafeArea from 'src/components/withSafeArea';
import Colors from 'src/constants/Colors';
import Fonts, {avenir, avenirBold, avenirSemiBold, ttNormsRegular} from 'src/constants/Fonts';
import {TITLE_COMMUNITY_GIVEAWAY} from 'src/constants/Texts';
import {GET_EVENT_AND_GIVEAWAY, GET_USERS_TEAMS} from 'src/graphql/queries/giveaway';
import styled from 'styled-components/native';
import {FlatGrid} from 'react-native-super-grid';
import {CREATE_GIVEAWAY} from 'src/graphql/mutations/giveaway';
import TeamCard from './components/TeamCard';

import * as anchor from "./../../../lib/anchor/index"
import { CLUSTER, GIVEAWAY_PROGRAM_ID } from 'src/config/env';
const GIVEAWAY_PROGRAM = new anchor.web3.PublicKey(GIVEAWAY_PROGRAM_ID)
import { giveawayabi } from 'src/abi/giveawayabi';
import AsyncStorage from '@react-native-community/async-storage';
import bs58 from 'bs58';
import nacl from 'tweetnacl';
import {
 getOrCreateAssociatedTokenAccount,
 TOKEN_PROGRAM_ID,
 ASSOCIATED_TOKEN_PROGRAM_ID
} from "@solana/spl-token";          

const encryptPayload = (payload, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');

  const nonce = nacl.randomBytes(24);

  const encryptedPayload = nacl.box.after(Buffer.from(JSON.stringify(payload)), nonce, sharedSecret);

  return [nonce, encryptedPayload];
};

const decryptPayload = (data, nonce, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');

  const decryptedData = nacl.box.open.after(bs58.decode(data), bs58.decode(nonce), sharedSecret);
  if (!decryptedData) {
    throw new Error('Unable to decrypt data');
  }
  return JSON.parse(Buffer.from(decryptedData).toString('utf8'));
};

const onTransactionRedirectLink = 'scoby://phantom/signAndSendTransaction';
const buildUrl = (path, params) => `phantom://v1/${path}?${params.toString()}`;

let deepLinkingListener = null
let hasStarted = false

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    ...Fonts.avenir,
    padding: 16,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    borderColor: Colors.white,
    marginVertical: 5,
  },
  inputDescription: {
    ...Fonts.avenir,
    padding: 16,
    paddingTop: 16,
    minHeight: 96,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    borderRadius: 10,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderColor: Colors.white,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginTop: 5,
  },
  textViewStyle: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: Colors.white,
  },
  textStyle: {
    ...Fonts.avenir,
    color: Colors.greySession,
    textAlign: 'right',
    padding: 8,
  },
  label: {
    ...Fonts.avenir,
    color: Colors.white,
    textAlign: 'left',
    padding: 0,
    marginLeft: 5,
  },
  inputControl: {
    marginTop: 10,
  },
  radioBtn: {flexDirection: 'row', alignItems: 'center'},
  RadioButtonBorder: {height: 100},
  teamCardWrapper: {
    paddingLeft: 0,
    marginTop: 10,
  },
  topicsWrapper: {
    marginLeft: 5,
    marginTop: 3,
  },
  teamFlag: {
    position: 'absolute',
    right: 0,
    top: 10,
    width: 88,
    height: 25,
  },
  teamOwnerWrapper: {
    alignItems: 'center',
    marginRight: 25,
    width: 90,
    maxWidth: 90,
    marginLeft: 0,
  },
  teamNameWrapper: {
    marginBottom: 5,
    marginLeft: 10,
  },
  teamNameText: {
    ...avenirBold,
    fontSize: 16,
  },
  teamCardBodyWrapper: {
    marginBottom: 10,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  teamAvatarOwnerWrapper: {
    borderWidth: 2,
  },
  teamAvatarOwner: {
    width: 50,
    height: 50,
  },
  teamOwnerFullName: {
    ...avenirBold,
    fontSize: 12,
    marginTop: 0,
    maxWidth: '100%',
  },
  teamOwnerName: {
    ...avenir,
    fontSize: 10,
    marginTop: 3,
    maxWidth: '100%',
  },
  teamCardDesc: {
    ...ttNormsRegular,
    fontSize: 12,
    lineHeight: 14,
    marginTop: 10,
    maxWidth: '65%',
    color: Colors.darkGrey,
  },
  teamCardDescContainer: {
    ...avenir,
    width: '67%',
  },
  topic: {
    paddingVertical: 3,
    marginBottom: 0,
  },
  topicIcon: {
    height: 15,
    width: 15,
  },
  topicText: {
    ...avenirSemiBold,
    fontSize: 12,
    color: Colors.primaryPurpleColor,
    maxWidth: '100%',
  },
  shareButton: {
    height: 26,
    width: 80,
    justifyContent: 'center',
    borderRadius: 5,
  },
  shareIcon: {
    width: 15,
    height: 15,
    marginRight: 3,
  },
  TextSelect: {
    color: '#000',
    fontFamily: 'Avenrir',
    fontSize: 10,
    fontWeight: '700',
    lineHeight: 12,
  },
  CheckBoxStyles: {
    width: 18,
    height: 18,
    alignSelf: 'flex-end',
    borderRadius: 10,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgba(255,255,255,0.05)',
    backgroundColor: 'transparent',
    marginTop: 10,
    marginRight: 15,
  },
  backbtncontainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const limit = 10;

const GiveawayCommunity = ({navigation, route}) => {
  const [selectedCard, setSelectedCard] = useState([]);
  const [isFirstTime, setIsFirstTime] = useState(true);
  const [page, setPage] = useState(1);
  const [isPage, setIsPaging] = useState(true);
  const [mydata, setMyData] = useState([]);

  const details = route?.params;
  const {CollectionsId, Description, EventId, giveawayType, SelectNFTids, Name, solValue, IsAllCollectionRequired, nftdata} =
    details;
  const solnew = parseInt(solValue);
  const [radioChecked, setRadioChecked] = useState('one');

  const [apifetch] = useLazyQuery(GET_USERS_TEAMS, {
    pollInterval: 10000,
    fetchPolicy: 'network-only',
    variables: {
      query: '',
      paging: {
        limit,
        page,
      },
    },
    onCompleted({getUsersTeams}) {
      let newData = mydata;
      if (page === 1) {
        newData = [];
      }
      if (getUsersTeams.data.length > 0) {
        for (let index = 0; index < getUsersTeams.data.length; index += 1) {
          const element = getUsersTeams.data[index];
          newData.push(element);
          setMyData(newData);
        }
        setIsPaging(true);
      } else {
        setIsPaging(false);
      }
    },
    onError() {
      setPage(false);
    },
  });

  const [createGiveAway] = useMutation(CREATE_GIVEAWAY, {
    variables: {
      giveaway: {
        collectionsId: CollectionsId,
        communitysId: selectedCard,
        description: Description,
        eventId: EventId,
        giveawayType,
        isAllCollectionRequired: IsAllCollectionRequired,
        isAllCommunityRequired: radioChecked === 'all',
        name: Name,
        nftsIds: SelectNFTids,
        value: solnew,
      }
    }
  });

  const handleDeepLink = ({url}) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        const urlData = new URL(url);
        const params = urlData.searchParams;
        if (params.get('errorCode')) {
          return
        }
        processTransaction(params)
      }
    });
  };

  const processTransaction = async(params) => {
    if(!hasStarted) {
      return
    }
    hasStarted = false
    const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
    const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
    const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));

    const signTransactionData = decryptPayload(
      params.get("data"),
      params.get("nonce"),
      sharedSecretDapp
    );

    try {
      await createGiveAway();
      Alert.alert('You have Created Giveaway Successfully');
      navigation.pop(6);
    } catch (err) {}
    
  }

  useEffect(() => {
    if (isFirstTime) {
      setIsFirstTime(false);
      setPage(1);
      apifetch({
        fetchPolicy: 'no-cache',
        variables: {
          query: '',
          paging: {
            limit,
            page: 1,
          },
        },
      });
    }
    if (deepLinkingListener) {
      deepLinkingListener.remove();
    }
    deepLinkingListener = Linking.addEventListener('url', handleDeepLink);
  }, []);

  const handleEndReached = useCallback(() => {
    if (!isPage) {
      return;
    }
    setPage(page + 1);
    apifetch({
      fetchPolicy: 'no-cache',
      variables: {
        query: '',
        paging: {
          limit,
          page: page + 1,
        },
      },
    });
  }, [apifetch, page]);

  const handleRefresh = useCallback(() => {
    setPage(1);
    setIsPaging(true);
    apifetch({
      fetchPolicy: 'no-cache',
      variables: {
        query: '',
        paging: {
          limit,
          page: 1,
        },
      },
    });
  }, [apifetch]);

  const BackNavigation = () => {
    navigation.goBack();
  };

  const setCard = (id) => {
    // alert(id)
    if (selectedCard.includes(id)) {
      setSelectedCard(selectedCard.filter((item) => item !== id));
    } else {
      setSelectedCard([...selectedCard, id]);
    }
  };

  const onSubmit = async () => {
    hasStarted = true

    const phantomWalletPublicKey = await AsyncStorage.getItem('public_key');

    const userWallet = new anchor.web3.PublicKey(phantomWalletPublicKey)

    const conn = new anchor.web3.Connection(anchor.web3.clusterApiUrl(CLUSTER));
    const randWallet = anchor.web3.Keypair.generate();
    const confirmOption = {commitment: 'finalized', preflightCommitment: 'finalized', skipPreflight: false};
    const provider = new anchor.AnchorProvider(conn, randWallet, confirmOption);
    const program = new anchor.Program(giveawayabi, GIVEAWAY_PROGRAM, provider)
    let transaction = new anchor.web3.Transaction();
    var msg_id=EventId.toString();

    if(giveawayType === "sol") {
      var no_of_sol=solValue;
      const [user_pda_sol, user_bump_sol] = 
      await anchor.web3.PublicKey.findProgramAddress(
        [userWallet.toBuffer(),Buffer.from(msg_id)],
        GIVEAWAY_PROGRAM
      );
      let check_sol_pda = await conn.getAccountInfo(user_pda_sol);
      if(check_sol_pda==null)
      {
       transaction.add(await program.methods.initializepdasol(user_bump_sol,msg_id)
       .accounts({
         userPdaAcct: user_pda_sol,
         sender: userWallet,
         systemProgram: anchor.web3.SystemProgram.programId,
       }).signers([])
       .instruction())
      }

      transaction.add(await program.methods.sendsolpda(user_bump_sol,
        new anchor.BN(no_of_sol*anchor.web3.LAMPORTS_PER_SOL),msg_id)
      .accounts({
        userPdaAcct: user_pda_sol,
        sender: userWallet,
        systemProgram: anchor.web3.SystemProgram.programId,
      }).signers([])
      .instruction())
    } else {
      let token_arr = []
      let myToken_acctA = await getOrCreateAssociatedTokenAccount(con,
        userWallet,
        new anchor.web3.PublicKey(nftdata[0].contractAdress),
        userWallet);

        let token_acct=myToken_acctA;
        let mint= new anchor.web3.PublicKey(nftdata[0].contractAdress);
        let amount=nftdata.length;
        token_arr.push(token_acct,mint,amount)

        const [user_pda_state, bump_state] = await anchor.web3.PublicKey.findProgramAddress(
          [userWallet.toBuffer(),token_acct.address.toBuffer(),Buffer.from(msg_id),Buffer.from("state")],
          GIVEAWAY_PROGRAM
        );
    
        if(await conn.getAccountInfo(user_pda_state)==null){
          transaction.add(await program.methods.initializestatepda(bump_state,
            msg_id)
          .accounts({
            statepda: user_pda_state,
            owner: userWallet,
            depositTokenAccount: token_acct.address,
            systemProgram: anchor.web3.SystemProgram.programId,
          }).instruction());
        }

        const [usertokenpda, bump_token] = await anchor.web3.PublicKey.findProgramAddress(
          [userWallet.toBuffer(),token_acct.address.toBuffer(),Buffer.from(msg_id)],
          GIVEAWAY_PROGRAM
        );

        if(await con.getAccountInfo(usertokenpda)==null)
        {
  
          transaction.add(await program.methods.initialisetokenpda(bump_token,
            msg_id)
          .accounts({
            tokenpda: usertokenpda,
            statepda: user_pda_state,
            mint :    mint,
            owner : userWallet,
            depositTokenAccount : token_acct.address,
            systemProgram: anchor.web3.SystemProgram.programId,
            rent : anchor.web3.SYSVAR_RENT_PUBKEY,
            tokenProgram : TOKEN_PROGRAM_ID
          }).signers([])
          .instruction())
        }

        /// call for token transfer from user to PDA token Account
        transaction.add(await program.methods.sendtokenpda(bump_state,bump_token,
          new anchor.BN(amount),
          msg_id)
        .accounts({
          tokenpda: usertokenpda,
          statepda: user_pda_state,
          mint :    mint,
          owner : userWallet,
          depositTokenAccount : token_acct.address,
          systemProgram: anchor.web3.SystemProgram.programId,
          rent : anchor.web3.SYSVAR_RENT_PUBKEY,
          tokenProgram : TOKEN_PROGRAM_ID
        }).signers([])
        .instruction())
    }

    transaction.feePayer = userWallet;
    const anyTransaction = transaction;
    anyTransaction.recentBlockhash = (await conn.getLatestBlockhash()).blockhash;

    const serializedTransaction = anyTransaction.serialize({
      requireAllSignatures: false
    });

    const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
    const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
    const dappPublicKey = await AsyncStorage.getItem('dapp_public_key');
    const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));
    const publicSession = await AsyncStorage.getItem('public_session');

    const payload = {
      session: publicSession,
      transaction: bs58.encode(serializedTransaction),
    };


    const [nonce, encryptedPayload] = encryptPayload(payload, sharedSecretDapp);

    const params = new URLSearchParams({
      dapp_encryption_public_key: dappPublicKey,
      nonce: bs58.encode(nonce),
      redirect_link: onTransactionRedirectLink,
      payload: bs58.encode(encryptedPayload)
    });
    const url = buildUrl('signAndSendTransaction', params);

    Linking.openURL(url);

  };

  return (
    <Wrapper>
      <View>
        <View style={styles.backbtncontainer}>
          <Ionicons
            name="chevron-back"
            color="white"
            size={30}
            style={{marginBottom: 30, marginLeft: -13}}
            onPress={() => BackNavigation()}
          />
        </View>

        <TitleText>{TITLE_COMMUNITY_GIVEAWAY}</TitleText>
        <View style={styles.inputControl}>
          <Text style={styles.desc}>Participants must be a member of one or all of these communities to win.</Text>
        </View>
        <View style={{marginTop: 15}}>
          <View style={styles.radioBtn}>
            <RadioButton
              style={styles.RadioButtonBorder}
              value="one"
              color="#EC008C"
              uncheckedColor="#86859E"
              status={radioChecked === 'one' ? 'checked' : 'unchecked'}
              onPress={() => setRadioChecked('one')}
            />
            <Text>Require one</Text>
          </View>

          <View style={styles.radioBtn}>
            <RadioButton
              style={styles.RadioButtonBorder}
              value="all"
              color="#EC008C"
              uncheckedColor="#86859E"
              status={radioChecked === 'all' ? 'checked' : 'unchecked'}
              onPress={() => setRadioChecked('all')}
            />
            <Text>Require all </Text>
          </View>
        </View>
      </View>
      <MainContent>
        <FlatGrid
          itemDimension={300}
          data={mydata}
          onEndReachedThreshold={0.5}
          onEndReached={handleEndReached}
          onRefresh={handleRefresh}
          refreshing={false}
          spacing={5}
          renderItem={({item}) => <TeamCard item={item} setCard={setCard} isSeleted={selectedCard.includes(item.id)} />}
        />
      </MainContent>

      <ButtonContainer>
        <NewLargeButton title="Back" transparent onPress={() => BackNavigation()} flex />

        <NewLargeButton flex title="Submit" onPress={onSubmit} />
      </ButtonContainer>
    </Wrapper>
  );
};

export default withSafeArea(GiveawayCommunity);

const Wrapper = styled(SafeAreaView)({
  marginTop: '5%',
  flexDirection: 'column',
  backgroundColor: Colors.blueBackgroundSession,
  marginHorizontal: 30,
  flex: 1,
  justifyContent: 'space-between',
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  lineHeight: '32px',
  color: Colors.white,
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
});

const MainContent = styled.View({
  flex: 1,
  marginTop: 30,
});
