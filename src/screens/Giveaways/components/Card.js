import coverImage from 'assets/images/profile/Cover.png';
import React from 'react';
import {CheckBox, StyleSheet, Text, View} from 'react-native';
import styled from 'styled-components/native';

const AvatarImage = styled.Image({
  width: '100%',
  height: 155,
  opacity: 1,
  borderRadius: 30,
});
const Card = ({item, setCard, isSeleted}) => {
  const styles = StyleSheet.create({
    cardContainer: {
      height: 262,
      width: 175,
      backgroundColor: '#0A0846',
      borderRadius: 30,
      padding: 7,
    },
    cardBottom: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'relative',
      marginTop: 10,
    },
  });
  return (
    <View style={styles.cardContainer}>
      <AvatarImage source={item.urlImage ? {uri: item.urlImage} : coverImage} style={{resizeMode: 'cover'}} />
      <View style={styles.cardBottom}>
        <View>
          <Text>{item.name}</Text>
          <Text style={{fontSize: 10, marginTop: 5}}>{item.serialNumber}</Text>
        </View>
        <View>
          <Text>Select</Text>
          <CheckBox
            value={isSeleted}
            // status={isSeleted}
            onValueChange={() => setCard(item.id)}
            tintColors={{true: '#EC008C', false: '#ffffff'}}
          />
          {/* <CheckBox
    checkBoxColor={'#EC008C'}
    isChecked={checked.includes(item.id)}
    onClick={() => {
      const newIds = [...checked];
      const index = newIds.indexOf(item.id);
      if (index > -1) {
        newIds.splice(index, 1); 
      } else {
        newIds.push(item.id)
      }
      setChecked(newIds)
    }}
   /> */}
        </View>
      </View>
    </View>
  );
};

export default Card;
