import coverImage from 'assets/images/profile/Cover.png';
import React from 'react';
import {CheckBox, StyleSheet, Text, View} from 'react-native';
import styled from 'styled-components/native';
import Colors from 'src/constants/Colors';
import Fonts, {avenir, avenirBold, avenirSemiBold, ttNormsRegular} from 'src/constants/Fonts';
import iconCook from '../../../../assets/images/fryingegg.png';
import girl from '../../../../assets/images/girl.png';
import iconKiwi from '../../../../assets/images/kiwi.png';
import avatarSrc from '../../../../assets/images/profile/avatarPlaceholder.png';

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
    },
    input: {
      ...Fonts.avenir,
      padding: 16,
      color: Colors.white,
      backgroundColor: Colors.transparent,
      borderRightWidth: 1,
      borderLeftWidth: 1,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderRadius: 10,
      borderColor: Colors.white,
      marginVertical: 5,
    },
    inputDescription: {
      ...Fonts.avenir,
      padding: 16,
      paddingTop: 16,
      minHeight: 96,
      color: Colors.white,
      backgroundColor: Colors.transparent,
      borderRadius: 10,
      borderRightWidth: 1,
      borderLeftWidth: 1,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: Colors.white,      
      marginTop: 5,
    },
    textViewStyle: {
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      backgroundColor: Colors.white,
    },
    textStyle: {
      ...Fonts.avenir,
      color: Colors.greySession,
      textAlign: 'right',
      padding: 8,
    },
    label: {
      ...Fonts.avenir,
      color: Colors.white,
      textAlign: 'left',
      padding: 0,
      marginLeft: 5,
    },
    inputControl: {
      marginTop: 10,
    },
    radioBtn: {flexDirection: 'row', alignItems: 'center'},
    RadioButtonBorder: {height: 100},
    teamCardWrapper: {
      paddingLeft: 0,
      marginTop: 10,
    },
    topicsWrapper: {
      marginLeft: 5,
      marginTop: 3,
    },
    teamFlag: {
      position: 'absolute',
      right: 0,
      top: 10,
      width: 88,
      height: 25,
    },
    teamOwnerWrapper: {
      alignItems: 'center',
      marginRight: 25,
      width: 90,
      maxWidth: 90,
      marginLeft: 0,
    },
    teamNameWrapper: {
      marginBottom: 5,
      marginLeft: 10,
    },
    teamNameText: {
      ...avenirBold,
      fontSize: 16,
      
    },
    teamCardBodyWrapper: {
      marginBottom: 10,
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
    teamAvatarOwnerWrapper: {
      borderWidth: 2,
    },
    teamAvatarOwner: {
      width: 50,
      height: 50,
    },
    teamOwnerFullName: {
      ...avenirBold,
      fontSize: 12,
      marginTop: 0,
      maxWidth: '100%',
    },
    teamOwnerName: {
      ...avenir,
      fontSize: 10,
      marginTop: 3,
      maxWidth: '100%',
    },
    teamCardDesc: {
      ...ttNormsRegular,
      fontSize: 12,
      lineHeight: 14,
      marginTop: 45,
      maxWidth: '65%',
      color: Colors.darkGrey,
    },
    teamCardDescContainer: {
      ...avenir,
      width: '67%',
    },
    topic: {
      paddingVertical: 3,
      marginBottom: 0,
    },
    topicIcon: {
      height: 15,
      width: 15,
    },
    topicText: {
      ...avenirSemiBold,
      fontSize: 12,
      color: Colors.primaryPurpleColor,
      maxWidth: '100%',
    },
    shareButton: {
      height: 26,
      width: 80,
      justifyContent: 'center',
      borderRadius: 5,
    },
    shareIcon: {
      width: 15,
      height: 15,
      marginRight: 3,
    },
    TextSelect: {
      //color: Colors.pink,
      fontFamily: 'Avenrir',
      fontSize: 10,
      fontWeight: '700',
      lineHeight: 12,
      position: 'relative',
      color:"red",
    },
    CheckBoxStyles: {
      width: 18,
      height: 18,
      alignSelf: 'flex-end',
      borderRadius: 10,
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: 'rgba(255,255,255,0.05)',
      backgroundColor: 'transparent',
      marginTop: 10,
      marginRight: 15,
    },
    backbtncontainer: {
      flex: 0,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  });


const AvatarImage = styled.Image({
  width: '15',
  height: 15,
  opacity: 1,
  borderRadius: 30,
});

const TeamCardWrapper = styled.View({
  backgroundColor: Colors.white,
  borderRadius: 8,
  padding: 12,
  marginTop: 20,
  marginBottom: 10,
  position: 'relative',
});

const TeamNameWrapper = styled.View({
  width: '60%',
});

const TeamNameText = styled.Text({
  ...avenirBold,
  fontSize: 14,
  color: Colors.black,
});

const TeamCardBodyWrapper = styled.View({
  flexDirection: 'row',
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  marginBottom: 22,
});

const TeamOwnerWrapper = styled.View({
  alignItems: 'center',
  marginLeft: 6,
});

const TeamAvatarOwnerWrapper = styled.View({
  borderRadius: 50,
  borderWidth: 3,
  borderColor: Colors.newPink,
});

const TeamOwnerFullName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 17,
  fontWeight: 'bold',
  color: Colors.violetColor,
  marginTop: 6,
});

const TeamOwnerName = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  color: Colors.violetColor,
  marginTop: 6,
});

const TeamAvatarOwner = styled.Image({
  width: 80,
  height: 80,
  borderRadius: 50,
  resizeMode: 'cover',
});

const TeamCardDesc = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  maxWidth: '55%',
  flexGrow: 1,
  marginTop:40,
  color: Colors.black,
});

const BottomWrapper = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
});

const TopicsWrapper = styled.View({
  flexDirection: 'row',
  flexWrap: 'wrap',
  maxWidth: '80%',
});

const Topic = styled.View({
  flexDirection: 'row',
  backgroundColor: Colors.violetColorSelfOpacity,
  borderRadius: 5,
  marginHorizontal: 4,
  marginVertical: 2,
  paddingHorizontal: 6,
  paddingVertical: 6,
  minWidth: 62,
});

const TopicText = styled.Text({
  marginLeft: 2,
});

const TeamCard = ({item, setCard, isSeleted}) => {
  const styles = StyleSheet.create({
    cardContainer: {
      width: "100%",
      backgroundColor: '#0A0846',
      borderRadius: 30,
      padding: 7,
    },
    cardBottom: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'relative',
      marginTop: 10,
    },
  });

  return (
    <View style={styles.cardContainer}>
        <TeamCardWrapper>
              <TeamNameWrapper style={styles.teamNameWrapper}>
                <TeamNameText style={styles.teamNameText}> {item.name} </TeamNameText>
                
              </TeamNameWrapper>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'flex-end',
                  alignItems: 'flex-end',
                  width: '50%',
                  position: 'absolute',
                  marginTop: 10,
                  right:30,
                   
                }}>
                <Text style={{color:'#EC008C'}}>Require</Text>
                <CheckBox
                  value={isSeleted}
                   onValueChange={() => setCard(item.id)}
                  style={styles.CheckBoxStyles}
                  tintColors={{true: '#EC008C', false: '#ffffff'}}
                />
              </View>

                <TeamCardBodyWrapper style={styles.teamCardBodyWrapper}>
                <TeamOwnerWrapper style={styles.teamOwnerWrapper}>
                  <TeamAvatarOwnerWrapper style={styles.teamAvatarOwnerWrapper}>
                    <TeamAvatarOwner source={item.avatar ? {uri: item.avatar} : avatarSrc} style={styles.teamAvatarOwner} />
                  </TeamAvatarOwnerWrapper>
                  <TeamOwnerFullName style={styles.teamOwnerFullName}>{item.ownerUser.fullName}</TeamOwnerFullName>
                  <TeamOwnerName style={styles.teamOwnerName}>{item.ownerUser.username}</TeamOwnerName>
                </TeamOwnerWrapper>

                <TeamCardDesc style={styles.teamCardDesc}>
                  {item.description}
                </TeamCardDesc>
              </TeamCardBodyWrapper>

              <BottomWrapper>
                <TopicsWrapper style={styles.topicsWrapper}>
                  {item.topics?.map(({icon, name: topicName}) => (
            <Topic style={styles.topic} key={icon}>
              <Text style={styles.topicIcon}>{icon}</Text>
              <TopicText style={styles.topicText}>{topicName}</TopicText>
            </Topic>
          ))}

                </TopicsWrapper>
              </BottomWrapper>
            </TeamCardWrapper>
            </View>
            );
};

export default TeamCard;