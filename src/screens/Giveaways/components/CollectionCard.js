import coverImage from 'assets/images/profile/Cover.png';
import React from 'react';
import {CheckBox, StyleSheet, Text, View} from 'react-native';
import styled from 'styled-components/native';

const AvatarImage = styled.Image({
  width: '100%',
  height: 155,
  opacity: 1,
  borderRadius: 30,
});

const Cardcollection = ({item, setCard, isSeleted}) => {
  const styles = StyleSheet.create({
    cardContainer: {
      height: 262,
      width: 155,
      backgroundColor: '#0A0846',
      borderRadius: 30,
      padding: 7,
    },
    cardBottom: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      position: 'relative',
      marginTop: 10,
    },
  });
  return (
    <View style={styles.cardContainer}>
      <AvatarImage source={item.avatarCreator ? {uri: item.avatarCreator} : coverImage} style={{resizeMode: 'cover'}} />
      <View style={styles.cardBottom}>
        <View>
          <Text>{item.title}</Text>
          <Text style={{fontSize: 10, marginTop: 5}}>{item.code}</Text>
        </View>
        <View>
          <Text>Select</Text>
          <CheckBox
            value={isSeleted}
            onValueChange={() => setCard(item.id)}
            tintColors={{true: '#EC008C', false: '#ffffff'}}
          />
        </View>
      </View>
    </View>
  );
};

export default Cardcollection;
