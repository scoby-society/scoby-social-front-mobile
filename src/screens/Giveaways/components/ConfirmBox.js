import { Alert } from 'react-native';

const confirmBox = ({ onSuccess, onFail, text }) =>
    Alert.alert(`Are your sure?`, `${text}?`, [
        {
            text: 'Yes',
            onPress: onSuccess,
        },

        {
            text: 'No',
            onPress: onFail,
        },
    ]);
export default confirmBox;
