import {useLazyQuery} from '@apollo/client';
import React, {useEffect, useState, useContext, useCallback} from 'react';
import {Alert, SafeAreaView, StyleSheet, Text, View, FlatList} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NewLargeButton from 'src/components/NewLargeButton';
import withSafeArea from 'src/components/withSafeArea';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {TITLE_NFT_GIVEAWAY} from 'src/constants/Texts';
import styled from 'styled-components/native';
import {GET_NFT_BY_ID_PAGING} from 'src/graphql/queries/giveaway';
import {FollowersContext} from 'src/containers/followers';
import Card from './components/Card';

const limit = 10;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 5,
    flex: 1,
  },
  icon: {
    marginBottom: 30,
    marginLeft: 10,
  },
  backbtncontainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '5%',
  },
  skipText: {
    ...Fonts.avenir,
    color: Colors.white,
  },
  container: {
    justifyContent: 'center',
    flex: 1,
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
});

const SelectNftGiveaway = ({navigation, route}) => {
  const {currentUserProfile} = useContext(FollowersContext);
  const [isFirstTime, setIsFirstTime] = useState(true);
  const [page, setPage] = useState(1);
  const [isPage, setIsPaging] = useState(true);
  const [mydata, setMyData] = useState([]);
  const [selectedCard, setSelectedCard] = useState([]);

  const [apifetch] = useLazyQuery(GET_NFT_BY_ID_PAGING, {
    pollInterval: 10000,
    fetchPolicy: 'network-only',
    variables: {
      id: currentUserProfile.id,
      paging: {
        limit,
        page,
      },
    },
    onCompleted({getNftByUserPaging}) {
      let newData = mydata;
      if (page === 1) {
        newData = [];
      }
      if (getNftByUserPaging.length > 0) {
        for (let index = 0; index < getNftByUserPaging.length; index += 1) {
          const element = getNftByUserPaging[index];
          newData.push(element);

          setMyData(newData);
        }
        setIsPaging(true);
      } else {
        setIsPaging(false);
      }
    },
    onError() {
      setPage(false);
    },
  });

  useEffect(() => {
    if (isFirstTime) {
      setIsFirstTime(false);
      setPage(1);
      apifetch({
        fetchPolicy: 'no-cache',
        variables: {
          paging: {
            limit,
            page: 1,
          },
        },
      });
    }
  }, []);

  const handleEndReached = useCallback(() => {
    if (!isPage) {
      return;
    }
    setPage(page + 1);
    apifetch({
      fetchPolicy: 'no-cache',
      variables: {
        paging: {
          limit,
          page: page + 1,
        },
      },
    });
  }, [apifetch, page]);

  const handleRefresh = useCallback(() => {
    setPage(1);
    setIsPaging(true);
    apifetch({
      fetchPolicy: 'no-cache',
      variables: {
        paging: {
          limit,
          page: 1,
        },
      },
    });
  }, [apifetch]);

  const Details = route?.params;

  const cleanFields = () => {
    navigation.goBack();
  };

  const setCard = (id) => {
    if (selectedCard.includes(id)) {
      setSelectedCard(selectedCard.filter((item) => item !== id));
    } else {
      setSelectedCard([...selectedCard, id]);
    }
  };

  const handleToNext = () => {
    if (selectedCard.length === 0) {
      return Alert.alert('Please Select NFT ');
    }
    const selectedNFTIds = [];
    for (let index = 0; index < selectedCard.length; index += 1) {
      const nftid = selectedCard[index];
      const findIndex = mydata.findIndex((dataObj) => dataObj.id === nftid);
      if (findIndex !== -1) {
        selectedNFTIds.push(mydata[findIndex]);
      }
    }
    const GiveawayDetails = {
      EventId: Details.EventId,
      Name: Details.Name,
      Description: Details.Description,
      nftdata: selectedNFTIds,
      SelectNFTids: selectedCard,
      giveawayType: 'nft',
    };
    navigation.navigate('GiveawayCollection', GiveawayDetails);
  };
  const handleSkip = () => {
    const GiveawayDetails = {
      EventId: Details.EventId,
      Name: Details.Name,
      Description: Details.Description,
      SelectNFTids: [],
      nftdata: [],
    };
    navigation.navigate('GiveawayAddSol', GiveawayDetails);
  };

  return (
    <>
      <Wrapper>
        <View>
          <View style={styles.backbtncontainer}>
            <Ionicons name="chevron-back" color="white" size={25} style={styles.icon} onPress={() => cleanFields()} />
            {selectedCard.length === 0 && (
              <Text style={styles.skipText} onPress={() => handleSkip()}>
                Skip
              </Text>
            )}
          </View>
          <View style={{marginLeft: 30}}>
            <TitleText>{TITLE_NFT_GIVEAWAY}</TitleText>
            <ContentText>
              <Text>
                Select the NFTs you would like to include in the giveaway. NOTE: These will be locked until you complete
                or cancel the giveaway.
              </Text>
            </ContentText>
          </View>
        </View>

        {mydata?.length ? (
          <FlatList
            numColumns={2}
            data={mydata}
            keyExtractor={(item, index) => index.toString()}
            onEndReachedThreshold={0.5}
            onEndReached={handleEndReached}
            onRefresh={handleRefresh}
            refreshing={false}
            enableEmptySections
            renderItem={({item}) => <Card item={item} setCard={setCard} isSeleted={selectedCard.includes(item.id)} />}
          />
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <TitleText style={{textAlign: 'center'}}>Empty Record</TitleText>
          </View>
        )}
        <ButtonContainer>
          <NewLargeButton title="Back" transparent onPress={() => cleanFields()} flex />
          {mydata.length > 0 && <NewLargeButton flex title="Next" onPress={handleToNext} />}
        </ButtonContainer>
      </Wrapper>
    </>
  );
};

export default withSafeArea(SelectNftGiveaway);

const Wrapper = styled(SafeAreaView)({
  marginTop: '5%',
  flexDirection: 'column',
  backgroundColor: Colors.blueBackgroundSession,
  marginHorizontal: 10,
  flex: 1,
  justifyContent: 'space-between',
  height: '90%',
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 24,
  lineHeight: '32px',
  color: Colors.white,
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
});

const ContentText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  paddingBottom: 16,
  color: Colors.white,
  marginTop: 10,
});
