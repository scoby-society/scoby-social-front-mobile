import React, {useState} from 'react';
import {Alert, StyleSheet, Text, TextInput, View, KeyboardAvoidingView, Platform} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NewLargeButton from 'src/components/NewLargeButton';
import withSafeArea from 'src/components/withSafeArea';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {TITLE_GIVEAWAY} from 'src/constants/Texts';
import styled from 'styled-components/native';
import {useRoute} from '@react-navigation/native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  input: {
    ...Fonts.avenir,
    padding: 16,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    borderColor: Colors.white,
    marginVertical: 5,
  },
  inputDescription: {
    ...Fonts.avenir,
    padding: 16,
    paddingTop: 16,
    minHeight: 96,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    borderRadius: 10,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderColor: Colors.white,
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginTop: 5,
  },
  textViewStyle: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: Colors.white,
  },
  textStyle: {
    ...Fonts.avenir,
    color: Colors.greySession,
    textAlign: 'right',
    padding: 8,
  },
  label: {
    ...Fonts.avenir,
    color: Colors.white,
    textAlign: 'left',
    padding: 0,
    marginLeft: 5,
  },
  inputControl: {
    marginTop: 10,
  },
});

const CreateGiveaways = ({navigation}) => {
  const [Name, setName] = useState('');
  const {params} = useRoute();
  const {EventId} = params;
  const [Description, setDescription] = useState('');

  const cleanFields = () => {
    setName('');
    setDescription('');
    navigation.goBack();
  };

  const handleToNext = () => {
    const letters = /^[a-zA-Z0-9_ ]*$/;
    if (!Name) {
      return Alert.alert('Please Enter Name');
    }
    if (!Description) {
      return Alert.alert('Please Enter Description');
    }
    if (Name.match(letters) && Description.match(letters)) {
      const GiveawayDetails = {
        Name,
        Description,
        EventId,
      };
      navigation.navigate('SelectNftGiveaway', GiveawayDetails);
    } else {
      return Alert.alert('Name and description should be alphanumeric');
    }
  };

  return (
    <>
      <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
        <WrapperOuter keyboardShouldPersistTaps="handled">
          <View>
            <Ionicons
              name="chevron-back"
              color="white"
              size={30}
              style={{marginBottom: 30, marginLeft: -13}}
              onPress={() => cleanFields()}
            />

            <TitleText>{TITLE_GIVEAWAY}</TitleText>
            <MainContent>
              <View style={styles.inputControl}>
                <Text style={styles.label}>Name</Text>
                <TextInput
                  style={styles.input}
                  onChangeText={(value) => setName(value)}
                  value={Name}
                  placeholder="Name"
                  returnKeyType="done"
                  placeholderTextColor={Colors.greySession}
                  maxLength={40}
                />
              </View>
              <View style={styles.inputControl}>
                <Text style={styles.label}>Description</Text>
                <TextInput
                  style={styles.inputDescription}
                  onChangeText={(value) => setDescription(value)}
                  value={Description}
                  placeholder="Description"
                  placeholderTextColor={Colors.greySession}
                  maxLength={140}
                  multiline
                  returnKeyType="done"
                  textAlignVertical="top"
                  numberOfLines={5}
                  blurOnSubmit
                />
              </View>
            </MainContent>
          </View>
        </WrapperOuter>
        <ButtonContainer>
          <NewLargeButton title="Back" transparent onPress={() => cleanFields()} flex />
          <NewLargeButton
            flex
            title="Next"
            // loading={loading}
            // disabled={!(Name && Description) || loading}
            onPress={handleToNext}
          />
        </ButtonContainer>
      </KeyboardAvoidingView>
    </>
  );
};

export default withSafeArea(CreateGiveaways);

const WrapperOuter = styled(KeyboardAvoidingView)({
  backgroundColor: Colors.blueBackgroundSession,
  marginHorizontal: 30,
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  lineHeight: '32px',
  color: Colors.white,
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
  padding: '0 30px',
  backgroundColor: Colors.blueBackgroundSession,
});

const MainContent = styled.View({
  flexDirection: 'column',
  marginTop: 30,
});
