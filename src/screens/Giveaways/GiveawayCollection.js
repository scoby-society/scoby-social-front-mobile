import React, {useState} from 'react';
import {Alert, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import {FlatGrid} from 'react-native-super-grid';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NewLargeButton from 'src/components/NewLargeButton';
import withSafeArea from 'src/components/withSafeArea';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {TITLE_COLLECTION_GIVEAWAY} from 'src/constants/Texts';
// import {GET_TEAM} from 'src/graphql/queries/team';
import styled from 'styled-components/native';
import {useQuery} from '@apollo/client';
import {GET_POOLS_FOR_COLLECTIONS} from 'src/graphql/queries/giveaway';

import Cardcollection from './components/CollectionCard';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    ...Fonts.avenir,
    padding: 16,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    borderColor: Colors.white,
    // borderBottomWidth: 0.5,
    marginVertical: 5,
  },
  desc: {
    ...Fonts.avenir,
    fontSize: 12,
    fontWeight: '500',
  },
  inputDescription: {
    ...Fonts.avenir,
    padding: 16,
    paddingTop: 16,
    minHeight: 96,
    color: Colors.white,
    backgroundColor: Colors.transparent,
    borderRadius: 10,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: Colors.white,
    // borderBottomWidth: StyleSheet.hairlineWidth,
    marginTop: 5,
  },
  textViewStyle: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    backgroundColor: Colors.white,
  },
  textStyle: {
    ...Fonts.avenir,
    color: Colors.greySession,
    textAlign: 'right',
    padding: 8,
  },
  label: {
    ...Fonts.avenir,
    color: Colors.white,
    textAlign: 'left',
    padding: 0,
    marginLeft: 5,
  },
  inputControl: {
    marginTop: 10,
  },
  radioBtn: {flexDirection: 'row', alignItems: 'center'},
  RadioButtonBorder: {height: 100},
  TwoBoxLayoutcontainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  OuterBox: {
    backgroundColor: 'rgba(255, 255, 255, 0.02)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#110c69',
    width: '47%',
    height: 262,
    borderRadius: 30,
    marginLeft: 8,
    marginTop: 10,
  },
  TextBox: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    margin: 15,
  },
  TextTitle: {
    color: '#fff',
    fontFamily: 'Avenrir',
    fontSize: 13,
    fontWeight: '600',
    lineHeight: 20,
  },
  TextSelect: {
    color: '#fff',
    fontFamily: 'Avenrir',
    fontSize: 10,
    fontWeight: '700',
    lineHeight: 12,
  },
  CheckBoxStyles: {
    width: 18,
    height: 18,
    alignSelf: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgba(255,255,255,0.05)',
    backgroundColor: 'transparent',
    marginTop: 10,
  },
  backbtncontainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  skipText: {
    ...Fonts.avenir,
    color: Colors.white,
  },
});

const GiveawayCollection = ({navigation, route}) => {
  const {data: {getPools = []} = []} = useQuery(GET_POOLS_FOR_COLLECTIONS, {
    pollInterval: 5000,
  });

  const [selectedCard, setSelectedCard] = useState([]);

  const BackNavigation = () => {
    navigation.goBack();
  };

  const [radioChecked, setRadioChecked] = useState('one');

  const setCard = (id) => {
    if (selectedCard.includes(id)) {
      setSelectedCard(selectedCard.filter((item) => item !== id));
    } else {
      setSelectedCard([...selectedCard, id]);
    }
  };

  const Details = route?.params;

  const handleGoToTopics = () => {
    if (!selectedCard.length) {
      Alert.alert('Please select atleast one collection');
      return;
    }

    const GiveawayDetails = {
      EventId: Details.EventId,
      Name: Details.Name,
      Description: Details.Description,
      SelectNFTids: Details.SelectNFTids,
      nftdata: Details.nftdata,
      solValue: Details.solValue,
      CollectionsId: selectedCard,
      IsAllCollectionRequired: radioChecked !== 'one',
      giveawayType: Details.giveawayType,
    };

    navigation.navigate('GiveawayCommunity', GiveawayDetails);
  };

  const handleSkip = () => {
    const GiveawayDetails = {
      EventId: Details.EventId,
      Name: Details.Name,
      Description: Details.Description,
      SelectNFTids: Details.SelectNFTids,
      nftdata: Details.nftdata,
      solValue: Details.solValue,
      CollectionsId: [],
      IsAllCollectionRequired: false,
      giveawayType: Details.giveawayType,
    };

    navigation.navigate('GiveawayCommunity', GiveawayDetails);
  };

  return (
    <Wrapper>
      <View>
        <View style={styles.backbtncontainer}>
          <Ionicons
            name="chevron-back"
            color="white"
            size={30}
            style={{marginBottom: 30, marginLeft: -13}}
            onPress={() => BackNavigation()}
          />
          <Text style={styles.skipText} onPress={() => handleSkip()}>
            Skip
          </Text>
        </View>
        <TitleText>{TITLE_COLLECTION_GIVEAWAY}</TitleText>
        <View style={styles.inputControl}>
          <Text style={styles.desc}>
            Participants must be holding an NFT from one or all of these collections to win the giveaway.
          </Text>
        </View>

        <View style={{marginTop: 15}}>
          <View style={styles.radioBtn}>
            <RadioButton
              style={styles.RadioButtonBorder}
              value="one"
              color="#EC008C"
              uncheckedColor="#86859E"
              status={radioChecked === 'one' ? 'checked' : 'unchecked'}
              onPress={() => setRadioChecked('one')}
            />
            <Text>Require one</Text>
          </View>

          <View style={styles.radioBtn}>
            <RadioButton
              style={styles.RadioButtonBorder}
              value="all"
              color="#EC008C"
              uncheckedColor="#86859E"
              status={radioChecked === 'all' ? 'checked' : 'unchecked'}
              onPress={() => setRadioChecked('all')}
            />
            <Text>Require all </Text>
          </View>
        </View>
      </View>
      <MainContent>
        <View style={styles.TwoBoxLayoutcontainer}>
          {/* <Text> sdfdf </Text>
              {getPoolsForCollection && (
                  )} */}

          <FlatGrid
            itemDimension={130}
            data={getPools}
            style={styles.gridView}
            // staticDimension={300}
            // fixed
            spacing={5}
            renderItem={({item}) => (
              <Cardcollection item={item} setCard={setCard} isSeleted={selectedCard.includes(item.id)} />
            )}
          />
        </View>
      </MainContent>
      <ButtonContainer>
        <NewLargeButton title="Back" transparent onPress={() => navigation.goBack()} flex />

        <NewLargeButton
          flex
          title="Next"
          // loading={loading}
          // disabled={!(Name && Description) || loading}
          onPress={handleGoToTopics}
        />
      </ButtonContainer>
    </Wrapper>
  );
};

export default withSafeArea(GiveawayCollection);

const Wrapper = styled(SafeAreaView)({
  marginTop: '5%',
  flexDirection: 'column',
  backgroundColor: Colors.blueBackgroundSession,
  marginHorizontal: 30,
  flex: 1,
  justifyContent: 'space-between',
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 22,
  lineHeight: '25px',
  color: Colors.white,
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
});

const MainContent = styled.View({
  marginTop: 30,
  flex: 1,
});
