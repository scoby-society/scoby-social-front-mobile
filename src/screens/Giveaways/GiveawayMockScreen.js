/* eslint-disable */
import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import React, {useEffect, useState, useContext} from 'react';
import {StyleSheet, View, Text, Dimensions, TouchableOpacity, Modal, Image, Alert, ActivityIndicator, Linking} from 'react-native';
import withSafeArea from 'src/components/withSafeArea';
import Colors from 'src/constants/Colors';
import { GET_EVENT_AND_GIVEAWAY, GET_NFT_ID } from 'src/graphql/queries/giveaway';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {FollowersContext} from 'src/containers/followers';
import { PROCESS_GIVEAWAY } from 'src/graphql/mutations/giveaway';

import * as anchor from "./../../../lib/anchor/index"
import { GIVEAWAY_PROGRAM_ID, CLUSTER } from 'src/config/env';
const GIVEAWAY_PROGRAM = new anchor.web3.PublicKey(GIVEAWAY_PROGRAM_ID)
import { giveawayabi } from 'src/abi/giveawayabi';
import AsyncStorage from '@react-native-community/async-storage';
import bs58 from 'bs58';
import nacl from 'tweetnacl';
import {
 getOrCreateAssociatedTokenAccount,
 TOKEN_PROGRAM_ID,
 ASSOCIATED_TOKEN_PROGRAM_ID
} from "@solana/spl-token";  

const encryptPayload = (payload, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');
  const nonce = nacl.randomBytes(24);
  const encryptedPayload = nacl.box.after(Buffer.from(JSON.stringify(payload)), nonce, sharedSecret);
  return [nonce, encryptedPayload];
};

const decryptPayload = (data, nonce, sharedSecret) => {
  if (!sharedSecret) throw new Error('missing shared secret');
  const decryptedData = nacl.box.open.after(bs58.decode(data), bs58.decode(nonce), sharedSecret);
  if (!decryptedData) {
    throw new Error('Unable to decrypt data');
  }
  return JSON.parse(Buffer.from(decryptedData).toString('utf8'));
};

const onTransactionRedirectLink = 'scoby://phantom/signAndSendTransaction';
const buildUrl = (path, params) => `phantom://v1/${path}?${params.toString()}`;

let deepLinkingListener = null
let hasStarted = false


const GiveawayMockScreen = ({navigation, route}) => {
    const windowWidth = Dimensions.get('window').width;
    const styles = StyleSheet.create({
        container: {
            flex:1,
            flexDirection: 'column',
            justifyContent: 'space-between',
            position:'relative'
        },
        maincontainer: {
            flex:1,
            backgroundColor:'gray',
            justifyContent:'center',
            alignContent:'center',
            alignItems:'center'
        },
        giveAwayBtn: {
            width:50,
            height:50,
            borderRadius: 50,
            backgroundColor:Colors.pink,
            position:'absolute',
            bottom:10,
            left: (windowWidth - 50) / 2,
            justifyContent:'center',
            alignItems:'center'
        },
        giveawayFirstScreen: {
           flex:1,
           flexDirection: 'column',
           justifyContent:'center',
           alignContent:'center'
        },
        giveawayFirstScreenInner: {
           flexDirection:'row',
           justifyContent: 'center',
           alignContent:'center'
        },
        giveawayFirstScreenBtn1: {
            borderColor:'white',
            borderWidth:1,
            borderRadius: 5,
            padding: 10,
            marginRight: 10,
            fontFamily: `AvenirNext-Bold`,
            fontSize: 16
        },
        giveawayFirstScreenBtn2: {
            backgroundColor:Colors.pink,
            padding: 10,
            fontFamily: `AvenirNext-Bold`,
            fontSize: 16,
            marginLeft: 10,
            borderRadius: 5
        },
        giveawayDetailScreenDetail: {
            backgroundColor:Colors.violetColorSelfOpacity,
            padding: 20,
            width: (windowWidth - 100),
            borderRadius: 10,
            flex:1,
            flexDirection: 'row',
            position:'relative'
        },
        giveawayDetailScreenDetailLeft: {
            marginRight: 15
        },
        giveawayDetailScreenDetailLeftImg: {
            width: 80,
            height: 80,
            borderRadius: 40
        },
        giveawayDetailScreenDetailTitle: {
            fontFamily: `AvenirNext-Bold`,
            fontWeight: 'normal',
            fontSize: 16,
            color: Colors.white
        },
        giveawayDetailScreenDetailTag: {
            fontFamily: `AvenirNext-Regular`,
            fontWeight: 'normal',
            fontSize: 14,
            color: Colors.white
        },
        giveawayDetailScreenDetailDesc: {
            fontFamily: `AvenirNext-Regular`,
            fontSize: 16,
            color: Colors.white,
            marginTop:8,
        },
        giveawayDetailScreenDetailClose: {
            position:'absolute',
            right:0,
            top:10,
            width:32,
            height:32
        },
        giveawayConfirmationScreen: {
            flexDirection:'column',
            justifyContent: 'center',
            alignContent:'center',
            alignItems:'center'
        },
        giveawayConfirmationScreenInfo: {
            marginBottom: 20
        },
        giveawayConfirmationScreenAction: {
            flexDirection:'row'
        },
        giveawayConfirmationScreenInfoText: {
            fontFamily: `AvenirNext-Bold`,
            fontWeight: 'normal',
            fontSize: 24,
            color: Colors.white
        },
        giveawayDetailScreenDetailBtn: {
            backgroundColor:Colors.pink,
            padding: 10,
            fontFamily: `AvenirNext-Bold`,
            fontSize: 16,
            borderRadius: 5,
            marginTop: 8
        },
        giveawayWinnerScreenDetail: {
            backgroundColor:Colors.violetColorSelfOpacity,
            flexDirection: 'column',
            padding: 20,
            width: (windowWidth - 100),
            borderRadius: 10,
            position:'relative',
        },
        giveawayWinnerScreenDetailUser: {
            flexDirection: 'row',
            marginBottom: 15,
            width: (windowWidth - 140),
        },
        requirementItem: {
            flexDirection: 'row',
            marginTop: 8
        },
        requirementItemIcon: {
           marginTop: 4
        },
        requirementItemDesc: {
            marginLeft: 10
        },
        screenLoading: {
          backgroundColor: 'rgba(0, 0, 0, 0.75)',
          position:'absolute',
          left:0,
          right:0,
          top:0,
          bottom:0,
          justifyContent:'center'
        }
    })

    const [hasGiveAway,setHasGiveAway] = useState(false)
    const [showGiveawayModel,setShowGiveawayModel] = useState(false)
    const [showGiveawayDetailModel,setShowGiveawayDetailModel] = useState(false)
    const [showGiveawayConfirmationModel,setShowGiveawayConfirmationModel] = useState(false)
    const [showGiveawayWinnerModel,setShowGiveawayWinnerModel] = useState(false)
    const [giveawayDetails,setGiveawayDetails] = useState(null)
    const [winner,setWinner] = useState(null)
    const [isOwner,setIsOwner] = useState(false)
    const [isCompleted,setIsCompleted] = useState(false)
    const [isLoading,setIsLoading] = useState(false)
    const [requirements,setRequirement] = useState([])
    const {currentUserProfile} = useContext(FollowersContext);
    const [mintAddress,setMintAddress] = useState('')

    const [processGiveAway] = useMutation(PROCESS_GIVEAWAY, {
        variables: {
            giveAwayId: giveawayDetails ? giveawayDetails.id : 0,
            userId: winner ? winner.id : 0
        },
      });

    const [getEventDetail] = useLazyQuery(GET_EVENT_AND_GIVEAWAY, {
        pollInterval: 10000,
        fetchPolicy: 'network-only',
        variables: {
            idEvent: route.params.id,
        },onCompleted(data) {
            if(data.getEventAndGiveAway.giveAway) {
                setGiveawayDetails(data.getEventAndGiveAway.giveAway)
                setIsOwner(data.getEventAndGiveAway.giveAway.ownerUser.id === currentUserProfile.id)
                setHasGiveAway(true)
                if(data.getEventAndGiveAway.giveAway.userWinner) {
                    setIsCompleted(true)
                    setWinner(data.getEventAndGiveAway.giveAway.userWinner)
                }
            }
        }
     });

     const {getNFTByID} = useQuery(GET_NFT_ID, {
        variables: {
            id: giveawayDetails ? giveawayDetails.nfts[0] : 0
        },onCompleted({getSporeDsById}) {
          setMintAddress(getSporeDsById.contractAdress)
        },
      });


  const handleDeepLink = ({url}) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        const urlData = new URL(url);
        const params = urlData.searchParams;
        if (params.get('errorCode')) {
          return
        }
        processTransaction(params)
      }
    });
  };

  const processTransaction = async(params) => {
    if(!hasStarted) {
      return
    }
    hasStarted = false
    const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
    const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
    const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));

    const signTransactionData = decryptPayload(
      params.get("data"),
      params.get("nonce"),
      sharedSecretDapp
    );

    setShowGiveawayWinnerModel(false) 
    setIsLoading(true)
    await processGiveAway();
    setIsLoading(false)
    Alert.alert('You have completed giveaway successfully');
    setIsCompleted(true)

    
  }

    useEffect(() => {
        getEventDetail();
        if (deepLinkingListener) {
            deepLinkingListener.remove();
        }
        deepLinkingListener = Linking.addEventListener('url', handleDeepLink);
    }, []);

    const openGiveawayPopup = () => {
        setShowGiveawayModel(true)
    }

    const viewGiveawayAction = () => {
        setShowGiveawayModel(false)
        setShowGiveawayDetailModel(true)
    }

    const giveawayAction = () => {
        setShowGiveawayModel(false)
        setShowGiveawayConfirmationModel(true);
    }

    const confirmGiveawayAction = () => {
        setShowGiveawayConfirmationModel(false);
        if(giveawayDetails.suscribeUsers.length === 0) {
            Alert.alert('No user available in giveaway');
            return
        }
        const winnerObj = giveawayDetails.suscribeUsers[Math.floor(Math.random() * giveawayDetails.suscribeUsers.length)];
        setWinner(winnerObj)
        let requirmentArray = [];
        if(giveawayDetails.community.length > 0) {
            for (let index = 0; index < giveawayDetails.community.length; index++) {
                const element = giveawayDetails.community[index];
                const findIndex = element.participantUsers.findIndex((dataObj)=>{
                    return dataObj.id === winnerObj.id
                })
                if(findIndex === -1) {
                    requirmentArray.push({desc: "Member is not part of community " +element.name, status: false })
                } else {
                    requirmentArray.push({desc: "Member is  part of community " +element.name, status: true })
                }
            }
        }
        setRequirement(requirmentArray)
        setShowGiveawayWinnerModel(true)
    }

    const ignoreWinnerAction = () => {
        setShowGiveawayWinnerModel(false)
        var suscribeUsers = []
        for (let index = 0; index < giveawayDetails.suscribeUsers.length; index++) {
            const element = giveawayDetails.suscribeUsers[index];
            if(winner.id !== element.id) {
                suscribeUsers.push(element)
            }
        }
        giveawayDetails.suscribeUsers = suscribeUsers
    }
    const processGiveAwayAction = async() => {
        hasStarted = true

        const phantomWalletPublicKey = await AsyncStorage.getItem('public_key');
        const userWallet = new anchor.web3.PublicKey(phantomWalletPublicKey)
    
        const conn = new anchor.web3.Connection( anchor.web3.clusterApiUrl(CLUSTER));
        const randWallet = anchor.web3.Keypair.generate();
        const confirmOption = {commitment: 'finalized', preflightCommitment: 'finalized', skipPreflight: false};
        const provider = new anchor.AnchorProvider(conn, randWallet, confirmOption);
        const program = new anchor.Program(giveawayabi, GIVEAWAY_PROGRAM, provider)
        var transaction = new anchor.web3.Transaction();
        var msg_id= route.params.id.toString()

        if(giveawayDetails.giveawayType === "sol") {
            const [user_pda_sol, user_bump_sol] = await anchor.web3.PublicKey.findProgramAddress(
                [userWallet.toBuffer(),Buffer.from(msg_id)],
                GIVEAWAY_PROGRAM
              );

            if(await conn.getAccountInfo(user_pda_sol)==null) {
                Alert.alert("pda account doesnt exist");
                return
            }

            let pda_balance= await conn.getAccountInfo(user_pda_sol);
      
            const [user_pda_ac, user_bump] = await anchor.web3.PublicKey.findProgramAddress(
              [userWallet.toBuffer()],
              GIVEAWAY_PROGRAM
            );
        
            transaction.add(await program.methods.claimsol(user_bump_sol,msg_id)
            .accounts({
              userPdaAcct: user_pda_sol,
              sender: userWallet,
              winner : new anchor.web3.PublicKey(winner.publicKey),
              platform : userWallet,
              owner : user_pda_ac,
              systemProgram: anchor.web3.SystemProgram.programId,
            }).signers([])
            .instruction())
        } else {
            await getNFTByID();
            var tokenaccount = await getOrCreateAssociatedTokenAccount(
                conn,
                userWallet,
                mintAddress,
                userWallet,
            );
            var mint=new anchor.web3.PublicKey(mintAddress);
            var amount=giveawayDetails.nfts.length

            const [user_pda_state, bump1] = await anchor.web3.PublicKey.findProgramAddress(
                [userWallet.toBuffer(),tokenaccount.address.toBuffer(),Buffer.from(msg_id),Buffer.from("state")],
                GIVEAWAY_PROGRAM
              );
          
              const [usertokenpda, bump2] = await anchor.web3.PublicKey.findProgramAddress(
                [userWallet.toBuffer(),tokenaccount.address.toBuffer(),Buffer.from(msg_id)],
                GIVEAWAY_PROGRAM
              );
      
              const [user_pda_ac, user_bump] = await PublicKey.findProgramAddress(
                [owner_wallet.publicKey.toBuffer()],
                GIVEAWAY_PROGRAM
              );
      
              if(await conn.getAccountInfo(user_pda_ac) == null)
              {
                Alert.alert("pda account doesnt exist");
                return
              }

              let wallet_to_deposit_to = await getOrCreateAssociatedTokenAccount(
                conn,
                new anchor.web3.PublicKey(winner.publicKey),
                mint,
                new anchor.web3.PublicKey(winner.publicKey),
              );

              transaction.add(await program.methods.sendtokenwinner(bump1,bump2,
                new anchor.BN(amount),msg_id)
              .accounts({
                tokenpda: usertokenpda,
                statepda: user_pda_state,
                walletToDepositTo: wallet_to_deposit_to.address,
                sender: userWallet,
                depositTokenAccount : tokenaccount.address,
                reciever: new anchor.web3.PublicKey(winner.publicKey),
                platform: userWallet,
                owner : user_pda_ac,    
                systemProgram: anchor.web3.SystemProgram.programId,
                rent: anchor.web3.SYSVAR_RENT_PUBKEY,
                associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
                tokenProgram:TOKEN_PROGRAM_ID
              }).signers([])
              .instruction())
        }

        transaction.feePayer = userWallet;
        const anyTransaction = transaction;
        anyTransaction.recentBlockhash = (await conn.getLatestBlockhash()).blockhash;
    
        const serializedTransaction = anyTransaction.serialize({
          requireAllSignatures: false
        });
    
        const phantomPublicKey = await AsyncStorage.getItem('phantom_encryption_public_key');
        const dappSecretKey = await AsyncStorage.getItem('dapp_secret_key');
        const dappPublicKey = await AsyncStorage.getItem('dapp_public_key');
        const sharedSecretDapp = nacl.box.before(bs58.decode(phantomPublicKey), bs58.decode(dappSecretKey));
        const publicSession = await AsyncStorage.getItem('public_session');
    
        const payload = {
          session: publicSession,
          transaction: bs58.encode(serializedTransaction),
        };
        const [nonce, encryptedPayload] = encryptPayload(payload, sharedSecretDapp);
        const params = new URLSearchParams({
          dapp_encryption_public_key: dappPublicKey,
          nonce: bs58.encode(nonce),
          redirect_link: onTransactionRedirectLink,
          payload: bs58.encode(encryptedPayload)
        });
        const url = buildUrl('signAndSendTransaction', params);
        Linking.openURL(url);
    }

    return (
        <>
          <View style={styles.container}>
            <View style={styles.maincontainer}>
               <Text>Live</Text>
            </View>
            {hasGiveAway && (!showGiveawayModel && !showGiveawayDetailModel && !showGiveawayConfirmationModel && !showGiveawayWinnerModel) &&
                <View style={styles.giveAwayBtn}>
                    <TouchableOpacity  onPress={openGiveawayPopup}> 
                        <FontAwesome
                            name="gift"
                            color="white"
                            size={30}
                        />
                    </TouchableOpacity>
                </View>
            }

            {isLoading &&
              <View style={styles.screenLoading}>
                 <ActivityIndicator size="large" color="#ffffff"/>
              </View>
            }

          </View>
        <Modal
            animationType="slide"
            visible={showGiveawayModel}
            transparent={true}
            onRequestClose={() => {
            setShowGiveawayModel(!showGiveawayModel);
            }}
        >
            <View style={styles.giveawayFirstScreen}>

                <View style={styles.giveawayFirstScreenInner}>
                    <View style={styles.giveawayFirstScreenBtn1}>
                        <TouchableOpacity onPress={viewGiveawayAction}> 
                            <Text>View Giveaway</Text>
                        </TouchableOpacity>
                    </View>
                    {isOwner && !isCompleted && 
                        <View style={styles.giveawayFirstScreenBtn2}>
                            <TouchableOpacity onPress={giveawayAction}> 
                                <Text>Giveaway Now</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    
                    {isCompleted &&
                    <View style={styles.giveawayFirstScreenBtn2}>
                        <TouchableOpacity onPress={()=> {
                            setShowGiveawayModel(false)
                            setShowGiveawayWinnerModel(true)
                        }}> 
                            <Text>View Winner</Text>
                        </TouchableOpacity>
                    </View>
                    }

                </View>
            </View>
        </Modal>

        <Modal
            animationType="slide"
            visible={showGiveawayDetailModel}
            transparent={true}
            onRequestClose={() => {
            setShowGiveawayDetailModel(!showGiveawayDetailModel);
            }}
        >
            <View style={styles.giveawayFirstScreen}>
                <View style={styles.giveawayFirstScreenInner}>
                  {giveawayDetails &&
                    <View style={styles.giveawayDetailScreenDetail}>
                        <View style={styles.giveawayDetailScreenDetailLeft}>
                            <Image source={giveawayDetails.ownerUser.avatar ? {uri: giveawayDetails.ownerUser.avatar} : avatarSrc} style={styles.giveawayDetailScreenDetailLeftImg} />
                        </View>
                        <View style={styles.giveawayDetailScreenDetailRight}>
                            <Text style={styles.giveawayDetailScreenDetailTitle}>
                                {giveawayDetails.name}
                            </Text>
                            <Text style={styles.giveawayDetailScreenDetailDesc}>
                                {giveawayDetails.description}
                            </Text>
                        </View>
                        <View style={styles.giveawayDetailScreenDetailClose}>
                            <TouchableOpacity  onPress={()=>{
                                setShowGiveawayDetailModel(false);
                            }}> 
                                <FontAwesome
                                    name="times"
                                    color="white"
                                    size={20}
                                />
                            </TouchableOpacity>
                       </View>
                    </View>
                  }

                </View>
            </View>
        </Modal>

        <Modal
            animationType="slide"
            visible={showGiveawayConfirmationModel}
            transparent={true}
            onRequestClose={() => {
            setShowGiveawayConfirmationModel(!showGiveawayConfirmationModel);
            }}
        >
            <View style={styles.giveawayFirstScreen}>
                <View style={styles.giveawayConfirmationScreen}>
                    <View style={styles.giveawayConfirmationScreenInfo}>
                        <Text style={styles.giveawayConfirmationScreenInfoText}>
                            Giveaway Now!
                        </Text>
                    </View>
                    <View style={styles.giveawayConfirmationScreenAction}>
                        <View style={styles.giveawayFirstScreenBtn1}>
                            <TouchableOpacity onPress={()=>{
                                setShowGiveawayConfirmationModel(false);
                            }}> 
                                <Text>Not quiet Yet</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.giveawayFirstScreenBtn2}>
                            <TouchableOpacity onPress={confirmGiveawayAction}> 
                                <Text>Yes!</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </Modal>

        <Modal
            animationType="slide"
            visible={showGiveawayWinnerModel}
            transparent={true}
            onRequestClose={() => {
            setShowGiveawayWinnerModel(!showGiveawayWinnerModel);
            }}
        >
            <View style={styles.giveawayFirstScreen}>
                <View style={styles.giveawayFirstScreenInner}>
                  {winner &&
                    <View style={styles.giveawayWinnerScreenDetail}>
                       <View style={styles.giveawayWinnerScreenDetailUser}>
                        <View style={styles.giveawayDetailScreenDetailLeft}>
                            <Image source={winner.avatar ? {uri: winner.avatar} : avatarSrc} style={styles.giveawayDetailScreenDetailLeftImg} />
                        </View>
                        <View style={styles.giveawayDetailScreenDetailRight}>
                            <Text style={styles.giveawayDetailScreenDetailTitle}>
                                {winner.fullName}
                            </Text>
                            <Text style={styles.giveawayDetailScreenDetailTag}>
                                @{winner.username}
                            </Text>
                            {winner.bio &&
                                <Text style={[styles.giveawayDetailScreenDetailDesc,{width:windowWidth - 240}]}>
                                    {winner.bio}
                                </Text>
                            }
                            {!isCompleted && 
                                <View style={styles.giveawayDetailScreenDetailBtn}>
                                    <TouchableOpacity onPress={processGiveAwayAction}> 
                                        <Text>Give Away</Text>
                                    </TouchableOpacity>
                                </View>
                            }

                            {isCompleted && 
                                <View style={styles.giveawayDetailScreenDetailBtn}>
                                    <Text>Winner</Text>
                                </View>
                            }

                        </View>
                       </View>

                        <View style={styles.giveawayDetailScreenDetailClose}>
                            <TouchableOpacity  onPress={ignoreWinnerAction}> 
                                <FontAwesome
                                    name="times"
                                    color="white"
                                    size={20}
                                />
                            </TouchableOpacity>
                       </View>
                       {requirements.length > 0 &&
                        <View style={styles.giveawayWinnerScreenDetailReq}>
                        <View>
                        <Text style={styles.giveawayDetailScreenDetailTitle}>
                            List of requirements
                            </Text>
                            </View>
                            {requirements.map((requirement) => (
                                <View style={styles.requirementItem}>
                                    {requirement.status &&
                                        <FontAwesome
                                            name="check"
                                            color="white"
                                            size={12}
                                            style={styles.requirementItemIcon}
                                        />
                                    }

                                    {!requirement.status &&
                                        <FontAwesome
                                            name="times"
                                            color="white"
                                            size={12}
                                            style={styles.requirementItemIcon}
                                        />
                                    }

                                    <Text style={styles.requirementItemDesc}>{requirement.desc}</Text>
                                </View>
                            ))}
                        </View>
                       }

                    </View>
                  }

                </View>
            </View>
        </Modal>
        </>
    )
}

export default withSafeArea(GiveawayMockScreen);