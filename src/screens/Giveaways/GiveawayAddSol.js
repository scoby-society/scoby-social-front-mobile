import royality from 'assets/images/royality.png';
import wave from 'assets/images/wave.png';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Alert, SafeAreaView, StyleSheet, Text, TextInput, View, KeyboardAvoidingView, Platform} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NewLargeButton from 'src/components/NewLargeButton';
import withSafeArea from 'src/components/withSafeArea';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {TITLE_ADDSOL_GIVEAWAY} from 'src/constants/Texts';
import styled from 'styled-components/native';
import {Connection, PublicKey, clusterApiUrl} from '@solana/web3.js';
import {CLUSTER} from 'src/config/env';
import AsyncStorage from '@react-native-community/async-storage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  gridView: {
    marginTop: 5,
    flex: 1,
  },
  icon: {
    marginBottom: 30,
    marginLeft: 10,
  },
  input: {
    ...Fonts.avenir,
    padding: 10,
    color: Colors.white,
    backgroundColor: '#0B0944',
    borderRightWidth: 1,
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRadius: 10,
    borderColor: Colors.blueBackgroundSession,
    marginVertical: 5,
    width: 100,
    height: 50,
  },
  inputUSD: {
    ...Fonts.avenir,
    color: Colors.white,
    borderColor: Colors.whiteText,
    borderBottomWidth: 0.5,
    width: 80,
    height: 50,
  },
  usdContent: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 25,
  },
  header: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subHeading: {
    color: Colors.whiteText,
    ...Fonts.avenirBold,
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 15,
    marginRight: 30,
    marginTop: 35,
  },
  backbtncontainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: '5%',
  },
  skipText: {
    ...Fonts.avenir,
    color: Colors.white,
  },
});

const GiveawayAddSol = ({navigation, route}) => {
  const Details = route?.params;
  const [isDescription, setIsDescription] = useState(true);
  const [number, onChangeNumber] = React.useState(null);
  const [solInUsd, setSolInUsd] = useState();

  const cleanFields = () => {
    navigation.goBack();
  };

  useEffect(() => {
    let mounted = true;
    (async () => {
      try {
        const value = await axios.get('https://min-api.cryptocompare.com/data/price?fsym=SOL&tsyms=USD');
        if (value.data.USD && mounted) {
          setSolInUsd(value.data.USD);
        }
      } catch (error) {
        return error;
      }
    })();
    return () => {
      mounted = false;
    };
  }, []);

  const handleToNext = async () => {
    if (!number) {
      return Alert.alert('Please Enter Sol value');
    }

    const phantomWalletPublicKey = await AsyncStorage.getItem('public_key');
    if (!phantomWalletPublicKey) {
      return Alert.alert('Please connect your wallet');
    }

    const connection = new Connection(clusterApiUrl(CLUSTER));
    let balance = await connection.getBalance(new PublicKey(phantomWalletPublicKey));
    balance /= 1000000000;

    if (balance < number) {
      return Alert.alert("You don't have enough balance to create giveaway");
    }

    const GiveawayDetails = {
      EventId: Details.EventId,
      Name: Details.Name,
      Description: Details.Description,
      SelectNFTids: Details.SelectNFTids,
      nftdata: Details.nftdata,
      solValue: number,
      giveawayType: 'sol',
    };
    navigation.navigate('GiveawayCollection', GiveawayDetails);
  };

  const handleSkip = () => {
    const GiveawayDetails = {
      EventId: Details.EventId,
      Name: Details.Name,
      Description: Details.Description,
      SelectNFTids: Details.SelectNFTids,
      nftdata: Details.nftdata,
      solValue: '',
    };
    navigation.navigate('GiveawayCollection', GiveawayDetails);
  };

  const usdValue = number * solInUsd;

  return (
    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
      <View style={styles.container}>
        <Wrapper>
          <View>
            <View style={styles.backbtncontainer}>
              <Ionicons name="chevron-back" color="white" size={25} style={styles.icon} onPress={() => cleanFields()} />
              {!Details.SelectNFTids.length === 0 && (
                <Text style={styles.skipText} onPress={() => handleSkip()}>
                  Skip
                </Text>
              )}
            </View>

            <View
              style={{
                marginLeft: 30,
                flex: 1,
                flexDirection: 'column',
                marginRight: 20,
              }}>
              <View style={styles.header}>
                <TitleText>{TITLE_ADDSOL_GIVEAWAY}</TitleText>
                <View
                  style={{
                    flex: 0,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                  }}>
                  <AvatarImage source={royality} style={{resizeMode: 'cover'}} />
                  {isDescription ? (
                    <FontAwesome
                      name="angle-down"
                      color="white"
                      size={30}
                      style={styles.icon}
                      onPress={() => setIsDescription(!isDescription)}
                    />
                  ) : (
                    <FontAwesome
                      name="angle-up"
                      color="white"
                      size={30}
                      style={styles.icon}
                      onPress={() => setIsDescription(!isDescription)}
                    />
                  )}
                </View>
              </View>

              {isDescription && (
                <ContentText>
                  <Text>
                    Add SOL tokens to increase the value of your giveaway. NOTE: These will be locked until you complete
                    or cancel the giveway.
                  </Text>{' '}
                </ContentText>
              )}
              <View style={styles.inputContainer}>
                <View>
                  <Text style={{marginLeft: '33%'}}>SOL</Text>
                  <TextInput
                    style={styles.input}
                    onChangeText={(value) => {
                      const re = /^\d+$/;
                      if (value === '' || re.test(value)) {
                        onChangeNumber(value);
                      }
                    }}
                    value={number}
                    placeholder="SOL"
                    returnKeyType="done"
                    keyboardType="numeric"
                    placeholderTextColor={Colors.greySession}
                    maxLength={40}
                  />
                </View>

                <AvatarImage source={wave} style={{resizeMode: 'cover'}} />

                <View>
                  <Text />

                  <View style={styles.inputUSD}>
                    <View style={styles.usdContent}>
                      <Text>{usdValue.toFixed(2)}</Text>
                      <Text style={{marginLeft: 10, fontSize: 15}}>USD</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <ButtonContainer>
            <NewLargeButton title="Back" transparent onPress={() => cleanFields()} flex />

            <NewLargeButton
              flex
              title="Next"
              // loading={loading}
              // disabled={!(Name && Description) || loading}
              onPress={handleToNext}
            />
          </ButtonContainer>
        </Wrapper>
      </View>
    </KeyboardAvoidingView>
  );
};

export default withSafeArea(GiveawayAddSol);

const Wrapper = styled(SafeAreaView)({
  marginTop: '5%',
  flexDirection: 'column',
  backgroundColor: Colors.blueBackgroundSession,
  marginHorizontal: 10,
  flex: 1,
  justifyContent: 'space-between',
  height: '90%',
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 24,
  lineHeight: '32px',
  color: Colors.white,
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
});

const ContentText = styled.Text({
  ...Fonts.avenirSemiBold,
  fontSize: 14,
  paddingBottom: 16,
  color: Colors.white,

  // marginTop: -450,
});
const AvatarImage = styled.Image({
  opacity: 1,
  borderRadius: 30,
  marginTop: '17%',
});
