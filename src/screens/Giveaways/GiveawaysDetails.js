import {useLazyQuery} from '@apollo/client';
import React, {useEffect, useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NewLargeButton from 'src/components/NewLargeButton';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {TITLE_GIVEAWAY} from 'src/constants/Texts';
import {GET_TEAM} from 'src/graphql/queries/team';
import styled from 'styled-components/native';

const Wrapper = styled(SafeAreaView)({
  marginTop: '5%',
  flexDirection: 'column',
  backgroundColor: Colors.blueBackgroundSession,
  marginHorizontal: 30,
  flex: 1,
  justifyContent: 'space-between',
  height: '90%',
});

const TitleText = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  lineHeight: '32px',
  color: Colors.white,
});

const ButtonContainer = styled.View({
  flexDirection: 'row',
});

const MainContent = styled.View({
  flexDirection: 'column',
  marginTop: 30,
});

const GiveawaysDetails = ({navigation, route}) => {
  const [NameCounter, setNameCounter] = useState(0);
  const [Name, setName] = useState('');
  const [DescriptionCounter, setDescriptionCounter] = useState(0);
  const [Description, setDescription] = useState('');
  const [SecondScreenCounter, setSecondScreenCounter] = useState(0);
  const [SecondScreenLink, setSecondScreenLink] = useState('');

  const [fetchTeam, {data: teamData, loading}] = useLazyQuery(GET_TEAM, {
    onCompleted: ({getTeam}) => {
      const {name, description, linkWebsite} = getTeam || {};
      setName(name);
      setDescription(description);
      setSecondScreenLink(linkWebsite);
    },
  });

  const {teamId} = route?.params || {};

  useEffect(() => {
    if (teamId) {
      fetchTeam({
        variables: {
          teamId,
        },
      });
    }
  }, [fetchTeam, teamId]);

  const inputTextUpdate = (name, updateText, updateCounter, text, counter) => {
    const nameInput = name || '';
    updateText(nameInput);
    if (text && text.length > nameInput.length) {
      if (counter > 0) {
        updateCounter(name.length);
      }
    } else if (text && text.length < nameInput.length) {
      updateCounter(name.length);
    } else {
      updateCounter(0);
    }
  };

  const cleanFields = () => {
    setNameCounter(0);
    setName('');
    setDescriptionCounter(0);
    setDescription('');
    navigation.goBack();
  };

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
    },
    input: {
      ...Fonts.avenir,
      padding: 16,
      color: Colors.white,
      backgroundColor: Colors.transparent,
      borderRightWidth: 1,
      borderLeftWidth: 1,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderRadius: 10,
      borderColor: Colors.white,
      borderBottomWidth: 0.5,
      marginVertical: 5,
    },
    inputDescription: {
      ...Fonts.avenir,
      padding: 16,
      paddingTop: 16,
      minHeight: 96,
      color: Colors.white,
      backgroundColor: Colors.transparent,
      borderRadius: 10,
      borderRightWidth: 1,
      borderLeftWidth: 1,
      borderTopWidth: 1,
      borderBottomWidth: 1,
      borderColor: Colors.white,
      borderBottomWidth: StyleSheet.hairlineWidth,
      marginTop: 5,
    },
    textViewStyle: {
      borderBottomLeftRadius: 10,
      borderBottomRightRadius: 10,
      backgroundColor: Colors.white,
    },
    textStyle: {
      ...Fonts.avenir,
      color: Colors.greySession,
      textAlign: 'right',
      padding: 8,
    },
    label: {
      ...Fonts.avenir,
      color: Colors.white,
      textAlign: 'left',
      padding: 0,
      marginLeft: 5,
    },
    inputControl: {
      marginTop: 10,
    },
  });

  const handleToNext = () => {
    if (Name && Description) {
      navigation.navigate('SelectNftGiveaway', {Name, Description});
    } else {
      alert('Please Enter Fields');
    }
    // let {params} = route;
    // params = {...params, Description, Name, SecondScreenLink: '', experience: 'team', team: teamData?.getTeam};
    // navigation.navigate('SelectTopics', {
    //   ...params,
    // });
  };

  return (
    <>
      <Wrapper>
        <View>
          <Ionicons
            name="chevron-back"
            color="white"
            size={30}
            style={{marginBottom: 30, marginLeft: -13}}
            onPress={() => cleanFields()}
          />

          <TitleText>{TITLE_GIVEAWAY}</TitleText>
          <MainContent>
            <View style={styles.inputControl}>
              <Text style={styles.label}>Name</Text>
              <TextInput
                style={styles.input}
                onChangeText={(value) => setName(value)}
                value={Name}
                placeholder="Name"
                returnKeyType="done"
                placeholderTextColor={Colors.greySession}
                maxLength={40}
              />
            </View>
            <View style={styles.inputControl}>
              <Text style={styles.label}>Description</Text>
              <TextInput
                style={styles.inputDescription}
                onChangeText={(value) => setDescription(value)}
                value={Description}
                placeholder="Description"
                placeholderTextColor={Colors.greySession}
                maxLength={140}
                multiline
                returnKeyType="done"
                textAlignVertical="top"
                numberOfLines={5}
                blurOnSubmit
              />
            </View>
          </MainContent>
        </View>
        <ButtonContainer>
          <NewLargeButton title="Back" transparent onPress={() => cleanFields()} flex />

          <NewLargeButton
            flex
            title="Next"
            loading={loading}
            // disabled={!(Name && Description) || loading}
            onPress={handleToNext}
          />
        </ButtonContainer>
      </Wrapper>
    </>
  );
};

export default withSafeAreaWithoutMenu(GiveawaysDetails);
