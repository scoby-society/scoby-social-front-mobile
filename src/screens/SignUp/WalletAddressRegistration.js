import React, {useState, useEffect, useCallback, useContext} from 'react';
import {KeyboardAvoidingView, StyleSheet, Image, Dimensions, Alert, Platform} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import fonts from 'src/constants/Fonts';
import {
  WALLET_ADDRESS_REGISTRATION_TEXT,
  WALLET_ADDRESS_REGISTRATION_HEADER,
  WALLET_ADDRESS_REGISTRATION_PLATFORM_HEADER,
  WALLET_PATRON,
  ACCOUNT_CREATED,
} from 'src/constants/Texts';
import Input from 'src/components/Input';
import NewLargeButton from 'src/components/NewLargeButton';
import BackButton from 'src/components/BackButton';
import logo from 'assets/images/logos/Scoby_Final_Logos/ScobyDude_preferred_logo/scoby_dude.png';
import SkipButton from 'src/components/SkipButton';
import AppLink from 'react-native-app-link';

import {useLazyQuery, useMutation} from '@apollo/client';
import {CREATE_USER_PROFILE} from 'src/graphql/mutations/auth';
import AsyncStorage from '@react-native-community/async-storage';

import {GET_USER_PROFILE} from 'src/graphql/queries/profile';
import {SEND_FCM_TOKEN} from 'src/graphql/mutations/fcm';
import {GlobalContext} from 'src/containers/global';
import {requestDeviceId, requestUserPermission} from 'src/utils/permission/notifications';
import {kitty} from 'src/config/api';

const {height, width} = Dimensions.get('window');

const Wrapper = styled(KeyboardAwareScrollView)({
  backgroundColor: colors.blueBackgroundSession,
  paddingTop: 44,
});
const LogoContainer = styled.View({
  alignItems: 'center',
  marginTop: 30,
});
const HeaderText = styled.Text({
  ...fonts.goudy,
  paddingTop: 30,
  paddingBottom: 8,
  color: colors.white,
  fontSize: 28,
  lineHeight: '32px',
});

const DescriptionText = styled.Text({
  ...fonts.avenir,
  fontSize: 14,
  color: colors.white,
  justifyContent: 'center',
});

const PlatformWrapper = styled.View({
  flex: 1,
  minHeight: 130,
  maxHeight: 'auto',
  paddingTop: 10,
  paddingBottom: 10,
  borderRadius: 10,
  paddingLeft: 50,
  paddingRight: 30,
  justifyContent: 'center',
  marginTop: 39,
  marginBottom: 73,
  backgroundColor: '#131142',
  shadowColor: '#131142',
  shadowOpacity: 10,
  shadowRadius: 0,
  elevation: '10px',
});

const ButtonsWrapper = styled.View({
  flexDirection: 'row',
  justifyContent: 'center',
  marginTop: 10,
});

const ButtonWrapper = styled.View({
  alignItems: 'center',
});

const OptionButton = styled.Pressable({
  width: 150,
  marginBottom: 5,
  height: 34,
  backgroundColor: '#360F7C',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 7,
});

const styles = StyleSheet.create({
  logo: {
    width: width * 0.2,
    height: height * 0.2,
    resizeMode: 'contain',
  },
  content: {
    padding: 24,
  },
  text: {
    fontWeight: 'bold',
  },
  container: {
    backgroundColor: 'red',
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
  },
});

const IOS_PHANTOM_LINK = 'https://apps.apple.com/us/app/phantom-solana-wallet/id1598432977';
const PLAY_STORE_SOLFARE = 'market://details?id=com.solflare.mobile&hl=es_CL&gl=US';
const IOS_GLOW_LINK = 'https://apps.apple.com/uy/app/glow-solana-wallet/id1599584512?l=es';
const IOS_SOLFLARE_LINK = 'https://apps.apple.com/us/app/solflare/id1580902717';

function WalletAddressRegistration({navigation}) {
  const [address, setAddress] = useState('');
  const [addressError, setAddressError] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [profile, setProfile] = useState(null);

  const [addPushToken] = useMutation(SEND_FCM_TOKEN);
  const {setIsLogged} = useContext(GlobalContext);

  const {topics} = useContext(GlobalContext);

  useEffect(() => {
    const getdata = async () => {
      try {
        const value = await AsyncStorage.getItem('profile');

        if (value) {
          const v = JSON.parse(value);

          v.map((f) => {
            setProfile(f);
            return true;
          });
        }
      } catch (error) {
        // Error retrieving data
      }
    };

    getdata();
  }, []);

  const [loadProfile] = useLazyQuery(
    GET_USER_PROFILE,
    {fetchPolicy: 'cache-and-network'},
    {
      onError: () => Alert.alert('Cannot load profile'),
    },
  );

  const sendFcmToken = useCallback(async () => {
    const token = await requestUserPermission();
    const deviceId = await requestDeviceId();
    if (token) {
      await addPushToken({
        variables: {
          deviceId,
          token,
        },
      });
    }
  }, [addPushToken]);

  const [createUserProfile] = useMutation(CREATE_USER_PROFILE, {
    onError(e) {
      // setUserNameError(false);
      // setPasswordError(false);
      if (e.networkError) {
        Alert.alert('Something went wrong. Please try again later', e.message);
      }
      if (e.graphQLErrors && e.graphQLErrors.length > 0) {
        if (e.graphQLErrors[0].extensions && e.graphQLErrors[0].extensions.code === 'ERR_USERNAME_EXISTS') {
          //  setUserNameError(e.message);
          Alert.alert('Something went wrong. Please try again later', e.message);
        } else if (e.graphQLErrors[0].extensions && e.graphQLErrors[0].extensions.code === 'ERR_VALIDATION_FAILED') {
          if (e.graphQLErrors[0].extensions.details[0].path[0] === 'password') {
            // setPasswordError(e.message);
            Alert.alert('Something went wrong. Please try again later', e.message);
          }
          if (e.graphQLErrors[0].extensions.details[0].path[0] === 'username') {
            //     setUserNameError(e.message);
            Alert.alert('Something went wrong. Please try again later', e.message);
          }
        } else {
          //        setPasswordError(e.message);
          //        setUserNameError(e.message);
          Alert.alert('Something went wrong. Please try again later', e.message);
        }
      }
    },
    onCompleted: async ({createUserProfile: userData}) => {
      Alert.alert(ACCOUNT_CREATED);
      await AsyncStorage.setItem('token', userData.auth?.authorizationToken);
      await sendFcmToken();
      await loadProfile({
        onCompleted: async (e) => {
          await kitty.endSession();
          await kitty
            .startSession({
              username: e.getUserProfile.id,
              authParams: {},
            })
            .then(() => {
              setIsLogged(true);
              navigation.reset({
                index: 0,
                routes: [{name: 'MainTabs', screen: 'Profile'}],
              });
            });
        },
      });
    },
  });

  const handleWalletChange = useCallback(async (e) => {
    const invalid = WALLET_PATRON.test(e);

    setAddress(e);
    if (e.length < 32 || e.length > 50 || invalid) {
      setAddressError('Sorry, that is not a valid Solana wallet addres');

      setDisabled(true);
    } else {
      setAddressError(false);
      setDisabled(false);
    }
  }, []);

  const toPlayStore = () => {
    AppLink.maybeOpenURL(PLAY_STORE_SOLFARE, {})
      .then(() => {
        navigation.replace('WalletRegistration');
      })
      .catch((err) => err);
  };

  const toIosStore = (option) => {
    if (option === 'phantom') {
      AppLink.maybeOpenURL(IOS_PHANTOM_LINK, {})
        .then(() => {
          navigation.replace('WalletRegistration');
        })
        .catch((err) => err);
    } else if (option === 'solflare') {
      AppLink.maybeOpenURL(IOS_SOLFLARE_LINK, {})
        .then(() => {
          navigation.replace('WalletRegistration');
        })
        .catch((err) => err);
    } else {
      AppLink.maybeOpenURL(IOS_GLOW_LINK, {})
        .then(() => {
          navigation.replace('WalletRegistration');
        })
        .catch((err) => err);
    }
  };

  const handleSave = useCallback(
    // eslint-disable-next-line  no-shadow
    (topics, publicKey) => {
      if (publicKey !== undefined) {
        profile.publicKey = publicKey;
      } else {
        profile.publicKey = null;
      }

      createUserProfile({variables: {profile: {...profile, topics}}});
    },
    [createUserProfile, profile],
  );
  const createAccount = () => {
    setDisabled(true);
    handleSave(topics, address);
    setAddress('');
  };

  const skip = () => {
    handleSave(topics, undefined);
  };

  return (
    <Wrapper keyboardShouldPersistTaps="handled" contentContainerStyle={styles.content}>
      <BackButton onPress={navigation.goBack} />
      <SkipButton skip={skip} />

      <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={24}>
        <LogoContainer>
          <Image source={logo} style={styles.logo} />
        </LogoContainer>
        <HeaderText> {WALLET_ADDRESS_REGISTRATION_HEADER}</HeaderText>
        <DescriptionText>{WALLET_ADDRESS_REGISTRATION_TEXT}</DescriptionText>
        <Input onChangeText={(e) => handleWalletChange(e)} isError={addressError} value={address} maxLength={50} />
        <PlatformWrapper>
          <DescriptionText>{WALLET_ADDRESS_REGISTRATION_PLATFORM_HEADER}</DescriptionText>

          <ButtonsWrapper>
            {Platform.OS === 'android' ? (
              <ButtonWrapper>
                <OptionButton onPress={toPlayStore}>
                  <DescriptionText style={styles.text}>Solflare</DescriptionText>
                </OptionButton>
              </ButtonWrapper>
            ) : (
              <ButtonWrapper>
                <OptionButton onPress={() => toIosStore('glow')}>
                  <DescriptionText style={styles.text}>Glow</DescriptionText>
                </OptionButton>
                <OptionButton onPress={() => toIosStore('solflare')}>
                  <DescriptionText style={styles.text}>Solflare</DescriptionText>
                </OptionButton>
                <OptionButton onPress={() => toIosStore('phantom')}>
                  <DescriptionText style={styles.text}>Phantom</DescriptionText>
                </OptionButton>
              </ButtonWrapper>
            )}
          </ButtonsWrapper>
        </PlatformWrapper>

        <NewLargeButton
          medium
          large
          disabled={disabled}
          // loading={loading}
          active={false}
          onPress={() => createAccount()}
          title="CREATE ACCOUNT"
        />
      </KeyboardAvoidingView>
    </Wrapper>
  );
}

export default WalletAddressRegistration;
