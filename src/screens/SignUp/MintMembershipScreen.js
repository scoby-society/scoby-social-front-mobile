/* eslint-disable max-len */
import * as React from 'react';
import {Alert, ActivityIndicator, View, Animated, Dimensions} from 'react-native';
import rnTextSize from 'react-native-text-size';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

import membershipImage from 'assets/images/membershipTest.png';
import membershipCardImage from 'assets/images/membershipCardTest.png';

import {MINT_MEMBERSHIP_TITLE, MINT_MEMBERSHIP_SUBTITLE, MISSING_WALLET} from 'src/constants/Texts';
import {GlobalContext} from 'src/containers/global';
import {useGetNftFromWallet} from 'src/utils/hook/getNftFromWallet';
import Colors from 'src/constants/Colors';

import {OpenArrowIco, CloseArrowIco} from 'assets/svg';

import MintResultMessage from 'src/components/MintResultMessage';
import OnBoardingWrapper from './components/OnBoardingWrapper';

const MembershipContainer = styled.View({
  width: '100%',
  marginTop: 30,
  paddingVertical: 15,
  borderRadius: 40,
  borderColor: 'rgba(255, 255, 255, 0.23)',
  backgroundColor: '#15205C',
  boxShadow: '1px 0px 14px rgba(0, 0, 0, 0.11)',
});

const MembershipImage = styled.Image({
  alignSelf: 'center',
  resizeMode: 'contain',
  borderRadius: 30,
});

const MembershipCard = styled.View({
  width: '90%',
  display: 'flex',
  flexDirection: 'row',
});

const TitleContainer = styled.View({
  width: '100%',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
  marginLeft: 40,
});

const MintMeButton = styled.TouchableOpacity({
  marginHorizontal: 20,
  marginTop: 20,
  marginBottom: 10,
  paddingVertical: 5,
  backgroundColor: '#EC008C',
  borderRadius: 6,
  display: 'flex',
  alignItems: 'center',
});

const MembershipCardWrapper = styled.View({
  maxHeight: '50%',
});

const StyledText = styled.Text`
  font-weight: ${(props) => (props.bold ? 800 : 'normal')};
  margin-vertical: ${(props) => (props.marginVertical ? props.marginVertical : 0)};
  margin-horizontal: ${(props) => (props.marginHorizontal ? props.marginHorizontal : 0)};
  color: #fff;
  text-align: ${(props) => (props.center ? 'center' : 'left')};
  font-size: ${(props) => props.size};
  text-decoration: ${(props) => (props.link ? 'underline #FFF' : null)};
  align-self: ${(props) => (props.link || props.selfStart ? 'flex-start' : 'center')};
`;

const MembershipImageWrapper = styled.View({
  width: '50%',
  margin: 10,
  backgroundColor: 'rgba(255, 255, 255, 0.08)',
  border: '2px solid rgba(255, 255, 255, 0.01)',
  borderRadius: 10,
});

const MembershipCardImage = styled.Image({
  marginHorizontal: 10,
  marginTop: 20,
  resizeMode: 'contain',
  borderRadius: 15,
});

const MembershipCardDescription = styled.View({
  width: '50%',
  margin: 5,
  display: 'flex',
});

const CardFooter = styled.View({
  marginTop: 20,
  marginBottom: 10,
});

const RoyaltiesButton = styled.TouchableOpacity({
  flexDirection: 'row',
  alignItems: 'center',
});

const MintMembershipScreen = ({navigation}) => {
  const startingHeight = 0;
  const startingOpacity = 1;
  const [fullHeight, setFullHeight] = React.useState(startingHeight);
  const [expanded, setExpanded] = React.useState(false);
  const {host, walletKey} = React.useContext(GlobalContext);
  const {
    mintMembership,
    loadingNft: nftsLoading,
    mintingInProgress,
    mintResult,
    resetMintResult,
  } = useGetNftFromWallet(host.current);
  const animatedHeight = React.useRef(new Animated.Value(startingHeight)).current;
  const animatedOpacity = React.useRef(new Animated.Value(startingOpacity)).current;

  const skip = () => {
    navigation.navigate('Sharing');
  };

  const measureTextHeight = async () => {
    const text =
      "Minting royalties for this Membership Pass will be shared 50% to Moto, 20% to Scoby 15% to Dina Miller (your host), 4% to John Freed (Deena's host) and 1% to Jen Williams (Pete's host).\n Trading Royalties for this Membership Pass will be shared 80% to you, 10% to Moto, 5% to Scoby, 3% to Dina Miller (your host) and 2% to John Freed (Dina's host).";

    const width = Dimensions.get('window').width * 0.7;

    const size = await rnTextSize.measure({
      text,
      width,
    });

    setFullHeight(size.height + 10);
  };

  const mint = () => {
    if (!walletKey) {
      Alert.alert(MISSING_WALLET);
      return;
    }

    resetMintResult();
    mintMembership();
  };

  React.useEffect(() => {
    measureTextHeight();
  }, []);

  React.useEffect(() => {
    // expanded?setText(props.text): setText(props.text.substring(0, 40));
    Animated.spring(animatedHeight, {
      friction: 100,
      toValue: expanded ? fullHeight : startingHeight,
      useNativeDriver: false,
    }).start();
    Animated.spring(animatedOpacity, {
      friction: 100,
      toValue: expanded ? 1 : 0,
      useNativeDriver: false,
    }).start();
  }, [expanded]);

  return (
    <OnBoardingWrapper
      navigation={navigation}
      skipStep={skip}
      title={MINT_MEMBERSHIP_TITLE}
      subTitle={MINT_MEMBERSHIP_SUBTITLE}
      currStep={6}>
      <View>
        <MintResultMessage result={mintResult} close={resetMintResult} />
        <MembershipContainer>
          <MembershipImage source={membershipImage} />
          <TitleContainer>
            <StyledText size={16} marginVertical={20} bold selfStart>
              Mapshifting
            </StyledText>
            <StyledText size={10} marginVertical={10}>
              A field guide to your sovereign soul.
            </StyledText>
          </TitleContainer>
          <MembershipCardWrapper>
            <LinearGradient
              style={{
                margin: 20,
                borderRadius: 14,
              }}
              colors={['#D858BC', '#3C00FF']}>
              <MembershipCard>
                <MembershipImageWrapper>
                  <MembershipCardImage source={membershipCardImage} />
                  <StyledText size={8} marginHorizontal={15} marginVertical={10} link>
                    www.mapshifting.com
                  </StyledText>
                  <CardFooter>
                    <RoyaltiesButton onPress={() => setExpanded(!expanded)}>
                      <StyledText size={12} marginHorizontal={10} bold>
                        👑 Royalties
                      </StyledText>
                      {expanded ? <CloseArrowIco /> : <OpenArrowIco />}
                    </RoyaltiesButton>
                  </CardFooter>
                </MembershipImageWrapper>
                <MembershipCardDescription>
                  <StyledText size={12} marginVertical={10} center bold>
                    Wholesome Activation
                  </StyledText>
                  <StyledText size={10} marginVertical={5} numberOfLines={5}>
                    Let's Activate! Scoby Social is Launching the Wholesome Activation campaing on the eye of our birth,
                    July 23, 2022, at a second before...
                  </StyledText>
                  <MintMeButton onPress={mint} disabled={nftsLoading || mintingInProgress}>
                    <StyledText size={12} bold>
                      {mintingInProgress || nftsLoading ? (
                        <ActivityIndicator size="small" color={Colors.white} />
                      ) : (
                        'Mint Me!'
                      )}
                    </StyledText>
                  </MintMeButton>
                  <StyledText size={10} bold>
                    Mint Price
                  </StyledText>
                  <StyledText size={10} bold>
                    10 SOL
                  </StyledText>
                </MembershipCardDescription>
              </MembershipCard>
              <Animated.View style={{height: animatedHeight, opacity: animatedOpacity}}>
                <View style={{padding: 15}}>
                  <StyledText size={12}>
                    {
                      "Minting royalties for this Membership Pass will be shared 50% to Moto, 20% to Scoby 15% to Dina Miller (your host), 4% to John Freed (Deena's host) and 1% to Jen Williams (Pete's host).\n\n Trading Royalties for this Membership Pass will be shared 80% to you, 10% to Moto, 5% to Scoby, 3% to Dina Miller (your host) and 2% to John Freed (Dina's host)."
                    }
                  </StyledText>
                </View>
              </Animated.View>
            </LinearGradient>
          </MembershipCardWrapper>
        </MembershipContainer>
      </View>
    </OnBoardingWrapper>
  );
};

export default MintMembershipScreen;
