import * as React from 'react';
import styled from 'styled-components/native';

import {HostSearchIco} from 'assets/svg';

import Colors from 'src/constants/Colors';

const SearchContainer = styled.View({
  margin: '10px 30px',
  padding: '5px 20px',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'rgba(255, 255, 255, 0.06)',
  // border: '1px solid rgba(255, 255, 255, 0.35)',
  borderRadius: 30,
});

const SearchInput = styled.TextInput({
  flex: 1,
  paddingTop: 10,
  paddingRight: 10,
  paddingBottom: 10,
  paddingLeft: 0,
  marginLeft: 20,
  color: Colors.white
});

const HostSearchInput = ({handleTextChange}) => {
  return (
    <SearchContainer>
      <HostSearchIco />
      <SearchInput placeholder="Search" onChangeText={handleTextChange} />
    </SearchContainer>
  );
};

export default HostSearchInput;
