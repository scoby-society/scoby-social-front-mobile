import * as React from 'react';
import {FlatList, Dimensions} from 'react-native';

import Svg, {Path} from 'react-native-svg';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

import logo from 'assets/images/logos/Scoby_Final_Logos/ScobyDude_preferred_logo/scoby_dude.png';

import {currencyFormatter} from 'src/utils/helpers';
import {getSolanaPrice} from 'src/utils/solanaUtils';

const HostCardWrapper = styled.View({
  minWidth: '90%',
  marginVertical: 15,
  borderRadius: 30,
  borderColor: 'rgba(255, 255, 255, 0.23)',
  display: 'flex',
  backgroundColor: '#0A0844',
  boxShadow: '1px 0px 14px rgba(0, 0, 0, 0.11)',
});

const HeaderCardWrapper = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: ${(props) => (props.hasHost ? 'space-between' : 'flex-end')};
`;

const Button = styled.TouchableOpacity({});

const HostProfileWrapper = styled.View({
  maxWidth: '100%',
  paddingHorizontal: 10,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-start',
  alignItems: 'center',
});

const HostImage = styled.Image({
  width: 70,
  height: 70,
  marginRight: 10,
  borderRadius: 50,
});

const HostDescriptionWrapper = styled.View({
  maxWidth: '70%',
  display: 'flex',
});

const setOpacity = (props) => {
  if (props.username) return '0.8';
  if (!props.title) return '0.7';
  return '1.0';
};

const HostDescriptionText = styled.Text`
  font-size: ${(props) => (props.title ? '16px' : '12px')};
  opacity: ${(props) => setOpacity(props)};
  color: #fff;
  margin-bottom: ${(props) => (props.username ? '5px' : 0)};
`;

const CardFooterWrapper = styled.View({
  width: '100%',
  marginVertical: 15,
  display: 'flex',
  justifyContent: 'flex-start',
});

const FooterWrapper = styled.View({
  maxWidth: '50%',
  marginTop: 5,
});

const StyledText = styled.Text`
  color: #FFF;
  font-weight: ${(props) => (props.bold ? 800 : 'normal')}
  font-size: ${(props) => (props.button ? '16px' : '12px')}
`;

const SVGWrapper = styled.View({
  alignSelf: 'flex-start',
});

const IconTextView = styled.View({
  height: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

const TextWrapper = styled.View({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
});

const RoyaltiesText = styled.Text`
  color: #fff;
  margin-bottom: ${(props) => (props.title ? 10 : 0)};
  font-weight: ${(props) => (props.bold ? 700 : 'normal')};
  text-align: center;
`;

const CurrencyText = styled.Text({
  color: '#FFF',
  fontWeight: 800,
});

const HostsList = ({hosts, selectHost, selectedHost, loading, isFirstHost, followHost}) => {
  const [solanaPrice, setSolanaPrice] = React.useState(0);

  const getSOLPrice = async () => {
    const price = await getSolanaPrice();
    setSolanaPrice(price);
  };

  const screenHeight = Dimensions.get('window').height;

  React.useEffect(() => {
    getSOLPrice();

    return () => {};
  }, []);

  const setFollow = (host) => {
    if (host.following) return 'Following';
    if (isFirstHost) return 'Follow';
    return 'Choose Host';
  };

  // eslint-disable-next-line max-len
  const lourem = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.`;

  return (
    <FlatList
      style={{maxHeight: screenHeight / 2}}
      data={hosts}
      renderItem={({item: host}) => (
        <HostCardWrapper>
          <HeaderCardWrapper hasHost={host.id === selectedHost}>
            {host.id === selectedHost && (
              <SVGWrapper>
                <Svg width="110" height="25" viewBox="0 0 110 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <Path
                    // eslint-disable-next-line max-len
                    d="M0 24.9997H109.207L96.707 12.5L109.207 0H67.207H45.707H35.207H29.707H27.207H25.207H24.707C8.20703 3 0.707031 16.9997 0 24.9997Z"
                    fill="#6E67F6"
                    fill-opacity="0.66"
                  />
                  <IconTextView>
                    <StyledText button bold>
                      Your Host
                    </StyledText>
                  </IconTextView>
                </Svg>
              </SVGWrapper>
            )}
            <LinearGradient
              style={{
                padding: 8,
                marginTop: 15,
                marginRight: 20,
                borderRadius: 15,
                alignSelf: 'flex-end',
              }}
              colors={['#6E67F6', '#8782EB']}
              start={{y: 0.0, x: 0.0}}
              end={{x: 1.0, y: 0.0}}>
              <Button
                disabled={loading || host.id === selectedHost || host.following}
                onPress={() => (isFirstHost ? followHost({variables: {userId: host.id}}) : selectHost(host.id))}>
                <StyledText bold button>
                  {setFollow(host)}
                </StyledText>
              </Button>
            </LinearGradient>
          </HeaderCardWrapper>
          <HostProfileWrapper>
            <HostImage source={host.avatar ? {uri: host.avatar} : logo} />
            <HostDescriptionWrapper>
              <HostDescriptionText title>{host.fullName}</HostDescriptionText>
              <HostDescriptionText username>{`@${host.username}`}</HostDescriptionText>
              <HostDescriptionText numberOfLines={3}>{host.bio || lourem}</HostDescriptionText>
            </HostDescriptionWrapper>
          </HostProfileWrapper>
          <CardFooterWrapper>
            <FooterWrapper>
              <RoyaltiesText title bold>
                Royalties Earned
              </RoyaltiesText>
              <TextWrapper>
                <RoyaltiesText>{host.totalRoyalties} </RoyaltiesText>
                <CurrencyText>SOL</CurrencyText>
              </TextWrapper>
              <TextWrapper>
                <RoyaltiesText>{currencyFormatter(host.totalRoyalties * solanaPrice) || 0.0} </RoyaltiesText>
                <CurrencyText>USD</CurrencyText>
              </TextWrapper>
            </FooterWrapper>
          </CardFooterWrapper>
        </HostCardWrapper>
      )}
      keyExtractor={(item) => item.id}
    />
  );
};

export default HostsList;
