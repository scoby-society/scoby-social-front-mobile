import * as React from 'react';
import {View, Text, ScrollView, Dimensions} from 'react-native';
import styled from 'styled-components/native';

import BackButton from 'src/components/BackButton';
import Steps from 'src/components/Steps';
import SkipButton from 'src/components/SkipButton';

import colors from 'src/constants/Colors';

const Container = styled.View({
  display: 'flex',
  alignItems: 'center',
  padding: '20px 5px',
});

const Wrapper = styled.ScrollView`
  height: ${(props) => props.height}px;
  margin-top: 40px;
  background-color: ${colors.blueBackgroundSession};
`;

const HeaderWrapper = styled.View({
  marginHorizontal: 12,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const BodyContainer = styled.View({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

const StyledText = styled.Text`
  font-weight: ${(props) => (props.title ? 900 : 'normal')};
  font-size: ${(props) => (props.title ? '24px' : '16px')};
  padding-left: ${(props) => (props.title ? '16px' : 0)};
  align-self: ${(props) => (props.title ? 'flex-start' : 'auto')};
  margin-bottom: ${(props) => (props.title ? '24px' : 0)};
  margin-right: ${(props) => (props.margin ? '15px' : 0)}
  color: ${colors.white};
`;

const TextWrapper = styled.View`
  width: ${(props) => (props.indicator ? '20%' : '80%')};
  display: flex;
  align-items: ${(props) => (props.indicator ? 'flex-end' : 'flex-start')};
`;

const TextContainer = styled.View({
  paddingHorizontal: '16px',
  display: 'flex',
  flexDirection: 'row',
});

const BotButtonContainer = styled.View({
  height: 50,
  justifyContent: 'space-evenly',
  paddingHorizontal: 40,
});

const Row = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
});

const BackButtonsecond = styled.TouchableOpacity({
  width: 107,
  height: 42,
  marginRight: 10,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 8,
});

const NextButton = styled.TouchableOpacity({
  width: 107,
  height: 42,
  background: '#CD068E',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 8,
  marginLeft: 10,
});

const OnBoardingWrapper = ({
  disabledButton,
  children,
  navigation,
  skipStep,
  title,
  subTitle,
  currStep,
  notSkippable,
}) => {
  const goBack = () => {
    navigation.goBack();
  };

  const screenHeight = Dimensions.get('window').height;

  return (
    <Wrapper contentContainerStyle={{flexGrow: 1}} height={screenHeight}>
      <View style={{padding: 0, margin: 0}}>
        <HeaderWrapper>
          <BackButton navigation={navigation} />
          {!notSkippable && <SkipButton skip={skipStep} />}
        </HeaderWrapper>
        <Container>
          <BodyContainer>
            <StyledText title>{title}</StyledText>
            <TextContainer>
              {subTitle && (
                <TextWrapper>
                  <StyledText>{subTitle}</StyledText>
                </TextWrapper>
              )}
              <TextWrapper indicator>
                <Steps FirstValue={currStep} SecondValue={7} />
              </TextWrapper>
            </TextContainer>
            {children}
          </BodyContainer>
        </Container>
      </View>
      <View>
        <BotButtonContainer>
          <Row>
            <BackButtonsecond onPress={goBack}>
              <Text style={{color: colors.white}}>Back</Text>
            </BackButtonsecond>
            <NextButton disabled={disabledButton} onPress={skipStep}>
              <Text style={{color: disabledButton ? 'rgba(0, 0, 0, 0.3)' : colors.white}}>Next</Text>
            </NextButton>
          </Row>
        </BotButtonContainer>
      </View>
    </Wrapper>
  );
};

export default OnBoardingWrapper;
