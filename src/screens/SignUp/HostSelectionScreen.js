import * as React from 'react';
import {ActivityIndicator, View} from 'react-native';

import {useLazyQuery, useMutation} from '@apollo/client';

import {GET_HOST_USER} from 'src/graphql/queries/auth';
import {CREATE_USER_REFERRED} from 'src/graphql/mutations/auth';
import {FOLLOW_USER} from 'src/graphql/mutations/followers';
import {HOST_TITLE, HOST_TEXT, YOUR_HOST_TITLE, YOUR_HOST_TEXT} from 'src/constants/Texts';
import {GlobalContext} from 'src/containers/global';

import HostSearchInput from './components/HostSearchInput';
import HostsList from './components/HostsList';
import OnBoardingWrapper from './components/OnBoardingWrapper';

const HostSelectionScreen = ({navigation}) => {
  const {referredId, host: hostData} = React.useContext(GlobalContext);
  const [loading, setLoading] = React.useState(true);
  const [isFirstHost, setIsFirstHost] = React.useState(false);
  const [hosts, setHosts] = React.useState([]);
  const [filteredHosts, setFilteredHosts] = React.useState([]);
  const selectedReferred = React.useRef(null);
  const [selectedHost, setSelectedHost] = React.useState(null);
  const [setHost, {loading: mutationLoading}] = useMutation(CREATE_USER_REFERRED, {
    notifyOnNetworkStatusChange: true,
    onCompleted() {
      setSelectedHost(selectedReferred.current);
      const host = filteredHosts.find((val) => val.id === selectedReferred.current);
      hostData.current = host;
    },
  });

  const [followUser, {loading: followLoading}] = useMutation(FOLLOW_USER, {
    notifyOnNetworkStatusChange: true,
    onCompleted() {
      setFilteredHosts((prevHost) => {
        prevHost[0].following = true;
        return prevHost;
      });
    },
  });

  const [loadHosts] = useLazyQuery(GET_HOST_USER, {
    variables: {id: Number(referredId)},
    onCompleted(response) {
      const deepLinkHosts = response.getHostUser.filter((host) => host.id === Number(referredId) && host.host);

      if (deepLinkHosts && deepLinkHosts.length > 0) {
        setIsFirstHost(true);
        setFilteredHosts(deepLinkHosts);
        setSelectedHost(deepLinkHosts[0].id);
        // eslint-disable-next-line prefer-destructuring
        hostData.current = deepLinkHosts[0];
        setHosts(deepLinkHosts);
      } else {
        setHosts(response.getHostUser);
        setFilteredHosts(response.getHostUser);
      }
      setLoading(false);
    },
    onError() {
      setLoading(false);
    },
  });

  const skip = () => {
    navigation.navigate('MintMembershipScreen');
  };

  const handleTextChange = (text) => {
    setFilteredHosts(hosts.filter((host) => host.fullName.includes(text) || host.username.includes(text)));
  };

  const selectHost = (hostId) => {
    selectedReferred.current = hostId;
    setSelectedHost(hostId);
    setHost({
      variables: {
        referredUserId: hostId,
      },
    });
  };

  React.useEffect(() => {
    loadHosts();
  }, []);

  return loading ? (
    <View style={{height: '100%', justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" color="white" hidesWhenStopped={false} />
    </View>
  ) : (
    <OnBoardingWrapper
      navigation={navigation}
      skipStep={skip}
      title={isFirstHost ? YOUR_HOST_TITLE : HOST_TITLE}
      subTitle={isFirstHost ? YOUR_HOST_TEXT : HOST_TEXT}
      currStep={5}
      disabledButton={!selectedHost}
      notSkippable>
      <HostSearchInput handleTextChange={handleTextChange} />
      <HostsList
        selectedHost={selectedHost}
        selectHost={selectHost}
        hosts={filteredHosts}
        loading={mutationLoading || followLoading}
        isFirstHost={isFirstHost}
        followHost={followUser}
      />
    </OnBoardingWrapper>
  );
};

export default HostSelectionScreen;
