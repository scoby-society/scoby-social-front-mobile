import React, {useCallback, useContext} from 'react';
import Topics from 'src/components/Topics';
import BackButton from 'src/components/BackButton';
import SkipButton from 'src/components/SkipButton';
import styled from 'styled-components/native';
import {UPDATE_PROFILE} from 'src/graphql/mutations/profile';
import {useMutation} from '@apollo/client';
import {GET_USER_PROFILE} from 'src/graphql/queries/profile';
import {Alert} from 'react-native';
import {FollowersContext} from 'src/containers/followers';

const ContainerHeader = styled.View({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  flexDirection: 'row',
  padding: '0px 10px',
});

const TopicsScreen = ({navigation, route}) => {
  // const [visible,setTopics,createUserProfile,profile,loading,setPublicKey] = route.params
  const createUserProfile = route?.params?.createUserProfile;
  const onBoarding = route?.params?.onBoarding;
  const profile = route?.params?.profile;
  const id = route?.params?.id;
  const {setCurrentUserProfile} = useContext(FollowersContext);

  // const profile = route.params.profile
  // const [profile] = useState(null);
  const saving = route?.params?.loading;
  /* const handleClose = useCallback(() => {
    setVisible(false);
  }, [setVisible]); */

  const [updateUserProfile] = useMutation(UPDATE_PROFILE, {
    refetchQueries: [{query: GET_USER_PROFILE}],
    onError(e) {
      Alert.alert(e.message);
    },
    onCompleted(e) {
      setCurrentUserProfile(e.updateUserProfile);
      navigation.navigate('SetUpYouProfile', {createUserProfile, id, onBoarding});
    },
  });

  const handleSave = useCallback(
    (data) => {
      const topics = data;
      updateUserProfile({
        variables: {
          profile: {
            topics,
          },
        },
      });
    },
    [createUserProfile, profile],
  );
  const skip = () => {
    navigation.navigate('SetUpYouProfile', {createUserProfile, id, onBoarding});
  };
  return (
    <>
      {onBoarding && (
        <ContainerHeader>
          <BackButton navigation={navigation} />
          <SkipButton skip={skip} />
        </ContainerHeader>
      )}
      <Topics
        showLogo={!onBoarding}
        onBoarding={onBoarding}
        navigation={navigation}
        signUp
        save={handleSave}
        profile={false}
        saving={saving}
        showTitle
        showSubtitle
      />
    </>
  );
};

export default TopicsScreen;
