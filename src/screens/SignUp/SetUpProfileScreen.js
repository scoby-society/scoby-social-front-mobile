import React, {useCallback, useEffect, useMemo, useState, useContext} from 'react';
import {Alert, View, Text, Dimensions, Image, StyleSheet, KeyboardAvoidingView} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
import {GlobalContext} from 'src/containers/global';
import NewLargeButton from 'src/components/NewLargeButton';
import Input from 'src/components/Input';
import {useLazyQuery, useMutation} from '@apollo/client';
import {CREATE_USER_PROFILE} from 'src/graphql/mutations/auth';
import styled from 'styled-components/native';
import colors from 'src/constants/Colors';
import {requestDeviceId, requestUserPermission} from 'src/utils/permission/notifications';
import {SEND_FCM_TOKEN} from 'src/graphql/mutations/fcm';
import {HidedPassword, ShowPassword, CorrectMarked} from 'assets/svg';
import {
  SIGNUP_SETUP_PROFILE_HEADER,
  SIGNUP_SETUP_PROFILE_DESCRIPTION,
  SIGNUP_SETUP_PROFILE_NEXT_BUTTON,
  SIGNUP_SETUP_PROFILE_PLACEHOLDER_USERNAME,
  SIGNUP_SETUP_PROFILE_PLACEHOLDER_PASSWORD,
  ACCOUNT_CREATED,
} from 'src/constants/Texts';
import Fonts from 'src/constants/Fonts';
import logo from 'assets/images/logos/Scoby_Final_Logos/ScobyDude_preferred_logo/scoby_dude.png';
import {GET_USER_PROFILE} from 'src/graphql/queries/profile';
import BackButton from 'src/components/BackButton';
import {kitty} from 'src/config/api';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import DateTimePickerModal from 'react-native-modal-datetime-picker';

const Container = styled.View({
  flex: 1,
  backgroundColor: colors.blueBackgroundSession,
});

const Wrapper = styled.ScrollView({
  flex: 1,
  backgroundColor: colors.blueBackgroundSession,
});

const HeaderContent = styled.View({});

const HeaderText = styled.Text({
  ...Fonts.goudy,
  fontSize: 28,
  color: colors.white,
  lineHeight: '32px',
});

const DescriptionText = styled.Text({
  ...Fonts.avenir,
  fontSize: 14,
  color: colors.white,
  paddingVertical: 16,
});

const ShowPassBtnContainer = styled.View({
  height: 35,
  justifyContent: 'space-between',
  flexDirection: 'row',
});

const ForgotBtn = styled.TouchableOpacity({});

const LogoContainer = styled.View({
  alignItems: 'center',
});
const Validations = styled.Text(({validate}) => ({
  color: validate === true ? 'rgb(250,250,250)' : 'rgb(150,150,150)',
  marginRight: 5,
}));
const ContainerText = styled.View({
  flexDirection: 'row',
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
});
const SetUpProfileScreen = ({navigation}) => {
  const {referredId, setInitialLink} = useContext(GlobalContext);

  const {height, width} = Dimensions.get('window');
  const [usernameError, setUserNameError] = useState(false);
  const [passwordError, setPasswordError] = useState(true);
  const [dateValid, setDateValid] = useState(false);
  const [birthday, setBirthday] = useState(moment().subtract(13, 'years').format('DD/MM/YYYY'));
  const [password, setPassword] = useState('');
  const [username, setName] = useState('');
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [hidePassword, setHidePassword] = useState(true);
  const [badEmail, setBadEmail] = useState(false);
  const [registrationToken, setRegistrationToken] = useState('');
  const [publicKey, setPublicKey] = useState(null);
  const [visible] = useState(false);
  const [addPushToken] = useMutation(SEND_FCM_TOKEN);
  // eslint-disable-next-line  no-unused-vars
  const [dateError, setDateError] = useState(false);
  const [visibleDateModal, setVisibleDateModal] = useState(false);
  const sendFcmToken = useCallback(
    async (e) => {
      const token = await requestUserPermission();
      const deviceId = await requestDeviceId();
      if (token) {
        await addPushToken({
          variables: {
            deviceId,
            token,
          },
        });
        kitty.endSession();
        await kitty
          .startSession({
            username: e.getUserProfile.id,
            authParams: {},
          })
          .then(() => {
            kitty.updateCurrentUser((user) => {
              user.properties = {...user, firebaseToken: token};
              return user;
            });
          });
      }
    },
    [addPushToken],
  );

  const [loadProfile] = useLazyQuery(
    GET_USER_PROFILE,
    {fetchPolicy: 'cache-and-network'},
    {
      onCompleted: async (e) => {
        await sendFcmToken(e);
      },
      onError: () => Alert.alert('Cannot load profile'),
    },
  );

  const [createUserProfile, {loading}] = useMutation(CREATE_USER_PROFILE, {
    onError(e) {
      setUserNameError(false);
      setPasswordError(false);
      if (e.networkError) {
        Alert.alert('Something went wrong. Please try again later', e.message);
      }
      if (e.graphQLErrors && e.graphQLErrors.length > 0) {
        if (e.graphQLErrors[0].extensions && e.graphQLErrors[0].extensions.code === 'ERR_USERNAME_EXISTS') {
          setUserNameError(e.message);
        } else if (e.graphQLErrors[0].extensions && e.graphQLErrors[0].extensions.code === 'ERR_VALIDATION_FAILED') {
          if (e.graphQLErrors[0].extensions.details[0].path[0] === 'password') {
            setPasswordError(e.message);
          }
          if (e.graphQLErrors[0].extensions.details[0].path[0] === 'username') {
            setUserNameError(e.message);
          }
        } else {
          setPasswordError(e.message);
          setUserNameError(e.message);
        }
      } else {
        Alert.alert('Something went wrong. Please try again later', e.message);
      }
    },
    onCompleted: async ({createUserProfile: userData}) => {
      Alert.alert(ACCOUNT_CREATED);

      await AsyncStorage.setItem('token', userData.auth?.authorizationToken);
      await loadProfile();

      navigation.navigate('WalletConnect', {
        visible,
        id: userData?.id,
        profile: userData,
        createUserProfile,
        from: 'register',
        loading,
        setPublicKey,
        onBoarding: true,
      });
    },
  });
  const validDate = useCallback((date) => {
    const birthdayNew = moment(date).format('DD/MM/YYYY');
    setBirthday(birthdayNew);
    if (birthdayNew.length === 10) {
      const actualDate = moment();

      if (actualDate.diff(moment(date), 'years') >= 14) {
        setDateValid(true);
        setDateError(false);
      } else {
        setDateValid(false);
        setDateError('The date that you have input is incorrect');
      }
    } else if (birthdayNew.length === 0) {
      setDateError(false);
    }
    setVisibleDateModal(false);
  }, []);

  const wellFormedName = useCallback((emailAddress) => {
    const reg = /.+@.+\.[A-Za-z]+$/;
    if (emailAddress && reg.test(emailAddress) === false) {
      setBadEmail(true);
    } else {
      setBadEmail(false);
    }
    setEmail(emailAddress);
  }, []);
  const [regpass, setRegpass] = useState({
    smal: false,
    capital: false,
    digit: false,
  });
  const validatePassword = useCallback((unverifyPassword) => {
    const smallLetter = new RegExp('^((?=.*[a-z]))');
    const CapitalLetter = new RegExp('^((?=.*[A-Z]))');
    const digit = new RegExp('^((?=.*[0-9]))');
    setRegpass({
      capital: CapitalLetter.test(unverifyPassword),
      smal: smallLetter.test(unverifyPassword),
      digit: digit.test(unverifyPassword),
    });

    const reg = new RegExp('^(((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])))(?=.{8,})');
    if ((unverifyPassword && reg.test(unverifyPassword) === false) || unverifyPassword === '') {
      setPasswordError(true);
    } else {
      setPasswordError(false);
    }
  }, []);

  const handleSubmit = useCallback(async () => {
    setTimeout(async () => {
      await AsyncStorage.setItem(
        'userCreatInfo',
        JSON.stringify({
          username: username.slice(1, username.length),
          password,
          birthday: moment(birthday, 'DD/MM/YYYY').toISOString(),
          registrationToken,
        }),
      );

      await AsyncStorage.setItem(
        'profile',
        JSON.stringify([
          {
            username: username.slice(1, username.length),
            password,
            birthday: moment(birthday, 'DD/MM/YYYY').toISOString(),
            registrationToken,
            email,
            fullName,
            publicKey,
          },
        ]),
      );
    }, 1000);

    const params = {
      variables: {
        profile: {
          username: username.slice(1, username.length),
          password,
          birthday: moment(birthday, 'DD/MM/YYYY').toISOString(),
          registrationToken,
          topics: [1],
          email,
          fullName,
          publicKey,
          referredUser: Number(referredId),
        },
      },
    };

    await createUserProfile(params);
    setInitialLink(null);
  }, [birthday, email, fullName, password, registrationToken, username, publicKey, createUserProfile]);

  const handleUsernameChange = useCallback((text) => {
    if (text.charAt(0) !== '@') {
      text = `@${text}`;
    }
    setName(text);
  }, []);
  const styles = StyleSheet.create({
    logo: {
      width: width * 0.2,
      height: height * 0.2,
      resizeMode: 'contain',
    },
    content: {
      padding: 24,
    },
  });
  const isDataValid = useMemo(
    () =>
      username &&
      !passwordError &&
      username.length >= 2 &&
      password &&
      password.length >= 8 &&
      email &&
      email.length > 0 &&
      !badEmail &&
      birthday &&
      dateValid,
    [badEmail, birthday, dateValid, email, password, username, passwordError],
  );

  useEffect(() => {
    async function updateToken() {
      setRegistrationToken(await AsyncStorage.getItem('registrationToken'));
    }

    updateToken();
  }, []);

  return (
    <Container>
      <DateTimePickerModal
        isVisible={visibleDateModal}
        mode="date"
        date={new Date(moment(birthday, 'DD/MM/YYYY').add(1, 'd'))}
        onConfirm={(e) => validDate(e)}
        onCancel={() => setVisibleDateModal(false)}
      />
      <Wrapper keyboardShouldPersistTaps="handled" contentContainerStyle={styles.content}>
        <BackButton navigation={navigation} />
        <LogoContainer>
          <Image source={logo} style={styles.logo} />
        </LogoContainer>
        <HeaderContent>
          <HeaderText>{SIGNUP_SETUP_PROFILE_HEADER}</HeaderText>
          <DescriptionText>{SIGNUP_SETUP_PROFILE_DESCRIPTION}</DescriptionText>
        </HeaderContent>
        <Input
          placeholder="Full Name *"
          maxLength={40}
          value={fullName}
          isError={false}
          onChangeText={(text) => {
            setFullName(text);
          }}
        />
        <Input
          placeholder="Email *"
          value={email}
          maxLength={40}
          isError={false}
          autoCapitalize="none"
          onChangeText={wellFormedName}
          keyboardType="email-address"
          textContentType="emailAddress"
        />
        <Input
          placeholder={`${SIGNUP_SETUP_PROFILE_PLACEHOLDER_USERNAME} *`}
          value={username}
          isError={usernameError}
          autoCapitalize="none"
          onChangeText={handleUsernameChange}
          marked={username.length >= 2 && !usernameError}
        />
        <View style={{width: '100%'}}>
          <TextInputMask
            placeholder="Birthday"
            customTextInput={Input}
            onPress={() => setVisibleDateModal(true)}
            customTextInputProps={{ref: null, calendarInput: true}}
            type="datetime"
            options={{
              format: 'MM/DD/YYYY',
            }}
            value={birthday}
            placeholderTextColor={colors.translucentBlack}
            returnKeyType="done"
            isError={dateError}
          />
          <ShowPassBtnContainer>
            <Text style={{color: 'white', fontSize: 10}}>Your age should be at least 13 years. </Text>
          </ShowPassBtnContainer>
        </View>
        <Input
          inlineImageLeft="search_icon"
          placeholder={`${SIGNUP_SETUP_PROFILE_PLACEHOLDER_PASSWORD} *`}
          maxLength={20}
          value={password}
          isError={passwordError}
          onChangeText={(text) => {
            setPassword(text);
            validatePassword(text);
          }}
          secureTextEntry={hidePassword}
          marked={password.length >= 8 && !passwordError && !usernameError}
        />
        <ShowPassBtnContainer>
          <Text style={{color: 'white', fontSize: 10}}>Password should contain at least: </Text>
          <ForgotBtn onPress={() => setHidePassword(!hidePassword)}>
            {hidePassword ? <HidedPassword /> : <ShowPassword />}
          </ForgotBtn>
        </ShowPassBtnContainer>
        <View>
          <ContainerText>
            <Validations validate={regpass.digit === true && true}>1 digit</Validations>
            {regpass.digit === true && <CorrectMarked />}
          </ContainerText>
          <ContainerText>
            <Validations validate={regpass.capital === true && true}>1 capital letter</Validations>
            {regpass.capital === true && <CorrectMarked />}
          </ContainerText>
          <ContainerText>
            <Validations validate={regpass.smal === true && true}>1 small letter</Validations>
            {regpass.smal === true && <CorrectMarked />}
          </ContainerText>
          <ContainerText>
            <Validations validate={password.length >= 8 && true}>More than 8 characters long</Validations>
            {password.length >= 8 && <CorrectMarked />}
          </ContainerText>
        </View>
        <NewLargeButton disabled={!isDataValid} onPress={handleSubmit} title={SIGNUP_SETUP_PROFILE_NEXT_BUTTON} />
      </Wrapper>
      <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={-64}>
        <View />
      </KeyboardAvoidingView>
    </Container>
  );
};

export default withSafeAreaWithoutMenu(SetUpProfileScreen);
