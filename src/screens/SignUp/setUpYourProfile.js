import React, {useEffect, useState, useContext} from 'react';
import BackButton from 'src/components/BackButton';
import {View, TouchableOpacity, Text, Image, ScrollView, Alert} from 'react-native';
import styled from 'styled-components/native';
import {ReactNativeFile} from 'apollo-upload-client';
import fonts from 'src/constants/Fonts';
import {SET_UP_YOUR_PROFILE_TITTLE, BACKGROUND_IMAGE_TEXT, PROFILE_IMAGE_TEXT, SHIPING_TEXT} from 'src/constants/Texts';
import Background from 'assets/images/profile/backgroundDefault.png';
import {UPDATE_PROFILE, UPLOAD_AVATAR, UPLOAD_COVER} from 'src/graphql/mutations/profile';
import {GET_USER_PROFILE} from 'src/graphql/queries/profile';
import Profile from 'assets/images/profile/profileDefault.png';
import Check from 'assets/images/modal/check.png';
import {FollowersContext} from 'src/containers/followers';
import {VERIFY_PUBLIC_KEY} from 'src/graphql/mutations/auth';
import AsyncStorage from '@react-native-community/async-storage';
import Steps from 'src/components/Steps';
import ImagePicker from 'react-native-image-crop-picker';
import {useMutation, useQuery} from '@apollo/client';
import Colors from 'src/constants/Colors';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';
import RadioButton from 'src/components/RadioButtom';

const NextButton = styled.TouchableOpacity({
  width: 107,
  height: 42,
  background: ' #CD068E',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 8,
  marginLeft: 10,
});
const BackButtonsecond = styled.TouchableOpacity({
  width: 107,
  height: 42,
  marginRight: 10,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 8,
});
const SimpleInput = styled.TextInput({
  border: 'solid 1px rgba(255, 255, 255, 0.54)',
  borderRadius: 8,
  display: 'flex',
  justifyContent: 'flex-start',
  padding: 12,
  color: Colors.white,
});
const ContainerInput = styled.View({
  width: '95%',
});
const Container = styled.View({
  alignItems: 'center',
});
const Row = styled.View({
  flexDirection: 'row',
  justifyContent: 'space-between',
  display: 'flex',
  alignItems: 'center',
});
const Column = styled.TouchableOpacity({
  marginHorizontal: 5,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  flexDirection: 'column',
});
const Tittle = styled.Text({
  ...fonts.goudy,
  fontSize: 22,
  color: 'white',
});
const ContainerPhoto = styled.View({
  backgroundColor: 'rgb(20,0,65)',
  padding: 5,
  borderRadius: 80,
  width: 110,
  height: 110,
  marginLeft: 30,
  marginTop: -50,
});
const BotButtonContainer = styled.View({
  marginTop: 40,
  marginBottom: 30,
});
const ProfileImg = styled.ImageBackground({
  width: 100,
  height: 100,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: 100,
});
const BackgroudImg = styled.ImageBackground({
  display: 'flex',
  marginTop: 35,
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
  height: 144,
});
const YelowText = styled.Text({
  color: 'rgba(255, 200, 0, 1)',
  marginLeft: -10,
});
const FuchsiaText = styled.Text({
  color: 'rgba(236, 0, 140, 1)',
  marginLeft: -10,
});
const BlueText = styled.Text({
  color: 'rgba(36, 189, 199, 1)',
  marginLeft: -10,
});
const GrayText = styled.Text({
  color: 'rgba(255, 255, 255, 1)',
  marginLeft: -10,
});
const PurpleText = styled.Text({
  color: 'rgba(101, 54, 187, 1)',
  marginLeft: -10,
});
const Size = ['2XS', 'XS', 'S', 'M', 'L', 'XL', '2XL', '3XL', '4XL'];
const ContainerColorText = styled.View({
  display: 'flex',
  justifyContent: 'flex-start',
  margin: '10px',
  width: '90%',
});
const initialProfile = {
  username: '',
  fullName: '',
  bio: '',
  location: '',
  topics: [],
  website: '',
  avatar: null,
  backgroundImage: null,
  email: '',
  publicKey: '',
};
const setUpYouProfile = ({navigation}) => {
  const {currentUserProfile, setCurrentUserProfile} = useContext(FollowersContext);
  const [shirtSize, setShirtSize] = useState('');
  const [hatSize, setHatSize] = useState('');
  const [bio, setBio] = useState('');
  const [location, setLocation] = useState('');
  const [website, setWebsite] = useState('');
  const [shiping, setShiping] = useState('');
  const [, setUserProfile] = useState(initialProfile);

  const {data} = useQuery(GET_USER_PROFILE, {
    onCompleted(response) {
      setCurrentUserProfile(response.getUserProfile);
      setUserProfile(response.getUserProfile);
    },
  });
  const {getUserProfile: {fullName, topics, username, email, publicKey} = initialProfile} = data;

  const [setVerifyPublicKey] = useMutation(VERIFY_PUBLIC_KEY, {
    async onCompleted({verifyPublicKey}) {
      await AsyncStorage.setItem('token', verifyPublicKey.authorizationToken);
    },
  });

  useEffect(() => {
    setVerifyPublicKey();
  }, [setVerifyPublicKey]);

  const [avatar, setAvatar] = useState();
  const [background, setBackground] = useState();
  const handleClickBackground = () => {
    ImagePicker.openPicker({
      width: 800,
      height: 400,
      cropping: true,
    }).then((image) => {
      setBackground(image.path);
    });
  };
  const handleClickAvatar = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then((image) => {
      setAvatar(image.path);
    });
  };

  function buildFile(uri) {
    return new ReactNativeFile({
      uri,
      name: 'image.jpg',
      type: 'image/jpeg',
    });
  }
  const [updateUserProfile] = useMutation(UPDATE_PROFILE, {
    refetchQueries: [{query: GET_USER_PROFILE}],
    onCompleted() {
      navigation.navigate('ShowNFT', {onBoarding: true});
    },
  });
  const [uploadAvatar] = useMutation(UPLOAD_AVATAR, {
    refetchQueries: [{query: GET_USER_PROFILE}],
    onCompleted(response) {
      setCurrentUserProfile({
        ...currentUserProfile,
        avatar: response.uploadFile.avatar,
      });
    },
  });

  const [uploadCover] = useMutation(UPLOAD_COVER, {
    refetchQueries: [{query: GET_USER_PROFILE}],
    onError(e) {
      const message = e.graphQLErrors[0].extensions?.details[0].message;
      if (message) {
        Alert.alert(e.message, message);
      } else {
        Alert.alert('hubo un error', e.message);
      }
    },
  });

  const handleSave = async () => {
    await updateUserProfile({
      variables: {
        profile: {
          fullName,
          username,
          email,
          bio,
          location,
          website,
          shipping: shiping,
          hatSize,
          shirtSize,
          topics: topics?.map((topic) => topic.id),
          publicKey,
        },
      },
    });
    if (avatar && avatar.length && avatar) {
      await uploadAvatar({
        variables: {
          avatar: buildFile(avatar),
        },
      });
    }
    if (background && background.length && background) {
      await uploadCover({
        variables: {
          backgroundImage: buildFile(background),
        },
      });
    }
  };

  const handleNext = () => {
    navigation.navigate('ShowNFT', {onBoarding: true});
  };

  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <BackButton navigation={navigation} />
        <TouchableOpacity onPress={handleNext} style={{marginRight: 20}}>
          <Text style={{color: '#FFFFFF', fontSize: 16}}>Skip</Text>
        </TouchableOpacity>
      </View>
      <ScrollView>
        <Container>
          <Row style={{display: 'flex', justifyContent: 'space-between', width: '85%'}}>
            <Tittle>{SET_UP_YOUR_PROFILE_TITTLE}</Tittle>
            <Steps FirstValue={3} SecondValue={7} />
          </Row>
          <TouchableOpacity style={{width: '100%'}} onPress={() => handleClickBackground()}>
            <BackgroudImg source={(background && {uri: background}) || Background}>
              <Text>{!background && BACKGROUND_IMAGE_TEXT}</Text>
            </BackgroudImg>
          </TouchableOpacity>

          <View style={{width: '100%'}}>
            <ContainerPhoto>
              <TouchableOpacity onPress={() => handleClickAvatar()}>
                <ProfileImg
                  source={(avatar && {uri: avatar}) || Profile}
                  imageStyle={{borderRadius: 200}}
                  resizeMode="cover"
                  width="100%"
                  height="100%">
                  <Text>{!avatar && PROFILE_IMAGE_TEXT}</Text>
                </ProfileImg>
              </TouchableOpacity>
            </ContainerPhoto>
          </View>
          <View
            style={{
              width: '102%',
              marginLeft: 45,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text
                style={{
                  ...fonts.avenir,
                  fontSize: 20,
                  color: Colors.white,
                }}>
                {fullName}
              </Text>
              <Image
                style={{
                  width: 30,
                  height: 30,
                }}
                source={Check}
              />
            </View>

            <Text
              style={{
                ...fonts.avenir,
                color: Colors.white,
              }}>
              @{username}
            </Text>
          </View>

          <ContainerInput>
            <ContainerColorText>
              <GrayText>Description</GrayText>
            </ContainerColorText>
            <SimpleInput
              style={{
                display: 'flex',
                flex: 1,
                color: Colors.white,
              }}
              multiline
              maxLength={140}
              editable
              placeholder="Provide a short bio."
              onChangeText={setBio}
              value={bio}
            />
          </ContainerInput>

          <ContainerInput>
            <ContainerColorText>
              <FuchsiaText>Website</FuchsiaText>
            </ContainerColorText>
            <SimpleInput placeholder="www.website.com" onChangeText={setWebsite} value={website} />
          </ContainerInput>

          <ContainerInput>
            <ContainerColorText>
              <PurpleText>Location</PurpleText>
            </ContainerColorText>
            <SimpleInput placeholder="Location" onChangeText={setLocation} value={location} />
          </ContainerInput>

          <ContainerInput>
            <ContainerColorText>
              <BlueText>Shipping Address</BlueText>
            </ContainerColorText>
            <SimpleInput placeholder="Shipping Address" onChangeText={setShiping} value={shiping} />
            <Text
              style={{
                fontSize: 10,
                width: '100%',
                padding: 5,
                color: 'rgba(159, 159, 159, 1)',
              }}>
              {SHIPING_TEXT}
            </Text>
          </ContainerInput>

          <ContainerColorText>
            <YelowText>Hoodie or shirt size</YelowText>
          </ContainerColorText>

          <Row>
            {Size.map((item) => (
              <Column onPress={() => setShirtSize(item)}>
                <Text style={{color: Colors.white}}>{item}</Text>
                <RadioButton checked={shirtSize === item} />
              </Column>
            ))}
          </Row>
          <ContainerColorText>
            <YelowText>Hat size</YelowText>
          </ContainerColorText>
          <Row>
            {Size.map((item, index) => (
              <Column onPress={() => setHatSize(item)} key={`${Math.random() * index}-${item.id}`}>
                <Text style={{color: Colors.white}}>{item}</Text>
                <RadioButton checked={hatSize === item} />
              </Column>
            ))}
          </Row>
          <BotButtonContainer>
            <Row>
              <BackButtonsecond onPress={() => navigation.goBack()}>
                <Text style={{color: Colors.white}}>Back</Text>
              </BackButtonsecond>
              <NextButton onPress={handleSave}>
                <Text style={{color: Colors.white}}>Next</Text>
              </NextButton>
            </Row>
          </BotButtonContainer>
        </Container>
      </ScrollView>
    </>
  );
};
export default withSafeAreaWithoutMenu(setUpYouProfile);
