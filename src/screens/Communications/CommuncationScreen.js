import React from 'react';
import withSafeArea from 'src/components/withSafeArea';
import colors from 'src/constants/Colors';
import CommunicationsTopTap from 'src/navigation/communicationsTopTabs';
import styled from 'styled-components/native';

const Wrapper = styled.View({
  flex: 1,
  marginHorizontal: 12,
  marginVertical: 30,
  backgroundColor: colors.blueBackgroundSession,
});

const CommunicationScreen = ({navigation}) => (
  <Wrapper>
    <CommunicationsTopTap navigation={navigation} />
  </Wrapper>
);

export default withSafeArea(CommunicationScreen);
