import React from 'react';
import 'react-native-gesture-handler';
import {createStackNavigator} from '@react-navigation/stack';
import {COMMUNICATION_KEYS} from './CommunicationKeys';
import CommunicationScreen from './CommuncationScreen';

const Stack = createStackNavigator();

const options = {headerShown: false, headerBackTitleVisible: false, headerTitleStyle: {display: 'none'}};

const CommunicationStack = () => (
  <Stack.Navigator initialRouteName={COMMUNICATION_KEYS.PROFILES}>
    <Stack.Screen options={options} name={COMMUNICATION_KEYS.PROFILES} component={CommunicationScreen} />
  </Stack.Navigator>
);

export default CommunicationStack;
