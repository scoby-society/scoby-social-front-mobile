import React, {useContext, useState} from 'react';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import {FollowersContext} from 'src/containers/followers';
import {useMutation} from '@apollo/client';
import {ACCEPT_INVITE, REQUEST_MEMBERSHIP} from 'src/graphql/mutations/team';
import {UNKNOWN_ERROR_TEXT} from 'src/constants/Texts';
import {Alert} from 'react-native';
import {TEAM_TYPES, USER_STATUSES} from 'src/constants/Variables';
import RegularButton from 'src/components/RegularButton';
import {GET_TEAMS} from 'src/graphql/queries/profile';

const TeamActionsButton = styled(RegularButton)({
  justifyContent: 'center',
  alignItems: 'center',
  width: 96,
  textAlign: 'center',
  fontSize: 14,
  ...Fonts.avenirSemiBold,
  borderRadius: 5,
});

export const TeamButton = ({teamType, pendingUsers, memberUsers, ownerUser, teamId}) => {
  const {currentUserProfile} = useContext(FollowersContext);
  const userId = currentUserProfile.id;
  const isOwner = ownerUser.id === userId;
  const isMember = memberUsers?.find((member) => member.user.id === userId && member.isAccepted);
  const isMemberInPendingList = !!pendingUsers?.find((member) => member.id === userId);
  const isMemberInvitedToTeam = !!memberUsers?.find((member) => member.user.id === userId && !member.isAccepted);
  const isTeamPublic = teamType === TEAM_TYPES.OPEN;

  const [userStatus, setUserStatus] = useState(() => {
    if (isOwner) return USER_STATUSES.OWNER;
    if (isMember) return USER_STATUSES.MEMBER;
    if (isMemberInPendingList) return USER_STATUSES.PENDING;
    if (isTeamPublic && isMemberInvitedToTeam) return USER_STATUSES.NEWBIE;

    return USER_STATUSES.STRANGER;
  });

  const [accept, {loading: isAcceptInProcess}] = useMutation(ACCEPT_INVITE, {
    refetchQueries: [
      {
        query: GET_TEAMS,
        variables: {
          paging: {
            limit: 20,
            page: 1,
          },
          query: '',
        },
      },
    ],
    onCompleted: () => setUserStatus(USER_STATUSES.MEMBER),
    onError: () => Alert.alert(UNKNOWN_ERROR_TEXT),
  });

  const [request, {loading: isMembershipRequesting}] = useMutation(REQUEST_MEMBERSHIP, {
    refetchQueries: [
      {
        query: GET_TEAMS,
        variables: {
          paging: {
            limit: 20,
            page: 1,
          },
          query: '',
        },
      },
    ],
    onCompleted: () => setUserStatus(USER_STATUSES.PENDING),
    onError: () => Alert.alert(UNKNOWN_ERROR_TEXT),
  });

  const joinTeam = () => accept({variables: {teamId}});

  const requestToJoin = () => request({variables: {teamId}});

  const btnTypeMap = {
    [USER_STATUSES.OWNER]: 'active',
    [USER_STATUSES.MEMBER]: 'active',
    [USER_STATUSES.PENDING]: 'deactivated',
    [USER_STATUSES.NEWBIE]: 'deactivated',
    [USER_STATUSES.STRANGER]: 'deactivated',
  };

  const isBtnDisabledMap = {
    [USER_STATUSES.OWNER]: true,
    [USER_STATUSES.MEMBER]: true,
    [USER_STATUSES.PENDING]: true,
    [USER_STATUSES.NEWBIE]: false,
    [USER_STATUSES.STRANGER]: false,
  };

  const btnBackgroundColorMap = {
    [USER_STATUSES.OWNER]: Colors.blueBackgroundSession,
    [USER_STATUSES.MEMBER]: Colors.blueBackgroundSession,
    [USER_STATUSES.PENDING]: Colors.disabledButton,
    [USER_STATUSES.NEWBIE]: Colors.newPink,
    [USER_STATUSES.STRANGER]: Colors.newPink,
  };

  const btnBorderColorMap = {
    [USER_STATUSES.OWNER]: Colors.white,
    [USER_STATUSES.MEMBER]: Colors.white,
    [USER_STATUSES.PENDING]: Colors.disabledButton,
    [USER_STATUSES.NEWBIE]: Colors.newPink,
    [USER_STATUSES.STRANGER]: Colors.newPink,
  };

  const onPressActionsMap = {
    [USER_STATUSES.OWNER]: null,
    [USER_STATUSES.MEMBER]: null,
    [USER_STATUSES.PENDING]: null,
    newbie: () => joinTeam(),
    stranger: () => requestToJoin(),
  };

  const btnTitleMap = {
    [USER_STATUSES.OWNER]: 'Owner',
    [USER_STATUSES.MEMBER]: 'Member',
    [USER_STATUSES.PENDING]: 'Pending',
    [USER_STATUSES.NEWBIE]: 'Accept',
    [USER_STATUSES.STRANGER]: isTeamPublic ? 'Join' : 'Apply',
  };

  const btnStyle = {
    backgroundColor: btnBackgroundColorMap[userStatus],
    borderColor: btnBorderColorMap[userStatus],
  };

  const isBtnActive = btnTypeMap[userStatus] === 'active';
  const isLoading = isAcceptInProcess || isMembershipRequesting;

  return (
    <TeamActionsButton
      style={btnStyle}
      active={isBtnActive}
      disabled={isBtnDisabledMap[userStatus]}
      onPress={onPressActionsMap[userStatus]}
      title={btnTitleMap[userStatus]}
      loading={isLoading}
    />
  );
};
