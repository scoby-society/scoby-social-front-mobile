import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import styled from 'styled-components';
import Colors from 'src/constants/Colors';
import Fonts from 'src/constants/Fonts';
import avatarSrc from 'assets/images/profile/avatarPlaceholder.png';
import {TeamButton} from './TeamButton';

const ListText = styled.Text({
  ...Fonts.avenir,
  fontSize: 16,
  color: Colors.white,
});

const Avatar = styled.Image({
  width: 44,
  height: 44,
  borderRadius: 44,
});

const User = styled.View({
  flexDirection: 'row',
  alignItems: 'center',
  marginVertical: 16,
});

const Username = styled.View({
  flex: 1,
  paddingLeft: 16,
  paddingRight: 32,
  flexDirection: 'row',
});

export const TeamItem = ({item}) => {
  const navigation = useNavigation();

  const goToTeamDetails = () => navigation.navigate('TeamScreen', {id: item.id});

  function getImgSource(avatar) {
    return avatar ? {uri: avatar} : avatarSrc;
  }

  const name = item.name || 'Unknown';

  return (
    <TouchableWithoutFeedback onPress={goToTeamDetails} touchSoundDisabled>
      <User>
        <Avatar source={getImgSource(item.avatar)} />
        <Username>
          <ListText numberOfLines={1}>{name}</ListText>
        </Username>
        <TeamButton
          teamType={item.teamType}
          pendingUsers={item.pendingUsers}
          memberUsers={item?.members}
          ownerUser={item.ownerUser}
          teamId={item.id}
        />
      </User>
    </TouchableWithoutFeedback>
  );
};
