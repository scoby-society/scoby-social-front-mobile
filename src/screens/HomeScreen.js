import React, {useCallback, useContext, useEffect, useState} from 'react';
import styled from 'styled-components/native';
import withSafeArea from 'src/components/withSafeArea';
import UniversalSearch from 'src/components/universalSearch/UniversalSearch';
import HomeFeedTabs from 'src/navigation/homeFeedTabs';
import {useLazyQuery} from '@apollo/client';
import {
  HOME_PROJECT_FEED,
  SEARCH_COLLECTIONS,
  SEARCH_COMMUNITIES,
  SEARCH_EVENT,
  SEARCH_SERIE,
} from 'src/graphql/queries/universalSearch';
import {LIMIT, PAGE} from 'src/utils/pagination';
import {FollowersContext} from 'src/containers/followers';

const Wrapper = styled.View({
  width: '100%',
  height: '100%',
});

const HomeScreen = () => {
  const {currentCategory, setCurrentCategory} = useContext(FollowersContext);
  const [searchWord, setSearchWord] = useState('');
  const [item, setItem] = useState([]);

  const [refetchSeries] = useLazyQuery(SEARCH_SERIE, {
    variables: {
      query: searchWord,
    },
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e));
    },
  });

  const [refetchEvent] = useLazyQuery(SEARCH_EVENT, {
    variables: {
      query: searchWord,
    },
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e));
    },
  });

  const [refetchCommunities] = useLazyQuery(SEARCH_COMMUNITIES, {
    variables: {
      query: searchWord,
      paging: {
        limit: LIMIT,
        page: PAGE,
      },
    },
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e));
    },
  });

  const [refetchCollection] = useLazyQuery(SEARCH_COLLECTIONS, {
    variables: {
      query: searchWord,
      paging: {
        limit: LIMIT,
        page: PAGE,
      },
    },
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e));
    },
  });

  const [refetch, {data}] = useLazyQuery(HOME_PROJECT_FEED, {
    variables: {
      paging: {
        limit: LIMIT,
        page: PAGE,
      },
      category: currentCategory,
    },
    onError(e) {
      // eslint-disable-next-line no-console
      console.log(JSON.stringify(e, null, 4));
    },
  });

  const filterProjects = useCallback(async () => {
    if (searchWord !== '') {
      let series = [];
      let event = [];
      let community = [];
      let collections = [];
      switch (currentCategory) {
        case 'all':
          series = await refetchSeries();
          event = await refetchCollection();
          community = await refetchCommunities();
          collections = await refetchEvent();
          return [
            ...(series.data.getSearchSerie ?? []),
            ...(event.data.getSearchEvent ?? []),
            ...(collections.data.getSearchCollection ?? []),
            ...(community.data.getSearchTeam ?? []),
          ];
        case 'collection':
          return (await refetchCollection()).data.getSearchCollection ?? [];
        case 'experience':
          series = await refetchSeries();
          event = await refetchEvent();
          return [...(series.data.getSearchSerie ?? []), ...(event.data.getSearchEvent ?? [])];
        case 'comunity':
          return (await refetchCommunities()).data.getSearchTeam ?? [];
        default:
          return [];
      }
    }
    return (await refetch()).data.getHomeProjectFeed;
  }, [searchWord, currentCategory]);

  useEffect(() => {
    const time = setTimeout(() => {
      filterProjects().then((e) => setItem(e));
    }, 20);
    return () => clearTimeout(time);
  }, [searchWord, currentCategory, data]);

  return (
    <Wrapper>
      <UniversalSearch setSearchWord={setSearchWord} searchWord={searchWord} />
      <HomeFeedTabs
        setCurrentCategory={setCurrentCategory}
        currentCategory={currentCategory}
        searchWord={searchWord}
        itemsSearch={item}
      />
    </Wrapper>
  );
};

export default withSafeArea(HomeScreen);
