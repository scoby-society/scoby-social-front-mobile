import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import NewHomeScreen from 'src/screens/RegistrationScreen/NewHomeScreen';

const Stack = createStackNavigator();

const RegistrationStack = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
    }}
    mode="modal"
    initialRouteName={NewHomeScreen}
  />
);

export default RegistrationStack;
