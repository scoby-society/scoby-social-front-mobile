import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {GlobalContext} from 'src/containers/global';
import SignUpWithPhoneScreen from 'src/screens/SignUp/SignUpWithPhoneScreen';
import VerifyPhoneScreen from 'src/screens/SignUp/VerifyPhoneScreen';
import SetUpProfileScreen from 'src/screens/SignUp/SetUpProfileScreen';
import TermsAndConditions from 'src/screens/SignUp/TermsAndConditions';
import LoginWithPhoneScreen from 'src/screens/Login/LoginWithPhoneScreen';
import WalletConnectScreen from 'src/screens/Login/WalletConnectScreen';
import NewPassword from 'src/screens/SignUp/NewPassword';
import ResetPassword from 'src/screens/ResetPassword';
import Topics from 'src/components/Topics';
import WalletAddressRegistration from 'src/screens/SignUp/WalletAddressRegistration';
import TopicsScreen from 'src/screens/SignUp/TopicsScreen';
import SelectLoginOption from 'src/screens/Login/SelectLoginOption';
import setUpYouProfile from 'src/screens/SignUp/setUpYourProfile';
import ShowNFT from 'src/screens/Profile/components/ShowNFT';
import Sharing from 'src/screens/Profile/components/Sharing';
import HostSelectionScreen from 'src/screens/SignUp/HostSelectionScreen';
import MintMembershipScreen from 'src/screens/SignUp/MintMembershipScreen';
import withSafeAreaWithoutMenu from 'src/components/withSafeAreaWithoutMenu';

const Stack = createStackNavigator();

const authDefaultOptions = {
  headerShown: false,
  headerTitle: '',
  headerBackTitleVisible: false,
  gestureEnabled: false,
};

const AuthStack = ({navigation}) => {
  const {isLogged, initialLink} = React.useContext(GlobalContext);

  React.useEffect(() => {
    if (initialLink && !isLogged) {
      navigation.current.navigate('TermsAndConditions');
    }
  }, [isLogged, initialLink]);

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}
      initialRouteName="LoginWithPhone">
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="SelectLoginOption"
        component={SelectLoginOption}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="WalletConnect"
        component={WalletConnectScreen}
      />
      <Stack.Screen options={{...authDefaultOptions, tabBarVisible: false}} name="ShowNFT" component={ShowNFT} />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="LoginWithPhone"
        component={LoginWithPhoneScreen}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="SignUpWithPhone"
        component={SignUpWithPhoneScreen}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="ResetPassword"
        component={ResetPassword}
      />
      <Stack.Screen options={authDefaultOptions} name="NewPassword" component={NewPassword} />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="VerifyPhone"
        component={VerifyPhoneScreen}
      />
      <Stack.Screen options={authDefaultOptions} name="SetSignUpTopics" component={Topics} />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="TermsAndConditions"
        component={TermsAndConditions}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="SetUpProfile"
        component={SetUpProfileScreen}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="SetUpYouProfile"
        component={setUpYouProfile}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="WalletRegistration"
        component={WalletAddressRegistration}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="TopicsScreen"
        component={withSafeAreaWithoutMenu(TopicsScreen)}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="HostSelectionScreen"
        component={HostSelectionScreen}
      />
      <Stack.Screen
        options={{...authDefaultOptions, tabBarVisible: false}}
        name="MintMembershipScreen"
        component={MintMembershipScreen}
      />
      <Stack.Screen options={{...authDefaultOptions, tabBarVisible: false}} name="Sharing" component={Sharing} />
    </Stack.Navigator>
  );
};

export default AuthStack;
