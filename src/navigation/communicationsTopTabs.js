import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import fonts from 'src/constants/Fonts';
import colors from 'src/constants/Colors';
import SearchScreen from 'src/screens/SearchScreen';
import {TeamItem} from 'src/screens/Communications/components/TeamItem';
import {useLazyGetTeams} from 'src/utils/hook/useLazyGetTeams';
import TeamsList from 'src/components/TeamsList/TeamsList';
import styled from 'styled-components/native';

const Tabs = createMaterialTopTabNavigator();

const tabStyle = () => ({
  tabBarStyle: {
    backgroundColor: colors.blueBackgroundSession,
  },
  tabBarLabelStyle: {
    fontFamily: fonts.avenir.fontFamily,
    fontSize: 20,
    textTransform: 'capitalize',
  },
  tabBarIndicatorStyle: {
    backgroundColor: colors.white,
  },
  tabBarIndicatorContainerStyle: {
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 1,
  },
  tabBarPressColor: colors.darkBlue,
  tabBarInactiveTintColor: colors.borderGrey,
  tabBarActiveTintColor: colors.white,
});

const CommunicationsTopTap = (props) => {
  const renderItem = ({item}) => <TeamItem item={item} />;

  return (
    <Tabs.Navigator initialRouteName="profiles" screenOptions={tabStyle}>
      <Tabs.Screen
        name="profiles"
        options={{
          tabBarLabel: 'Members',
        }}>
        {({navigation}) => <SearchScreen navigation={navigation} />}
      </Tabs.Screen>
      <Tabs.Screen name="Communities">
        {() => (
          <Wrapper>
            <TeamsList
              navigation={props.navigation}
              limit={20}
              renderItem={renderItem}
              useLazyQueryHook={useLazyGetTeams}
              objectName="getAllTeams"
            />
          </Wrapper>
        )}
      </Tabs.Screen>
    </Tabs.Navigator>
  );
};

const Wrapper = styled.View({
  paddingHorizontal: 24,
});

export default CommunicationsTopTap;
