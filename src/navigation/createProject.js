import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
// import ProjectScreen from 'src/screens/CreateProject/Project';
import CreationProjectScreen from 'src/screens/CreateProject/CreationProject';
import DecorateProjectScreen from 'src/screens/CreateProject/DecorateProject';
import CollectionProjectScreen from 'src/screens/CreateProject/CollectionProject';
import PrivacyProjectScreen from 'src/screens/CreateProject/PrivacyProject';
import ShareSession from 'src/screens/CreateExperience/ShareSession';
import EnhanceProjectScreen from 'src/screens/CreateProject/EnhanceProject';
import ChooseExperienceSession from 'src/screens/CreateExperience/ChooseExperience';
import OneViewProjectScreen from 'src/screens/CreateProject/OneViewProject';
import CreateNewPrograms from 'src/screens/CreatePrograms/CreateNewPrograms';
import SelectCollection from 'src/screens/CreatePrograms/SelectCollection';
import PrivacySettings from 'src/screens/CreatePrograms/PrivacySettings';
import TopicsPrograms from 'src/screens/CreatePrograms/Topics';
import UploadIconCover from 'src/screens/CreatePrograms/UploadIconCover';

const Stack = createStackNavigator();

const ProjectStack = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
      gestureEnabled: false,
    }}
    initialRouteName={CreationProjectScreen}>
    <Stack.Screen
      name="CreationProjectScreen"
      component={CreationProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="ChoiseExperience"
      component={ChooseExperienceSession}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    {/* <Stack.Screen
      name="ProjectScreen"
      component={ProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    /> */}
    <Stack.Screen
      name="DecorateProjectScreen"
      component={DecorateProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="CollectionProjectScreen"
      component={CollectionProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="PrivacyProjectScreen"
      component={PrivacyProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="EnhanceProjectScreen"
      component={EnhanceProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="OneViewProjectScreen"
      component={OneViewProjectScreen}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="ShareSession"
      component={ShareSession}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="CreateNewProgram"
      component={CreateNewPrograms}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="SelectCollection"
      component={SelectCollection}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="PrivacySettings"
      component={PrivacySettings}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="SetTopics"
      component={TopicsPrograms}
      options={{
        tabBarVisible: false,
      }}
    />
    <Stack.Screen
      name="UploadIconCover"
      component={UploadIconCover}
      options={{
        tabBarVisible: false,
      }}
    />
  </Stack.Navigator>
);

export default ProjectStack;
