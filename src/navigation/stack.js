import {useLazyQuery} from '@apollo/client';
import {withInAppNotification} from '@chatkitty/react-native-in-app-notification';
import notifee from '@notifee/react-native';
import {createStackNavigator} from '@react-navigation/stack';
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useContext, useEffect, useMemo} from 'react';
import {ActivityIndicator, Alert, Text, View} from 'react-native';
import ChatKitty from 'src/components/Chat';
import Chat from 'src/components/Chat/index-old';
import {kitty} from 'src/config/api';
import {INVALID_DYNAMIC_LINK, UNAUTHORIZED_USER_ERROR_VIEW_TEXT} from 'src/constants/Texts';
import {FollowersContext} from 'src/containers/followers';
import {GlobalContext} from 'src/containers/global';
import {GET_COUNTER_ACTIVITY} from 'src/graphql/queries/activity';
import {ACTIVITY_KEYS} from 'src/screens/Activity/ActivityKeys';
import ChannelsLandingView from 'src/screens/channels/ChannelsLandingView';
import CreateSessionScreen from 'src/screens/CreateExperience/CreateSession/CreateSessionScreen';
import FinishSession from 'src/screens/CreateExperience/CreateSession/FinishSession';
import JoinSession from 'src/screens/CreateExperience/CreateSession/JoinSession';
import InviteFollowers from 'src/screens/CreateExperience/CreateTeam/InviteFollowers';
import NameTeam from 'src/screens/CreateExperience/CreateTeam/NameTeam';
import PreviewTeam from 'src/screens/CreateExperience/CreateTeam/PreviewTeam';
import TeamSettings from 'src/screens/CreateExperience/CreateTeam/TeamSettings';
import SelectTopics from 'src/screens/CreateExperience/SelectTopics';
import ShareSession from 'src/screens/CreateExperience/ShareSession';
import GiveawayAddSol from 'src/screens/Giveaways/GiveawayAddSol';
import GiveawayCollection from 'src/screens/Giveaways/GiveawayCollection';
import GiveawayCommunity from 'src/screens/Giveaways/GiveawayCommunity';
import SelectNftGiveaway from 'src/screens/Giveaways/SelectNftGiveaway';
import SeriesLandingView from 'src/screens/Series/SeriesLandingView';
import EventsLandingView from 'src/screens/Events/EventsLandingView';
import ShowNFT from 'src/screens/Profile/components/ShowNFT';
import SeriesMembers from 'src/screens/Series/SeriesMembers';
import SettingsScreen from 'src/screens/SettingsScreen';
import TeamScreen from 'src/screens/Team';
import TeamMembers from 'src/screens/Team/TeamMembers';
import UserDetailInfoModal from 'src/screens/UserDetailInfoModal';
import CreateVonageBox from 'src/screens/VonageBoxScreen/CreateVonageBox';
import JoinVonageBox from 'src/screens/VonageBoxScreen/JoinVonageBox';
import Royalties from 'src/screens/Royalties/Royalties';
import WalletConnectScreen from 'src/screens/Login/WalletConnectScreen';
import OneViewProjectScreen from 'src/screens/CreateProject/OneViewProject';
import CreateGiveaways from 'src/screens/Giveaways/CreateGiveaways';
import ListNftUser from 'src/screens/Nft/listNftUser';
import GiveawayMockScreen from 'src/screens/Giveaways/GiveawayMockScreen';
import {useGetNftFromWallet} from 'src/utils/hook/getNftFromWallet';
import {ProgressBar} from 'react-native-paper';
import Colors from 'src/constants/Colors';
import EditSeriesStack from './editSeries';
import Tabs from './tabs';
import {stackConfig} from './config';
import EditChannelsStack from './editChannel';
import ProjectStack from './createProject';
import CreateSessionStack from './createExperience';
import EditEventsStack from './editEvents';
import LaunchMainScreen from 'src/screens/Launch/LaunchMainScreen';

const Stack = createStackNavigator();

function MainStack({navigation}) {
  const {isLogged, initialLink} = useContext(GlobalContext);
  const {setNotifications, currentUserProfile, setChannels} = useContext(FollowersContext);
  const {loadingNft, progress, total} = useGetNftFromWallet(currentUserProfile);

  useEffect(() => {
    if (kitty.currentUser) {
      kitty.getChannels({filter: {joined: true}}).then((result) => {
        setChannels(result.paginator.items);
      });
    }
  }, [kitty.currentUser]);

  const [updateActivities] = useLazyQuery(GET_COUNTER_ACTIVITY, {
    onCompleted({getCounterActivity}) {
      setNotifications(getCounterActivity?.counter);
    },
  });

  useEffect(() => {
    if (currentUserProfile.id) {
      updateActivities();
    }
  }, [updateActivities]);

  const initialParams = useMemo(() => {
    if (initialLink) {
      const matches = initialLink.url.match(/user\/\d+/);
      const userID = matches ? matches[0].replace('user/', '') : null;
      return {userID};
    }
    return {};
  }, [initialLink]);

  useEffect(() => {
    if (navigation.current && !isLogged && initialLink) {
      Alert.alert(UNAUTHORIZED_USER_ERROR_VIEW_TEXT);
      return;
    }
    if (!initialLink) {
      return;
    }

    const matches = initialLink.url.match(/user\/\d+/);
    const userID = matches ? matches[0].replace('user/', '') : null;

    // const matchesProject = initialLink.url.match(/projects\/\d+/);
    const projectsID = matches ? matches[0].replace('projects/', '') : null;

    const matchesTeam = initialLink.url.match(/team\/\d+/);
    const teamID = matchesTeam ? matchesTeam[0].replace('team/', '') : null;

    if (teamID) return navigation.current.navigate('TeamScreen', {id: Number(teamID)});

    if (projectsID) {
      navigation.current.navigate('OneViewProjectScreen', {projectId: projectsID});
    } else {
      Alert.alert(INVALID_DYNAMIC_LINK);
    }

    if (userID) {
      navigation.current.navigate('UserDetailInfoModal', {userID, fade: true});
    } else {
      Alert.alert(INVALID_DYNAMIC_LINK);
    }
  }, [initialLink, isLogged, navigation]);

  useEffect(() => {
    if (navigation.current && !isLogged) {
      const state = navigation.current.getRootState();
      const currentRoute = state.routes[state.index].name;
      if (currentRoute !== 'Auth') {
        navigation.current.reset({index: 0, routes: [{name: 'Auth', screen: 'LoginWithPhone'}]});
      }
    }
  });

  useEffect(
    () =>
      notifee.onForegroundEvent(({detail}) => {
        const {pressAction} = detail;

        if (pressAction?.id === 'reply')
          return navigation.current.navigate(ACTIVITY_KEYS.PRIVATE_CHAT, {id: detail.notification.data.senderId});
      }),
    [],
  );

  if (loadingNft) {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color="#fff" />
        <ProgressBar
          progress={progress / total ? progress / total : 0}
          color={Colors.white}
          style={{width: 200, height: 10, marginVertical: 20}}
        />
        <Text style={{color: '#fff'}}>
          {`${Number.isNaN(Math.round((progress * 100) / total)) ? 0 : Math.round((progress * 100) / total)}%`}
        </Text>
      </View>
    );
  }

  return (
    <Stack.Navigator
      initialRouteName="MainTabs"
      screenOptions={{
        ...stackConfig('Auth'),
        gestureEnabled: false,
      }}>
      <Stack.Screen
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
        }}
        name="MainTabs"
        component={withInAppNotification(Tabs)}
      />

      <Stack.Screen
        name="CreateVonageBox"
        component={CreateVonageBox}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="ProjectStack"
        component={ProjectStack}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="JoinVonageBox"
        component={JoinVonageBox}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="JoinSession"
        component={JoinSession}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="CreateSession"
        component={CreateSessionStack}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        name="FinishSession"
        component={FinishSession}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
        name="ShowNFT"
        component={ShowNFT}
      />
      <Stack.Screen
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
        name="Settings"
        component={SettingsScreen}
      />
      <Stack.Screen
        name="UserDetailInfoModal"
        component={UserDetailInfoModal}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="OneViewProjectScreen"
        component={OneViewProjectScreen}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="SeriesLandingView"
        component={SeriesLandingView}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="EventLandingView"
        component={EventsLandingView}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="ChannelLandingView"
        component={ChannelsLandingView}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="SeriesMembersJoined"
        component={SeriesMembers}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="Royalties"
        component={Royalties}
        initialParams={initialParams}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name={ACTIVITY_KEYS.PRIVATE_CHAT}
        component={Chat}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name={ACTIVITY_KEYS.PRIVATE_CHAT_KITTY}
        component={ChatKitty}
        options={() => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="EditSeriesStack"
        component={EditSeriesStack}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="EditEventStack"
        component={EditEventsStack}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="EditChannelStack"
        component={EditChannelsStack}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen
        name="InviteUsers"
        component={ShareSession}
        options={{
          headerShown: false,
          headerBackTitleVisible: false,
          headerTitleStyle: {display: 'none'},
        }}
      />
      <Stack.Screen name="NameTeam" component={NameTeam} />
      <Stack.Screen name="CreateGiveaways" component={CreateGiveaways} />
      <Stack.Screen name="GiveawayMockScreen" component={GiveawayMockScreen} />
      <Stack.Screen name="SelectNftGiveaway" component={SelectNftGiveaway} />
      <Stack.Screen name="GiveawayAddSol" component={GiveawayAddSol} />
      <Stack.Screen name="GiveawayCollection" component={GiveawayCollection} />
      <Stack.Screen name="GiveawayCommunity" component={GiveawayCommunity} />
      <Stack.Screen name="SelectTopics" component={SelectTopics} />
      <Stack.Screen name="PreviewTeam" component={PreviewTeam} />
      <Stack.Screen name="TeamSettings" component={TeamSettings} />
      <Stack.Screen name="InviteFollowers" component={InviteFollowers} />
      <Stack.Screen name="TeamScreen" component={TeamScreen} />
      <Stack.Screen name="TeamMembers" component={TeamMembers} />
      <Stack.Screen name="ForeignUserProfile" component={UserDetailInfoModal} />
      <Stack.Screen name="UserNftList" component={ListNftUser} />
      <Stack.Screen
        name="CreateSeriesSession"
        component={CreateSessionScreen}
        options={{
          tabBarVisible: false,
        }}
      />
      <Stack.Screen options={{tabBarVisible: false}} name="WalletConnectUser" component={WalletConnectScreen} />
   
      {/*LOUNCHPAD ROUTES*/}
      <Stack.Screen name="Launch" component={LaunchMainScreen} />

   
    </Stack.Navigator>
  );
}

export default MainStack;
