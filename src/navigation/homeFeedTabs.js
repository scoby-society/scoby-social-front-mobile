/* eslint-disable no-use-before-define */
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Fonts, {avenirBold} from 'src/constants/Fonts';
import {useRoute} from '@react-navigation/native';
import Sessions from 'src/components/HomeExperiences';
import colors from '../constants/Colors';

const Tabs = createMaterialTopTabNavigator();

function MyTabBar({state, descriptors, navigation, setCurrentCategory}) {
  const getOptions = (options, route) => {
    if (options.tabBarLabel !== undefined) {
      return options.tabBarLabel;
    }
    if (options.title !== undefined) {
      return options.title;
    }
    return route.name;
  };

  return (
    <View
      style={{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: colors.white,
        borderBottomWidth: 0.5,
        marginTop: 10,
        marginBottom: 5,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label = getOptions(options, route);

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            setCurrentCategory(route.name);
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
          setCurrentCategory(route.name);
          navigation.navigate(route.name);
        };

        return (
          <TouchableOpacity
            key={`${Math.random() + index}`}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            testID={options.tabBarTestID}
            style={{
              paddingHorizontal: 10,
              borderBottomColor: colors.white,
              borderBottomWidth: isFocused ? 1 : 0,
            }}
            onPress={onPress}
            onLongPress={onLongPress}>
            <Text
              style={{
                fontSize: 14,
                color: colors.white,
                fontWeight: isFocused ? '700' : '500',
                fontFamily: Fonts.avenir.fontFamily,
              }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const tabStyle = {
  tabBarStyle: {
    backgroundColor: colors.blueBackgroundSession,
  },
  tabBarLabelStyle: {
    ...avenirBold,
    fontSize: 11,
    textTransform: 'capitalize',
    width: '100%',
  },
  tabBarIndicatorStyle: {
    backgroundColor: colors.white,
  },
  tabBarIndicatorContainerStyle: {
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 0.5,
    height: 40,
  },
  tabBarPressColor: colors.darkBlue,
  tabBarInactiveTintColor: colors.borderGrey,
  tabBarActiveTintColor: colors.white,
};

const HomeFeedTabs = ({setCurrentCategory, itemsSearch}) => {
  const route = useRoute();

  return (
    <Tabs.Navigator
      initialRouteName="All"
      screenOptions={tabStyle}
      tabBar={(props) => <MyTabBar {...props} setCurrentCategory={setCurrentCategory} routes={route} />}>
      <Tabs.Screen
        name="all"
        options={{
          tabBarLabel: 'All',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => <Sessions navigation={navigation} itemsSearch={itemsSearch} />}
      </Tabs.Screen>
      <Tabs.Screen
        name="collection"
        options={{
          tabBarLabel: 'Collections',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => <Sessions navigation={navigation} itemsSearch={itemsSearch} />}
      </Tabs.Screen>
      <Tabs.Screen
        name="experience"
        options={{
          tabBarLabel: 'Experiences',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => <Sessions navigation={navigation} itemsSearch={itemsSearch} />}
      </Tabs.Screen>
      <Tabs.Screen
        name="comunity"
        options={{
          tabBarLabel: 'Communities',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => <Sessions navigation={navigation} itemsSearch={itemsSearch} />}
      </Tabs.Screen>
    </Tabs.Navigator>
  );
};

export default HomeFeedTabs;
