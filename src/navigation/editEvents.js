import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {SeriesProvider} from 'src/utils/SeriesContext';
import ScheduleEvent from 'src/screens/CreateExperience/CreateEvent/ScheduleEvent';
import NameEvent from 'src/screens/CreateExperience/CreateEvent/NameEvent';
import PreviewEvent from 'src/screens/CreateExperience/CreateEvent/PreviewEvent';

const Stack = createStackNavigator();

const EditEventsStack = ({route}) => (
  <SeriesProvider>
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}
      initialRouteName="EditSchedule">
      <Stack.Screen
        name="EditEventSchedule"
        component={ScheduleEvent}
        showLabel={false}
        options={{
          tabBarVisible: false,
        }}
        initialParams={route.params}
      />
      <Stack.Screen
        name="EventName"
        component={NameEvent}
        showLabel={false}
        options={{
          tabBarVisible: false,
        }}
        initialParams={route.params}
      />
      <Stack.Screen
        name="PreviewEvent"
        component={PreviewEvent}
        showLabel={false}
        options={{
          tabBarVisible: false,
        }}
        initialParams={route.params}
      />
    </Stack.Navigator>
  </SeriesProvider>
);

export default EditEventsStack;
