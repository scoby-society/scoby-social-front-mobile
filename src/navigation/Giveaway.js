import {createStackNavigator} from '@react-navigation/stack';
import react from 'react';
import ChooseExperienceSession from 'src/screens/CreateExperience/ChooseExperience';
import {SelectNft} from 'src/screens/CreateExperience/SelectNft';

const Stack = createStackNavigator();

const GiveawayStack = () => (
  <Stack.Navigator
    screenOptions={{
      headerShown: false,
      gestureEnabled: false,
    }}
    initialRouteName="ChoiseExperience">
    <Stack.Screen
      name="ChoiseExperience"
      component={ChooseExperienceSession}
      showLabel={false}
      options={{
        tabBarVisible: false,
      }}
    />
    {/* <Stack.Screen name="SelectNft" component={SelectNft} /> */}
  </Stack.Navigator>
);

export default GiveawayStack;
