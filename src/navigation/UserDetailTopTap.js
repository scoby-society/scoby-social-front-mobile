import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React from 'react';
import ExperiencesForeignUser from 'src/screens/Profile/components/ExperiencesForeignUser';
import {avenirBold} from 'src/constants/Fonts';
import {SHARED_EXPERIENCES} from 'src/constants/Texts';
import NftListProfile from 'src/screens/Nft/NftListProfile';
import colors from '../constants/Colors';

const Tabs = createMaterialTopTabNavigator();

const tabStyle = () => ({
  tabBarStyle: {
    backgroundColor: colors.blueBackgroundSession,
  },
  tabBarLabelStyle: {
    ...avenirBold,
    fontSize: 14,
  },
  tabBarIndicatorStyle: {
    backgroundColor: colors.white,
  },
  tabBarIndicatorContainerStyle: {
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 1,
  },
  tabBarPressColor: colors.darkBlue,
  tabBarInactiveTintColor: colors.borderGrey,
  tabBarActiveTintColor: colors.white,
});

const UserDetailTopTap = ({id, handleClose, setShowHeader = undefined}) => (
  <Tabs.Navigator initialRouteName="Experiences" screenOptions={tabStyle}>
    <Tabs.Screen
      name="Experiences"
      options={{
        tabBarLabel: SHARED_EXPERIENCES,
        tabBarLabelStyle: {
          textTransform: 'capitalize',
        },
      }}>
      {({navigation}) => <ExperiencesForeignUser navigation={navigation} id={id} handleClose={handleClose} />}
    </Tabs.Screen>
    <Tabs.Screen
      name="NFTs"
      options={{
        tabBarLabel: 'NFTs',
        tabBarLabelStyle: {
          textTransform: 'none',
        },
      }}>
      {({navigation}) => <NftListProfile setShowHeader={setShowHeader} navigation={navigation} id={id} />}
    </Tabs.Screen>
  </Tabs.Navigator>
);

export default UserDetailTopTap;
