import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {SeriesProvider} from 'src/utils/SeriesContext';
import PreviewChannel from 'src/screens/CreateExperience/CreateChannel/PreviewChannel';
import NameChannel from 'src/screens/CreateExperience/CreateChannel/NameChannel';

const Stack = createStackNavigator();

const EditChannelsStack = ({route}) => (
  <SeriesProvider>
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: false,
      }}
      initialRouteName="ChannelName">
      <Stack.Screen
        name="ChannelName"
        component={NameChannel}
        showLabel={false}
        options={{
          tabBarVisible: false,
        }}
        initialParams={route.params}
      />
      <Stack.Screen
        name="PreviewChannel"
        component={PreviewChannel}
        showLabel={false}
        options={{
          tabBarVisible: false,
        }}
        initialParams={route.params}
      />
    </Stack.Navigator>
  </SeriesProvider>
);

export default EditChannelsStack;
