/* eslint-disable no-use-before-define */
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {View, Text, TouchableOpacity, FlatList, StyleSheet} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Fonts, {avenirBold} from 'src/constants/Fonts';
import {PAGE, LIMIT} from 'src/utils/pagination';
import {useLazyQuery} from '@apollo/client';
import {GET_PROJECTS_BY_USER} from 'src/graphql/queries/universalSearch';
import {CardHomeProject} from 'src/screens/CreateProject/components/CardHomeProject';
import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../constants/Colors';

const Tabs = createMaterialTopTabNavigator();

const styles = StyleSheet.create({
  btn: {
    borderRadius: 17,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
  },
});

function MyTabBar({state, descriptors, navigation, refetch}) {
  const getOptions = (options, route) => {
    if (options.tabBarLabel !== undefined) {
      return options.tabBarLabel;
    }
    if (options.title !== undefined) {
      return options.title;
    }
    return route.name;
  };

  useEffect(() => {
    refetch(state.routes[state.index].name);
  }, [state.index]);

  return (
    <View
      style={{
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: colors.white,
        borderBottomWidth: 0.5,
        marginTop: 10,
        marginBottom: 5,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label = getOptions(options, route);

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
          refetch(route.name);
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
          refetch(route.name);
        };

        return (
          <TouchableOpacity
            key={`${Math.random() + index}`}
            accessibilityRole="button"
            accessibilityStates={isFocused ? ['selected'] : []}
            testID={options.tabBarTestID}
            style={{
              paddingHorizontal: 10,
              borderBottomColor: colors.white,
              borderBottomWidth: isFocused ? 1 : 0,
            }}
            onPress={onPress}
            onLongPress={onLongPress}>
            <Text
              style={{
                fontSize: 14,
                color: colors.white,
                fontWeight: isFocused ? '700' : '500',
                fontFamily: Fonts.avenir.fontFamily,
              }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const tabStyle = {
  tabBarStyle: {
    backgroundColor: colors.blueBackgroundSession,
  },
  tabBarLabelStyle: {
    ...avenirBold,
    fontSize: 11,
    textTransform: 'capitalize',
    width: '100%',
  },
  tabBarIndicatorStyle: {
    backgroundColor: colors.white,
  },
  tabBarIndicatorContainerStyle: {
    borderBottomColor: colors.borderGrey,
    borderBottomWidth: 0.5,
    height: 40,
  },
  tabBarPressColor: colors.darkBlue,
  tabBarInactiveTintColor: colors.borderGrey,
  tabBarActiveTintColor: colors.white,
};

const Menu = styled.View(() => ({
  display: 'flex',
  width: '100%',
  alignItems: 'flex-end',
  position: 'absolute',
  zIndex: 1,
  paddingVertical: 10,
  paddingRight: 20,
}));

const TextItem = styled.Text({
  ...Fonts.avenir,
  color: colors.white,
});

const MenuButtom = styled.TouchableOpacity({
  width: 80,
  paddingHorizontal: 10,
  paddingVertical: 5,
  elevation: '7',
});

const MenuButtomText = styled.Text({
  ...Fonts.avenir,
  color: colors.white,
  textAlign: 'center',
});

const List = styled.View({
  backgroundColor: 'rgba(255, 255, 255, 0.86)',
  borderRadius: 10,
  paddingLeft: 15,
  position: 'relative',
  elevation: '6',
});

const selectorItems = [
  {
    key: 'All',
    value: 'all',
  },
  {
    key: 'Founder',
    value: 'founder',
  },
  {
    key: 'Joined',
    value: 'joined',
  },
];

const ProfileList = ({setShowHeader, id = null}) => {
  const [select, setSelect] = useState('all');
  const [items, setItems] = useState([]);
  const [, setPage] = useState();
  const [visibleButtom, setVisibleButtom] = useState(false);
  const [menuSelectedButtom, setMenuSelectedButtom] = useState(selectorItems[0]);
  const FlatListRef = useRef(null);

  const [refetch] = useLazyQuery(GET_PROJECTS_BY_USER, {
    fetchPolicy: 'network-only',
    variables: {
      paging: {
        limit: LIMIT,
        page: PAGE,
      },
      category: menuSelectedButtom.value,
      type: select,
      profileId: id || null,
    },
    onCompleted: (data) => {
      setItems(data.getProjectsForUser);
    },
  });

  const handleRefresh = useCallback(
    (category) => {
      setPage(1);
      refetch({
        variables: {
          paging: {
            limit: LIMIT,
            page: PAGE,
          },
          profileId: id || null,
          type: category,
          category: menuSelectedButtom.value,
        },
        onCompleted: (data) => {
          setItems(data.getProjectsForUser);
        },
      });
    },
    [menuSelectedButtom.value, refetch],
  );

  useEffect(() => {
    setPage(1);
    refetch();
  }, [menuSelectedButtom]);

  const TypeSelector = () => (
    <Menu open={visibleButtom}>
      {visibleButtom && (
        <List>
          {selectorItems.map((item, i) => (
            <MenuButtom
              key={`${i + Math.random()}`}
              onPress={() => {
                setVisibleButtom(false);
                setMenuSelectedButtom(item);
              }}>
              <TextItem style={{color: colors.purple, fontWeight: '500', fontFamily: Fonts.avenir.fontFamily}}>
                {item.key}
              </TextItem>
            </MenuButtom>
          ))}
        </List>
      )}
      <LinearGradient
        style={styles.btn}
        colors={['rgba(110, 103, 246, 0.76)', 'rgba(255, 255, 255, 0.09)', 'rgba(135, 130, 235, 0.41)']}
        start={{x: 0, y: 0.5}}
        end={{x: 1, y: 0}}>
        {!visibleButtom && (
          <MenuButtom onPress={() => setVisibleButtom(true)}>
            <MenuButtomText>{menuSelectedButtom.key}</MenuButtomText>
          </MenuButtom>
        )}
      </LinearGradient>
    </Menu>
  );

  const FlatListProjects = useCallback(
    ({navigation}) => (
      <FlatList
        onScroll={() => {
          if (typeof setShowHeader === 'function') {
            setShowHeader(false);
          }
        }}
        data={items || []}
        style={{height: 1500}}
        ref={FlatListRef}
        contentContainerStyle={{zIndex: -1, elevation: -3, paddingTop: 60}}
        ListHeaderComponentStyle={{zIndex: 1, elevation: 4}}
        keyExtractor={(item) => `${item.id}-${Math.random()}`}
        renderItem={(item) => <CardHomeProject dato={item} navigation={navigation} />}
        numColumns={1}
      />
    ),
    [items],
  );

  return (
    <Tabs.Navigator
      initialRouteName="All"
      screenOptions={tabStyle}
      tabBar={(props) => <MyTabBar {...props} setSelect={setSelect} refetch={handleRefresh} />}>
      <Tabs.Screen
        name="all"
        options={{
          tabBarLabel: 'All',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => (
          <>
            <TypeSelector />
            <FlatListProjects navigation={navigation} />
          </>
        )}
      </Tabs.Screen>
      <Tabs.Screen
        name="collection"
        options={{
          tabBarLabel: 'Collections',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => (
          <>
            <TypeSelector />
            <FlatListProjects navigation={navigation} />
          </>
        )}
      </Tabs.Screen>
      <Tabs.Screen
        name="experience"
        options={{
          tabBarLabel: 'Experiences',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => (
          <>
            <TypeSelector />
            <FlatListProjects navigation={navigation} />
          </>
        )}
      </Tabs.Screen>
      <Tabs.Screen
        name="comunity"
        options={{
          tabBarLabel: 'Communities',
          tabBarLabelStyle: tabStyle.tabBarLabelStyle,
        }}>
        {({navigation}) => (
          <>
            <TypeSelector />
            <FlatListProjects navigation={navigation} />
          </>
        )}
      </Tabs.Screen>
    </Tabs.Navigator>
  );
};

export default ProfileList;
