import {gql} from '@apollo/client';

export const SHOW_SPORES = gql`
  mutation showSpores($idSpores: [Int!]!) {
    showSpores(idSpores: $idSpores)
  }
`;

export const HIDE_SPORES = gql`
  mutation hideSpores($idSpores: [Int!]!) {
    hideSpores(idSpores: $idSpores)
  }
`;

export const UPDATE_PROFILE_NFT = gql`
  mutation updateProfileNft($data: [createNft!]) {
    updateProfileNft(data: $data) {
      id
      tokenId
      isVisible
      contractAdress
    }
  }
`;
