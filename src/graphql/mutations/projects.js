import {gql} from '@apollo/client';

export const CREATE_PROJECT = gql`
  mutation ($projectObj: CreateProjectInput!, $coverImage: Upload!, $tile: Upload!) {
    createProject(projectObj: $projectObj, coverImage: $coverImage, tile: $tile) {
      id
      name
      description
      join
      project
      tile
      coverImage
      nft {
        id
        serialNumber
        uri
        urlImage
      }
      user {
        id
        fullName
        avatar
        email
      }
    }
  }
`;

export const JOIN_PROJECT = gql`
  mutation joinProject($projectId: Float!) {
    joinProject(projectId: $projectId) {
      id
      suscribeUsers {
        id
        username
      }
    }
  }
`;

export const LEAVE_PROJECT = gql`
  mutation leaveProject($projectId: Float!) {
    leaveProject(projectId: $projectId) {
      id
      suscribeUsers {
        id
        username
      }
    }
  }
`;

export const EDIT_PROJECT = gql`
  mutation editProject($projectId: Float!, $projectEditObj: projectEdit!, $coverImage: Upload!, $tile: Upload!) {
    editProject(projectId: $projectId, projectEditObj: $projectEditObj, coverImage: $coverImage, tile: $tile) {
      id
      name
      description
      join
      project
      tile
      coverImage
      nft {
        id
        serialNumber
        uri
        urlImage
      }
      user {
        id
        fullName
        avatar
        email
      }
    }
  }
`;

export const INVITE_PROJECT = gql`
  mutation invitedUsersProjects(
    $idProject: Float!
    $invitedUsers: [Int!]!
    $isTeamsMembersInvited: Boolean
    $teamInvitation: [Int!]
  ) {
    invitedUsersProjects(
      idProject: $idProject
      invitedUsers: $invitedUsers
      isTeamsMembersInvited: $isTeamsMembersInvited
      teamInvitation: $teamInvitation
    ) {
      id
    }
  }
`;

export const END_PROJECT = gql`
  mutation endProject($projectId: Float!) {
    endProject(projectId: $projectId) {
      id
    }
  }
`;
