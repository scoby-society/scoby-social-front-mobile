import {gql} from '@apollo/client';

export const CREATE_GIVEAWAY = gql`
  mutation createGiveAway($giveaway: GiveAwayCreation!) {
    createGiveAway(giveaway: $giveaway) {
      id
    }
  }
`;

export const PROCESS_GIVEAWAY = gql`
  mutation userWinnerGiveAway($giveAwayId: Float!, $userId: Float!) {
    userWinnerGiveAway(giveAwayId: $giveAwayId, userId: $userId) {
      id
    }
  }
`;

export const DELETE_GIVEAWAY = gql`
  mutation deleteGiveAway($id: Float!) {
    deleteGiveAway(id: $id) {
      id
    }
  }
`;

export const LEAVE_GIVEAWAY = gql`
  mutation leaveGiveAway($id: Float!) {
    leaveGiveAway(id: $id) {
      id
    }
  }
`;

export const JOIN_GIVEAWAY = gql`
  mutation joinGiveAway($id: Float!) {
    joinGiveAway(id: $id) {
      id
    }
  }
`;
