import {gql} from '@apollo/client';

export const CREATE_CHANNEL = gql`
  mutation createChat($avatar: Upload, $backgroundImage: Upload, $chat: CreateChatExperience!) {
    createChat(avatar: $avatar, backgroundImage: $backgroundImage, chat: $chat) {
      id
      avatar
    }
  }
`;

export const EDIT_CHANNEL = gql`
  mutation editChat($chat: ChatExperienceUpdate!, $backgroundImage: Upload!, $avatar: Upload!, $id: Float!) {
    editChat(chat: $chat, backgroundImage: $backgroundImage, avatar: $avatar, id: $id) {
      id
    }
  }
`;

export const JOIN_CHANNEL = gql`
  mutation joinChatExperience($id: Float!) {
    joinChatExperience(id: $id) {
      id
    }
  }
`;

export const LEAVE_CHANNEL = gql`
  mutation leaveChatExperience($id: Float!) {
    leaveChatExperience(id: $id) {
      id
    }
  }
`;

export const END_CHANNEL = gql`
  mutation endChatExperience($idChat: Float!) {
    endChatExperience(idChat: $idChat) {
      id
    }
  }
`;
