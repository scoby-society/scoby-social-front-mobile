import {gql} from '@apollo/client';

export const BLOCK_USER = gql`
  mutation blockedUser($id: Float!) {
    blockedUser(id: $id) {
      id
    }
  }
`;

export const REPORT_USER = gql`
  mutation createReport($id: Float!) {
    createReport(id: $id) {
      id
    }
  }
`;

export const UN_BLOCK_USER = gql`
  mutation unblockUser($id: Float!) {
    unblockUser(id: $id) {
      id
    }
  }
`;
