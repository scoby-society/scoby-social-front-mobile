import {gql} from '@apollo/client';

export const INVITE_EVENT = gql`
  mutation invitedUserEvent(
    $idEvent: Float!
    $invitedUsers: [Int!]!
    $isTeamsMembersInvited: Boolean
    $teamInvitation: [Int!]
  ) {
    invitedUserEvent(
      idEvent: $idEvent
      invitedUsers: $invitedUsers
      isTeamsMembersInvited: $isTeamsMembersInvited
      teamInvitation: $teamInvitation
    ) {
      id
    }
  }
`;

export const CREATE_EVENT = gql`
  mutation createEvent($avatar: Upload!, $backgroundImage: Upload!, $event: EventCreation!) {
    createEvent(avatar: $avatar, backgroundImage: $backgroundImage, event: $event) {
      id
      avatar
      backgroundImage
      createdAt
      day
      description
      end
      finishedAt
      ownerUser {
        avatar
        fullName
        username
      }
      session {
        id
      }
      start
      suscribeUsers {
        avatar
        backgroundImage
      }
      title
      topics {
        icon
        id
        name
      }
      updatedAt
    }
  }
`;

export const CREATE_SESSION_EVENT = gql`
  mutation liveEvent($idEvent: Float!) {
    liveEvent(idEvent: $idEvent) {
      session {
        createdAt
        title
        description
        finishedAt
        id
        vonageSessionToken
        secondScreenLink
        topics {
          id
          name
          icon
        }
        ownerUser {
          id
          username
          fullName
          location
          avatar
          vonageUserToken
        }
        participantUsers {
          id
          username
          fullName
          location
          avatar
        }
        viewerUsers {
          id
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          fullName
          location
          phone
          updatedAt
          username
          website
        }
        updatedAt
      }
      token
      vonageApiToken
    }
  }
`;

export const EDIT_EVENTS = gql`
  mutation editEvent($avatar: Upload!, $backgroundImage: Upload!, $event: EventUpdate!, $id: Float!) {
    editEvent(avatar: $avatar, backgroundImage: $backgroundImage, event: $event, id: $id) {
      id
    }
  }
`;

export const DELETE_EVENT = gql`
  mutation endEvent($idEvent: Float!) {
    endEvent(idEvent: $idEvent) {
      id
    }
  }
`;

export const JOIN_EVENT = gql`
  mutation joinEvent($id: Float!) {
    joinEvent(id: $id) {
      id
    }
  }
`;

export const LEAVE_EVENT = gql`
  mutation leaveEvent($id: Float!) {
    leaveEvent(id: $id) {
      id
    }
  }
`;
