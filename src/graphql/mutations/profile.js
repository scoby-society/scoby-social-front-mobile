import {gql} from '@apollo/client';

export const UPDATE_PROFILE = gql`
  mutation updateUserProfile($profile: UserProfileEdit!) {
    updateUserProfile(profile: $profile) {
      avatar
      bio
      fullName
      username
      location
      website
      shipping
      hatSize
      shirtSize
      email
      publicKey
      topics {
        id
      }
      id
      backgroundImage
      birthday
      createdAt
      phone
      updatedAt
      role
      followStats {
        followingCurrentUser
        followedByCurrentUser
      }
      followCounts {
        followers
        following
      }
    }
  }
`;

export const FLAG = gql`
  mutation markUserInappropriate($userId: Float!) {
    markUserInappropriate(userId: $userId) {
      id
    }
  }
`;

export const UPLOAD_AVATAR = gql`
  mutation ($avatar: Upload!) {
    uploadFile(avatar: $avatar) {
      avatar
    }
  }
`;

export const UPLOAD_COVER = gql`
  mutation ($backgroundImage: Upload!) {
    uploadFile(backgroundImage: $backgroundImage) {
      backgroundImage
    }
  }
`;

export const DELETE_ACCOUNT = gql`
  mutation {
    deleteUser
  }
`;
