import {gql} from '@apollo/client';

export const GET_EVENT_AND_GIVEAWAY = gql`
  query getEventAndGiveAway($idEvent: Float!) {
    getEventAndGiveAway(idEvent: $idEvent) {
      event {
        id
        title
        description
        finishedAt
        ownerUser {
          id
        }
      }
      giveAway {
        id
        name
        description
        finishedAt
        giveawayType
        nfts
        suscribeUsers {
          id
          avatar
          fullName
          username
          bio
          publicKey
        }
        ownerUser {
          id
          avatar
          publicKey
        }
        collections {
          title
          avatarCreator
          cardProjectAvatar
          cardProjectDescription
          id
        }
        community {
          avatar
          name
          description
          ownerUser {
            fullName
            username
          }
          participantUsers {
            id
          }
          topics {
            name
            icon
          }
        }
        userWinner {
          id
          avatar
          fullName
          username
          bio
          publicKey
        }
      }
    }
  }
`;

export const GET_NFT_BY_ID_PAGING = gql`
  query getNftByUserPaging($id: Float!, $paging: PagingInput) {
    getNftByUserPaging(id: $id, paging: $paging) {
      id
      name
      serialNumber
      urlImage
      contractAdress
      tokenId
    }
  }
`;

export const GET_POOLS_FOR_COLLECTIONS = gql`
  query getPools {
    getPools {
      id
      title
      avatarCreator
    }
  }
`;

export const GET_USERS_TEAMS = gql`
  query getUsersTeams($paging: PagingInput!, $query: String) {
    getUsersTeams(paging: $paging, query: $query) {
      data {
        id
        name
        avatar
        description
        membersAllowedToInvite
        topics {
          icon
          id
          name
        }
        ownerUser {
          avatar
          backgroundImage
          fullName
          id
          username
        }
        members {
          id
          isAccepted
          user {
            avatar
            backgroundImage
            bio
            email
            fullName
            id
            role
            username
            website
            location
            followCounts {
              followers
              following
            }
            followStats {
              followingCurrentUser
              followedByCurrentUser
            }
          }
        }
      }
      paging {
        limit
        page
        total
      }
    }
  }
`;

export const GET_NFT_ID = gql`
  query getSporeDsById($id: String) {
    getSporeDsById(id: $id) {
      contractAdress
    }
  }
`;
