import {gql} from '@apollo/client';

export const GET_NFT_BY_ID = gql`
  query getNftByUser($id: Float!, $collection: String) {
    getNftByUser(id: $id, collection: $collection) {
      id
      serialNumber
      uri
      typeCollections
      contractAdress
      urlImage
      isVisible
      symbol
      user {
        id
        avatar
      }
      description
      name
      walletAdress
    }
  }
`;

export const GET_SPORES_DATA_SCIENCES = gql`
  query getAllSporesDataScience {
    getAllSporesDataScience {
      contractAdress
      id
      name
      symbol
      serialNumber
      uri
      urlImage
      user {
        avatar
        fullName
        bio
      }
      walletAdress
    }
  }
`;
