import {gql} from '@apollo/client';

export const NETWORK_ROYALTIES = gql`
  query getSocialNetworks {
    getSocialNetworks {
      id
      creatorRoyalties
      connectorRoyalties
      invitationSent
      invitationsRequested
      totalRoyalties
      ownerUser {
        id
        avatar
        fullName
        username
      }
    }
  }
`;

export const GET_ALL_POOL=gql`
  query getPools{
    getPools{
      id
      avatarCreator
      pool
      title
      symbol
    }
  }

`

export const MY_NETWORK_DATA = gql`
query getMemberNetworkById($id:Float!){ 
  getMemberNetworkById(id:$id){
   totalContribution
   userContribudor{
   id
   avatar
   username 
   fullName 
  }
  invitationsRequest
  invitationsSend
  asCollector
  asConnector
  asCreator
  }
}`;
