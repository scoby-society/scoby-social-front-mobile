import {gql} from '@apollo/client';

export const SEARCH_EVENT = gql`
  query getSearchEvent($query: String!) {
    getSearchEvent(query: $query) {
      id
      title
      backgroundImage
      description
      eventType
      eventType
      avatar
      topics {
        id
        name
        icon
      }
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        followCounts {
          followers
          following
        }
        followStats {
          followedByCurrentUser
          followingCurrentUser
        }
        fullName
        hatSize
        website
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        updatedAt
        username
        vonageUserToken
      }
    }
  }
`;

export const SEARCH_COLLECTIONS = gql`
  query getSearchCollection($query: String!, $paging: PagingInput) {
    getSearchCollection(query: $query, paging: $paging) {
      id
      title
      description
      avatar
      tile
      linkWebsite
      typeData
      topics {
        id
        name
        icon
      }
      nft {
        id
        webSite
        background
        blockChain
        contractAdress
        creator
        description
        effect
        isVisible
        metaData
        name
        serialNumber
        symbol
        tokenId
        typeCollections
        uri
        urlImage
        user {
          id
        }
        walletAdress
      }
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        followCounts {
          followers
          following
        }
        followStats {
          followedByCurrentUser
          followingCurrentUser
        }
        fullName
        hatSize
        website
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        updatedAt
        username
        vonageUserToken
      }
    }
  }
`;

export const SEARCH_MEMBERS = gql`
  query getSearchMembers($query: String!) {
    getSearchMembers(query: $query) {
      id
    }
  }
`;

export const SEARCH_SERIE = gql`
  query getSearchSerie($query: String!) {
    getSearchSerie(query: $query) {
      id
      serieType
      seriesName
      description
      avatar
      schedule {
        day
        end
        start
      }
      topics {
        id
        name
        icon
      }
      session {
        vonageSessionToken
      }
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        followCounts {
          followers
          following
        }
        followStats {
          followedByCurrentUser
          followingCurrentUser
        }
        fullName
        hatSize
        website
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        updatedAt
        username
        vonageUserToken
      }
    }
  }
`;

export const SEARCH_SESSIONS = gql`
  query getSearchSession($query: String) {
    getSearchSession(query: $query) {
      id
      isAvailable
      isPrivate
      title
      description
      createdAt
      finishedAt
      updatedAt
      vonageSessionToken
      viewers
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      participantUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      viewerUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      greenRoomUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      topics {
        icon
        id
        name
      }
    }
  }
`;

export const SEARCH_COMMUNITIES = gql`
  query getSearchTeam($query: String!, $paging: PagingInput) {
    getSearchTeam(query: $query, paging: $paging) {
      id
      typeData

      description
      avatar

      topics {
        id
        name
        icon
      }
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        followCounts {
          followers
          following
        }
        followStats {
          followedByCurrentUser
          followingCurrentUser
        }
        fullName
        hatSize
        website
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        updatedAt
        username
        vonageUserToken
      }
    }
  }
`;

export const HOME_PROJECT_FEED = gql`
  query getHomeProjectFeed($paging: PagingInput, $category: String) {
    getHomeProjectFeed(paging: $paging, category: $category) {
      linkWebsite
      isAvailable
      vonageSessionToken
      isPrivate
      subscribed
      viewers
      viewerUsers {
        id
        email
      }
      typeData
      typeExperience
      id
      avatar
      title
      name
      tile
      schedule {
        day
        end
        id
        start
      }
      description
      calendarName
      className
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        followCounts {
          followers
          following
        }
        followStats {
          followedByCurrentUser
          followingCurrentUser
        }
        fullName
        hatSize
        website
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        updatedAt
        username
        vonageUserToken
      }
      suscribeUsers {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        fullName
        hatSize
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        topics {
          icon
          id
          name
        }
        updatedAt
        username
        vonageUserToken
        website
      }
      topics {
        id
        name
        icon
      }
      teamType
      eventType
      end
      start
      greenRoomUsers {
        id
      }
      day
      nft {
        id
        webSite
        background
        blockChain
        contractAdress
        creator
        description
        effect
        isVisible
        metaData
        name
        serialNumber
        symbol
        tokenId
        typeCollections
        uri
        urlImage
        user {
          id
        }
        walletAdress
      }
      projects {
        id
        name
        coverImage
        description
        join
        project
        tile
        finishedAt
      }
      serieType
      seriesName
    }
  }
`;

export const GET_PROJECTS_BY_USER = gql`
  query getProjectsForUser($paging: PagingInput, $category: String, $type: String, $profileId: Int) {
    getProjectsForUser(paging: $paging, category: $category, type: $type, profileId: $profileId) {
      name
      tile
      coverImage
      join
      project
      linkWebsite
      isAvailable
      isPrivate
      viewerUsers {
        id
        email
      }
      typeData
      typeExperience
      id
      avatar
      title
      typeExperience
      description
      calendarName
      className
      suscribeUsers {
        id
        fullName
      }
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        email
        followCounts {
          followers
          following
        }
        followStats {
          followedByCurrentUser
          followingCurrentUser
        }
        fullName
        hatSize
        website
        location
        phone
        publicKey
        role
        shipping
        shirtSize
        updatedAt
        username
        vonageUserToken
      }
      topics {
        id
        name
        icon
      }
      teamType
      eventType
      end
      start
      greenRoomUsers {
        id
      }
      day
      nft {
        id
        webSite
        background
        blockChain
        contractAdress
        creator
        description
        effect
        isVisible
        metaData
        name
        serialNumber
        symbol
        tokenId
        typeCollections
        uri
        urlImage
        user {
          id
        }
        walletAdress
      }
      user {
        id
        fullName
      }
      finishedAt
      projects {
        id
        name
        coverImage
        description
        join
        project
        tile
        finishedAt
        suscribeUsers {
          id
          fullName
        }
        user {
          id
        }
      }
    }
  }
`;
