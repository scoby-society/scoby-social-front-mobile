import {gql} from '@apollo/client';

export const GET_LIVE_SESSIONS = gql`
  query getLiveSessionsPaging($paging: PagingInput) {
    getLiveSessionsPaging(paging: $paging) {
      id
      isAvailable
      isPrivate
      title
      description
      createdAt
      finishedAt
      updatedAt
      vonageSessionToken
      viewers
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      participantUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      viewerUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      greenRoomUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      topics {
        icon
        id
        name
      }
    }
  }
`;

export const GET_LIVE_SESSION = gql`
  query getSession($sessionId: Float!) {
    getSession(id: $sessionId) {
      id
      title
      isAvailable
      description
      createdAt
      finishedAt
      updatedAt
      vonageSessionToken
      viewers
      ownerUser {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      participantUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      viewerUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      greenRoomUsers {
        id
        avatar
        backgroundImage
        bio
        createdAt
        fullName
        location
        phone
        updatedAt
        username
        website
      }
      topics {
        icon
        id
        name
      }
    }
  }
`;
