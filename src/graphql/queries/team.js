import {gql} from '@apollo/client';

export const GET_TEAM = gql`
  query getTeam($teamId: Int!) {
    getTeam(teamId: $teamId) {
      avatar
      backgroundImage
      createdAt
      description
      id
      linkWebsite
      teamType
      membersAllowedToHost
      membersAllowedToInvite
      members {
        id
        isAccepted
        user {
          avatar
          backgroundImage
          bio
          email
          fullName
          id
          role
          username
          website
          location
          followCounts {
            followers
            following
          }
          followStats {
            followingCurrentUser
            followedByCurrentUser
          }
        }
      }
      name
      ownerUser {
        id
        avatar
        username
        fullName
        email
      }
      participantUsers {
        avatar
        username
        fullName
        email
      }
      pendingUsers {
        id
      }
      topics {
        icon
        id
        name
      }
    }
  }
`;

export const GET_USERS_TEAMS = gql`
  query getUsersTeams($paging: PagingInput!, $query: String) {
    getUsersTeams(paging: $paging, query: $query) {
      data {
        id
        name
        avatar
        description
        membersAllowedToInvite
        topics {
          icon
          id
          name
        }
        members {
          id
          isAccepted
          user {
            avatar
            backgroundImage
            bio
            email
            fullName
            id
            role
            username
            website
            location
            followCounts {
              followers
              following
            }
            followStats {
              followingCurrentUser
              followedByCurrentUser
            }
          }
        }
        ownerUser {
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          email
          followCounts {
            followers
            following
          }
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
          fullName
          id
          location
          phone
          role
          topics {
            icon
            id
            name
          }
          updatedAt
          username
          vonageUserToken
          website
        }
        teamType
        pendingUsers {
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          email
          followCounts {
            followers
            following
          }
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
          fullName
          id
          location
          phone
          role
          topics {
            icon
            id
            name
          }
          updatedAt
          username
          vonageUserToken
          website
        }
      }
      paging {
        limit
        page
        total
      }
    }
  }
`;

export const GET_TEAM_USERS_TO_INVITE_BY_NFT = gql`
query getUsersTeamsToInviteNft($nftsRequired:[Int!]!, $paging:PagingInput!, $query:String){
  getUsersTeamsToInviteNft(nftsRequired:$nftsRequired, paging:$paging, query:$query){
    data {
        id
        name
        avatar
        isNft
        membersAllowedToInvite
        members {
          id
          isAccepted
          user {
            avatar
            backgroundImage
            bio
            email
            fullName
            id
            role
            username
            website
            location
            followCounts {
              followers
              following
            }
            followStats {
              followingCurrentUser
              followedByCurrentUser
            }
          }
        }
      }
      paging {
        limit
        page
        total
      }
    }
  }
`

export const GET_TEAM_USERS_TO_INVITE = gql`
  query getUsersTeamsToInvite($paging: PagingInput!, $query: String) {
    getUsersTeamsToInvite(paging: $paging, query: $query) @connection(key: "data", filter: ["query"]) {
      data {
        id
        name
        avatar
        membersAllowedToInvite
        members {
          id
          isAccepted
          user {
            avatar
            backgroundImage
            bio
            email
            fullName
            id
            role
            username
            website
            location
            followCounts {
              followers
              following
            }
            followStats {
              followingCurrentUser
              followedByCurrentUser
            }
          }
        }
      }
      paging {
        limit
        page
        total
      }
    }
  }
`;

export const GET_TEAM_USERS_TO_INVITE_IDS = gql`
  query getUsersTeamsToInvite($paging: PagingInput!, $query: String) {
    getUsersTeamsToInvite(paging: $paging, query: $query) @connection(key: "data", filter: ["query"]) {
      paging {
        limit
        page
        total
      }
    }
  }
`;
