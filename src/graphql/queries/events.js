import {gql} from '@apollo/client';

export const GET_LIVE_EVENTS = gql`
  query getLiveEventsPaging($paging: PagingInput) {
    getLiveEventsPaging(paging: $paging) {
      id
      isAvailable
      subscribed
      viewers
      description
      backgroundImage
      avatar
      start
      end
      day
      title
      createdAt
      updatedAt
      finishedAt
      nftsRequired
      ownerUser {
        id
        avatar
        fullName
        username
      }
      topics {
        id
        icon
        name
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
        followStats {
          followingCurrentUser
          followedByCurrentUser
        }
      }
      session {
        id
        vonageSessionToken
        title
        description
      }
    }
  }
`;
export const GET_EVENTS = gql`
  query getEventsPaging($paging: PagingInput) {
    getEventsPaging(paging: $paging) {
      id
      isAvailable
      subscribed
      end
      start
      day
      title
      description
      backgroundImage
      avatar
      createdAt
      updatedAt
      finishedAt
      nftsRequired
      ownerUser {
        id
        avatar
        fullName
        username
      }
      topics {
        id
        icon
        name
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
        followStats {
          followingCurrentUser
          followedByCurrentUser
        }
      }
    }
  }
`;

export const GET_EVENT_BY_ID = gql`
  query getEventbyId($id: Float!) {
    getEventbyId(id: $id) {
      avatar
      backgroundImage
      day
      description
      end
      id
      isAvailable
      nftsRequired
      projects {
        id
        name
        tile
        coverImage
      }
      session {
        id
        vonageSessionToken
        title
        description
      }
      ownerUser {
        avatar
        fullName
        id
        username
      }
      start
      subscribed
      suscribeUsers {
        id
        avatar
        fullName
        username
        followStats {
          followingCurrentUser
          followedByCurrentUser
        }
      }
      title
      topics {
        icon
        id
        name
      }
    }
  }
`;
