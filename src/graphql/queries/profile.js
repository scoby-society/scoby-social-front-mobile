import {gql} from '@apollo/client';

export const GET_USER_PROFILE = gql`
  query getUserProfile($id: Float) {
    getUserProfile(id: $id) {
      id
      avatar
      role
      backgroundImage
      bio
      birthday
      createdAt
      fullName
      id
      location
      phone
      updatedAt
      username
      website
      email
      publicKey
      role
      followStats {
        followingCurrentUser
        followedByCurrentUser
      }
      followCounts {
        followers
        following
      }
      topics {
        id
      }
    }
  }
`;
export const GET_TOPICS = gql`
  query getTopics {
    getTopics {
      id
      name
      icon
    }
  }
`;

export const GET_USER_EVENTS = gql`
  query getUserEvents($id: Float) {
    getUserEvents(id: $id) {
      avatar
      backgroundImage
      id
      day
      end
      start
      title
      subscribed
      eventType
      session {
        id
        vonageSessionToken
        title
        description
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
      }
      ownerUser {
        avatar
        fullName
        username
        id
      }
      topics {
        icon
        id
        name
      }
      description
    }
  }
`;

export const GET_USER_SERIES = gql`
  query getUserSeries($id: Float) {
    getUserSeries(id: $id) {
      id
      calendarName
      subscribed
      className
      seriesName
      serieType
      schedule {
        day
        end
        start
      }
      description
      backgroundImage
      avatar
      createdAt
      updatedAt
      finishedAt
      session {
        id
        vonageSessionToken
        title
        description
      }
      ownerUser {
        id
        avatar
        fullName
        username
      }
      topics {
        id
        icon
        name
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
        followStats {
          followingCurrentUser
          followedByCurrentUser
        }
      }
    }
  }
`;

export const GET_ACTIVITY = gql`
  query getActivity($paging: PagingInput) {
    getActivity(paging: $paging) @connection(key: "data") {
      data {
        id
        createdAt
        procedure_action
        additionalPayload
        sourceUser {
          id
          avatar
          fullName
          role
          username
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
        }
        type_action
      }
      paging {
        page
        limit
        total
      }
    }
  }
`;

export const GET_USERS = gql`
  query getUsers($paging: PagingInput, $query: String) {
    getUsers(paging: $paging, query: $query) @connection(key: "data", filter: ["query"]) {
      data {
        id
        avatar
        backgroundImage
        bio
        birthday
        createdAt
        fullName
        id
        location
        phone
        updatedAt
        username
        website
        role
        topics {
          id
        }
        followStats {
          followingCurrentUser
          followedByCurrentUser
        }
        followCounts {
          followers
          following
        }
      }
      paging {
        page
        limit
        total
      }
      query
    }
  }
`;

export const GET_TEAMS = gql`
  query getAllTeams($paging: PagingInput!, $query: String) {
    getAllTeams(paging: $paging, query: $query) @connection(key: "data", filter: ["query"]) {
      data {
        teamType
        name
        avatar
        id
        ownerUser {
          id
        }
        members {
          isAccepted
          user {
            id
          }
        }
        pendingUsers {
          id
        }
      }
      paging {
        page
        limit
        total
      }
    }
  }
`;

export const GET_NFTS_BY_USER = gql`
  query getNftByUser($id: Float!) {
    getNftByUser(id: $id) {
      name
      contractAdress
      isVisible
      serialNumber
      tokenId
      uri
      urlImage
      walletAdress
      background
      blockChain
      creator
      description
      effect
      metaData
      symbol
      webSite
      user {
        id
        fullName
        id
        avatar
        username
        website
        email
        publicKey
      }
    }
  }
`;

export const GET_NFTS_BY_WALLET = gql`
  query getNftByWallet($walletAdress: String!) {
    getNftByWallet(walletAdress: $walletAdress) {
      name
      contractAdress
      isVisible
      serialNumber
      tokenId
      uri
      urlImage
      walletAdress
      background
      blockChain
      creator
      description
      effect
      metaData
      symbol
      webSite
      user {
        id
        fullName
        id
        avatar
        username
        website
        email
        publicKey
      }
    }
  }
`;
