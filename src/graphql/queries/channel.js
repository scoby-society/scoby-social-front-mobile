import {gql} from '@apollo/client';

export const GET_CHANNELS_PAGING = gql`
  query getChatPaging($paging: PagingInput) {
    getChatPaging(paging: $paging) {
      id
      description
      title
      avatar
      backgroundImage
      chatType
      createdAt
      finishedAt
      isAvailable
      nft {
        background
        id
      }
      nftsRequired
      ownerUser {
        avatar
        id
      }
      subscribed
      suscribeUsers {
        avatar
        id
      }
      topics {
        icon
        name
        id
      }
      updatedAt
      viewers
    }
  }
`;

export const GET_CHANNELS = gql`
  query getAllChatExperience {
    getAllChatExperience {
      id
      chatType
      title
      avatar
      backgroundImage
      topics {
        icon
        id
        name
      }
      updatedAt
      createdAt
      description
      finishedAt
      isAvailable
      nft {
        id
      }
      nftsRequired
      ownerUser {
        id
        avatar
        fullName
        username
      }
      subscribed
      suscribeUsers {
        id
        avatar
        fullName
        username
      }
    }
  }
`;

export const GET_CHANNEL_BY_ID = gql`
  query getChatById($id: Float!) {
    getChatById(id: $id) {
      id
      chatType
      title
      avatar
      backgroundImage
      topics {
        icon
        id
        name
      }
      updatedAt
      createdAt
      description
      finishedAt
      isAvailable
      nft {
        id
      }
      projects {
        id
        name
        tile
        coverImage
      }
      nftsRequired
      ownerUser {
        id
        avatar
        fullName
        username
      }
      subscribed
      suscribeUsers {
        id
        avatar
        fullName
        username
      }
    }
  }
`;
