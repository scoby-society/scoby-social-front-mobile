import {gql} from '@apollo/client';

export const LOGIN_USER = gql`
  query loginUser($password: String!, $phone: String!) {
    loginUser(password: $password, phone: $phone) {
      authorizationToken
    }
  }
`;

export const GET_HOST_USER = gql`
  query getHostUser($id: Float!) {
    getHostUser(id: $id) {
      id
      username
      fullName
      avatar
      backgroundImage
      host
      bio
      totalRoyalties
      publicKey
    }
  }
`;
