import {gql} from '@apollo/client';

export const GET_BLOCK_USERS = gql`
  query getBlockUser($id: Float) {
    getBlockUser(id: $id) {
      id
      targetUser {
        username
        fullName
        id
      }
    }
  }
`;

export const GET_TARGET_USERS = gql`
  query getTargetBlockUser($id: Float) {
    getTargetBlockUser(id: $id) {
      id
      sourceUser {
        id
        username
      }
      targetUser {
        id
        username
      }
    }
  }
`;
