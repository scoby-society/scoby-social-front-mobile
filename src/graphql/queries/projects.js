import {gql} from '@apollo/client';

export const GET_ALL_PROJECT = gql`
  query getAllProjects {
    getAllProjects {
      id
      name
      description
      tile
      coverImage
      project
      join
      user {
        email
      }
      nft {
        id
        name
        serialNumber
        walletAdress
        urlImage
        symbol
      }
    }
  }
`;

export const GET_PROJECT_BY_ID = gql`
  query getProjectById($projectId: Float!) {
    getProjectById(projectId: $projectId) {
      id
      name
      description
      coverImage
      tile
      ownerProject
      suscribeProject
      join
      project
      user {
        id
        email
        avatar
        username
      }
      nft {
        id
        serialNumber
        walletAdress
        symbol
        name
        description
        isVisible
        uri
        urlImage
        serialNumber
        contractAdress
      }
      suscribeUsers {
        id
        email
        avatar
      }
      team {
        id
        avatar
        backgroundImage
        description
        name
        topics {
          icon
          id
          name
        }
        ownerUser {
          avatar
          backgroundImage
          birthday
          createdAt
          email
          fullName
          hatSize
          id
          location
          phone
          publicKey
          role
          shipping
          shirtSize
          topics {
            icon
            id
            name
          }
          updatedAt
          username
          vonageUserToken
          website
        }
        teamType
      }
      experience {
        id
        name
        avatar
        isAvailable
        backgroundImage
        calendarName
        title
        vonageSessionToken
        className
        topics {
          icon
          id
          name
        }
        participantUsers {
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          email
          followCounts {
            followers
            following
          }
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
          fullName
          hatSize
          location
          phone
          publicKey
          role
          shipping
          shirtSize
          updatedAt
          username
          vonageUserToken
          website
          topics {
            icon
            id
            name
          }
        }
        seriesName
        start
        subscribed
        suscribeUsers {
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          email
          followCounts {
            followers
            following
          }
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
          fullName
          hatSize
          location
          id
          website
          vonageUserToken
          username
          updatedAt
          shirtSize
          shipping
          role
          publicKey
          phone
          topics {
            icon
            id
            name
          }
        }
        viewers
        viewerUsers {
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          email
          followCounts {
            followers
            following
          }
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
          fullName
          hatSize
          id
          location
          phone
          publicKey
          role
          shipping
          shirtSize
        }
        createdAt
        day
        description
        end
        typeExperience
        serieType
        sessionType
        eventType
        finishedAt
        greenRoomUsers {
          avatar
          backgroundImage
          bio
          birthday
          createdAt
          email
          followCounts {
            followers
            following
          }
          followStats {
            followedByCurrentUser
            followingCurrentUser
          }
          fullName
          hatSize
          location
          phone
          publicKey
          role
          shipping
          shirtSize
          topics {
            icon
            id
            name
          }
        }
      }
      ownerProject
      suscribeProject
    }
  }
`;

export const GET_PROJECTS_BY_USER = gql`
  query getProjectsForUser {
    getProjectsForUser {
      id
      name
      description
      tile
      project
      user {
        email
      }
      nft {
        id
        serialNumber
        walletAdress
        symbol
        name
        description
      }
      session {
        id
      }
    }
  }
`;
