import {gql} from '@apollo/client';

export const GET_LIVE_SERIES = gql`
  query getLiveSeriePaging($paging: PagingInput) {
    getLiveSeriePaging(paging: $paging) {
      id
      isAvailable
      subscribed
      calendarName
      className
      seriesName
      nftsRequired
      schedule {
        day
        end
        start
      }
      session {
        id
        vonageSessionToken
        title
        description
      }
      description
      backgroundImage
      avatar
      createdAt
      updatedAt
      finishedAt
      ownerUser {
        id
        avatar
        fullName
        username
      }
      topics {
        id
        icon
        name
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
      }
    }
  }
`;

export const GET_SERIES = gql`
  query getSeriePaging($paging: PagingInput) {
    getSeriePaging(paging: $paging) {
      id
      isAvailable
      subscribed
      calendarName
      className
      seriesName
      nftsRequired
      schedule {
        day
        end
        start
      }
      description
      backgroundImage
      avatar
      createdAt
      updatedAt
      finishedAt
      ownerUser {
        id
        avatar
        fullName
        username
      }
      topics {
        id
        icon
        name
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
      }
      session {
        id
        vonageSessionToken
        title
        description
      }
    }
  }
`;

export const GET_SERIE_BY_ID = gql`
  query getSerieById($id: Float!) {
    getSerieById(id: $id) {
      id
      isAvailable
      subscribed
      calendarName
      className
      seriesName
      nftsRequired
      schedule {
        day
        end
        start
      }
      description
      backgroundImage
      avatar
      createdAt
      updatedAt
      finishedAt
      projects {
        id
        name
        tile
        coverImage
      }
      session {
        id
        vonageSessionToken
        title
        description
      }
      session {
        id
        vonageSessionToken
        title
        description
      }
      ownerUser {
        id
        avatar
        fullName
        username
      }
      topics {
        id
        icon
        name
      }
      suscribeUsers {
        id
        avatar
        fullName
        username
        followStats {
          followingCurrentUser
          followedByCurrentUser
        }
      }
    }
  }
`;
