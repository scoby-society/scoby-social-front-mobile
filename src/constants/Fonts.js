import {Platform} from 'react-native';

export const goudy = Platform.select({
  android: {
    fontFamily: `Goudy-Heavyface-Regular`,
    fontWeight: 'normal',
  },
  default: {
    fontFamily: 'Goudy Heavyface',
    fontWeight: '400',
  },
});

export const avenir = Platform.select({
  android: {
    fontFamily: 'AvenirNext-Regular',
    fontWeight: 'normal',
  },
  default: {
    fontFamily: 'Avenir Next LT Pro',
    fontWeight: '400',
  },
});

export const avenirSemiBold = Platform.select({
  android: {
    fontFamily: `AvenirNext-Regular`,
    fontWeight: 'normal',
  },
  default: {
    fontFamily: 'Avenir Next LT Pro',
    fontWeight: '600',
  },
});

export const avenirBold = Platform.select({
  android: {
    fontFamily: `AvenirNext-Bold`,
    fontWeight: 'normal',
  },
  default: {
    fontFamily: 'Avenir Next LT Pro',
    fontWeight: '700',
  },
});

export const ttNormsBold = Platform.select({
  android: {
    fontFamily: 'TTNorms-Bold',
    fontWeight: 'normal',
  },
  default: {
    fontFamily: 'TTNorms-Bold',
    fontWeight: '700',
  },
});

export const ttNormsRegular = Platform.select({
  android: {
    fontFamily: `TTNorms-Regular`,
    fontWeight: 'normal',
  },
  default: {
    fontFamily: 'TTNormsPro-Regular',
    fontWeight: '400',
  },
});

export default {goudy, avenir, avenirSemiBold, avenirBold, ttNormsRegular, ttNormsBold};
