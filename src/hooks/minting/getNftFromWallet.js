/* eslint-disable indent */
import {clusterApiUrl} from '@solana/web3.js';
import {CLUSTER, TOKEN_METADATA} from 'src/config/env';
import * as anchor from 'src/dist/browser/index';
import {TOKEN_PROGRAM_ID} from '@project-serum/anchor/dist/cjs/utils/token';
import {programs} from '@metaplex/js';
import {useMutation} from '@apollo/client';
import {UPDATE_PROFILE_NFT} from 'src/graphql/mutations/NFT';

const {useEffect, useState, useCallback} = require('react');

const TOKEN_METADATA_PROGRAM_ID = new anchor.web3.PublicKey(TOKEN_METADATA);
const {
  metadata: {Metadata},
} = programs;

const splitArrayIntoBatches = (arr, limit) =>
  arr.reduce((memo, item) => {
    if (memo.length && memo[memo.length - 1].length < limit) {
      memo[memo.length - 1].push(item);
      return memo;
    }
    return [...memo, [item]];
  }, []);

const pact = (tasks, concurrency, interval = 0, failFast = true) => {
  const processBatches = (batches, prevResults = []) => {
    if (!batches.length) {
      return Promise.resolve(prevResults);
    }

    return Promise.all(batches[0].map((fn) => (failFast ? fn() : fn().catch((err) => err)))).then((batchResults) => {
      const results = [...prevResults, ...batchResults];
      return batches.length <= 1
        ? results
        : new Promise((resolve, reject) =>
            setTimeout(() => processBatches(batches.slice(1), results).then(resolve, reject), interval),
          );
    });
  };

  return processBatches(splitArrayIntoBatches(tasks, concurrency));
};

export const useGetNftFromWallet = (USER) => {
  const connection = new anchor.web3.Connection(clusterApiUrl(CLUSTER));
  const [nft, setNft] = useState([]);
  const [nftDb, setNftDb] = useState([]);
  const [loading, setLoading] = useState(false);

  const [updateNft] = useMutation(UPDATE_PROFILE_NFT);

  const getMetadata = async (mint) =>
    (
      await anchor.web3.PublicKey.findProgramAddress(
        [Buffer.from('metadata'), TOKEN_METADATA_PROGRAM_ID.toBuffer(), mint.toBuffer()],
        TOKEN_METADATA_PROGRAM_ID,
      )
    )[0];

  const findTokens = useCallback(
    async (token, owner) => {
      try {
        const isToken = token.account.data.parsed.info.tokenAmount;
        if (isToken.amount === '1' && isToken.decimals === 0) {
          const nftMint = new anchor.web3.PublicKey(token.account.data.parsed.info.mint);
          const pda = await getMetadata(nftMint);
          const accountInfo = await connection.getParsedAccountInfo(pda);
          const metadata = new Metadata(owner.toString(), accountInfo.value);
          const {data} = metadata.data;
          const uri = await fetch(data.uri).then((response) => response.json());
          return {
            data,
            mint: nftMint,
            metadata: pda,
            tokenAccount: token.pubkey,
            uri,
            updateAuthority: metadata.data.updateAuthority,
          };
        }
      } catch (err) {
        return '';
      }
    },
    [connection],
  );

  const getNftsForOwner = useCallback(
    async (wallet) => {
      setLoading(true);
      const tokenAccounts = await connection.getParsedTokenAccountsByOwner(
        wallet,
        {programId: TOKEN_PROGRAM_ID},
        'finalized',
      );
      const task = tokenAccounts.value.map((token) => () => findTokens(token, wallet));
      const result = await pact(task, 15, 1000, true);
      const userNft = result.filter((item) => item?.data?.name !== undefined);
      setNft(userNft);
      setLoading(false);
    },
    [connection],
  );

  const getAttibute = useCallback((attributes, typeAttibute) => {
    if (attributes) {
      const findBackGround = attributes.map((item) => {
        if (item.trait_type === typeAttibute) {
          return item.value;
        }
        return null;
      });
      return findBackGround.filter((type) => type !== null)[0];
    }
    return null;
  }, []);

  useEffect(() => {
    if (nft.length === 0) {
      return;
    }
    const filterUri = nft.map((item) => {
      const parseEdition = item?.data?.name?.match(/(\d+)/g);
      const edition = parseEdition ? parseEdition[0] : item?.uri?.edition ?? item?.uri?.properties?.edition ?? 0;
      const creators = item?.data?.creators;
      return {
        name: item.data.name,
        contractAdress: item.mint.toString(),
        isVisible: true,
        serialNft: parseInt(edition, 10),
        tokenId: item.updateAuthority,
        uri: item.data.uri,
        urlImage: item?.uri?.image,
        walletAdress: USER?.publicKey,
        background: getAttibute(item?.uri?.attributes, 'Background') ?? null,
        blockChain: 'solana',
        creator: creators ? creators[0]?.address : '',
        description: item?.uri?.description ?? '',
        effect: 'solana',
        metaData: 'Editable',
        symbol: item?.uri?.symbol,
        webSite: item?.uri?.external_url ?? null,
      };
    });
    if (filterUri[0].walletAdress) {
      updateNft({
        variables: {
          data: filterUri,
          idUser: USER?.id,
        },
        onError() {},
      });
    }
    const addUser = filterUri.map((item) => ({...item, user: USER}));
    setNftDb(addUser);
  }, [nft]);

  useEffect(() => {
    if (USER?.publicKey) {
      const ParentWallet = new anchor.web3.PublicKey(USER?.publicKey);
      getNftsForOwner(ParentWallet);
    } else {
      setNft([]);
      setNftDb([]);
    }
  }, [USER.publicKey]);

  return {userNft: nft, loadingNft: loading, nftDb};
};
