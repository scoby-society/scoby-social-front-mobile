import {useState, useContext, useCallback} from 'react';
import {GET_BLOCK_USERS, GET_TARGET_USERS} from 'src/graphql/queries/blockAndReport';
import {useMutation, useQuery} from '@apollo/client';
import {FollowersContext} from 'src/containers/followers';
import {UN_BLOCK_USER} from 'src/graphql/mutations/blockAndReport';

export const useBlock = () => {
  const [blockUsers, setBlockUsers] = useState([]);
  const [targetUsers] = useState([]);

  const {currentUserProfile} = useContext(FollowersContext);
  const {id} = currentUserProfile;
  const [blockID, setBlockID] = useState('');
  const [reload,setReload]=useState(false)
  useQuery(GET_BLOCK_USERS, {
    pollInterval: 2000,
    variables: {
      id,
    },
    fetchPolicy: 'network-only',
    onCompleted() {
      const arr = [];
      setBlockUsers(arr);
    },
  });

  useQuery(GET_TARGET_USERS, {
    pollInterval: 2000,
    variables: {
      id,
    },
    fetchPolicy: 'network-only',
    onCompleted() {},
  });

  const isBlocked = useCallback(
    (ID) => {
      setBlockID(ID);
      const ub = blockUsers.filter((user) => user.id === parseInt(ID, 10));

      if (ub.length > 0) {
        return true;
      }
      return false;
    },
    [blockUsers],
  );

  const imBlocked = (ID) => {
    const ub = targetUsers.filter((user) => user.sourceUser.id === parseInt(ID, 10));

    if (ub.length > 0) {
      return true;
    }
    return false;
  };

  const [unblockUser] = useMutation(UN_BLOCK_USER, {
    notifyOnNetworkStatusChange: true,
    variables: {
      id: blockID,
    },
    onCompleted() {
      setBlockUsers(blockUsers.filter((user) => user.id !== blockID));
      isBlocked(blockID);
    },
  });

  return {
    isBlocked,
    unblockUser,
    blockUsers,
    imBlocked,
    blockID,
    reload,
    setReload
  };
};
