import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator /* ,Text */} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import styled, {ThemeProvider} from 'styled-components/native';
import Navigation from 'src/navigation';
import NoInternetModal from 'src/components/Modal/NoInternetModal';
import theme from 'src/config/theme';
import GlobalProvider, {GlobalContext} from 'src/containers/global';
import {MediaSoupProvider} from 'src/containers/mediasoup';
import FollowersProvider, {defaultFollowersContext} from 'src/containers/followers';
import Colors from 'src/constants/Colors';
import {ApolloProvider} from '@apollo/client';
import useApolloClient from './graphql/ApolloClient';
import {GET_USER_PROFILE} from './graphql/queries/profile';
import {kitty} from './config/api';
import {requestDeviceId, requestUserPermission} from './utils/permission/notifications';
import {SEND_FCM_TOKEN} from './graphql/mutations/fcm';

const LoadingContainer = styled.View({
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: Colors.blueBackgroundSession,
});

function AppContainer() {
  const client = useApolloClient();
  const {setInitialLink, setIsLogged, setId, setWalletKey} = useContext(GlobalContext);
  const [loadingToken, setLoadingToken] = useState(true);
  const [loadingLink, setLoadingLink] = useState(true);
  const [loadingKitty, setLoadingKitty] = useState(true);
  const [userUpdate, setUser] = useState(defaultFollowersContext.currentUserProfile);

  const initializeApp = async () => {
    const walletKey = await AsyncStorage.getItem('public_key');
    if (walletKey) {
      setWalletKey(walletKey);
    } else {
      setWalletKey(null);
    }
    if (loadingToken) {
      const res = await client.query({query: GET_USER_PROFILE}).catch(() => {
        setIsLogged(false);
        setLoadingKitty(false);
        setLoadingToken(false);
      });

      const data = res?.data;

      if (!data) {
        setIsLogged(false);
        await AsyncStorage.removeItem('token');
        return;
      }

      try {
        const id = data?.getUserProfile?.id;
        setIsLogged(id && id !== null);
        setId(id || null);
      } catch {
        setIsLogged(false);
        await AsyncStorage.removeItem('token');
      } finally {
        const token = await requestUserPermission();
        const deviceId = await requestDeviceId();
        await client.mutate({mutation: SEND_FCM_TOKEN, variables: {deviceId, token}});
        setUser({...data?.getUserProfile, token});
        if (!kitty.currentUser) {
          await kitty.startSession({username: data?.getUserProfile?.id, authParams: {}}).then(() => {
            kitty.updateCurrentUser((user) => {
              if (token) {
                setIsLogged(true);
              }
              user.properties = {...user, firebaseToken: token};
              return user;
            });
          });
        }
        setLoadingKitty(false);
        setLoadingToken(false);
      }
    }
  };

  const processInitialLink = async () => {
    const link = await dynamicLinks().getInitialLink();
    setInitialLink(link);
    setLoadingLink(false);
  };

  useEffect(() => {
    initializeApp();
  }, [loadingToken, setIsLogged]);

  useEffect(() => {
    processInitialLink();
  }, [setInitialLink]);

  if (loadingToken || loadingLink || loadingKitty) {
    return (
      <LoadingContainer>
        <ActivityIndicator color={Colors.white} size="large" />
      </LoadingContainer>
    );
  }

  return (
    <ApolloProvider client={client}>
      <FollowersProvider setProfile={userUpdate}>
        <Navigation />
      </FollowersProvider>
    </ApolloProvider>
  );
}

export default function Root() {
  return (
    <GlobalProvider>
      <ThemeProvider theme={theme}>
        <NoInternetModal />
        <MediaSoupProvider>
          <AppContainer />
        </MediaSoupProvider>
      </ThemeProvider>
    </GlobalProvider>
  );
}
